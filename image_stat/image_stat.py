#!/usr/bin/env python
#

'''
@author: rajkovic
'''
from __future__ import print_function
from __future__ import division
import os
import inotify.adapters
import time
import sys
import numpy
from PIL import Image
from scipy import ndimage

old_cenx = 0
old_ceny = 0
old_detx = 0
old_detz = 0

def find_cen(im):
    dd=1*im
    s = dd.flatten()
    s.sort()
    dd[dd<s[-1500]]=0
    dd[dd>s[-5]]=0
    cm_x,cm_y = ndimage.measurements.center_of_mass(dd)
    return(int(cm_x),int(cm_y))


def stat_calc(pp,nn):
    global old_cenx, old_ceny, old_detx, old_detz

    cenx = 0
    ceny = 0
    detx = 0
    detz = 0

    # poen image:
    im_data = numpy.array(Image.open(os.path.join(pp,nn)))


    # create output folder if doesn't exist; if can't create, use image folder
    out_fol = os.path.realpath(os.path.join(pp,'../stat'))
    if not os.path.isdir(out_fol):
        try:
            os.mkdir(out_fol)
        except:
            out_fol = pp

    # read prp file if exists, get detector position (not yet implemented in prp!!!)
    #ff_prp = os.path.join(pp,os.path.splitext(nn)[0]+'.prp')

    #if os.path.isfile(ff_prp):
    #    with open(ff_prp) as prp:
    #       for line in prp.readlines():
    #            if line.startwith('dtex'):

    # try to read integ.mpp
    integ_mpp = os.path.realpath(os.path.join(pp,'../analysis','integ.mpp'))
    if os.path.isfile(integ_mpp):
        with open(integ_mpp) as mpp:
            for line in mpp.readlines():
                if line.startwith('-c'):
                    # get center info
                    try:
                        cenx,ceny = [float(z) for z in line.split()[1:]]
                        ceny = im_data.shape[0] - ceny
                    except:
                        cenx,ceny = [0,0]
                # if line.startswith('-m'):
                # get mask

    # find center if needed (no integ.mpp data AND (no pre-calcualted data OR detector moved))
    #print(cenx,ceny,old_cenx,old_ceny)
    if cenx*ceny==0 and (old_cenx*old_ceny==0 or ((detx-old_detx) > 0.01 or (detz-old_detz) > 0.01)):
        ceny,cenx = find_cen(im_data)
        cen_info = 'calculated'
    elif cenx*ceny != 0:
        # center from inetg.mpp, go with it
        cen_info = 'from integ.mpp'
    else:
        # use old center
        cenx=old_cenx
        ceny=old_ceny
        cen_info = 'reused'

    # calculate

    # remove all negative values (use mask), currently set to 0 but it still changes mean
    im_mean = numpy.mean(numpy.clip(im_data,0,None))

    # write stat file
    with open(os.path.join(out_fol,os.path.splitext(nn)[0]+'.stat'),'w+') as sf:
        sf.write('Name: {}\n'.format(nn))
        sf.write('Center: ({},{}) - {}\n'.format(cenx,im_data.shape[0]-ceny,cen_info))
        sf.write('Mean: {}\n'.format(numpy.mean(im_data)))

    print("  -->  "+os.path.join(os.path.relpath(pp,watch_fol),str(nn))+"   --> stat")


    old_cenx = cenx
    old_ceny = ceny
    old_detx = detx
    old_detz = detz

if __name__ == '__main__':

    if len(sys.argv) > 1:
        if sys.argv[1] == '-h':
            print("Usage:\n")
            print("image_stat.py [path]\n")
            print("Program will wait for .tif images saved in 'data' subfolder(s)")
            print("and will create a 'stat' subfolder with a .stat file for each image ")
            print("containing basic statistical information about the image.")
            print("If path is not specified, /mnt/home/staff/UserSetup is used.\n")
            sys.exit()
        else:
            if os.path.isdir(sys.argv[1]):
                watch_fol = os.path.realpath(sys.argv[1])
            else:
                print("\nThat is not a valid folder!\n")
                sys.exit()
    else:
        watch_fol = '/fs/home/staff/UserSetup'

    print("\nHelp is available with 'image_stat.py -h'.\n")
    print("Watching '{}' recursively.\n".format(watch_fol))
    #i = inotify.adapters.InotifyTree(watch_fol,mask=inotify.constants.IN_CLOSE_WRITE)  # IN_MOVED_TO
    i = inotify.adapters.InotifyTree(watch_fol)
    while 1:
        try:
            events = i.event_gen(yield_nones=False, timeout_s=5)
            events = list(events)

            for ee in events:
                print(ee[3],*ee[1],sep='\t\t')
                if (not ee[3].startswith('_tmp_')) and os.path.splitext(ee[3])[1]=='.tif' and os.path.split(ee[2])[-1]=='data' and ee[1][0]=='IN_MOVED_TO':
                    stat_calc(ee[2],ee[3])
                    #print(ee[2],ee[3])
        except KeyboardInterrupt:
            sys.exit()


