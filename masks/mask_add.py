#!/usr/bin/env python

'''
@author: rajkovic
'''

import sys
import os
import numpy
from PIL import Image

# need this import for Image.frombytes to work
import scipy.misc

if __name__ == "__main__":
    if (len(sys.argv) < 3 or len(sys.argv)>4):
         sys.exit(""" Usage:
         
         {0} [mask_1.tif] [mask_2.tif] <mask_sum.tif>
         
         If filename not specified, sum of two masks will be saved as mask_sum.tif.
         """.format(os.path.basename(sys.argv[0])))
    try:     
        mask_1_im = Image.open(sys.argv[1]).convert('I')
        mask_1 = numpy.array(mask_1_im)
    except:
        sys.exit("Can't open '{}'.".format(sys.argv[1]))
            
    try:
        mask_2_im = Image.open(sys.argv[2]).convert('I')
        mask_2 = numpy.array(mask_2_im)
    except:
        sys.exit("Can't open '{}'.".format(sys.argv[2]))
    
    if mask_1.shape != mask_2.shape:
        sys.exit('Mask files don\'t have the same dimensions: {} vs {}.'.format(mask_1.transpose().shape,mask_2.transpose().shape))
    
    mask_sum = numpy.where(numpy.logical_and(mask_1,mask_2),1,0).astype('uint16')
    im = Image.frombytes('I;16',(mask_sum.shape[1],mask_sum.shape[0]),mask_sum.tobytes())
    
    
    if len(sys.argv) == 4:
        save_name = sys.argv[3]
    else:
        save_name = 'mask_sum.tif'
    
    im.save(save_name)
    
    
         
         
        