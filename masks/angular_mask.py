#!/usr/bin/env python

'''
@author: rajkovic
'''

import os
import sys
from PIL import Image
import numpy

# need this import for Image.frombytes to work
import scipy.misc 

def ang_mask(sh,sv,cx,cy,start_ang,ang_range,name='angular_mask.tif'):
     # keep starting angle in (0,360) range
    start_ang=numpy.mod(start_ang,360)
    xm, ym = numpy.meshgrid(numpy.arange(sh),numpy.arange(sv))
    zm = numpy.arctan2(ym-cy,xm-cx)
    # numpy.arctan2 returns (-pi,pi) values so we need two conditions connected by OR
    zm = numpy.where(numpy.logical_or(numpy.logical_and(zm>numpy.radians(start_ang-ang_range),zm<numpy.radians(start_ang+ang_range)),numpy.logical_and((zm+2*numpy.pi)>numpy.radians(start_ang-ang_range),(zm+2*numpy.pi)<numpy.radians(start_ang+ang_range))),1,0)
    zm = numpy.flipud(zm.astype('uint16'))
    im = Image.frombytes('I;16',(sh,sv),zm.tobytes())
    im.save(name)
    
    
if __name__ == "__main__":
    if (len(sys.argv) < 7 or len(sys.argv)>8):
         sys.exit(""" Usage:
         
         {0} [mask x-size] [mask y-size] [centar x-position] [centar y-position] [starting angle] [+/-angular range] <file name>
         
         If file name is not given, default angular_maks.tif is used.
         
         Example - for Pilatus 1M detector, beam center at (400,300), wedge from 40 to 70 (55+/-15) degrees:
         
         {0} 981 1043 400 300 55 15 my_mask.tif

            """.format(os.path.basename(sys.argv[0])))
            
    if len(sys.argv) == 7:
        ang_mask(numpy.int(sys.argv[1]),numpy.int(sys.argv[2]),numpy.float(sys.argv[3]),numpy.float(sys.argv[4]),numpy.float(sys.argv[5]),numpy.float(sys.argv[6]))
    elif len(sys.argv) == 8:
        ang_mask(numpy.int(sys.argv[1]),numpy.int(sys.argv[2]),numpy.float(sys.argv[3]),numpy.float(sys.argv[4]),numpy.float(sys.argv[5]),numpy.float(sys.argv[6]),sys.argv[7])