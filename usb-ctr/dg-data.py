#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division
import glob
import numpy
import matplotlib.pyplot as plt
import os
import sys

if __name__ == "__main__":
    usb_files = glob.glob('./dg-data/*usb_s1.int')
    #usb_files.sort(key=os.path.getmtime)

    usb_list = []
    daq_list = []
    usb_list_norm = []
    daq_list_norm = []

    rslt = 'results.dat'

    with open(rslt,'w') as rr:
        rr.write('freq \t pulse \t ct_usb \t stdev \t ct_daq \t stdev \t ct/s/Hz usb \t stdev \t ct/s/Hz daq \t stdev \n')
        rr.write('----\t'*10)
        rr.write('\n')

    for usb_int in usb_files:
        fr = int(os.path.basename(usb_int).split('_')[0][2:-3])
        if usb_int.split('_')[0][-3] == 'M':
            fr =fr *1e6
        elif usb_int.split('_')[0][-3] == 'k':
            fr =fr *1e3

        fr_str = os.path.basename(usb_int).split('_')[0][2:]
        pd_str = os.path.basename(usb_int).split('_')[1]

        with open(usb_int) as uuii:
            usb_str = uuii.readlines()
        usb_dat = map(lambda x: float(x.split()[0]), usb_str)

        daq_int = ''.join(usb_int.split('_usb_'))
        with open(daq_int) as ddii:
            daq_str = ddii.readlines()
        daq_dat = map(lambda x: float(x.split()[0]), daq_str)

        usb_dat_h = numpy.array(usb_dat[int(len(usb_dat)/2):])
        daq_dat_h = numpy.array(daq_dat[int(len(daq_dat)/2):])

        ud_n = usb_dat_h/1.8/fr
        dd_n = daq_dat_h/1.8/fr

        with open(rslt,'a') as rr:
            rr.write('{} \t {} \t {}\t{:d} \t {}\t{:d} \t {:.3}\t{:.2} \t {:.3}\t{:.2}\n'.format(fr_str,pd_str,usb_dat_h.mean(),int(usb_dat_h.std()),daq_dat_h.mean(),int(daq_dat_h.std()),ud_n.mean(),ud_n.std(),dd_n.mean(),dd_n.std()))
            #rr.write('{} \t {} \t {:>10}+-{:5d} {:>10}+-{:>5d} {:>10.3}+-{:>5.1} {:>10.3}+-{:>5.1}\n'.format(fr_str,pd_str,usb_dat_h.mean(),int(usb_dat_h.std()),daq_dat_h.mean(),int(daq_dat_h.std()),ud_n.mean(),ud_n.std(),dd_n.mean(),dd_n.std()))


        #if (daq_dat_h[1]/usb_dat_h[1] > 4):
        #    print(usb_int)

        plt.figure()
        plt.title(fr_str+' '+pd_str)
        plt.plot(usb_dat_h/usb_dat_h.mean(),'*',label='usb')
        plt.plot(daq_dat_h/daq_dat_h.mean(),'+r',markeredgewidth=3,label='daq')
        #plt.plot(usb_dat_h/daq_dat_h,'o',label='usb/daq')
        plt.xlim(left=-1)
        #plt.ylim(top=1.015)
        plt.legend()
        plt.savefig('plots/{}_{}.png'.format(fr_str,pd_str))
        plt.close()


        if (any(y<0 for y in usb_dat_h) or any(y<0 for y in daq_dat_h)):
            print(usb_int)
            pass
        else:
            usb_list.extend(usb_dat_h)
            daq_list.extend(daq_dat_h)
            usb_list_norm.extend(usb_dat_h/1.8/fr)
            daq_list_norm.extend(daq_dat_h/1.8/fr)


    intrat = numpy.array(daq_list)/numpy.array(usb_list)

    #print(daq_list)
    #print(usb_list)
    plt.figure()
    plt.plot(intrat,'x')
    plt.ylim([intrat.min()*.95,intrat.max()*1.05])
    plt.xlabel('image number')
    plt.ylabel('int_daq/int_usb')
    plt.savefig('plots/int_ratio.png')
    plt.close()

    plt.figure()
    plt.plot(numpy.array(daq_list),numpy.array(usb_list),'x')
    plt.xlabel('int_daq')
    plt.ylabel('int_usb')
    plt.savefig('plots/usb_vs_daq.png')
    plt.close()

    plt.figure()
    plt.plot(numpy.array(daq_list_norm),numpy.array(usb_list_norm),'x')
    #plt.gca().set_aspect('equal')
    plt.axis('square')
    plt.xlabel('int_daq/s/Hz')
    plt.ylabel('int_usb/s/Hz')
    plt.savefig('plots/usb_vs_daq_norm.png')
    plt.close()


