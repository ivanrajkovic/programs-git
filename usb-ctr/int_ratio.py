#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division
import glob
import numpy
import matplotlib.pyplot as plt
import os

if __name__ == "__main__":
    usb_files = glob.glob('*usb_s1.int')
    usb_files.sort(key=os.path.getmtime)

    usb_list = []
    daq_list = []

    for usb_int in usb_files:
        with open(usb_int) as uuii:
            usb_str = uuii.readlines()
        usb_dat = map(lambda x: float(x.split()[0]), usb_str)

        daq_int = ''.join(usb_int.split('_usb_'))
        with open(daq_int) as ddii:
            daq_str = ddii.readlines()
        daq_dat = map(lambda x: float(x.split()[0]), daq_str)

        usb_dat_h = usb_dat[int(len(usb_dat)/2):]
        daq_dat_h = daq_dat[int(len(daq_dat)/2):]

        #if (daq_dat_h[1]/usb_dat_h[1] > 4):
        #    print(usb_int)


        if (any(y <0 for y in usb_dat_h) or any(y <0 for y in daq_dat_h)):
            #print(usb_int)
            pass
        else:
            usb_list.extend(usb_dat_h)
            daq_list.extend(daq_dat_h)


    intrat = numpy.array(daq_list)/numpy.array(usb_list)

    #print(daq_list)
    #print(usb_list)
    plt.figure()
    plt.plot(intrat,'x')
    plt.ylim([intrat.min()*.95,intrat.max()*1.05])
    plt.xlabel('image number')
    plt.ylabel('int_daq/int_usb')
    plt.figure()
    plt.plot(numpy.array(daq_list),numpy.array(usb_list),'x')
    plt.xlabel('int_daq')
    plt.ylabel('int_usb')
    plt.show()
