#!/usr/bin/env python

import os
import subprocess
import sys
import glob
import fileinput
import re

integmpp = 'integ.mpp'
data_folder = os.path.join('..','data')


if len(sys.argv) < 2 or len(sys.argv) > 3:
    sys.exit("\nUsage: "+ sys.argv[0] + " <sample number> [buffer number]\n\nShould be run from the analysis folder.\nIt will add '-h' and '-f' lines to integ.mpp and run\n 'sastool integ.mpp'.")

for line in fileinput.input(integmpp, inplace = 1): 
    line = re.sub('^-h','#-h',line)
    line = re.sub('^-f','#-f',line)
    line = re.sub('-s no','-s yes',line)
    if len(sys.argv) == 2:
        line = re.sub('^-s','#-s',line)
    if len(sys.argv) == 3:
        line = re.sub('^#-s','-s',line)	
    sys.stdout.write(line)


s_number = sys.argv[1]


s_file = glob.glob(os.path.join(data_folder,'*_T'+str(s_number).zfill(3))+'_0001.tif')[0]
s_int = glob.glob(os.path.join(data_folder,'*_T'+str(s_number).zfill(3))+'_s1.int')[0]



if len(sys.argv) == 3:
    b_number = sys.argv[2]    
    b_file = glob.glob(os.path.join(data_folder,'*_T'+str(b_number).zfill(3))+'_0001.tif')[0]
    b_int = glob.glob(os.path.join(data_folder,'*_T'+str(b_number).zfill(3))+'_s1.int')[0]


with open(s_int) as f:
    for i, l in enumerate(f):
        pass
s_num = (i+1)/2

if len(sys.argv) == 3:
    with open(b_int) as f:
        for j, l in enumerate(f):
            pass

    b_num = (j+1)/2


ff = open(integmpp,'a')

new_line_f = '-f '+s_file
new_line_h = '-h '+s_int+' '+str(int(s_num))
if len(sys.argv) == 3:
    new_line_f = new_line_f+" "+b_file
    new_line_h = new_line_h+" "+b_int+' '+str(int(b_num))
ff.write("#\n")
ff.write(new_line_h+"\n")
ff.write(new_line_f+"\n")
ff.close()




subprocess.call(['sastool',integmpp])
