
def starthtml(logfile, allplot):
    starthtml = """
    <html>
        <head>
            <title>SAXS Analysis</title>
            
<style media="screen" type="text/css">
            
table a:link {
    color: #666;
    font-weight: bold;
    text-decoration:none;
}
table a:visited {
    color: #999999;
    font-weight:bold;
    text-decoration:none;
}
table a:active,
table a:hover {
    color: #bd5a35;
    text-decoration:underline;
}
table {
    font-family:Arial, Helvetica, sans-serif;
    color:#666;
    font-size:12px;
    text-shadow: 1px 1px 0px #fff;
    background:#eaebec;
    margin:20px;
    border:#ccc 1px solid;

    -moz-border-radius:3px;
    -webkit-border-radius:3px;
    border-radius:3px;

    -moz-box-shadow: 0 1px 2px #d1d1d1;
    -webkit-box-shadow: 0 1px 2px #d1d1d1;
    box-shadow: 0 1px 2px #d1d1d1;
}
table th {
    padding:21px 25px 22px 25px;
    border-top:1px solid #fafafa;
    border-bottom:1px solid #e0e0e0;

    background: #ededed;
    background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
    background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
table th:first-child {
    text-align: left;
    padding-left:20px;
}
table tr:first-child th:first-child {
    -moz-border-radius-topleft:3px;
    -webkit-border-top-left-radius:3px;
    border-top-left-radius:3px;
}
table tr:first-child th:last-child {
    -moz-border-radius-topright:3px;
    -webkit-border-top-right-radius:3px;
    border-top-right-radius:3px;
}
table tr {
    text-align: center;
    padding-left:20px;
}
table td:first-child {
    text-align: left;
    padding-left:20px;
    border-left: 0;
}
table td {
    padding:18px;
    border-top: 1px solid #ffffff;
    border-bottom:1px solid #e0e0e0;
    border-left: 1px solid #e0e0e0;

    background: #fafafa;
    background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
    background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
}
table tr.even td {
    background: #f6f6f6;
    background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
    background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
}
table tr:last-child td {
    border-bottom:0;
}
table tr:last-child td:first-child {
    -moz-border-radius-bottomleft:3px;
    -webkit-border-bottom-left-radius:3px;
    border-bottom-left-radius:3px;
}
table tr:last-child td:last-child {
    -moz-border-radius-bottomright:3px;
    -webkit-border-bottom-right-radius:3px;
    border-bottom-right-radius:3px;
}
table tr:hover td {
    background: #f2f2f2;
    background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
    background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);    
}
            
            
 </style>           
            
            
          
                 
            
        </head>
        <body>
            <table align="center" border="1" cellpadding="1" cellspacing="0"  summary="SAXS Analysis" width="920px">
                <caption>
                    &nbsp;</caption>
                <thead>
                    <tr>
                        <th scope="col">
                            <span style="font-family:verdana,geneva,sans-serif;">Result</span></th>
                        <th scope="col">
                            <span style="font-family:verdana,geneva,sans-serif;">Time</span></th>
                        <th scope="col">
                            <span style="font-family:verdana,geneva,sans-serif;">Sub file</span></th>
                        <th scope="col">
                            <span style="font-family:verdana,geneva,sans-serif;">buffer (rejected) sample (rejected)</span></th>
                        <th scope="col">
                            <span style="font-family:verdana,geneva,sans-serif;">buffer uv</span></th>
                        <th scope="col">
                            <span style="font-family:verdana,geneva,sans-serif;"> sample uv</span></th>                
                        <th scope="col">
                            <span style="font-family:verdana,geneva,sans-serif;">autorg rg and i0</span></th>
                        <th scope="col">
                            <font class="Apple-style-span" face="verdana, geneva, sans-serif">datgnom reciprocal rg</font></th>
                        <th scope="col">
                            <font class="Apple-style-span" face="verdana, geneva, sans-serif">datgnom dmax</font></th>
                        <th scope="col">
                        <span style="font-family:verdana,geneva,sans-serif;">Image graphs</span></th>
                        <th scope="col">
                            <span style="font-family:verdana,geneva,sans-serif;">Data graphs</span></th>
                        <th scope="col">
                            <span style="font-family:verdana,geneva,sans-serif;">Data analysis graphs</span></th>
                        <th scope="col">
                            <span style="font-family:verdana,geneva,sans-serif;">Row analysis</span></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td COLSPAN="13">This information is meant for a quick data check only.  Please analyse your data carefully by hand!</td>
                    </tr>
                </tfoot>
                <tbody>

    """ 
    return starthtml



def endhtml():
    endhtml = """</tbody></table></body></html>"""
    return(endhtml)
    
def table_line(row_color, data_result, time_taken, prp, sub_file, buffer_frames, buffer_rejected, sample_frames, sample_rejected, buffer_uv, sample_uv,  rg, i0, autorg, autorg_rg, autorg_rg_std, autorg_i0, autorg_i0_std, points, datgnom_rg, dmax, image_data_png,png_file, data_png, row_png ) :
    datgnom_log = "%s_pr.log" %( sub_file[0:len(sub_file)-8])
    autorg_log = "%s_rg.log" % sub_file[0:len(sub_file)-4]
    table_line = \
                  """   <tr %s>
                        <td style="text-align: center;">
                        %s</td>
                     <td style="text-align: center;">
                        %s</td>
                    <td style="text-align: center;">
                        <a href="other_files/%s">%s</a></td>
                         
                    <td style="text-align: center;">
                        <a href="other_files/%s">%s (%s)</a><br/><a href="other_files/%s">%s (%s)</a> <a href="other_files/%s">Images</a></td>
                         
                    <td style="text-align: center;">
                        %s</td>
                    <td style="text-align: center;">
                        %s  </td>                   
                    <td style="text-align: center;">
                            <span style="font-family:verdana,geneva,sans-serif;"><a href="%s">%s<br/>(+/-%s)<br/>%d<br/>(+/-%s)<br/>%s </a></span></td>
                        <td scope="col">
                            <font class="Apple-style-span" face="verdana, geneva, sans-serif">%5.2f </td></font></td>
                        <td scope="col">
                            <font class="Apple-style-span" face="verdana, geneva, sans-serif"><a href="other_files/%s">%5.2f</a> </td></font></td>
                        <td style="text-align: center;">
                        <img alt="" src="other_files/%s" /></td> 
                        <td style="text-align: center;">
                        <img alt="" src="other_files/%s" /></td>
                        <td style="text-align: center;">
                        <img alt="" src="other_files/%s" /></td>
                        <td style="text-align: center;">
                        <img alt="" src="other_files/%s" /></td>                                
                    </tr>
                    """ % (row_color, \
                           data_result, \
                           time_taken, \
                           sub_file, \
                           sub_file[0:-7],  \
                           sub_file[0:-8]+"_sas.log", \
                
                           buffer_frames, \
                           buffer_rejected, \
                           sub_file[0:-8]+"_sas.log", \
                           sample_frames, \
                           sample_rejected, \
                           sub_file[0:-8]+"_images.html",  \
                           buffer_uv, \
                
                           sample_uv, \
                           
                           autorg_log , \
                           autorg_rg, \
                           autorg_rg_std, \
                           float(autorg_i0), \
                           autorg_i0_std, \
                           points, \
                           float(datgnom_rg), \
                           datgnom_log,\
                           float(dmax), \
                           image_data_png, \
                           png_file, \
                           data_png, \
                           row_png, \
                           )
    return(table_line)



