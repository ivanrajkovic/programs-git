#!/usr/bin/env python

'''
Created on Jun 17, 2013

@author: lc71

Python program utlizing Atsas and Sastool to analysse the results of Bl42 autosampelr data collection


Requires:
    program_paths.txt - a file with paths to autorg, datgnom, datcmp and sastool
    The above programs installed on the system
    

Example usuage:
    Needs to be run in directory above data and analysis.  
    

'''

from sys import platform
from os import  getcwd, mkdir, chdir, listdir, path, walk, system
from time import sleep
from shutil import move
from SAXShtml import starthtml, endhtml, table_line
from analyse_sub import analyse_sub
from analyse_sub import _run_autorg as run_autorg
import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys
import glob
import re


''' global variables for program paths - set in intial set up '''

autorg_path = 'autorg'
datgnom_path= 'datgnom'
datcmp_path = 'datcmp'
sastool_path = 'sastool'


def saxs_pipe(sn,bn):
    ''' start main analysis procedure '''
    data_dir = _get_data_directory()
    integ_file_location = _get_integ_file()
    template_file = _make_folders_and_copy_integ_file(integ_file_location)
    _analyse_folder(data_dir, template_file,sn,bn)
    _clean_up(getcwd())
    return True

def _get_data_directory():
    ''' get data directory '''
    data_dir = getcwd()  + "/data/"
    return data_dir

    
def _get_integ_file():
    ''' get integ.mpp details '''           
    integ_file_location = getcwd()  + "/analysis/integ.mpp"
    return integ_file_location
        
         
def _make_folders_and_copy_integ_file(integ_file_location):
    ''' copy integ file - modify as needs be '''
    integ_file_text = ''
    try:
        integ_file = open(integ_file_location, 'r')
        # need to step through file, and copy mask file etc.
        for line in integ_file:
            if line.startswith('-m'):
                mask_name = line.split()[2]
                if mask_name.startswith('./'):
                    mask_name = mask_name[2:]
                if '/' in mask_name:
                    mask_name = mask_name.split('/')[-1]
                line = '-m yes %s/analysis/%s\n' % (getcwd(), mask_name  )
            if line.startswith('-s'):
                line = '-s no\n'                
            integ_file_text = integ_file_text + line
        integ_file.close()
    except IOError, (errno, strerror):
        print "Problem reading integ.mpp - error(%s): %s" % (errno, strerror)    
    print "Making output folder"
    try:
        mkdir("automatic_analysis")
    except OSError, (errno, strerror):
        print "automatic_analysis directory already exists"
    chdir("automatic_analysis")
    try:
        mkdir("sub_files")
        mkdir("other_files")
    except OSError, (errno, strerror):
        print "directories already exist"
    try:
        template_file_location = getcwd()  + "/template.mpp"
        template_file  = open(template_file_location, 'w')
        template_file.write(integ_file_text)
        template_file.close()
    except OSError, (errno, strerror):
        print "Problem writing template"
    return template_file_location 


def _analyse_folder(data_dir, template_file,sn,bn):
    ''' analyse folder  '''
    sample_buffer_pairs = _search_data(data_dir,sn,bn)
    log_file = _run_sastool(template_file, True, sample_buffer_pairs)
    _read_log_file(log_file, data_dir)
    _analyse_images(sample_buffer_pairs, log_file, data_dir)


def _analyse_images(sample_buffer_pairs, log_file, data_dir):
    for pair in sample_buffer_pairs:
        buffer = pair.split()[2]
        sample = pair.split()[1]
        _get_variance(buffer, sample, log_file)
    pass
 
        
def _get_variance(buffer, sample, log_file):
    log_file_line = "TIFF data files: %s %s" % (sample, buffer)
    variance_lines = False
    Buffer_frame = True
    buffer_variance = []
    sample_variance = []
    buffer_tot_q = []
    buffer_tot_i = []
    sample_tot_i = []    
    buffer_tot_file = '%s/automatic_analysis/%s' %('/'.join(buffer.split('/')[:-2]), "%stot" % buffer.split('/')[-1][:-3])
    try:
        file1 = open(buffer_tot_file , 'r')
        for file_line in file1.readlines():
            buffer_tot_q.append( float( file_line.split()[0] ) )
            buffer_tot_i.append( float( file_line.split()[1] ) )
        file1.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)             
    sample_tot_file = '%s/automatic_analysis/%s' %('/'.join(sample.split('/')[:-2]), "%stot" % sample.split('/')[-1][:-3])  
    try:
        file1 = open(sample_tot_file , 'r')
        for file_line in file1.readlines():
            sample_tot_i.append( float( file_line.split()[1] ) )
        file1.close()    
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)
    
    try:
        file1 = open(log_file, 'r')
        for file_line in file1.readlines():
            if log_file_line in file_line:
                variance_lines  = True
            if variance_lines and file_line.startswith('frame') and Buffer_frame:
                buffer_variance.append(float( file_line.split()[2]))
            if variance_lines and file_line.startswith('frame') and not Buffer_frame:
                sample_variance.append(float( file_line.split()[2]))
            if variance_lines and file_line.startswith('Final'):
                _plot_image_info(buffer_variance, sample_variance, sample.split('/')[-1], buffer_tot_q, buffer_tot_i, sample_tot_i , sample)
                variance_lines  = False
            if variance_lines and file_line.startswith('Average variance for buffer'):
                Buffer_frame = False                
        file1.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)   
    pass


def _plot_image_info(buffer_variance, sample_variance, sample_root, buffer_tot_q, buffer_tot_i, sample_tot_i, sample ):
    output_file = "%s_image_data.png" % sample_root[:-4]
    plt.figure(num=None, figsize=(9,2), dpi=80, facecolor='w', edgecolor='k')
    plt.subplot(141)
    plt.plot(range(1, len(buffer_variance)+1), buffer_variance)
    plt.plot(range(1, len(sample_variance)+1), sample_variance)
    title = "Variance"
    plt.title(title)
    plt.subplot(142)
    plt.plot(buffer_tot_q, buffer_tot_i)
    plt.plot(buffer_tot_q, sample_tot_i)
    title = "Tot files"
    plt.title(title)
    plt.subplot(143)   
    files_found = []
    rgs = []
    rgs_std = [] 
    directory = '%s/automatic_analysis/' %('/'.join(sample.split('/')[:-2])) 
    for root, dirs, files in walk(directory):
        for line in files:
            #only works upto 100
            if '.dat' in line and (sample.split('/')[-1][:-6]) in line and not 'other_files' in root and not '_sub.dat' in line:                
                files_found.append(path.join(root, line))
                autorg_results =  run_autorg(path.join(root, line))
                rgs.append(float(autorg_results[1]))
                rgs_std.append(float(autorg_results[2]))
    files_found.sort(key=lambda s: path.getmtime(s))
    for file in files_found:
        try:
            file1 = open(file, 'r')
            file_q= []
            file_i = []
            for file_line in file1.readlines():
                if  float( file_line.split()[0] ) < 0.2:
                    file_q.append( float( file_line.split()[0] ) )
                    file_i.append( float( file_line.split()[1] ) )             
            file1.close()
            plt.plot(file_q,file_i)
        except IOError, (errno, strerror):
            print "reading image file I/O error(%s): %s" % (errno, strerror)  
    title = "dat files < 0.2"
    plt.title(title)     
    plt.subplot(144)    
    plt.errorbar(range(1, len(rgs)+1), rgs , yerr= rgs_std )
    title = "Image rg"
    plt.title(title)         
            
    plt.savefig(output_file)  
    pass



def _search_data(directory,sn,bn):
    ''' '''
    sample_file=''
    buffer_file=''
    sample_buffer_pairs = []
    
    brr = re.compile('.*B'+str(bn).zfill(3)+'.*_0+1\.tif')
    srr = re.compile('.*S'+str(sn).zfill(3)+'.*_0+1\.tif')
    
    for file in listdir(directory):
        if brr.match(file):
            buffer_file = file
        if srr.match(file):
            sample_file = file
    
    sample_buffer_pairs.append("-f %s %s" % ( path.join(directory,sample_file), path.join(directory,buffer_file )))
    return sample_buffer_pairs
 


   

def _run_sastool(template_file, subtract, sample_buffer_pairs):
    ''' run sastool on each new set of data'''
    template_lines = []
    log_file = ''
    try:
        file1 = open(template_file, 'r')
        for lineA in file1:
            if subtract and '-s' in lineA:
                template_lines.append( '-s yes\n')
            elif '-s' in lineA:
                template_lines.append( '-s no\n')
            if lineA[0:len(lineA)-1] in sample_buffer_pairs :
                sample_buffer_pairs.remove(lineA[0:len(lineA)-1])
            if lineA[1:len(lineA)-1] in sample_buffer_pairs:
                sample_buffer_pairs.remove(lineA[1:len(lineA)-1])
            if not lineA.startswith('-f') and '-s' not in lineA:
                template_lines.append( lineA)
            if lineA.startswith('-f'):
                template_lines.append( '#'+lineA)
            if lineA.startswith('-l'):
                log_file =  lineA.split(' ')[1]
        file1.close()
        file1 = open(template_file, 'w')
        for line in template_lines:
            file1.write(line)
        for line2 in sample_buffer_pairs:
            file1.write(line2+"\n")
        file1.close()    
    except IOError, (errno, strerror):
        print "I/O error(%s): %s" % (errno, strerror)
    command1 = "%s %s" % (sastool_path, template_file)
    system(command1)
    return log_file[0:-1]


def _read_log_file(logfile, directory):
    ''' main log file processing script - needs refactoring '''
    ''' TODO - cleanup '''
    try:
        #logfile_html = "%s.html" % logfile[0:-4]
        logfile_html = 'results.html'
        allplot  = "%s.png" % logfile[0:-4]
        htmltop = starthtml(logfile, allplot)
        try:
            file1 = open(logfile_html, 'r')
            htmltop = ""
            for lineA in file1:
                if "</table></body></html>" not in lineA :
                    htmltop = "%s%s" % (htmltop, lineA)
        except IOError, (errno, strerror):
            print "Making New log file %s" % logfile_html      
        file1 = open(logfile, 'r')
	#below is an attempt to use pickles to store row infor, so it remains valid in the gap between one sample and another.
        try:
		row_info = pickle.load( open( "save.p", "rb" ) )
        except IOError, (errno, strerror): 
        	row_info = {'current_row':'start', 'current_name':'', 'rgs': [], 'rgs_std': [],'i0': [], 'i0_std': [], 'buffer_file': [], 'data_files':[] , 'directory':directory}
        buffer_name = ""
        sample_name= ""
        buffer_frames = 0
        sample_frames= 0
        buffer_rejected = 0
        sample_rejected= 0
        sub_file = ''
	sub_file_moved = ''
        sub_file_number = 0
        alllog = ""
        file3_data =[]
        sastools_subsection =[]
        images_html = []
        images_names = []
        file_root = ''
        for lineA in file1:
            sastools_subsection.append(lineA)
            data_line = lineA.split()
            alllog = "%s%s" % (alllog, lineA)
            if len(data_line) > 0:
                if data_line[0]  == 'TIFF':
                    buffer_name =data_line[4]
                    sample_name =data_line[3] #careful of this and below use..
                    file_root = data_line[3] .split('/')[-1].split('.')[0]
                    file_root  = file_root.split('_')
                    file_root.pop()
                    file_root = '_'.join(file_root)+'_'
                if data_line[0]  == 'Buffer':
                    buffer_frames =data_line[1].split(":")[1]
                    buffer_rejected= data_line[2].split(":")[1]
                if data_line[0]  == 'Sample':
                    sample_frames= data_line[1].split(":")[1]
                    sample_rejected= data_line[2].split(":")[1]
                if data_line[0]  == 'Final':
                    sample_name= data_line[1].split(":")[1]
                    remove_path = sample_name.split('/')
                    sub_file = remove_path[-1]
                    sub_file = sub_file[0:-3]+"sub"
		    sub_file_moved = 'other_files/'+ sub_file
                    png_file = sub_file[0:-3]+"png"
                    sastools_subsection_file = sub_file[0:-4]+"_sas.log"
                    file_sastools = open(sastools_subsection_file, 'w')
                    for line in sastools_subsection:
                        file_sastools.write(line)
                    file_sastools.close()
                    sastools_subsection =[]
                    images_html_file  = sub_file[0:-4]+"_images.html"
                    file_images = open(images_html_file, 'w')
                    file_images.write("""""<html><head><title></title></head><body><table border="1" cellspacing="0" align="left"><tbody>""")
                    for line in images_html:
                        file_images.write(line)
                    file_images.write("""</tbody></table></body></html>""")
                    file_images.close
                    images_html = []                                       
                    time_taken = _get_collection_time(sub_file, directory) 
                    uv_values = _get_uv_values(sub_file, buffer_name, directory) 
                    sub_results = analyse_sub(sub_file)
                    data_result = 'Data looks okay!'
                    #row_color = '''style="background-color:green;"'''
                    row_color = '''"'''
                    if float(sub_results[6][2]) > (float(sub_results[5][1]) + 2) or float(sub_results[6][2]) < (float(sub_results[5][1]) - 2):
                        data_result  = 'Check data - real and reciporocal rgs differ: %5.2f  %5.2f  ' % ( float(sub_results[5][1]) , float(sub_results[6][2]))
                        row_color = ''''''
                    if int(sample_rejected) > 2:
                        data_result  = 'Check data - %s frames rejected  ' % ( sample_rejected  )
                        row_color = '''style="background-color:red;"'''
                    if  int(sample_rejected) > 2 and (float(sub_results[6][2]) > (float(sub_results[5][1]) + 2) or float(sub_results[6][2]) < (float(sub_results[5][1]) - 2)):
                        data_result  = 'Check data - real and reciporocal rgs differ: %5.2f  %5.2f  ' % ( float(sub_results[5][1]) , float(sub_results[6][2]))
                        data_result  =  data_result  + '.\n  Also %s frames were rejected' % ( sample_rejected  )   
                        row_color = ''''''                                                      

                    working_name =  '_'.join(sub_results[0].split('_')[:-5]) 
                    working_row = sub_results[0].split('_')[-5][:-1]
                    if row_info['current_row'] == 'start':
                        row_info['current_row'] = working_row
			row_info['current_name'] = working_name
                    if row_info['current_row'] == working_row and row_info['current_name'] == working_name:
                        row_info['rgs'].append( float (sub_results[5][1]))
                        row_info['rgs_std'].append( float (sub_results[5][2]))
                        row_info['i0'].append( float( sub_results[5][3]))
                        row_info['i0_std'].append( float (sub_results[5][4]))
                        row_info['data_files'].append(sub_file)
                        
                    else:
		    	row_info['current_name'] =  '_'.join(sub_results[0].split('_')[:-5])
                        row_info['current_row'] = working_row
                        row_info['rgs']= [float (sub_results[5][1] )] 
                        row_info['rgs_std'] = [float (sub_results[5][2] )]
                        row_info['i0'] = [float (sub_results[5][3] )]
                        row_info['i0_std'] = [float (sub_results[5][4] )]
                        row_info['data_files']= [sub_file]
                    pickle.dump( row_info, open( "save.p", "wb" ) )  # save whatever value pickle has                		
                    row_png = _row_graphs(row_info)                   
                    
                    
                    
                    file3_data.append(table_line(row_color, \
                                                 data_result, \
                                                 time_taken[0],\
                                                 time_taken[1],\
                                                 sub_results[0],\
                                                 buffer_frames, \
                                                 buffer_rejected, \
                                                 sample_frames, \
                                                 sample_rejected,  \
                                                 uv_values[0], \
                                                 uv_values[1], \
                                                 sub_results[1], \
                                                 sub_results[2], \
                                                 sub_results[5][0], \
                                                 sub_results[5][1], \
                                                 sub_results[5][2], \
                                                 sub_results[5][3], \
                                                 sub_results[5][4], \
                                                 sub_results[5][5], \
                                                 sub_results[6][2], \
                                                 sub_results[6][0], \
                                                 "%s_image_data.png" %png_file[:-4], \
                                                 png_file, \
                                                 sub_results[7], \
                                                 row_png                                                 
                                                 ))
                    images_names = []                   
                    sub_file_number = sub_file_number + 1             
                    file3 = open(logfile_html, 'w')
                    file3.write(htmltop)
                    for line in file3_data:
                        file3.write(line)    
                    file3.write(endhtml())
                    file3.close()                
                if "buffer files:" in lineA:
                    file_root = buffer_name.split('.')[0] 
                if "sample files:" in lineA:
                    file_root = sample_name.split('.')[0]    
                if data_line[0] =='frame':
                    number = data_line[1][1:len(data_line[1])-1]
                    if len(number) == 1 : number = "0%s" % number
                    file_root  = file_root.split('_')
                    file_root.pop()
                    file_root = '_'.join(file_root)+'_'
                    tif_file = "%s%s.tif" %(file_root,  number) # will break will over a hundred images mind...
                    name = tif_file.split('/')[-1]
                    name = "%s_image.png" %name.split('.')[0]
                    command = "convert %s -resize 320x320 -equalize  -contrast-stretch 0 %s" % (tif_file, name  )
                    system(command)
                    text_for_image = "<tr><th ><img src='%s' alt='image' /></th><th >%s</th></tr>" %(name, tif_file)
                    images_html.append(text_for_image)
                    images_names.append(tif_file)
        file1.close()    
   
    except IOError, (errno, strerror):
        print "I/O error(%s): %s" % (errno, strerror)    
    return


def _get_collection_time(datfile, directory):
    prp_file =  "%s.prp" % (datfile[0:len(datfile)-4])
    fullpath = "%s%s" % (directory, prp_file)
    time = "0"
    try:
        file1 = open(fullpath, 'r')
        for file_line in file1.readlines():
            if 'Time this file was written:' in file_line:
                time = file_line.split('written: ')[1]        
        file1.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)    
    return [time,fullpath]
    
    
def _get_uv_values(datfile, buffer, directory):
    sample_uv_file =  "%s00_uv_01.txt" % (datfile[0:len(datfile)-6])
    fullpath = "%s%s" % (directory, sample_uv_file)
    sample_uv = "na"
    try:
        file1 = open(fullpath, 'r')
        for file_line in file1.readlines():
            if file_line.startswith("280.185"):
                sample_uv = file_line.split()[1]      
        file1.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)
    buffer_uv_file =  "%s00_uv_01.txt" % (buffer[0:len(buffer)-6])        
    fullpath = buffer_uv_file
    buffer_uv = "na"
    try:
        file1 = open(fullpath, 'r')
        for file_line in file1.readlines():
            if file_line.startswith("280.185"):
                buffer_uv = file_line.split()[1]      
        file1.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)            
    return [buffer_uv, sample_uv]


def _row_graphs(row_info):
    output_file = "%s_row_data.png" % row_info['current_name']
    plt.figure(num=None, figsize=(9,2), dpi=80, facecolor='w', edgecolor='k')    
    plt.subplot(141)    
    files_found = []
    directory = '%s/automatic_analysis/' %('/'.join(row_info['directory'].split('/')[:-1]))
    for root, dirs, files in walk(directory):
        for line in files:
            #only works upto 100
	    if '.dat' in line and row_info['current_row'] in line and '_B' in line and line.startswith(row_info['current_name']): 
            #if '.dat' in line and row_info['current_row'] in line and not 'other_files' in root and  '_B' in line and line.startswith(row_info['current_name']):                
                files_found.append(path.join(root, line))
    files_found.sort(key=lambda s: path.getmtime(s))
    for file in files_found:
        try:
            file1 = open(file, 'r')
            file_q= []
            file_i = []
            for file_line in file1.readlines():
                    file_q.append( float( file_line.split()[0] ) )
                    file_i.append( float( file_line.split()[1] ) )             
            file1.close()
            plt.plot(file_q,file_i)
        except IOError, (errno, strerror):
            print "reading the buffer file file I/O error(%s): %s - %s" % (errno, strerror, file)
    plt.grid(True)  
    title = "Buffer files"
    plt.title(title)         
    plt.subplot(142)
    
    print 'Here is row dictionary', row_info['data_files']     
    for file in row_info['data_files']:
        try:
	    filename1 = "%s%s" % (directory, file)
	    print 'trying to open %s in automatic_analysis directory' % (filename1) 
            file1 = open(filename1, 'r')
            file_q= []
            file_i = []
            for file_line in file1.readlines():
                file_q.append( float( file_line.split()[0] ) )
                if float( file_line.split()[1] ) > 0:
                    file_i.append( float( file_line.split()[1] ) )
                else:
                    file_i.append( 0.000001)             
            file1.close()
            plt.semilogy(file_q,file_i) 
	    print "Read in automatic_analysis directory"  
        except IOError, (errno, strerror):
            print "Failed to read %s in automatic_analysis" % (file)
	    
	####
	#   This is an awful hack to fix the subfile not being found problem
	####
	   
        try:
	    filename2 = "%s%s%s" % (directory,'sub_files/', file)
	    print 'trying to open %s in sub files' % (filename2) 
            file1 = open(filename2, 'r')	    
            file_q= []
            file_i = []
            for file_line in file1.readlines():
                file_q.append( float( file_line.split()[0] ) )
                if float( file_line.split()[1] ) > 0:
                    file_i.append( float( file_line.split()[1] ) )
                else:
                    file_i.append( 0.000001)             
            file1.close()
            plt.semilogy(file_q,file_i)
	    print 'read in sub_file directory'  
        except IOError, (errno, strerror):
            print "Failed to read %s in /sub_files" % (file)
	#####
	#   End of hack!
	####
    plt.grid(True)
    title = "Sub files"
    plt.title(title)            
    plt.subplot(143)
    plt.plot(range(1, len(row_info['rgs'])+1), row_info['rgs'])
    plt.errorbar(range(1, len(row_info['rgs'])+1), row_info['rgs'] , yerr= row_info['rgs_std'] )
    title = "Rg"
    plt.title(title)
    plt.subplot(144)
    plt.errorbar(range(1, len(row_info['i0'])+1), row_info['i0'] , yerr= row_info['i0_std'] )
    title = "i0"
    plt.title(title)
    plt.savefig(output_file)    
    return output_file

           


def _clean_up(path):
    ''' move files into seperate folders '''
    try:
        row_info = pickle.load( open( "save.p", "rb" ) )
    except IOError, (errno, strerror): 
        row_info = {'current_row':'start', 'current_name':'', 'rgs': [], 'rgs_std': [],'i0': [], 'i0_std': [], 'buffer_file': [], 'data_files':[] , 'directory':getcwd()}
    files = listdir(path)
    files.sort()
    for f in files:
        src = path+'/'+f
        dst = path +"/other_files/" + f
        if src.endswith('.png') or src.endswith('.dat') or src.endswith('.log') \
        or src.endswith('.tot') or src.endswith('.txt') or src.endswith('_images.html')\
        or src.endswith('.out') or '_dammif' in f:
            move(src, dst)
        if src.endswith('.sub'):
	    print src
	    dst = path +"/sub_files/" + f
	    if src in row_info['data_files']:
	    	row_info['data_files'] = dst
            move(src, dst)
    pickle.dump( row_info, open( "save.p", "wb" ) ) 
    pass


if __name__ == '__main__':
    bn = sys.argv[2]
    sn = sys.argv[1]
    saxs_pipe(sn,bn)
    pass
