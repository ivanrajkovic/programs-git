#include <stdlib.h>
#include <stdio.h>
#include <pcre.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>


int main(int argc, char *argv[]) {

	if (argc<2) return 1; 	
	
	char *find_reg = malloc(30);
	//char *ptr;
	pcre *reCompiled;
	pcre_extra *pcreExtra;
	int pcreExecRet;
	const char *pcreErrorStr;
	int pcreErrorOffset;
	int i = 0;
	char *s_name;
	char *b_name;
	char c[1000];	

	char *find_reg_buf = malloc(30);
	pcre *reCompiled_buf;
	pcre_extra *pcreExtra_buf;
	int pcreExecRet_buf;
	const char *pcreErrorStr_buf;
	int pcreErrorOffset_buf;
	int j = 0;

	//pid_t forkPid;	
	//int returnStatus;

	// linked list
	struct list {
    char *string;
    struct list *next;
	};

	typedef struct list LIST;
	
	
	
	//sprintf(find_reg,"^.*[SBT]%03d.*_0+1.tif$",strtol(argv[1],&ptr,10));
	sprintf(find_reg,"^.*[SBT]%03ld_0_0+1.tif$",strtoul(argv[1],NULL,10));
	printf("\nSearching for: %s \n\n",find_reg);
	
	reCompiled = pcre_compile(find_reg, 0, &pcreErrorStr, &pcreErrorOffset, NULL);
	pcreExtra = pcre_study(reCompiled, 0, &pcreErrorStr);
	free(find_reg);
	
	LIST *current, *head;
    head = current = NULL;



	if (argc == 3) {
		
		
		sprintf(find_reg_buf,"^.*[SBT]%03ld_0_0+1.tif$",strtoul(argv[2],NULL,10));
		printf("Searching for: %s \n\n",find_reg_buf);
		reCompiled_buf = pcre_compile(find_reg_buf, 0, &pcreErrorStr_buf, &pcreErrorOffset_buf, NULL);
		pcreExtra_buf = pcre_study(reCompiled_buf, 0, &pcreErrorStr_buf);
	}
	free(find_reg_buf);


	LIST *current_buf, *head_buf;
   	head_buf = current_buf = NULL;

    struct dirent *entry;
    DIR *dir = opendir("../data/");
    //DIR *dir = opendir(".");
    if (dir == NULL) {
        return 1;
    }



    while ((entry = readdir(dir)) != NULL) {

		pcreExecRet = pcre_exec(reCompiled, pcreExtra, entry->d_name, strlen(entry->d_name), 0, 0, NULL, 0);
		if (pcreExecRet == 0) {
			// number of files counter
			i++;
			//printf("sam: %s\n",entry->d_name);
			LIST *node = malloc(sizeof(LIST));
			// either malloc+strcpy or strdup:			
			//node->string = malloc(strlen(entry->d_name)+1);
			//strcpy(node->string,entry->d_name);			
			node->string = strdup(entry->d_name);
			node->next =NULL;
			if(head == NULL){
    			current = head = node;
			} else {
		    	current = current->next = node;
    		}
		}

		if (argc == 3) {
			pcreExecRet_buf = pcre_exec(reCompiled_buf, pcreExtra_buf, entry->d_name, strlen(entry->d_name), 0, 0, NULL, 0);	
			if (pcreExecRet_buf == 0) {
				j++;
				//printf("buf: %s\n",entry->d_name);
				LIST *node_buf = malloc(sizeof(LIST));
				node_buf->string = strdup(entry->d_name);
				node_buf->next =NULL;
				if(head_buf == NULL){
    				current_buf = head_buf = node_buf;
				} else {
		    		current_buf = current_buf->next = node_buf;
    			}
		
			}
		}

    }

    closedir(dir);
	free(reCompiled);
	free(pcreExtra);
	if (argc == 3) {
		free(reCompiled_buf);
		free(pcreExtra_buf);
	}	
	
	
	// exit if more than one file found
	
	if (i>1) {
		printf("more than one file found! \n");
		for(current = head; current ; current=current->next){
    	   printf("%s \n", current->string);
   		 }
		printf("number of files: %d \n\n",i);
		return 1;
	} else {
			s_name = strdup(head->string);
			printf("using sample : %s\n\n",s_name);		
	}
	
	
	
	if (argc == 3) {
		if (j>1) {
			printf("more than one file found! \n");
			for(current_buf = head_buf; current_buf ; current_buf=current_buf->next){
			   printf("%s \n", current_buf->string);
	   		 }
			printf("number of files: %d \n\n",j);
			return 1;
		} else {
				b_name = strdup(head_buf->string);
				printf("using buffer : %s\n\n",b_name);	
		}
		
	}

	
	// edit integ.mpp (read -> make tmp file -> move tmp file)
	FILE *integmpp = fopen("integ.mpp", "r");
	FILE *integ_tmp = fopen("integ.tmp","w");
	
	while (fgets(c, sizeof c, integmpp) != NULL) 
	{
		if (strncmp(c,"-f",2) == 0) fputs("#",integ_tmp);
		if (strncmp(c,"-s no",5) == 0) strcpy(c,"-s yes\n");
		if (argc == 3) {
			if (strncmp(c,"#-s",3) == 0) strcpy(c,"-s yes\n");
		} else {
			if (strncmp(c,"-s",2) == 0) strcpy(c,"#-s yes\n");
		}
		fputs(c, integ_tmp);
	}
	//fputs("-f ",integ_tmp);
	//fputs(s_name,integ_tmp);
	//if (argc == 3) {
	//	fputs(" ",integ_tmp);
	//	fputs(b_name,integ_tmp);
	//}
	//fputs("\n",integ_tmp);
	
	if (argc == 2) fprintf(integ_tmp,"-f ../data/%s \n",s_name);
	if (argc == 3) fprintf(integ_tmp,"-f ../data/%s ../data/%s \n",s_name,b_name);
	
	fclose(integ_tmp);
	fclose(integmpp);
	
	// make a fork and move file / start sastool in defferent process
	//if (forkPid = fork() == 0) {
	//	execl( "/bin/mv", "mv", "./integ.tmp", "./integ.mpp",(char *)0);
	//	exit(0);
	//}
	


	// wait for move to finish    
    //waitpid(forkPid, &returnStatus, 0);

	//if (forkPid = fork() == 0) {
	//	execl("/mnt/home/sw/bin/sastool", "sastool","integ.mpp",(char *)0);
	//	exit(0);
	//}
	
	// wait for sastool to finish
    //waitpid(forkPid, &returnStatus, 0);

	// back to main process

	// use popen/pclose to run commands, make sure that the command is executed (mv_file != NULL)
	FILE *mv_file = popen("mv ./integ.tmp ./integ.mpp","r");
	if (mv_file == NULL) {
		printf("%s","Error writing integ.mpp");
		return 1;
	}
	while (fgets(c, sizeof c, mv_file) != NULL) printf("%s",c);
	pclose(mv_file);
	
	FILE *run_sastool = popen("sastool integ.mpp","r");
	if (run_sastool == NULL) {
		printf("%s","Error running sastool");
		return 1;
	}

	while (fgets(c, sizeof c, run_sastool) != NULL) printf("%s",c);
	pclose(run_sastool);
	
	
	// delete sample/buffer list
	while ((current = head) != NULL) {	
		head = head->next;
		free(current->string);
		free(current); 
	}
	head =NULL;

	while ((current_buf = head_buf) != NULL) {	
		head_buf = head_buf->next;
		free(current_buf->string);
		free(current_buf); 
	}
	head_buf =NULL;


	free(s_name);
	if (argc == 3) free(b_name);
	return 0;

}
