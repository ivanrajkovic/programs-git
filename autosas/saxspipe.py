'''
Created on Jun 17, 2013

@author: lc71

Python program utlizing Atsas and Sastool to analysse the results of Bl42 autosampelr data collection


Requires:
    program_paths.txt - a file with paths to autorg, datgnom, datcmp and sastool
    The above programs installed on the system
    

Example usuage:
    Needs to be run in directory above data and analysis.  
    

'''

from sys import platform
from os import  getcwd, mkdir, chdir, listdir, path, walk, system
from time import sleep
from shutil import move
from SAXShtml import starthtml, endhtml, table_line
from analyse_sub import analyse_sub
from analyse_sub import _run_autorg as run_autorg
import matplotlib.pyplot as plt
import numpy as np
import pickle


''' global variables for program paths - set in intial set up '''

autorg_path = None
datgnom_path= None
gdatcmp_path = None
sastool_path = None


def initial_setup():
    ''' read program_paths.txt'''    
    _read_program_paths()
    _check_paths()
    return True


def saxs_pipe():
    ''' start main analysis procedure '''
    data_dir = _get_data_directory()
    integ_file_location = _get_integ_file()
    template_file = _make_folders_and_copy_integ_file(integ_file_location)
    _analysis_loop(data_dir, template_file)
    return True


def _read_program_paths():
    '''  parse through program_paths.txt and extract program info ''' 
    global autorg_path 
    global datgnom_path
    global datcmp_path
    global sastool_path      
    print 'Platform identified as: %s' % platform
    print 'searching program_paths.txt for program links'
    paths_file = ''
    if platform == 'linux2':
        paths_file =  '/mnt/home/lgcarter/pipeline/code/program_paths.txt'
    if platform == 'win32':
        paths_file =  'program_paths.txt'
    try:
        path_file = open(paths_file , 'r')
        #path_file = open('/mnt/home/lgcarter/pipeline/code/program_paths.txt', 'r')
        for file_line in path_file.readlines():
            program_paths = file_line.split('\t')
            if  program_paths[0] == platform and program_paths[1] == 'autorg':autorg_path = program_paths[2].strip()
            if  program_paths[0] == platform and program_paths[1] == 'datgnom':datgnom_path = program_paths[2].strip()
            if  program_paths[0] == platform and program_paths[1] == 'datcmp':datcmp_path = program_paths[2].strip()
            if  program_paths[0] == platform and program_paths[1] == 'sastool':sastool_path = program_paths[2].strip()
        path_file.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)


def _check_paths():
    '''  check all paths were read  '''
    ''' TODO: Check progams do exist and run '''    
    if autorg_path == None:
        print 'autorg path not set. See program_paths.txt'
        exit()
    else:
        print 'auotrg path: %s' % autorg_path        
    if datgnom_path == None:
        print 'datgnom path not set. See program_paths.txt'
        exit()
    else:
        print 'datgnom path: %s' % datgnom_path
    if datcmp_path == None:
        print 'datcmp path not set. See program_paths.txt'
        exit()
    else:
        print 'datcmp path: %s' % datcmp_path
    if sastool_path == None:
        print 'sastool path not set. See program_paths.txt'
        exit()
    else:
        print 'sastool path: %s' % sastool_path
        
    

def _get_data_directory():
    ''' get data directory '''
    data_dir = getcwd()  + "/data/"
    var = raw_input("Presuming running above data and analysis dirs, therefore: %s\nHit return to use default, or else type in new full path:\n" % data_dir)
    if len(var) <1:
        print "Using default data directory"
    else:
        data_dir = var
        print "Using: %s" % data_dir
    return data_dir

    
def _get_integ_file():
    ''' get integ.mpp details '''           
    integ_file_location = getcwd()  + "/analysis/integ.mpp"
    var = raw_input("Which integ.mpp to use ?\nPresuming: %s\nHit return to use default, or else type in new full path:\n" % integ_file_location)
    if len(var) <2:
        print "Using default integ.mpp" 
    else:
        integ_file_location = var
        print "Using: %s" % integ_file_location
    return integ_file_location
        
         
def _make_folders_and_copy_integ_file(integ_file_location):
    ''' copy integ file - modify as needs be '''
    integ_file_text = ''
    try:
        integ_file = open(integ_file_location, 'r')
        # need to step through file, and copy mask file etc.
        for line in integ_file:
            if line.startswith('-m'):
                mask_name = line.split()[2]
                if mask_name.startswith('./'):
                    mask_name = mask_name[2:]
                if '/' in mask_name:
                    mask_name = mask_name.split('/')[-1]
                line = '-m yes %s/analysis/%s\n' % (getcwd(), mask_name  )
            if line.startswith('-s'):
                line = '-s no\n'                
            integ_file_text = integ_file_text + line
        integ_file.close()
    except IOError, (errno, strerror):
        print "Problem reading integ.mpp - error(%s): %s" % (errno, strerror)    
    print "Making output folder"
    try:
        mkdir("automatic_analysis")
    except OSError, (errno, strerror):
        print "automatic_analysis directory already exists"
    chdir("automatic_analysis")
    try:
        mkdir("sub_files")
        mkdir("other_files")
    except OSError, (errno, strerror):
        print "directories already exist"
    try:
        template_file_location = getcwd()  + "/template.mpp"
        template_file  = open(template_file_location, 'w')
        template_file.write(integ_file_text)
        template_file.close()
    except OSError, (errno, strerror):
        print "Problem writing template"
    return template_file_location 
     
 
def _analysis_loop(data_dir, template_file):    
    ''' loop to wait for new data '''
    while 1< 2:
        _analyse_folder(data_dir, template_file)
        _clean_up(getcwd())
        print "waiting for new images"
        sleep(90)


def _analyse_folder(data_dir, template_file):
    ''' analyse folder  '''
    sample_buffer_pairs = _search_data(data_dir)
    log_file = _run_sastool(template_file, True, sample_buffer_pairs)
    _read_log_file(log_file, data_dir)
    _analyse_images(sample_buffer_pairs, log_file, data_dir)


def _analyse_images(sample_buffer_pairs, log_file, data_dir):
    for pair in sample_buffer_pairs:
        buffer = pair.split()[2]
        sample = pair.split()[1]
        _get_variance(buffer, sample, log_file)
    pass
 
        
def _get_variance(buffer, sample, log_file):
    log_file_line = "TIFF data files: %s %s" % (sample, buffer)
    variance_lines = False
    Buffer_frame = True
    buffer_variance = []
    sample_variance = []
    buffer_tot_q = []
    buffer_tot_i = []
    sample_tot_i = []    
    buffer_tot_file = '%s/automatic_analysis/%s' %('/'.join(buffer.split('/')[:-2]), "%stot" % buffer.split('/')[-1][:-3])
    try:
        file1 = open(buffer_tot_file , 'r')
        for file_line in file1.readlines():
            buffer_tot_q.append( float( file_line.split()[0] ) )
            buffer_tot_i.append( float( file_line.split()[1] ) )
        file1.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)             
    sample_tot_file = '%s/automatic_analysis/%s' %('/'.join(sample.split('/')[:-2]), "%stot" % sample.split('/')[-1][:-3])  
    try:
        file1 = open(sample_tot_file , 'r')
        for file_line in file1.readlines():
            sample_tot_i.append( float( file_line.split()[1] ) )
        file1.close()    
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)
    
    try:
        file1 = open(log_file, 'r')
        for file_line in file1.readlines():
            if log_file_line in file_line:
                variance_lines  = True
            if variance_lines and file_line.startswith('frame') and Buffer_frame:
                buffer_variance.append(float( file_line.split()[2]))
            if variance_lines and file_line.startswith('frame') and not Buffer_frame:
                sample_variance.append(float( file_line.split()[2]))
            if variance_lines and file_line.startswith('Final'):
                _plot_image_info(buffer_variance, sample_variance, sample.split('/')[-1], buffer_tot_q, buffer_tot_i, sample_tot_i , sample)
                variance_lines  = False
            if variance_lines and file_line.startswith('Average variance for buffer'):
                Buffer_frame = False                
        file1.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)   
    pass


def _plot_image_info(buffer_variance, sample_variance, sample_root, buffer_tot_q, buffer_tot_i, sample_tot_i, sample ):
    output_file = "%s_image_data.png" % sample_root[:-4]
    plt.figure(num=None, figsize=(9,2), dpi=80, facecolor='w', edgecolor='k')
    plt.subplot(141)
    plt.plot(range(1, len(buffer_variance)+1), buffer_variance)
    plt.plot(range(1, len(sample_variance)+1), sample_variance)
    title = "Variance"
    plt.title(title)
    plt.subplot(142)
    plt.plot(buffer_tot_q, buffer_tot_i)
    plt.plot(buffer_tot_q, sample_tot_i)
    title = "Tot files"
    plt.title(title)
    plt.subplot(143)   
    files_found = []
    rgs = []
    rgs_std = [] 
    directory = '%s/automatic_analysis/' %('/'.join(sample.split('/')[:-2])) 
    for root, dirs, files in walk(directory):
        for line in files:
            #only works upto 100
            if '.dat' in line and (sample.split('/')[-1][:-6]) in line and not 'other_files' in root and not '_sub.dat' in line:                
                files_found.append(path.join(root, line))
                autorg_results =  run_autorg(path.join(root, line))
                rgs.append(float(autorg_results[1]))
                rgs_std.append(float(autorg_results[2]))
    files_found.sort(key=lambda s: path.getmtime(s))
    for file in files_found:
        try:
            file1 = open(file, 'r')
            file_q= []
            file_i = []
            for file_line in file1.readlines():
                if  float( file_line.split()[0] ) < 0.2:
                    file_q.append( float( file_line.split()[0] ) )
                    file_i.append( float( file_line.split()[1] ) )             
            file1.close()
            plt.plot(file_q,file_i)
        except IOError, (errno, strerror):
            print "reading image file I/O error(%s): %s" % (errno, strerror)  
    title = "dat files < 0.2"
    plt.title(title)     
    plt.subplot(144)    
    plt.errorbar(range(1, len(rgs)+1), rgs , yerr= rgs_std )
    title = "Image rg"
    plt.title(title)         
            
    plt.savefig(output_file)  
    pass



def _search_data(directory):
    ''' '''
    files_found = []
    for root, dirs, files in walk(directory):
        for line in files:
            if (line.endswith("01.tif") ) and ("_S"  in line):
                files_found.append(  path.join(root, line))
    files_found.sort(key=lambda s: path.getmtime(s))
    sample_buffer_pairs = _work_through_files(files_found)
    return sample_buffer_pairs


def _work_through_files(files_found):
    ''' look for new sampel files and corresponding buffer '''
    sample_buffer_pairs = []
    for file in files_found:
        directory = file.split('/')
        full_root = directory[len(directory)-1]
        name_root = full_root.split('_S')[0]
        directory.pop(-1)
        directory = ('/').join(directory)
        buffer_files = _get_buffer_name_from_root(name_root,directory, full_root )
        if len(buffer_files)>0:
            sample_buffer_pairs.append( "-f %s %s/%s" % ( file,  directory, buffer_files[0] ) )
    return sample_buffer_pairs
    

def _get_buffer_name_from_root(name_root, directory, full_name_root):
        ''' returns the last buffer before sample, and checks same amount images '''
        ''' TODO - ugly needs refactoring '''
        buffer_files = []
        buffer_last_number = 0
        sample_last_number = 0
        buffer_name= "%s%s" % (name_root[0:-1], 'A')
        sample_number=full_name_root.split('_S')[1].split('_0_01')[0]
        if sample_number.startswith('0'):
            sample_number = sample_number[1:len(sample_number)]
        sample_buffer = 0
        buffer_dict = {} # using dict to store buffers - think I can do this much better
        for root, dirs, files in walk(directory):
            for line in files:
                if line.startswith(name_root):
                    if int(line.split('_')[-1][0:-4]) > int(sample_last_number):
                        sample_last_number = line.split('_')[-1][0:-4]
                if  line.startswith(buffer_name):
                    if int(line.split('_')[-1][0:-4]) > int (buffer_last_number):
                        buffer_last_number = line.split('_')[-1][0:-4]
                if  line.startswith(buffer_name) and line.endswith("01.tif") :
                    buffer_number  = line.split('_0_01')[0]
                    buffer_number =buffer_number.split('_B')[1]
                    if buffer_number.startswith('0'):
                        buffer_number = buffer_number[1:len(buffer_number)]
                    buffer_dict[buffer_number]=line
                    if (int(buffer_number) < int(sample_number)) and (sample_buffer < buffer_number):
                        sample_buffer = buffer_number        
        if buffer_dict.get(sample_buffer):
            if sample_last_number == buffer_last_number:
                buffer_files.append(buffer_dict.get(sample_buffer))                             
        return buffer_files


def _run_sastool(template_file, subtract, sample_buffer_pairs):
    ''' run sastool on each new set of data'''
    template_lines = []
    log_file = ''
    try:
        file1 = open(template_file, 'r')
        for lineA in file1:
            if subtract and '-s' in lineA:
                template_lines.append( '-s yes\n')
            elif '-s' in lineA:
                template_lines.append( '-s no\n')
            if lineA[0:len(lineA)-1] in sample_buffer_pairs :
                sample_buffer_pairs.remove(lineA[0:len(lineA)-1])
            if lineA[1:len(lineA)-1] in sample_buffer_pairs:
                sample_buffer_pairs.remove(lineA[1:len(lineA)-1])
            if not lineA.startswith('-f') and '-s' not in lineA:
                template_lines.append( lineA)
            if lineA.startswith('-f'):
                template_lines.append( '#'+lineA)
            if lineA.startswith('-l'):
                log_file =  lineA.split(' ')[1]
        file1.close()
        file1 = open(template_file, 'w')
        for line in template_lines:
            file1.write(line)
        for line2 in sample_buffer_pairs:
            file1.write(line2+"\n")
        file1.close()    
    except IOError, (errno, strerror):
        print "I/O error(%s): %s" % (errno, strerror)
    command1 = "%s %s" % (sastool_path, template_file)
    system(command1)
    return log_file[0:-1]


def _read_log_file(logfile, directory):
    ''' main log file processing script - needs refactoring '''
    ''' TODO - cleanup '''
    try:
        logfile_html = "%s.html" % logfile[0:-4]
        allplot  = "%s.png" % logfile[0:-4]
        htmltop = starthtml(logfile, allplot)
        try:
            file1 = open(logfile_html, 'r')
            htmltop = ""
            for lineA in file1:
                if "</table></body></html>" not in lineA :
                    htmltop = "%s%s" % (htmltop, lineA)
        except IOError, (errno, strerror):
            print "Making New log file %s" % logfile_html      
        file1 = open(logfile, 'r')
	#below is an attempt to use pickles to store row infor, so it remains valid in the gap between one sample and another.
        try:
		row_info = pickle.load( open( "save.p", "rb" ) )
        except IOError, (errno, strerror): 
        	row_info = {'current_row':'start', 'current_name':'', 'rgs': [], 'rgs_std': [],'i0': [], 'i0_std': [], 'buffer_file': [], 'data_files':[] , 'directory':directory}
        buffer_name = ""
        sample_name= ""
        buffer_frames = 0
        sample_frames= 0
        buffer_rejected = 0
        sample_rejected= 0
        sub_file = ''
	sub_file_moved = ''
        sub_file_number = 0
        alllog = ""
        file3_data =[]
        sastools_subsection =[]
        images_html = []
        images_names = []
        file_root = ''
        for lineA in file1:
            sastools_subsection.append(lineA)
            data_line = lineA.split()
            alllog = "%s%s" % (alllog, lineA)
            if len(data_line) > 0:
                if data_line[0]  == 'TIFF':
                    buffer_name =data_line[4]
                    sample_name =data_line[3] #careful of this and below use..
                    file_root = data_line[3] .split('/')[-1].split('.')[0]
                    file_root  = file_root.split('_')
                    file_root.pop()
                    file_root = '_'.join(file_root)+'_'
                if data_line[0]  == 'Buffer':
                    buffer_frames =data_line[1].split(":")[1]
                    buffer_rejected= data_line[2].split(":")[1]
                if data_line[0]  == 'Sample':
                    sample_frames= data_line[1].split(":")[1]
                    sample_rejected= data_line[2].split(":")[1]
                if data_line[0]  == 'Final':
                    sample_name= data_line[1].split(":")[1]
                    remove_path = sample_name.split('/')
                    sub_file = remove_path[-1]
                    sub_file = sub_file[0:-3]+"sub"
		    sub_file_moved = 'other_files/'+ sub_file
                    png_file = sub_file[0:-3]+"png"
                    sastools_subsection_file = sub_file[0:-4]+"_sas.log"
                    file_sastools = open(sastools_subsection_file, 'w')
                    for line in sastools_subsection:
                        file_sastools.write(line)
                    file_sastools.close()
                    sastools_subsection =[]
                    images_html_file  = sub_file[0:-4]+"_images.html"
                    file_images = open(images_html_file, 'w')
                    file_images.write("""""<html><head><title></title></head><body><table border="1" cellspacing="0" align="left"><tbody>""")
                    for line in images_html:
                        file_images.write(line)
                    file_images.write("""</tbody></table></body></html>""")
                    file_images.close
                    images_html = []                                       
                    time_taken = _get_collection_time(sub_file, directory) 
                    uv_values = _get_uv_values(sub_file, buffer_name, directory) 
                    sub_results = analyse_sub(sub_file)
                    data_result = 'Data looks okay!'
                    #row_color = '''style="background-color:green;"'''
                    row_color = '''"'''
                    if float(sub_results[6][2]) > (float(sub_results[5][1]) + 2) or float(sub_results[6][2]) < (float(sub_results[5][1]) - 2):
                        data_result  = 'Check data - real and reciporocal rgs differ: %5.2f  %5.2f  ' % ( float(sub_results[5][1]) , float(sub_results[6][2]))
                        row_color = ''''''
                    if int(sample_rejected) > 2:
                        data_result  = 'Check data - %s frames rejected  ' % ( sample_rejected  )
                        row_color = '''style="background-color:red;"'''
                    if  int(sample_rejected) > 2 and (float(sub_results[6][2]) > (float(sub_results[5][1]) + 2) or float(sub_results[6][2]) < (float(sub_results[5][1]) - 2)):
                        data_result  = 'Check data - real and reciporocal rgs differ: %5.2f  %5.2f  ' % ( float(sub_results[5][1]) , float(sub_results[6][2]))
                        data_result  =  data_result  + '.\n  Also %s frames were rejected' % ( sample_rejected  )   
                        row_color = ''''''                                                      

                    working_name =  '_'.join(sub_results[0].split('_')[:-5]) 
                    working_row = sub_results[0].split('_')[-5][:-1]
                    if row_info['current_row'] == 'start':
                        row_info['current_row'] = working_row
			row_info['current_name'] = working_name
                    if row_info['current_row'] == working_row and row_info['current_name'] == working_name:
                        row_info['rgs'].append( float (sub_results[5][1]))
                        row_info['rgs_std'].append( float (sub_results[5][2]))
                        row_info['i0'].append( float( sub_results[5][3]))
                        row_info['i0_std'].append( float (sub_results[5][4]))
                        row_info['data_files'].append(sub_file)
                        
                    else:
		    	row_info['current_name'] =  '_'.join(sub_results[0].split('_')[:-5])
                        row_info['current_row'] = working_row
                        row_info['rgs']= [float (sub_results[5][1] )] 
                        row_info['rgs_std'] = [float (sub_results[5][2] )]
                        row_info['i0'] = [float (sub_results[5][3] )]
                        row_info['i0_std'] = [float (sub_results[5][4] )]
                        row_info['data_files']= [sub_file]
                    pickle.dump( row_info, open( "save.p", "wb" ) )  # save whatever value pickle has                		
                    row_png = _row_graphs(row_info)                   
                    
                    
                    
                    file3_data.append(table_line(row_color, \
                                                 data_result, \
                                                 time_taken[0],\
                                                 time_taken[1],\
                                                 sub_results[0],\
                                                 buffer_frames, \
                                                 buffer_rejected, \
                                                 sample_frames, \
                                                 sample_rejected,  \
                                                 uv_values[0], \
                                                 uv_values[1], \
                                                 sub_results[1], \
                                                 sub_results[2], \
                                                 sub_results[5][0], \
                                                 sub_results[5][1], \
                                                 sub_results[5][2], \
                                                 sub_results[5][3], \
                                                 sub_results[5][4], \
                                                 sub_results[5][5], \
                                                 sub_results[6][2], \
                                                 sub_results[6][0], \
                                                 "%s_image_data.png" %png_file[:-4], \
                                                 png_file, \
                                                 sub_results[7], \
                                                 row_png                                                 
                                                 ))
                    images_names = []                   
                    sub_file_number = sub_file_number + 1             
                    file3 = open(logfile_html, 'w')
                    file3.write(htmltop)
                    for line in file3_data:
                        file3.write(line)    
                    file3.write(endhtml())
                    file3.close()                
                if "buffer files:" in lineA:
                    file_root = buffer_name.split('.')[0] 
                if "sample files:" in lineA:
                    file_root = sample_name.split('.')[0]    
                if data_line[0] =='frame':
                    number = data_line[1][1:len(data_line[1])-1]
                    if len(number) == 1 : number = "0%s" % number
                    file_root  = file_root.split('_')
                    file_root.pop()
                    file_root = '_'.join(file_root)+'_'
                    tif_file = "%s%s.tif" %(file_root,  number) # will break will over a hundred images mind...
                    name = tif_file.split('/')[-1]
                    name = "%s_image.png" %name.split('.')[0]
                    command = "convert %s -resize 320x320 -equalize  -contrast-stretch 0 %s" % (tif_file, name  )
                    system(command)
                    text_for_image = "<tr><th ><img src='%s' alt='image' /></th><th >%s</th></tr>" %(name, tif_file)
                    images_html.append(text_for_image)
                    images_names.append(tif_file)
        file1.close()    
   
    except IOError, (errno, strerror):
        print "I/O error(%s): %s" % (errno, strerror)    
    return


def _get_collection_time(datfile, directory):
    prp_file =  "%s.prp" % (datfile[0:len(datfile)-4])
    fullpath = "%s%s" % (directory, prp_file)
    time = "0"
    try:
        file1 = open(fullpath, 'r')
        for file_line in file1.readlines():
            if 'Time this file was written:' in file_line:
                time = file_line.split('written: ')[1]        
        file1.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)    
    return [time,fullpath]
    
    
def _get_uv_values(datfile, buffer, directory):
    sample_uv_file =  "%s00_uv_01.txt" % (datfile[0:len(datfile)-6])
    fullpath = "%s%s" % (directory, sample_uv_file)
    sample_uv = "na"
    try:
        file1 = open(fullpath, 'r')
        for file_line in file1.readlines():
            if file_line.startswith("280.185"):
                sample_uv = file_line.split()[1]      
        file1.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)
    buffer_uv_file =  "%s00_uv_01.txt" % (buffer[0:len(buffer)-6])        
    fullpath = buffer_uv_file
    buffer_uv = "na"
    try:
        file1 = open(fullpath, 'r')
        for file_line in file1.readlines():
            if file_line.startswith("280.185"):
                buffer_uv = file_line.split()[1]      
        file1.close()
    except IOError, (errno, strerror):
        print "reading image file I/O error(%s): %s" % (errno, strerror)            
    return [buffer_uv, sample_uv]


def _row_graphs(row_info):
    output_file = "%s_row_data.png" % row_info['current_name']
    plt.figure(num=None, figsize=(9,2), dpi=80, facecolor='w', edgecolor='k')    
    plt.subplot(141)    
    files_found = []
    directory = '%s/automatic_analysis/' %('/'.join(row_info['directory'].split('/')[:-1]))
    for root, dirs, files in walk(directory):
        for line in files:
            #only works upto 100
	    if '.dat' in line and row_info['current_row'] in line and '_B' in line and line.startswith(row_info['current_name']): 
            #if '.dat' in line and row_info['current_row'] in line and not 'other_files' in root and  '_B' in line and line.startswith(row_info['current_name']):                
                files_found.append(path.join(root, line))
    files_found.sort(key=lambda s: path.getmtime(s))
    for file in files_found:
        try:
            file1 = open(file, 'r')
            file_q= []
            file_i = []
            for file_line in file1.readlines():
                    file_q.append( float( file_line.split()[0] ) )
                    file_i.append( float( file_line.split()[1] ) )             
            file1.close()
            plt.plot(file_q,file_i)
        except IOError, (errno, strerror):
            print "reading the buffer file file I/O error(%s): %s - %s" % (errno, strerror, file)
    plt.grid(True)  
    title = "Buffer files"
    plt.title(title)         
    plt.subplot(142)
    
    print 'Here is row dictionary', row_info['data_files']     
    for file in row_info['data_files']:
        try:
	    filename1 = "%s%s" % (directory, file)
	    print 'trying to open %s in automatic_analysis directory' % (filename1) 
            file1 = open(filename1, 'r')
            file_q= []
            file_i = []
            for file_line in file1.readlines():
                file_q.append( float( file_line.split()[0] ) )
                if float( file_line.split()[1] ) > 0:
                    file_i.append( float( file_line.split()[1] ) )
                else:
                    file_i.append( 0.000001)             
            file1.close()
            plt.semilogy(file_q,file_i) 
	    print "Read in automatic_analysis directory"  
        except IOError, (errno, strerror):
            print "Failed to read %s in automatic_analysis" % (file)
	    
	####
	#   This is an awful hack to fix the subfile not being found problem
	####
	   
        try:
	    filename2 = "%s%s%s" % (directory,'sub_files/', file)
	    print 'trying to open %s in sub files' % (filename2) 
            file1 = open(filename2, 'r')	    
            file_q= []
            file_i = []
            for file_line in file1.readlines():
                file_q.append( float( file_line.split()[0] ) )
                if float( file_line.split()[1] ) > 0:
                    file_i.append( float( file_line.split()[1] ) )
                else:
                    file_i.append( 0.000001)             
            file1.close()
            plt.semilogy(file_q,file_i)
	    print 'read in sub_file directory'  
        except IOError, (errno, strerror):
            print "Failed to read %s in /sub_files" % (file)
	#####
	#   End of hack!
	####
    plt.grid(True)
    title = "Sub files"
    plt.title(title)            
    plt.subplot(143)
    plt.plot(range(1, len(row_info['rgs'])+1), row_info['rgs'])
    plt.errorbar(range(1, len(row_info['rgs'])+1), row_info['rgs'] , yerr= row_info['rgs_std'] )
    title = "Rg"
    plt.title(title)
    plt.subplot(144)
    plt.errorbar(range(1, len(row_info['i0'])+1), row_info['i0'] , yerr= row_info['i0_std'] )
    title = "i0"
    plt.title(title)
    plt.savefig(output_file)    
    return output_file

           


def _clean_up(path):
    ''' move files into seperate folders '''
    try:
        row_info = pickle.load( open( "save.p", "rb" ) )
    except IOError, (errno, strerror): 
        row_info = {'current_row':'start', 'current_name':'', 'rgs': [], 'rgs_std': [],'i0': [], 'i0_std': [], 'buffer_file': [], 'data_files':[] , 'directory':getcwd()}
    files = listdir(path)
    files.sort()
    for f in files:
        src = path+'/'+f
        dst = path +"/other_files/" + f
        if src.endswith('.png') or src.endswith('.dat') or src.endswith('.log') \
        or src.endswith('.tot') or src.endswith('.txt') or src.endswith('_images.html')\
        or src.endswith('.out') or '_dammif' in f:
            move(src, dst)
        if src.endswith('.sub'):
	    print src
	    dst = path +"/sub_files/" + f
	    if src in row_info['data_files']:
	    	row_info['data_files'] = dst
            move(src, dst)
    pickle.dump( row_info, open( "save.p", "wb" ) ) 
    pass


if __name__ == '__main__':
    initial_setup()
    saxs_pipe()
    pass
