import sys, os, subprocess,datetime,re, time, signal
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

def analyse_sub(sub_file):
    sub_file_dat = "%s_sub.dat" % (sub_file[0:len(sub_file)-4])
    command3 = "datop DIV %s 1 -o %s" % (sub_file, sub_file_dat)
    os.system(command3)    
    dmax = 1
    autorg_results= _run_autorg(sub_file_dat)
    datgnom_results= _run_datgnom(sub_file)
    _run_dammif(sub_file)         
    graph_file = "%spng"% (sub_file[0:len(sub_file)-3])
    data_png = "%s_data.png" % (sub_file[0:len(sub_file)-4])
    _simple_plots(sub_file)
    _empty_data_plots(sub_file)
    if float(autorg_results[1]) > 1 and float(datgnom_results[0]) > 1:
        start_point =autorg_results[5].split()[0]
        end_point = autorg_results[5].split()[2]
        try:
            start_point =autorg_results[5].split()[0]
            end_point = autorg_results[5].split()[2]
            gnom_file = "%sout" % sub_file [0:-3]
            file2g = open(gnom_file, 'r')
            write = False
            gnome_file = "g_%stxt" % sub_file[0:-3]
            file3g = open(gnome_file ,"w")
            for line in file2g.readlines():
                if "Distance distribution" in line:  write = True
                if write and line.startswith('  0'): file3g.write(line)
            file2g.close()
            file3g.close()                     
        except:
            print "problem with getting rg values"
        _data_plots(sub_file, start_point, end_point, autorg_results[1], autorg_results[3], gnome_file)             
    return [sub_file_dat, autorg_results[1], autorg_results[3], dmax, graph_file, autorg_results, datgnom_results, data_png]


def _time_out(command, timeout, shell_command):
    """call shell-command and either return its output or kill it
    if it doesn't normally exit within timeout seconds and return None
    Adapted from;http://code.pui.ch/2007/02/19/set-timeout-for-a-shell-command-in-python/"""
    if shell_command:
        cmd = command
    else:
        cmd = command.split(" ")
    start = datetime.datetime.now()
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=shell_command)
    while process.poll() is None:
        time.sleep(0.2)
        now = datetime.datetime.now()
        if (now - start).seconds> timeout:
            os.kill(process.pid, signal.SIGKILL)
            os.waitpid(-1, os.WNOHANG)
            print "Command timed out!"
            return None
    return process.stdout.readlines()



def _run_datgnom(sub_file):
    pr_log = "%s_pr.log" % sub_file[0:len(sub_file)-4]
    command2 = "datgnom %s > %s_pr.log" %(sub_file, sub_file[0:len(sub_file)-4])
    os.system(command2)
    dmax=1
    datgnom_rg=1
    datgnom_rrg=1
    try:
        file1 = open(pr_log  , 'r')
        for file_line in file1.readlines():
            if 'Dmax =  ' in file_line:
                dmax = file_line.split()[2]
            if 'Guinier =    ' in file_line:
                datgnom_rg = file_line.split()[2]
                datgnom_rrg = file_line.split()[5]
    except IOError, (errno, strerror):
        print "datgom io error"    
    return [dmax, datgnom_rg, datgnom_rrg]


def _run_autorg(sub_file):
    rg = "1"
    rg_std = "1"
    i0 = "1"
    i0_std = "1"
    points = "1"    
    rg_log = "%s_rg.log" % sub_file[0:len(sub_file)-4]
    command4 = "autorg %s > %s" % (sub_file,rg_log) 
    os.system(command4)
    try:
        file1 = open(rg_log  , 'r')
        for file_line in file1.readlines():
            if 'Rg   =   ' in file_line:
                rg = file_line.split()[2]
                rg_std = file_line.split()[4]
            if 'I(0) =   ' in file_line:
                i0 = file_line.split()[2]
                i0_std = file_line.split()[4]
            if 'Points   ' in file_line:
                points = file_line[9:-1]    
        file1.close() 
        rg = rg.replace('%','')
        if float(rg) > 500:
            rg = "1"
            rg_std = "1"
            i0 = "1"
            i0_std = "1"
            points = "1"
    except IOError, (errno, strerror):
        print " rg log file I/O error(%s): %s" % (errno, strerror)
    return [rg_log, rg, rg_std, i0, i0_std, points]


def _run_dammif(sub_file):
    for i in range(1,5):
        command5 = "dammif -p=%s_dammif_%s --mode=fast %s.out > dammif.tmp" % (sub_file[0:len(sub_file)-4], i, sub_file[0:len(sub_file)-4])
        #print command5
    pass



def _simple_plots(sub_file):
    font = {'family' : 'normal',
            'weight' : 'normal',
            'size'   : 8}

    matplotlib.rc('font', **font)
    q = []
    i = []
    try:
        file1 = open(sub_file  , 'r')
        for file_line in file1.readlines():
            q.append(file_line.split(' ')[0] )
            i.append(file_line.split(' ')[1] )
        file1.close()
    except IOError, (errno, strerror):
        print "io error"
    plt.figure(num=None, figsize=(6,2), dpi=80, facecolor='w', edgecolor='k')
    plt.subplot(131)
    print '\n\n min is %s max is %s\n\n' % (min(i), max(i) )
    if float(max(i)) > 0.00001:
        plt.semilogy(q,i)
        title = "%s" % ('Log i v q')
        plt.title(title)
        plt.grid(True)
    else:
        plt.plot(q,i)
        title = "%s" % ('No positive values i v q')
        plt.title(title)
        plt.grid(True)        
    plt.subplot(132)
    if float(max(i)) > 0.00001:
        plt.loglog(q,i)
        title = "%s" % ('Log i v log q')
        plt.title(title)
        plt.grid(True)
    else:
        plt.plot(q,i)
        title = "%s" % ('No positive values i v q')
        plt.title(title)
        plt.grid(True)  
    kratky_y = []
    for position in range(len(q)):
        kratky_y.append(float(i[position]) * float(q[position])**2) 
    plt.subplot(133)
    plt.plot(q,kratky_y)
    title = "%s" % ('Kratky')
    plt.title(title)
    plt.grid(True)   
    plt.savefig('%spng' % sub_file[:-3])


def _empty_data_plots(sub_file):
    # empty plot
    plt.figure(num=None, figsize=(6,2), dpi=80, facecolor='w', edgecolor='k')
    plt.subplot(121)
    plt.plot([1],[1])
    plt.title('No good rg')
    plt.subplot(122) 
    plt.plot([1],[1]) 
    plt.title('no good pr')       
    plt.savefig('%s_data.png' % sub_file[:-4])
    


def _data_plots(sub_file, start, end, rg, i0, gnome_file):
    font = {'family' : 'normal',
            'weight' : 'normal',
            'size'   : 8}

    matplotlib.rc('font', **font)
    x = []
    y = []
    a = []
    b = []
    line_counter = 1
    try:
        file1 = open(sub_file  , 'r')
        for file_line in file1.readlines():
            if line_counter >= int(start) and line_counter <= int(end):
                x.append(float(file_line.split(' ')[0]))
                y.append(float(file_line.split(' ')[1]) )
            line_counter = line_counter + 1
        file1.close()
    except IOError, (errno, strerror):
        print "data read  error"
        
    try:      
        file2 = open(gnome_file  , 'r')
        for file_line in file2.readlines():
            if len(file_line) >2:
                a.append(float(file_line.split()[0]))
                b.append(float(file_line.split()[1]) )                 
        file2.close() 
    except IOError, (errno, strerror):
        print "gnome read  error"             
    if len(x) <2: x= [1,1,1]
    if len(y) <2: y= [1,1,1]
    if len(a) <2: a= [1,1,1]
    if len(b) <2: b= [1,1,1]
    plt.figure(num=None, figsize=(6,2), dpi=80, facecolor='w', edgecolor='k')
    plt.subplot(121)
    plt.plot(x,y)
    try:
        coefs = np.lib.polyfit(x, y, 1) 
        fit_y = np.lib.polyval(coefs, x) #5
        plt.plot(x, fit_y, 'b--')
    except ValueError:
        print 'problem plotting coef' 
    title = "rg:%s i0:%s" % (rg, i0)
    plt.title(title)
    plt.subplot(122) 
    plt.plot(a,b) 
    title = "pr function"
    plt.title(title)       
    plt.savefig('%s_data.png' % sub_file[:-4])


if __name__ == '__main__':
    if len(sys.argv) > 1:
        sub_results = analyse_sub(sys.argv[1])
        print "Subfile:%s\trg:%s\ti0:%s\tdmax:%5.2f\tgraph:%s\tdata_graph:%s" %(sub_results[0], sub_results[1], sub_results[2], float(sub_results[6][0]) ,sub_results[4],  sub_results[7]  )
    else:
        print "you must supply a sub file!"


