#!/usr/bin/env python

import asyncore
import pyinotify
import os
import time
import subprocess

def quick_check(notifier):
            assert notifier._timeout is not None, 'Notifier must be constructed with a short timeout'
            notifier.process_events()
            while notifier.check_events():  #loop in case more events appear while we are processing
                  notifier.read_events()
                  notifier.process_events()
list_int = []
list_s = []
new_time = time.time()
new_s=0

wm = pyinotify.WatchManager()  
mask = pyinotify.IN_CREATE
data_fol = '../data'
folwatch = os.path.normpath(os.path.join(os.getcwd(),data_fol))
print(folwatch)
class EventHandler(pyinotify.ProcessEvent):
    def process_IN_CREATE(self,event):
        global new_s
        global new_time
        new_file = os.path.basename(event.name)
        if new_file.split('.')[-1] == 'tif':
            new_s = int(new_file.split('_')[-3][1:])
            new_time = os.path.getmtime(os.path.join(data_fol,new_file))
            if not(new_s in list_s):
                list_s.append(new_s)
                list_diff = list(set(list_s)-set(list_int))
                if len(list_diff) > 1:
                    subprocess.call(['/mnt/home/staff/bin/autosas.py',str(min(list_diff))])
                    list_int.append(min(list_diff))
            
                    
                
     
wdd = wm.add_watch(folwatch, mask)
qnotifier = pyinotify.Notifier(wm, EventHandler(), timeout=1000)

while 1:
    quick_check(qnotifier)
    list_diff = list(set(list_s)-set(list_int))
    print('Waiting for integration {}'.format(list_diff))
    if time.time()-new_time > 60 and len(list_diff) > 0:
        subprocess.call(['autosas.py',str(new_s)])
        list_int.append(new_s)

