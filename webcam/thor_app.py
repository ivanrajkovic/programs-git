#Import necessary libraries
import os
import numpy
from flask import Flask, render_template, Response, request, send_file
import cv2
from waitress import serve

from thorlabs_tsi_sdk.tl_camera import TLCameraSDK, OPERATION_MODE
from thorlabs_tsi_sdk.tl_mono_to_color_processor import MonoToColorProcessorSDK
from thorlabs_tsi_sdk.tl_camera_enums import SENSOR_TYPE


os.environ['PATH'] = 'C:\\Users\\rajkovic\\Documents\\programs-git\\thor_cam\\dlls' + os.pathsep + os.environ['PATH']
os.add_dll_directory('C:\\Users\\rajkovic\\Documents\\programs-git\\thor_cam\\dlls')

#Initialize the Flask app
app = Flask(__name__)


db_gain = 20.0
cam_exp = 100 # in ms

sdk =  TLCameraSDK()
available_cameras = sdk.discover_available_cameras()
if len(available_cameras) < 1:
    print("no cameras detected")

camera = sdk.open_camera(available_cameras[0])

camera.exposure_time_us = int(cam_exp*1000)  # set exposure in us
camera.frames_per_trigger_zero_for_unlimited = 0  # start camera in continuous mode
camera.image_poll_timeout_ms = 5500 # 2.5 second polling timeout

im_x_full = camera.sensor_width_pixels
im_y_full = camera.sensor_height_pixels

# set camera to full chip and no binning
camera.roi=(0,0,im_x_full-1,im_y_full-1)
camera.binx = 1
camera.biny = 1

im_x = camera.image_width_pixels
im_y = camera.image_height_pixels


gain_index = camera.convert_decibels_to_gain(db_gain)
camera.gain = gain_index
#print(f"Set camera gain to {camera.convert_gain_to_decibels(camera.gain)}")


mono_to_color_sdk = MonoToColorProcessorSDK()
mono_to_color_processor = mono_to_color_sdk.create_mono_to_color_processor(
    camera.camera_sensor_type,
    camera.color_filter_array_phase,
    camera.get_color_correction_matrix(),
    camera.get_default_white_balance_matrix(),
    camera.bit_depth
            )

# can't use 1 image to buffer, needs to be at least 2
camera.arm(2)

camera.issue_software_trigger()

c_up=0
c_bt=0
c_lf=0
c_rg=0

im_screen_w = 50

# just some values to start with
#im_x=1000
#im_y=1000

px_bin=2.0

def get_param(req):
    global im_x, im_y, c_up, c_bt, c_lf, c_rg, px_bin, im_screen_w, cam_exp, db_gain, camera
    if req.method == 'POST':
        #print(request.form)

        if req.form.get('reset'):
            c_up =0
            c_bt = 0
            c_lf = 0
            c_rg = 0
            px_bin = 2.0
            im_screen_w = 50
            cam_exp = 100
            db_gain=20
            camera.exposure_time_us = int(cam_exp*1000)
            camera.gain = camera.convert_decibels_to_gain(db_gain)


        else:
            c_up = int(req.form.get('crop_up'))
            c_bt = int(req.form.get('crop_bt'))
            c_rg = int(req.form.get('crop_rg'))
            c_lf = int(req.form.get('crop_lf'))
            if (c_up + c_bt + 4) > im_y:
                c_bt = im_y -c_up - 4
            if (c_lf + c_rg + 4) > im_x:
                c_rg = im_x -c_lf - 4
            px_bin = float(req.form.get('px_bin'))
            im_screen_w = int(req.form.get('im_w'))

            new_cam_exp = float(req.form.get('texp'))
            new_db_gain = float(req.form.get('gain'))
            if new_cam_exp != cam_exp:
                cam_exp = new_cam_exp
                camera.exposure_time_us = int(cam_exp*1000)
            if new_db_gain != db_gain:
                db_gain=new_db_gain
                camera.gain = camera.convert_decibels_to_gain(db_gain)
                #print(camera.convert_gain_to_decibels(camera.gain))



    return(c_up, c_bt, c_lf, c_rg, px_bin, im_screen_w, cam_exp, db_gain)


def get_dd():
    global im_x, im_y, c_up, c_bt, c_lf, c_rg, px_bin, im_screen_w, cam_exp, db_gain
    # data:
    data=[]

    data.append(c_up)                                                       # 0     crop up
    data.append(c_bt)                                                       # 1     crop bottom
    data.append(c_lf)                                                       # 2     crop left
    data.append(c_rg)                                                       # 3     crop roght
    data.append(px_bin)                                                     # 4     pixel binning
    data.append(int(im_x-c_rg-4))                                           # 5     max value for c_lf
    data.append(int(im_x-c_lf-4))                                           # 6     max value for c_rg
    data.append(int(im_y-c_bt-4))                                           # 7     max value for c_up
    data.append(int(im_y-c_up-4))                                           # 8     max value for c_bt
    data.append(im_screen_w)                                                # 9     image width, screen percentage
    data.append(im_x)                                                       # 10    image horizontal size
    data.append(im_y)                                                       # 11    image vertical size
    data.append(int(numpy.ceil((im_x - c_lf-c_rg)/px_bin)))                 # 12    image horizontal size, after cropping and scaling (use ceil to avoid size 0)
    data.append(int(numpy.ceil((im_y - c_up-c_bt)/px_bin)))                 # 13    image vertical size, after cropping and scaling
    data.append(int(numpy.ceil((im_screen_w*(im_x-c_lf-c_rg)/100)/px_bin))) # 14    image width on screen
    data.append(int(numpy.ceil((im_screen_w*(im_y-c_up-c_bt)/100)/px_bin))) # 15    image heigth on screen
    data.append(int(cam_exp))                                               # 16    camera exposure time (is ms)
    data.append(db_gain)                                                    # 17    camera gain (in dB)


    #data=[c_up,c_bt,c_lf,c_rg,px_bin,int(im_x-c_rg-4),int(im_x-c_lf-4),int(im_y-c_bt-4),int(im_y-c_up-4),im_screen_w,im_x,im_y,int(numpy.ceil((im_x - c_lf-c_rg)/px_bin)),int(numpy.ceil((im_y - c_up - c_bt)/px_bin)),int(numpy.ceil((im_screen_w*(im_x-c_lf-c_rg)/100)/px_bin)),int(numpy.ceil((im_screen_w*(im_y-c_up-c_bt)/100)/px_bin)),int(cam_exp),db_gain]

    return(data)




def gen_frames():
    global im_x, im_y, c_up, c_bt, c_lf, c_rg, px_bin, camera
    while True:
        ff = camera.get_pending_frame_or_null()
        if ff is not None:
            #print("frame #{} received!".format(frame.frame_count))
            #frame = numpy.copy(ff.image_buffer)
            frame = mono_to_color_processor.transform_to_24(ff.image_buffer, im_x, im_y).reshape(im_y, im_x, 3)
            #frame = frame.reshape(im_y, im_x, 3)



        else:
            print("timeout reached during polling, program exiting...")
            break



        # crop and resize (and reverse collor channels RGB ->BGR, not sure why, happened after camera firmware update)
        frame = frame[c_up:im_y-c_bt+1,c_lf:im_x-c_rg+1,::-1]



        im_x_ac = im_x - c_lf-c_rg
        im_y_ac = im_y - c_up - c_bt

        frame = cv2.resize(frame,(int(numpy.ceil(im_x_ac/px_bin)),int(numpy.ceil(im_y_ac/px_bin))))
        ret, buffer = cv2.imencode('.png', frame)
        frame = buffer.tobytes()
        yield (b'--frame\r\n' b'Content-Type: image/png\r\n\r\n' + frame + b'\r\n')  # concat frame one by one




@app.route('/index_live', methods=['GET', 'POST'])
def index():
    #global im_x, im_y, c_up, c_bt, c_lf, c_rg, px_bin, im_screen_w, cam_exp, db_gain, camera

    # redirect firefox to static feed
    #hh = request.headers.get('User-Agent')
    #if 'Firefox' in hh:
    #    return "<script>window.location.replace('/index_static')</script>", 200

    c_up, c_bt, c_lf, c_rg, px_bin, im_screen_w, cam_exp, db_gain= get_param(request)

    dd = get_dd()

    return render_template('index.html',data=dd)

@app.route('/', methods=['GET', 'POST'])
def index_static():
    #global im_x, im_y, c_up, c_bt, c_lf, c_rg, px_bin, im_screen_w, cam_exp, db_gain, camera
    if request.method == 'POST':
        #print(request.form)

        c_up, c_bt, c_lf, c_rg, px_bin, im_screen_w, cam_exp, db_gain = get_param(request)

        # take 2 images to clear out the buffer after changing gain or exposure time
        next(gen_frames())
        next(gen_frames())

    dd = get_dd()

    return render_template('index_static.html',data=dd)



@app.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/img')
def img():
    tt = next(gen_frames()).split(b'Content-Type: image/png\r\n\r\n')[-1]
    return Response(tt, mimetype='image/png')


if __name__ == "__main__":
    #app.run(debug=True)
    # get one image to get image size info
    next(gen_frames())
    serve(app,host='127.0.0.1', port=8080)
    mono_to_color_sdk.dispose()
    camera.dispose()
    sdk.dispose()