#Import necessary libraries
from flask import Flask, render_template, Response, request, send_file
import cv2
from waitress import serve


#Initialize the Flask app
app = Flask(__name__)

camera = cv2.VideoCapture(0)


c_up=0
c_bt=0
c_lf=0
c_rg=0

im_screen_w = 50

# just some values to start with
#im_x=1000
#im_y=1000

px_bin=1

def gen_frames():
    global im_x, im_y, c_up, c_bt, c_lf, c_rg, px_bin
    while True:
        success, frame = camera.read()  # read the camera frame
        if not success:
            break
        else:
            im_x = frame.shape[1]
            im_y = frame.shape[0]
            # crop and resize
            frame = frame[c_up:im_y-c_bt+1,c_lf:im_x-c_rg+1,:]

            im_x_ac = im_x - c_lf-c_rg
            im_y_ac = im_y - c_up - c_bt

            frame = cv2.resize(frame,(int(im_x_ac/px_bin),int(im_y_ac/px_bin)))
            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one


@app.route('/', methods=['GET', 'POST'])
def index():
    global im_x, im_y, c_up, c_bt, c_lf, c_rg, px_bin, im_screen_w
    if request.method == 'POST':
        #print(request.form)

        if request.form.get('reset'):
            c_up =0
            c_bt = 0
            c_lf = 0
            c_rg = 0
            px_bin = 1.0
            im_screen_w = 50
        else:
            c_up = int(request.form.get('crop_up'))
            c_bt = int(request.form.get('crop_bt'))
            c_rg = int(request.form.get('crop_rg'))
            c_lf = int(request.form.get('crop_lf'))
            px_bin = float(request.form.get('px_bin'))
            im_screen_w = int(request.form.get('im_w'))

    return render_template('index.html',data=[c_up,c_bt,c_lf,c_rg,px_bin,int(im_x-c_rg-4),int(im_x-c_lf-4),int(im_y-c_bt-4),int(im_y-c_up-4),im_screen_w,im_x,im_y,int((im_x - c_lf-c_rg)/px_bin),int((im_y - c_up - c_bt)/px_bin),int((im_screen_w*(im_x-c_lf-c_rg)/100)/px_bin)])

@app.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/img')
def img():
    tt = next(gen_frames()).split(b'Content-Type: image/jpeg\r\n\r\n')[-1]
    return Response(tt, mimetype='image/jpg')


if __name__ == "__main__":
    #app.run(debug=True)
    # get one image to get image size info
    next(gen_frames())
    serve(app,host='127.0.0.1', port=8080)