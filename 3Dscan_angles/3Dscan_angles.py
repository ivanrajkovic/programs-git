#!/usr/bin/env python2
# -*- coding: utf-8 -*-
'''
@author: rajkovic
'''


from __future__ import division
import argparse
import numpy


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('ang_start', nargs='?', type=int, help='starting angle number, default=0',default=0)
    #parser.add_argument('ang_stop', nargs='?', type=float ,help='maximum angle, default=180',default=180)
    parser.add_argument('ang_num', nargs='?', type=int ,help='number of angles, default=180',default=180)
    #parser.add_argument('-ra','--ratio-angle', type=float , help='ratio between angles, default = ((1+sqrt(5))/2)',default=((1+numpy.sqrt(5))/2))
    parser.add_argument('-of','--out-file', help='output file, default = angles.txt', default='angles.txt')

    arg_in = parser.parse_args()

    gold = (1+5**.5)/2
    #with open(arg_in.out_file,'w') as of:
    for ii in range(arg_in.ang_start,arg_in.ang_start+arg_in.ang_num):
        next_ang = numpy.mod(ii*gold,1)*180
        print('{} {:.2f}'.format(ii,next_ang))
        #of.write("{:.2f}\n".format(next_ang))


