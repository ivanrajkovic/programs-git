import matplotlib
import matplotlib.pyplot as plt
import numpy

def roipoly():
    XY = plt.ginput(0,0)

    nrows = sum( abs( numpy.r_[ plt.gcf().get_axes()[0].get_xbound() ] ) )
    ncols = sum( abs( numpy.r_[ plt.gcf().get_axes()[0].get_ybound() ] ) )
    nrows,ncols = int(nrows), int(ncols)

    path = matplotlib.path.Path( XY )
    mask = numpy.array([ path.contains_point((j,i)) for i in range(nrows) for j in range(ncols)  ]).reshape(ncols,nrows)

    return XY, mask

