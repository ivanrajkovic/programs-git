#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division

import os
import glob
import numpy
import sys

import matplotlib.pyplot as plt


#intfiles = sorted(glob.glob('*.int'), key=os.path.getmtime)
intfiles = sorted(glob.glob(sys.argv[1]+'*.int'))

#print('\n'.join(intfiles))

intmat = []

for ii,ff in enumerate(intfiles):
    newint = numpy.loadtxt(ff)
    i2 = newint[int(len(newint)/2):]
    if ii&1:
        intmat.append(i2[::-1])
    else:
        intmat.append(i2)

intmat.reverse()

plt.figure()
plt.pcolormesh(intmat)# ,cmap = plt.get_cmap('PiYG'))
plt.colorbar()
plt.xlabel('images [hor]')
plt.ylabel('scans [ver]')
plt.ylim((0,len(intmat)))
plt.xlim((0,len(max(intmat,key=len))))
plt.show()


