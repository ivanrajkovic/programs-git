#!/mnt/home/rajkovic/miniconda/envs/py3/bin/python
### #!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division

import os
import glob
import numpy
import sys
import argparse
import re
from mayavi import mlab

os.environ['MESA_GL_VERSION_OVERRIDE']='3.2'

def find_sd(dfol):
    first_list = glob.glob(dfol+'/*/')
    llist= []
    rrr = re.compile('.*\/\d{1,3}p\d\d\/$')
    for dd in first_list:
        if rrr.match(dd):
            llist.append(dd)
    return(llist)

def input_sd(dfol,in_ang):
    try:
        start_ang, step_ang, end_ang = (float(x) for x in in_ang.split(":"))
    except:
        sys.exit('wrong angles input')

    ilist = []

    for iii in numpy.arange(start_ang,end_ang+step_ang,step_ang):
        if iii <=end_ang:
            ilist.append(os.path.join(dfol,'{:.2f}'.format(iii).replace('.','p')))
    return(ilist)

def find_int_num(ffol):
    imlist = glob.glob(ffol+'/*5.cbf')
    slist = []
    for im in imlist:
        snum = int(im.split('_')[-3][1:])
        if not (snum in slist):
            slist.append(snum)
    return(len(slist))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-d','--directory', default='./' , help='data directory')
    parser.add_argument('-a','--angles', default='all' , help='angles  \'start:step:end\' or \'all\' (default: all)')
    parser.add_argument('-w','--wrap', default=-10 , type=int, help='number of pixels to wrap around (default: -10)')
    parser.add_argument('-n','--int_num', default=0 , type=int, help='number of int files, (defualt: 0 - calculate from 1st scan)')


    arg_in = parser.parse_args()

    #print(arg_in)

    #fold = '/mnt/home/b_georgiadis/20201120_ADtomo_35035_35036/tomo'
    #fold='/mnt/home/b_georgiadis/20201211_34879_35260/tomo'
    # get a list of data subfolders
    if arg_in.angles == 'all':
        sd_list = find_sd(arg_in.directory)
    else:
        sd_list = input_sd(arg_in.directory,arg_in.angles)

    if len(sd_list) == 0:
        sys.exit('no folders!')

    # empty array for data
    int_3d = numpy.array([])

    # get number of int files (scans)
    if arg_in.int_num == 0:
        int_num = find_int_num(os.path.join(arg_in.directory,sd_list[0]))
    else:
        int_num = arg_in.int_num

    # get int files data
    for sd in sd_list:
        print('{}'.format(sd.split('/')[-1]))
        intfiles = sorted(glob.glob(sd+'/*.int'))
        intmat = []
        first_snum = int(intfiles[0].split('_')[-3][1:])
        prev_snum = first_snum
        newint = numpy.loadtxt(intfiles[00])
        i2 = newint[int(len(newint)/2):]
        scan_len = len(i2)
        intmat.append(i2)

        for ff in intfiles[1:]:
            while int(ff.split('_')[-3][1:])-prev_snum > 1:
                print('{} is missing an int file!'.format(sd.split('/')[-1]))
                i2 = [0] * scan_len
                intmat.append(i2)
                #print(numpy.array(intmat).shape)
                #print('added line')
                prev_snum=prev_snum+1

            newint = numpy.loadtxt(ff)
            prev_snum = int(ff.split('_')[-3][1:])
            i2 = newint[int(len(newint)/2):]

            if (prev_snum-first_snum)&1:
                intmat.append(numpy.roll(i2[::-1],arg_in.wrap))
            else:
                intmat.append(i2)


        if (int(ff.split('_')[-3][1:]) - first_snum +1 ) != int_num:
           for gg in numpy.arange(int_num - (int(ff.split('_')[-3][1:]) - first_snum +1)):
                print('{} is missing a start/end int file!'.format(sd.split('/')[-1]))
                intmat.append([0] * scan_len)


        if sd == sd_list[0]:
            #print(numpy.array(intmat).shape)
            int_3d = numpy.array(intmat)

        else:
            #print(numpy.array(intmat).shape)
            int_3d = numpy.dstack((int_3d,numpy.array(intmat)))





    mlab.contour3d(int_3d,contours=125)
    mlab.show()



