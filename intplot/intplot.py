#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division

import os
import glob
import numpy
import sys

import matplotlib.pyplot as plt
rolldata = 0

if len(sys.argv)>1:
    rolldata= int(sys.argv[1])

#intfiles = sorted(glob.glob('*.int'), key=os.path.getmtime)
intfiles = sorted(glob.glob('*.int'))

#print('\n'.join(intfiles))

intmat = []

for ii,ff in enumerate(intfiles):
    newint = numpy.loadtxt(ff)
    i2 = newint[int(len(newint)/2):]
    if ii&1:
        intmat.append(numpy.roll(i2[::-1],rolldata))
    else:
        intmat.append(i2)

#intmat.reverse()

plt.figure()
plt.pcolormesh(intmat)# ,cmap = plt.get_cmap('PiYG'))
plt.colorbar()
plt.xlabel('images [hor]')
plt.ylabel('scans [ver]')
plt.ylim((len(intmat)),0)
plt.xlim((0,len(max(intmat,key=len))))
plt.show()


