import numpy as np
from scipy.interpolate import pchip
import matplotlib.pyplot as plt

zz=np.array([
[10.0,15,21,12,10],
[15,27,33,26,11],
[18,38,44,38,24],
[20,41,47,42,25],
[21,42,48,43,26],
[21,42,48,43,26],
[19,38,45,39,25],
[15,28,33,32,17],
[10,15,23,21,15]])

#zz=np.flipud(zz)

xx=[-7,-6,-4,-2,0,2,4,6,7]

yy=[-3,-1.5,0,1.5,3]

xxi=np.arange(-7,7,.2)
yyi=np.arange(-3,3,.2)

zz2 = np.empty([len(xxi), len(yy)], dtype=zz.dtype)
for k in range(len(yy)):
    zz2[:,k] = np.squeeze(pchip(xx, zz[:,k])(xxi))
    
qq = np.empty([len(xxi), len(yyi)], dtype=zz.dtype)
for k in range(len(xxi)):
    qq[k,:] = np.squeeze(pchip(yy, zz2[k,:])(yyi))



plt.imshow(qq,interpolation=None,extent=(yyi.min(),yyi.max(),xxi.min(),xxi.max()))
