#!/bin/bash
input="$1"
URL="http://134.79.35.121/jpg/4/image.jpg"

if [ -z "$input" ]; then
  echo "USAGE: $0 [interval in minutes]" 
  exit 1
else
  interval=`bc -l <<<"(60*$input)-0.2"`;
fi

while [ 0 = 0 ]; do
  timestamp=$(date +%Y_%m_%d_%H_%M_%S)
  day=$(date +%m_%d) 
  mkdir -p ${day}
  wget "$URL" -O "${day}/image_${timestamp}.jpg"
  echo "Sleeping for $input minute(s)"
  sleep "$interval"
done;
