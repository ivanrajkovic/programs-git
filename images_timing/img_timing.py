#!/usr/bin/env python

'''
@author: rajkovic
'''


from __future__ import division

import os
import glob
import numpy
import sys
import matplotlib.pyplot as plt
import datetime


savefol = '/mnt/home/rajkovic/bitbucket/programs-git/images_timing/'

#fol='/mnt/home/staff/Commissioning2019/20191114_SEC_BSA_test/data'

#fol = '/mnt/home/staff/Commissioning2019/20191111_timing_test/data_overnight'
fol = '/mnt/home/staff/UserSetup/20191115_Bu/data'

os.chdir(fol)

ser_num='33'

#ser_num = sys.argv[2]

#, key=os.path.getmtime
imgfiles = sorted(glob.glob('*{}_0_*.tif'.format(str.zfill(ser_num,3))))
prpfiles = sorted(glob.glob('*{}_0_*.prp'.format(str.zfill(ser_num,3))))

delta_t = numpy.empty(len(imgfiles)-1)
delta_t_prp = numpy.empty(len(imgfiles)-1)

#prev_t = os.path.getmtime(imgfiles[0])
prev_t = os.stat(imgfiles[0]).st_mtime

for ii,ff in enumerate(imgfiles[1:]):
    #current_t = os.path.getmtime(ff)
    current_t = os.stat(ff).st_mtime
    delta_t[ii]=current_t - prev_t
    prev_t = current_t

prev_t_prp = os.stat(prpfiles[0]).st_mtime

for ii,ff in enumerate(prpfiles[1:]):
    current_t_prp = os.stat(ff).st_mtime
    delta_t_prp[ii]=current_t_prp - prev_t_prp
    prev_t_prp = current_t_prp

im_pos=numpy.where(delta_t>delta_t.mean()+0.4)
#im_pos=numpy.where(delta_t>delta_t.max()-0.5)


fig,ax = plt.subplots(nrows=3,ncols=2,figsize=[12,12])
fig.text(0.5,0.95, '{}/{}'.format('/'.join(fol.split('/')[-3:-1]),imgfiles[0]) ,ha='center',size=22)


ax = plt.subplot(3,2,1)
plt.plot(delta_t,'o')
plt.title('tif files time diff')


ax = plt.subplot(3,2,2)
plt.hist(delta_t,bins=50)
plt.title('tif files time diff (hist)')


ax = plt.subplot(3,2,3)
plt.plot(delta_t_prp,'o')
plt.title('prp files time diff')

ax = plt.subplot(3,2,4)
plt.hist(delta_t_prp,bins=50)
plt.title('prp files time diff (hist)')


ax = plt.subplot(3,2,5)
plt.plot(delta_t[200:250],'b-')
plt.plot(delta_t_prp[200:250],'r-')
plt.title('tif (b) and prp (r) time diff (img 200-250)')


ax = plt.subplot(3,2,6)
plt.plot(numpy.ediff1d(im_pos))
plt.title('spacing between longer tif files intervals')
#plt.show()
plt.savefig(os.path.join(savefol,os.path.splitext(imgfiles[0])[0]+'.png'))