#!/usr/bin/env python

import requests
import re
from os.path import expanduser
import webbrowser
import datetime
import calendar
import platform
import os

homefol = expanduser("~")

file_save = os.path.join(homefol,'schedule.html')

url = "http://www-ssrl.slac.stanford.edu/xray_schedule.html"

r = requests.get(url)

a = re.split('</table>',r.text,flags=re.IGNORECASE)

new_schedule = '<HTML>\n<HEAD>\n<TITLE> SSRL BL4-2 SCHEDULE </TITLE>\n</HEAD>\n<HTML>'
new_schedule=new_schedule+'</p><h2><b><a name="TOC"></a></b>\n</h2><p></p><p></p><p></p>\n<center><table cols="4" cellspacing="5" border="2" bgcolor="FFFFFFF"><caption><b><font="+1">Weeks of the Schedule'+r.text.split('Weeks of the Schedule')[1].split('/table')[0].replace("&nbsp;","")+'/table><br><br>'
start = '<A NAME'
start_html5 = '<A id'
end = '</A>'


for l in range(len(a)):
    try:
        result=re.search('%s(.*)%s' % (start, end), a[l]).group(1).replace("&nbsp;","")
        new_schedule=new_schedule+'\n'+start_html5+result+end
    except:
        pass
    match = re.search('4-2',a[l])
    if match:
        new_schedule = new_schedule + a[l] + '</TABLE>'
        new_schedule=new_schedule+'<br>\n<a href="#">Back to top</a>'

new_schedule = new_schedule+'</TABLE>\n</HTML>'

with open(file_save, 'w') as f:
    f.write(new_schedule)

# time delta from Monday
dt = datetime.timedelta(datetime.datetime.today().weekday())
# find date for Monday
dd = (datetime.date.today()-dt).day
mm = calendar.month_abbr[(datetime.date.today()-dt).month]
yy = (datetime.date.today()-dt).year
date_tag = '#'+str(mm)+'.'+str(dd).zfill(2)+','+str(yy)


if platform.system() == 'Windows':
    try:
        webbrowser.get('windows-default').open_new_tab('file:'+file_save+date_tag)
    except:
        webbrowser.open_new_tab(file_save+date_tag)
            
else:
    pid=os.fork()
    if pid:
        pass
    else:
        webbrowser.get('firefox %s').open_new_tab('file:'+file_save+date_tag)

