#!/usr/bin/env python

import subprocess
from bs4 import BeautifulSoup
import requests
import re
import numpy as np
from os.path import expanduser

homefol = expanduser("~")
file_save = homefol+'/schedule_4-2.txt'


url = "http://www-ssrl.slac.stanford.edu/xray_schedule.html"
r = requests.get(url)
a = r.text.split('</TABLE>')

days = ['Mo','Tu','We','Th','Fr','Sa','Su']

final_list= [['Date    ','        1st shift','        2nd shift','        3rd shift'],['-----------------','-----------------','-----------------','-----------------']]


for l in range(len(a)):
    match = re.search('4-2',a[l])
    if match:
        soup = BeautifulSoup(a[l], 'html.parser')
        all_text = ''.join(soup.findAll(text=True))
        ee = all_text.replace(u'\xa0',' ').splitlines()
        ee = list(filter(None, ee))
        for k in range(1,int((len(ee)-1)/4)+1):
            final_list.append([' '.join(ee[k].split())+', '+days[k-1],ee[k+7],ee[k+14],ee[k+21]])
        final_list.append(['','','',''])
np.savetxt(file_save, final_list, delimiter="\t", fmt='%s')


subprocess.call(["gedit",file_save])







