#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division
import numpy

import glob
import sys
import os





first_prp = sys.argv[1]

all_prp = glob.glob("_".join(first_prp.split("_")[:-1])+"*.prp")

ll = len(all_prp)
print('\nfound {} prp files'.format(ll))
int_data = numpy.zeros(int(2*ll))

for cc in range(ll):
    ii = 0
    with open(all_prp[cc]) as rr:
        for line in rr:
            if "I_2" in line:
                int_data[ll+cc] = float(line.split('=')[1].split()[0])


print('\nfound {} intensities\n'.format(len(int_data)))

int_file ="_".join(first_prp.split("_")[:-1])+"s1.int"

if os.path.exists(int_file):
    int_file = int_file+".created"

print('saving to {}\n\n'.format(int_file))

numpy.savetxt(int_file,int_data,fmt='%.2f')