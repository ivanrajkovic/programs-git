#!/usr/bin/python python
import os
import sys
import pyinotify
import time
import shutil
import glob

def name_parts(img_name):
    all_parts = img_name.split('_')
    
    ext_str = all_parts[-1].split('.')[1]
    num_str = all_parts[-1].split('.')[0]
    xx_str = all_parts[-2]
    ser_str = all_parts[-3]
    pos_str = all_parts[-4]
    nam_str = '_'.join(all_parts[0:-4])
 
    return [nam_str, pos_str, ser_str, xx_str, num_str, ext_str]


if len(sys.argv) != 3:
	sys.exit("\nUsage: "+ sys.argv[0] + " folder_to_watch folder_to_copy_from\n")

folwatch = sys.argv[1]
folcopy = sys.argv[2]


wm = pyinotify.WatchManager()  # Watch Manager
mask = pyinotify.IN_CREATE 

class EventHandler(pyinotify.ProcessEvent):
    def process_IN_CREATE(self, event):
	nnn = os.path.basename(event.pathname)
	[nam_str, pos_str, ser_str, xx, num_str, ext_str] = name_parts(nnn)
	print "Creating:", nnn , "\n"
	time.sleep(1)
	file_to_copy = glob.glob(folcopy+'/*'+pos_str+'*'+num_str+'.'+ext_str)
	print "Copying:", file_to_copy[0],"\n" ,"to:", folwatch+"/"+nnn , "\n"
	shutil.copy2(file_to_copy[0],event.pathname)
	
	
handler = EventHandler()
notifier = pyinotify.Notifier(wm, handler)
wdd = wm.add_watch(folwatch, mask, rec=True)

notifier.loop()
