import matplotlib.pyplot as plt
from PIL import Image, ImageDraw, ImageMath
import os
import numpy
#from skimage import draw



os.chdir('c:\\Users\\rajkovic\\Documents\\programs\\makemask\\')
img = Image.open('test.png')

(hh,ww) = img.size
coords = []

def onclick(event):
    global ix, iy
    ix, iy = event.xdata, event.ydata
    print(ix, iy)
    global coords
    if len(coords) > 0:
        plt.plot([int(ix),numpy.asarray(coords[-1][0])],[int(iy),int(numpy.asarray(coords[-1][1]))],color='yellow')
    plt.imshow(img)
    coords.append((ix, iy))





fig = plt.figure()
ax = fig.add_subplot(111)
plt.imshow(img)
cid = fig.canvas.mpl_connect('button_press_event', onclick)









mimg = Image.new('L', (hh, ww), 0)
ImageDraw.Draw(mimg).polygon(coords, outline=1, fill=1)
mimg=mimg.convert("I;16")
mimg.save('mimg_16bit.tif')

#mimg.save('test_mask_3.tif')

plt.imshow(Image.blend(img.convert("L"),mimg.convert("L"),.95))

plt.imshow(ImageMath.eval("a-120*b", a= img.convert("L"), b=mimg.convert("L")))