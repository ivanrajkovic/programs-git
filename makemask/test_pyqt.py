import os
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
from PIL import Image


os.chdir('c:\\Users\\rajkovic\\Documents\\programs\\makemask\\')

img = Image.open('test.png').convert("L")
(hh,ww) = img.size
img_data = np.transpose(np.array(img))

## create GUI
app = QtGui.QApplication([])
w = pg.GraphicsWindow(size=(hh,ww), border=True)
w2 = w.addLayout(row=0, col=0)
v2a = w2.addViewBox(row=0, col=0, lockAspect=False)

img1a = pg.ImageItem(img_data)
v2a.addItem(img1a)

roisel = pg.PolyLineROI([[100,100], [110,110], [110,130], [130,110]], closed=True,  pen=(2,5))
v2a.addItem(roisel)


roisel.getSceneHandlePositions(0)[1].x()
roisel.getSceneHandlePositions(0)[1].y()


import pyqtgraph.examples
pyqtgraph.examples.run()