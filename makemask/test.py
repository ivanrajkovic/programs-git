import matplotlib.pyplot as plt
from PIL import Image
import os
import numpy

os.chdir('c:\\Users\\rajkovic\\Documents\\programs\\makemask\\')
img = Image.open('test.png')

coords = []
fig = plt.figure()
ax = fig.add_subplot(111)
plt.imshow(img)
cid = fig.canvas.mpl_connect('button_press_event', onclick)




def onclick(event):
    global ix, iy
    ix, iy = event.xdata, event.ydata
    print(ix, iy)
    global coords
    img.putpixel((int(ix),int(iy)),(0,0,0))
    if len(coords) > 0:
        plt.plot([int(ix),int(iy)], numpy.asarray(coords[-1]))
    plt.imshow(img)
    coords.append((ix, iy))




# for i in range(0,1):

#    cid = fig.canvas.mpl_connect('button_press_event', onclick)