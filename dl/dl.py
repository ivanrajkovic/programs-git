from scipy import constants
from scipy.constants import codata
from pint import UnitRegistry


ureg = UnitRegistry()

ee_0 = codata.value('electric constant') * ureg.parse_expression(codata.unit('electric constant'))
ff = codata.value('Faraday constant') * ureg.parse_expression(codata.unit('Faraday constant'))
rr = codata.value('molar gas constant') * ureg.parse_expression(codata.unit('molar gas constant'))


cons_dl = pow(ee_0*rr/2,0.5) / ff

tt = 273 * ureg.K

dens = 1.0373 * ureg.g / ureg.ml

ii = 1.5 * ureg.mole / ureg.kg

rd = cons_dl * pow (65.4035*tt/(dens * ii ),.5)

rd.ito(ureg.nm)
print(rd)