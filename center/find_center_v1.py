#!/usr/bin/env python
from __future__ import division

import os
import sys
import time
import glob
import fileinput
import shutil
import subprocess
import re
import PIL
import numpy
from scipy import signal
from scipy.optimize import curve_fit, leastsq
from matplotlib import pyplot as plt
from matplotlib.image import imread


# gaussian fit function (1d)
def gauss(x, *p):
    A, mu, sigma = p
    return A*numpy.exp(-(x-mu)**2/(2.*sigma**2))
    
# fit 1d gaussian
def fit1dgauss(data,cen,dx):
    cen = int(cen)
    dx = int(dx)
    pp = [data[(cen-dx):(cen+dx),1].max()-data[(cen-dx):(cen+dx),1].min(),cen,40/det_bin]
    coeff, var_matrix = curve_fit(gauss,data[(cen-dx):(cen+dx),0],data[(cen-dx):(cen+dx),1],p0=pp)
    return coeff, var_matrix

# finding moments for 2d gaussian fitting
def moments(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution by calculating its
    moments """
    total = data.sum()
    X, Y = numpy.indices(data.shape)
    x = (X*data).sum()/total
    y = (Y*data).sum()/total
    col = data[:, int(y)]
    width_x = numpy.sqrt(abs((numpy.arange(col.size)-y)**2*col).sum()/col.sum())
    row = data[int(x), :]
    width_y = numpy.sqrt(abs((numpy.arange(row.size)-x)**2*row).sum()/row.sum())
    A = data.max()
    return A, x, y, width_x, width_y

# 2d gaussian function
def gauss2d(A, center_x, center_y, width_x, width_y):
    return lambda x,y: A*numpy.exp(-(((center_x-x)/width_x)**2+((center_y-y)/width_y)**2)/2)

# 2d gaussian fitting
def fit2dgauss(data):
    """Returns (A, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    params = moments(data)
    errorfunction = lambda p: numpy.ravel(gauss2d(*p)(*numpy.indices(data.shape)) -
                                 data)
    p, success = leastsq(errorfunction, params)
    return p

# Returns sx x sy array od zeros with a circle at rx,ry with radius rr and intensity int
def circ(sx,sy,rx,ry,rr,int):
    ccxx, ccyy = numpy.mgrid[:sx, :sy]
    ccircle = (ccxx - rx) ** 2 + (ccyy - ry) ** 2
    cc=(ccircle<rr**2)*int
    return cc

# Returns centar from 2d data by finding maximum of the autocorrelation
def cen_corr_max(data2d):
    data_corr = signal.fftconvolve(data2d,data2d)
    cx_crmax, cy_crmax = numpy.unravel_index(numpy.argmax(data_corr), data_corr.shape)
    bx_crmax = (1.0*cy_crmax+1)/2
    by_crmax = agbe_size-(1.0*cx_crmax+1)/2
    return bx_crmax, by_crmax

# Returns centar from 2d data by fitting 2g gassian to the autocorrelation data
def cen_corr_gauss(data2d,dx,dy):
    data_corr = signal.fftconvolve(data2d,data2d)
    cx_crmax, cy_crmax = numpy.unravel_index(numpy.argmax(data_corr), data_corr.shape)
    cr_gf = fit2dgauss(data_corr[cx_crmax-dx:cx_crmax+dx,cy_crmax-dy:cy_crmax+dy])
    cx_cg = cx_crmax-dx+cr_gf[1]
    cy_cg = cy_crmax-dy+cr_gf[2]
    bx_cg = (1.0*cy_cg+1)/2
    by_cg = agbe_size-(1.0*cx_cg+1)/2
    return bx_cg, by_cg
    
def integmpp_a_param_change(mpp,xx,yy,st_ang,an_rg,pp,dd):
        x1 = pp*numpy.cos(numpy.radians(st_ang+dd*90+an_rg))      
        x2 = pp*numpy.cos(numpy.radians(st_ang+dd*90-an_rg))
        y1 = pp*numpy.sin(numpy.radians(st_ang+dd*90+an_rg))
        y2 = pp*numpy.sin(numpy.radians(st_ang+dd*90-an_rg))
        for line in fileinput.input(mpp, inplace = 1):
            line = re.sub('-a no points -?\d+\.?\d* -?\d+\.?\d* -?\d+\.?\d* -?\d+\.?\d*','-a no points '+str(xx+x1)+' '+str(yy+y1)+' '+str(xx+x2)+' '+str(yy+y2) ,line)
            line = re.sub('^-c \d+(\.\d+)? \d+(\.\d+)?','-c '+str(xx)+' '+str(yy),line)
            sys.stdout.write(line)

def plot_center(data2d,cenx,ceny,ring):
    size=int(data2d.shape[0])
    beam_target = numpy.zeros([size,size])
    beam_target_neg = numpy.zeros([size,size])
    

    target_rd = [1,0.75,0.5,0.25,0.1]

    for tt in range(len(target_rd)):
        beam_target=beam_target + ((-1)**tt) * circ(size,size,size-cenx,ceny,target_rd[tt]*size/128,1)

    for tt in range(1,len(target_rd)):
        beam_target_neg=beam_target_neg - ((-1)**tt) * circ(size,size,size-cenx,ceny,target_rd[tt]*size/128,agbe_2d.max())
    ring_circ = circ(size,size,size-cenx,ceny,ring+16/det_bin,1)-circ(size,size,size-cenx,ceny,ring-16/det_bin,1)
    plt.figure('beam center')
    plt.imshow(data2d*(1-beam_target)*(1-ring_circ)+beam_target_neg,clim=(0,data2d.max()/40))
        
        
## Program start here

# Set some variables
data_folder = os.path.join('..', 'data')
integcenmpp = 'integ_cen.mpp'
integmpp = 'integ.mpp'
cen_fold = 'cen'

# Set variables that might be given on the command line    
interactive = 0   
agbe_sn = 0
agbe_tif = ''

try:
    expname = '_'.join(os.getcwd().split(os.sep)[-2].split('_')[1:])
except:
    expname = ''

# parse command line arguments, if any
if len(sys.argv) > 1:
    for i in range(1,len(sys.argv)):
        
        if (sys.argv[i][0] == 'i' or sys.argv[i][0] == 'I') and len(sys.argv[i]) == 1:
            interactive = 1
        
        try:
            agbe_sn = int(sys.argv[i])
        except:
            pass
        
        if sys.argv[i][-3:] == 'tif':
            agbe_tif = sys.argv[i]

# find the newest agbe file if a series number nor a file name is not specified on the command line
if agbe_sn == 0 and len(agbe_tif) == 0:
    agbe_tif = max(glob.iglob(os.path.join(data_folder,'[Aa][Gg][Bb][Ee]*01.tif')), key=os.path.getctime)
# if a file name is given, use that
elif len(agbe_tif) > 0:
    pass
# or find the file number from a series number
else:
    agbe_tif=glob.glob(os.path.join(data_folder,'*'+str(agbe_sn).zfill(3)+'_0_01.tif'))[0]

# sastool datfile name
agbe_datfile = os.path.basename(agbe_tif)[:-3]+'dat'

# Open agbe file    
agbe_2d = imread(agbe_tif)

# Get info from the file
agbe_size = int(agbe_2d.shape[0])
det_bin = int(6144/agbe_size)

# Get info from prp file

with open(agbe_tif[:-4]+'.prp') as agbe_prp:
    for line in agbe_prp.readlines():
        if 'Beam energy' in line:
            e_kev=float(line.split('=')[1].split(' ')[0])/1000
        if 'Pipe length' in line:
            det_dist=float(line.split("=")[1].split(' ')[0])

# Calculate approximate position of first agbe ring
if interactive == 1:
    pass
else:
    agbe_ring_est = ((det_dist+180.)/1000)*numpy.tan(2*numpy.arcsin((12.3984/e_kev)*(2*numpy.pi/58.38)/(4*numpy.pi)))/(.225/6144*det_bin)

# if if ring position < 100px, use a different ring
rrn = int(numpy.ceil(100/agbe_ring_est))


# calulate autocorrelation and find the maximum position

cx_crmax, cy_crmax = cen_corr_max(agbe_2d)


# put zeros in the center if the image to lower the background in autocorrelation
# and find the center again
agbe_2d_circ = agbe_2d*(1-circ(agbe_size,agbe_size,agbe_size-cy_crmax,cx_crmax,agbe_ring_est/2,1))

# fit 2d gaussian
if interactive == 1:
    pass   # TODO
else:
    cr_dx = int(80/det_bin)
    cr_dy = int(80/det_bin)
    
cx_cg, cy_cg = cen_corr_gauss(agbe_2d_circ,cr_dx,cr_dy)

if numpy.sqrt((cx_crmax-cx_cg)**2+(cy_crmax-cy_cg)**2)>10:
    cx_cg = cx_crmax
    cy_cg = cy_crmax

# create a new integcenmpp file

ff = open(integcenmpp,'w')
ff.write('-l '+time.strftime("%Y%m%d")+'_cen.log\n')
ff.write('-c {0:.3f} {1:.3f}\n'.format(cx_cg,cy_cg))
ff.write('-i yes i2 1000\n')
#ff.write('-r yes 1.5\n')
ff.write('-z yes 3.5\n')
ff.write('-a no points 100 100 100 101\n')
ff.write('-f '+agbe_tif+'\n')
ff.close()

if interactive == 1:
    #TODO
    pass
else:
    # set some variables for angular integration
    start_ang = 0
    ang_rg = 10
    
    a_px = 50
    
    # number of pixel around the estimated peak where to search for a peak
    # half way from center to peak or (160/detector binning), whichever smaller
    px_peak_search_rg = int(min(160/det_bin,rrn*agbe_ring_est/2))
    
    # number of pixels around the peak to fit a gauss
    px_num_g = int(px_peak_search_rg/2)
    #px_num_g = int(min(80/det_bin,px_peak_search_rg))
    
    
    # How many times to perform refinement
    repeats = 3

# TODO
# check if the ring is visible in all directions
# ring_vis = 4 - all directions; 3 - in three directions; 0-2 : two or less directions
ring_vis = 4

if ring_vis < 3:
    print('Approximate center position: x={0:.3f}, y={1:.3f}'.format(cy_cg,cx_cg))
    print('Can\'t refine center position, ring is not on the detecor in at least 3 directions')
    cx_final = cy_cg
    cy_final = cx_cg
elif ring_vis == 3:
    #TODO
    print('Approximate center position: x={0:.3f}, y={1:.3f}'.format(cy_cg,cx_cg))
    print('ring is on the detecor in 3 directions, refinment of the center position is not possible yet')
    cx_final = cy_cg
    cy_final = cx_cg
else:
    agbe_ring_dir = numpy.zeros([repeats,4])
    ref_cen = numpy.zeros([repeats+1,2])
    ref_cen[0,:] = [cx_cg,cy_cg]
    if not os.path.exists(cen_fold):
        os.makedirs(cen_fold)
    plt.figure('agbe peak 4d')
    for rr in range(1,repeats+1):
        for i in range(0,4):
            integmpp_a_param_change(integcenmpp,ref_cen[rr-1,0],ref_cen[rr-1,1],start_ang,ang_rg,a_px,i)
            subprocess.call(['sastool',integcenmpp])
            agbe_1d = numpy.genfromtxt(agbe_datfile)
            shutil.move(agbe_datfile,os.path.join(cen_fold,agbe_datfile[:-4]+'_r'+str(rr)+'_d'+str(i)+'.dat'))
            agbe_max_px = agbe_1d[int(rrn*agbe_ring_est-px_peak_search_rg):int(rrn*agbe_ring_est+px_peak_search_rg),1].argmax() + rrn*agbe_ring_est-px_peak_search_rg
            coeff, var_matrix = fit1dgauss(agbe_1d,agbe_max_px,px_num_g)
            agbe_ring_dir[rr-1,i] = coeff[1]
            if rr == repeats:
                agbe_1d[:,1] = numpy.maximum(agbe_1d[:,1],.1)
                plt.plot(agbe_1d[:,0],numpy.log(agbe_1d[:,1]))
        
        ref_cen[rr,0] = ref_cen[rr-1,0] + agbe_ring_dir[rr-1,0]/2 - agbe_ring_dir[rr-1,2]/2
        ref_cen[rr,1] = ref_cen[rr-1,1] + agbe_ring_dir[rr-1,1]/2 - agbe_ring_dir[rr-1,3]/2
        
    
    shutil.move(integcenmpp,os.path.join(cen_fold,integcenmpp))
 
    cx_final = ref_cen[-1,0]
    cy_final = ref_cen[-1,1]
    
    
# Check if the mask file exists
mask_msk = ''
mask_tif = ''
try:
    mask_tif = max(glob.iglob('mask*.tif'), key=os.path.getctime)
except:
    pass

if mask_tif == '':
    try:
        mask_msk = max(glob.iglob('*msk'), key=os.path.getctime)
    except:
        pass

if mask_msk != '':
    subprocess.call(['msk2tif.py',mask_msk])
    mask_tif = mask_msk[:-3]+'tif'

# Prepare integ.mpp
if os.path.exists(integmpp):
    # change existing file
    for line in fileinput.input(integmpp, inplace = 1):
        line = re.sub('^-c \d+(\.\d+)? \d+(\.\d+)?','-c {0:.3f} {1:.3f}'.format(cx_final,cy_final),line)
        if mask_tif!='':
            line = re.sub('^#+-m.*$','-m yes '+os.path.join(os.path.realpath('.'),mask_tif),line)
        else:
            line = re.sub('^-m','#-m',line)
        if expname == '':
            line = re.sub('^-l.*$','-l '+time.strftime("%Y%m%d")+'.log',line)
        else:
            line = re.sub('^-l.*$','-l '+time.strftime("%Y%m%d")+'_'+expname+'.log',line)
        line = re.sub('^-q yes','#-q yes',line)
        line = re.sub('^-s','#-s',line)
        line = re.sub('^-f','#-f',line)
        if (not line.startswith('-a') and not re.match(r'^\s*$', line)):        
            sys.stdout.write(line)

    ff = open(integmpp,'a')
    if mask_tif == '':
        ff.write('-a yes points 1 1 1 1 \n')
    ff.write('-f '+agbe_tif+'\n')
    ff.close()

        
else:
    # make a new file
    ff = open(integmpp,'w')
    if expname == '':
        ff.write('-l '+time.strftime("%Y%m%d")+'.log\n')
    else:
        ff.write('-l '+time.strftime("%Y%m%d")+'_'+expname+'.log\n')
    ff.write('-c {0:.3f} {1:.3f}\n'.format(cx_final,cy_final))
    ff.write('#-q yes 0 0\n')
    ff.write('-i yes i2 500000\n')
    ff.write('-r yes 1.5\n')
    ff.write('#-s yes\n')
    if mask_tif != '':
        ff.write('-m yes '+os.path.join(os.path.realpath('.'),mask_tif)+'\n')
    else:
        ff.write('#-m yes /path/to/mask.tif\n')
        ff.write('-a yes points 1 1 1 1\n')
    ff.write('-z yes 3.5\n')
    ff.write('-f '+agbe_tif+'\n')
    ff.close()


# integrate agbe ang load the 1d spectra    
subprocess.call(['sastool',integmpp])

agbe_1d = numpy.genfromtxt(agbe_datfile)
# q=0 is always at 0 px
ring_px = numpy.array([0])

# find the position of the first agbe ring
#rad_max = agbe_1d[int(agbe_ring_dir[-1,2]-50):,1].argmax()
rad_max = int(agbe_1d[int(agbe_ring_est-px_peak_search_rg):,1].argmax()+agbe_ring_est-px_peak_search_rg)



px_peak_search_rg = int(min(160/det_bin,agbe_ring_est/2))
px_num_g = int(px_peak_search_rg/2)

# fit gauss to find first peak position
coeff, var_matrix = fit1dgauss(agbe_1d,rad_max,px_num_g)
ring_px = numpy.append(ring_px,coeff[1])


# find positions of other rings

for jj in range(2,9):
    px_peak_search_rg = int(min(160/det_bin,jj*agbe_ring_est/2))
    px_num_g = int(px_peak_search_rg/2)
    if agbe_1d.shape[0] > int(jj * ring_px[1] + px_num_g):
        coeff, var_matrix = fit1dgauss(agbe_1d,jj*ring_px[jj-1]/(jj-1),px_num_g)
        ring_px = numpy.append(ring_px,coeff[1])
        



# add q info into integmpp
qinfo = ''
for jj in range(len(ring_px)):
    qinfo = qinfo+' '+'{:.2f}'.format(ring_px[jj])+' '+str(jj*0.1076)

for line in fileinput.input(integmpp, inplace = 1):
    line = re.sub('.*-q .*','-q yes'+qinfo,line)
    sys.stdout.write(line)

# run sastool
subprocess.call(['sastool',integmpp])

# if without mask file, delete '-a' line
if mask_tif == '':
    for line in fileinput.input(integmpp, inplace = 1):
        if not line.startswith('-a'):        
            sys.stdout.write(line)
    print('\n\n\n*************************************************')
    print("\033[95m {}\033[00m".format('!! ** **  There is no usable mask file.  ** ** !!'))
    print("\033[95m {}\033[00m".format('!! Please add \'-m\' option to integ.mpp file. !!'))

agbe_1d = numpy.genfromtxt(agbe_datfile)
agbe_1d[:,1] = numpy.maximum(agbe_1d[:,1],.1)


print('*****************************************************')
print('Beam center is at ({0:.3f}, {1:.3f})'.format(cx_final,cy_final))
print('*****************************************************')

distance=[]

print('Calculated detector distances are:\n')
for yy in range(1,len(ring_px)):
    dd = ring_px[yy]*det_bin*(.225/6144)/numpy.tan(2*yy*numpy.arcsin((12.3984/e_kev)/(2*58.38)))
    distance = numpy.append(distance,dd)
    print('Ring {0:d}:\t pixel {1:.2f}:\t {2:.1f} mm'.format(yy,ring_px[yy],dd*1000))
print('----------------------------------------------------------')
print('Mean distance:\t\t {0:.1f} mm\n'.format(1000*numpy.mean(distance)))

plt.figure('agbe scattering')
plt.plot(agbe_1d[:,0],numpy.log(agbe_1d[:,1]))
for ii in range(1,len(ring_px)):
    plt.axvline(ii*2*numpy.pi/58.38,color='r')



# Plot center position and first agbe ring (add .5 because sastool and python index pixels differently)
plot_center(agbe_2d,cy_final+.5,cx_final+.5,ring_px[rrn])        
plt.show()




 
    
    






 
 
            








