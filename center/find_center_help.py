#!/usr/bin/env python

print("\n\
Running:\n\n\
\
$ find_center.py\n\n\
\
in 'analysis' directory should work automatically.\n\
If there is no '../data' folder the folder choosing dialog will appear to select the folder containing 2d images.\n\
If there are more than one agbe files the last one is used. Specifying series number of agbe file will ensure that that one is used, e.g.:\n\n\
\
$ find_center.py 35\n\n\
\
By default, script will search for agbe ring in 45(+n*90) degree angle from center position. If the ring is not visible the script will fail. In that case he script can be run in the interactive mode:\n\n\
\
$ find_center.py i\n\n\
\
1. the script will specify which agbe file it found and offer to select another one. By pressing ENTER the script will continue with the file found. By typing anything and then pressing ENTER you will get a file choosing dialog to select a new agbe file.\n\n\
\
2. agbe 2d image with approximate center position will be shown. You can zoom into image by pressing 5th box from the right in the lower part of the image ('zoom to rectangle'). After that you can zoom by drawing a rectangle with the mouse left click-and-hold. If you are happy with the position of the center just close the image. Right click will specify another center position. If you make more than one right click on the image the last one will be used. The center position drawn on the image will not be updated during this process. Close the image and the new image with the updated center position will appear. You can repeat the procedure of selecting a new center or just close the image. Once there was no right click on the image the script will continue.\n\n\
\
3. input the angular range to integrate each angle when searching for the agbe ring. Default (accepted by just pressing ENTER) is +-5.\n\n\
\
4. agbe 2d image with angles that will be integrated while searching for the center are shown. If some of these 'rays' are not intercepting 1st agbe ring the script will fail. You can select new angles by right-clicking on the image. Image will not update angles position during selection process.  Select at least 4 angles. Close the image and the new image with updates angles will be shown. If you close the image without right-clicking on it the script will continue with shown angles.\n\n\n\
\
\
\
Final result (with or without 'i' option):\n\
- image 'beam center': agbe 2d image with beam center and first agbe ring shown.\n\
- image agbe ring - directions: 1d scattering in all directions. In center is correct the agbe ring should be at the same distance for all\n\
- agbe scattering: final agbe 1d scattering with q calibration. Red vertical lines represent theoretical agbe ring positions.\n\
\n\n\n\
")
