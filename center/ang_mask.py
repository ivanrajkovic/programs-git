import numpy
from PIL import Image
from matplotlib import pyplot as plt

def ang_mask(sh,sv,cx,cy,start_ang,ang_range,name='angular_mask.tif'):
    
    # keep starting angle in (0,360) range
    start_ang=numpy.mod(start_ang,360)
    
    xm, ym = numpy.meshgrid(numpy.arange(sh),numpy.arange(sv))
    
    zm = numpy.arctan2(ym-cy,xm-cx)
    
    
    # numpy.arctan2 returns (-pi,pi) values so we need two conditions connected by OR
    
    # for (0,pi) range
    #qq = numpy.where(numpy.logical_and(zm>numpy.radians(start_ang-ang_range),zm<numpy.radians(start_ang+ang_range)),1,0)
    # for (-pi,0)
    #ww = numpy.where(numpy.logical_and((zm+2*numpy.pi)>numpy.radians(start_ang-ang_range),(zm+2*numpy.pi)<numpy.radians(start_ang+ang_range)),1,0)
    
    # both of them together:
    
    zm = numpy.where(numpy.logical_or(numpy.logical_and(zm>numpy.radians(start_ang-ang_range),zm<numpy.radians(start_ang+ang_range)),numpy.logical_and((zm+2*numpy.pi)>numpy.radians(start_ang-ang_range),(zm+2*numpy.pi)<numpy.radians(start_ang+ang_range))),1,0)
    
    
    #im = Image.fromarray(numpy.flipud((zm).astype('uint8')))
    
    zm = numpy.flipud(zm.astype('uint16'))
    im = Image.frombytes('I;16',(sh,sv),zm.tobytes())
    im.save(name)
    #plt.figure(start_ang)
    #plt.imshow(numpy.flipud(zm))
    #plt.show()
    
    
for rr in range(-10,500,10):
    ang_mask(100,100,40,40,rr,10)
