#!/usr/bin/env python

import glob
import fileinput
import re
import sys
import subprocess
import numpy
from matplotlib import pyplot as plt
from shutil import copyfile
import math
from scipy.optimize import curve_fit

integmpp_orig = './integ.mpp'
integmpp = './integ_cen.mpp'
agbe = './agbe_S007_0_01.tif'

repeats = 3



end_px=-1

if len(sys.argv) > 4:
	repeats = int(sys.argv[4])

cen = numpy.zeros([repeats+1,2])
cen[0,0] = 1541
cen[0,1] = 2414

if len(sys.argv) > 3:
	cen[0,1] = sys.argv[3]
if len(sys.argv) > 2:
	cen[0,0] = sys.argv[2]
if len(sys.argv) > 1:
	agbe = sys.argv[1]

datfile = agbe[:-3]+'dat'

def gauss(x, *p):
    A, mu, sigma = p
    return A*numpy.exp(-(x-mu)**2/(2.*sigma**2))


copyfile(integmpp_orig, integmpp)


for line in fileinput.input(integmpp, inplace = 1): 
    line = re.sub('^-m','#-m',line)
    line = re.sub('^-s','#-s',line)
    line = re.sub('^-q','#-q',line)
    line = re.sub('^-a','#-a',line)
    line = re.sub('^-c \d+(\.\d+)? \d+(\.\d+)?','-c '+str(cen[0,0])+' '+str(cen[0,1]),line)
    line = re.sub('^-f','#-f',line)
    sys.stdout.write(line)
    
ff = open(integmpp,'a')
ff.write('-a no points 1 1 2 2 \n')
ff.write('-f '+agbe+'\n')
ff.close()


# starting angle and angular range (st_ang+-ang_rg)
st_ang = 0
ang_rg = 30
cr = 50

md = numpy.zeros([repeats,4])

# number of pixels for gauss fit
pgf = 20
kk = 1

for rr in range(1,repeats+1):
    for i in range(0,4):
        x1 = cr*math.sin(math.radians(st_ang+i*90+ang_rg))      
        x2 = cr*math.sin(math.radians(st_ang+i*90-ang_rg))
        y1 = cr*math.cos(math.radians(st_ang+i*90+ang_rg))
        y2 = cr*math.cos(math.radians(st_ang+i*90-ang_rg))
        for line in fileinput.input(integmpp, inplace = 1):
            line = re.sub('-a no points -?\d+\.?\d* -?\d+\.?\d* -?\d+\.?\d* -?\d+\.?\d*','-a no points '+str(cen[rr-1,0]+x1)+' '+str(cen[rr-1,1]+y1)+' '+str(cen[rr-1,0]+x2)+' '+str(cen[rr-1,1]+y2) ,line)
            line = re.sub('^-c \d+(\.\d+)? \d+(\.\d+)?','-c '+str(cen[rr-1,0])+' '+str(cen[rr-1,1]),line)
            sys.stdout.write(line)

        subprocess.call(['sastool',integmpp])
        ddd = numpy.genfromtxt(datfile)
	if ddd.shape[0]>550:
		kk = 4	
	st_px=kk*70
        rad_max = ddd[st_px:end_px,1].argmax()
        p0 = [ddd[st_px:end_px,1].max()-ddd[st_px:end_px,1].min(),st_px+rad_max,kk*5]
        coeff, var_matrix = curve_fit(gauss,ddd[int(rad_max+st_px-kk*pgf/2):int(rad_max+st_px+kk*pgf/2),0],ddd[int(rad_max+st_px-kk*pgf/2):int(rad_max+st_px+kk*pgf/2),1],p0=p0)
        md[rr-1,i] = coeff[1]
    
    cen[rr,0] = cen[rr-1,0] + md[rr-1,1]/2 - md[rr-1,3]/2
    cen[rr,1] = cen[rr-1,1] + md[rr-1,0]/2 - md[rr-1,2]/2
    
print(cen)
print(md)


        
        


    
