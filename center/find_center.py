#!/usr/bin/env python
from __future__ import division

import os
import sys
import time
import glob
import fileinput
import shutil
import subprocess
import re
from PIL import Image
#import PIL
import math
import numpy
from scipy import signal
from scipy.optimize import curve_fit, leastsq
from matplotlib import pyplot as plt
from matplotlib.image import imread
from Tkinter import Tk
from tkFileDialog import askopenfilename, askdirectory
import ttk
import fabio
import argparse
import curses

numpy.seterr(divide='ignore')

# gaussian fit function (1d)
def gauss(x, *p):
    A, mu, sigma = p
    return A*numpy.exp(-(x-mu)**2/(2.*sigma**2))
    
# fit 1d gaussian
def fit1dgauss(data,cen,dx):
    cen = int(cen)
    dx = int(dx)
    pp = [data[(cen-dx):(cen+dx),1].max()-data[(cen-dx):(cen+dx),1].min(),cen,40/det_bin]
    coeff, var_matrix = curve_fit(gauss,data[(cen-dx):(cen+dx),0],data[(cen-dx):(cen+dx),1],p0=pp)
    return coeff, var_matrix

# finding moments for 2d gaussian fitting
def moments(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution by calculating its
    moments """
    total = data.sum()
    X, Y = numpy.indices(data.shape)
    x = (X*data).sum()/total
    y = (Y*data).sum()/total
    col = data[:, int(y)]
    width_x = numpy.sqrt(abs((numpy.arange(col.size)-y)**2*col).sum()/col.sum())
    row = data[int(x), :]
    width_y = numpy.sqrt(abs((numpy.arange(row.size)-x)**2*row).sum()/row.sum())
    A = data.max()
    return A, x, y, width_x, width_y

# 2d gaussian function
def gauss2d(A, center_x, center_y, width_x, width_y):
    return lambda x,y: A*numpy.exp(-(((center_x-x)/width_x)**2+((center_y-y)/width_y)**2)/2)

# 2d gaussian fitting
def fit2dgauss(data):
    """Returns (A, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    params = moments(data)
    errorfunction = lambda p: numpy.ravel(gauss2d(*p)(*numpy.indices(data.shape)) -
                                 data)
    p, success = leastsq(errorfunction, params)
    return p

def ang_mask(sh,sv,cx,cy,start_ang,ang_range,name='angular_mask.tif'):
     # keep starting angle in (0,360) range
    start_ang=numpy.mod(start_ang,360)
    xm, ym = numpy.meshgrid(numpy.arange(sh),numpy.arange(sv))
    zm = numpy.arctan2(ym-cy,xm-cx)
    # numpy.arctan2 returns (-pi,pi) values so we need two conditions connected by OR
    zm = numpy.where(numpy.logical_or(numpy.logical_and(zm>numpy.radians(start_ang-ang_range),zm<numpy.radians(start_ang+ang_range)),numpy.logical_and((zm+2*numpy.pi)>numpy.radians(start_ang-ang_range),(zm+2*numpy.pi)<numpy.radians(start_ang+ang_range))),1,0)
    zm = numpy.flipud(zm.astype('uint16'))
    im = Image.frombytes('I;16',(sh,sv),zm.tobytes())
    im.save(name)


# Returns sx x sy array od zeros with a circle at rx,ry with radius rr and intensity int
def circ(sx,sy,rx,ry,rr,int):
    ccxx, ccyy = numpy.mgrid[:sx, :sy]
    ccircle = (ccxx - rx) ** 2 + (ccyy - ry) ** 2
    cc=(ccircle<rr**2)*int
    return cc

# Returns centar from 2d data by finding maximum of the autocorrelation
def cen_corr_max(data2d):
    data_corr = signal.fftconvolve(data2d,data2d)
    cx_crmax, cy_crmax = numpy.unravel_index(numpy.argmax(data_corr), data_corr.shape)
    bx_crmax = (1.0*cy_crmax+1)/2
    by_crmax = agbe_size_v-(1.0*cx_crmax+1)/2
    return bx_crmax, by_crmax

# Returns centar from 2d data by fitting 2g gassian to the autocorrelation data
def cen_corr_gauss(data2d,dx,dy):
    data_corr = signal.fftconvolve(data2d,data2d)
    cx_crmax, cy_crmax = numpy.unravel_index(numpy.argmax(data_corr), data_corr.shape)
    cr_gf = fit2dgauss(data_corr[cx_crmax-dx:cx_crmax+dx,cy_crmax-dy:cy_crmax+dy])
    cx_cg = cx_crmax-dx+cr_gf[1]
    cy_cg = cy_crmax-dy+cr_gf[2]
    bx_cg = (1.0*cy_cg+1)/2
    by_cg = agbe_size_v-(1.0*cx_cg+1)/2
    return bx_cg, by_cg

# cirlce fitting
def calc_R(x,y, xc, yc):
    """ calculate the distance of each 2D points from the center (xc, yc) """
    return numpy.sqrt((x-xc)**2 + (y-yc)**2)

def f_2(c, x, y):
    """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
    Ri = calc_R(x, y, *c)
    return Ri - Ri.mean()
# end circle fitting

def plot_center(data2d,cenx,ceny,ring):
    size_h=int(data2d.shape[0])
    size_v=int(data2d.shape[1])
    beam_target_nul = numpy.zeros([size_h,size_v])
    #beam_target_max = numpy.zeros([size_h,size_v])
    

    #target_rd = [1,0.75,0.5,0.25,0.1]
    target_rd=numpy.linspace(1.5,0.2,5)

    for tt in range(len(target_rd)):
        beam_target_nul=beam_target_nul + ((-1)**tt) * circ(size_h,size_v,size_h-cenx,ceny,target_rd[tt]*size_h/128,1)

    beam_target_max=circ(size_h,size_v,size_h-cenx,ceny,target_rd[1]*size_h/128,data2d.max())
        
    if det == 'mx':
        ring_circ = circ(size_h,size_v,size_h-cenx,ceny,ring+16/det_bin,1)-circ(size_h,size_v,size_h-cenx,ceny,ring-16/det_bin,1)
    else:
        ring_circ = circ(size_h,size_v,size_h-cenx,ceny,ring+4,1)-circ(size_h,size_v,size_h-cenx,ceny,ring-4,1)
    plt.figure('beam center')
    plt.imshow(numpy.log10(abs((data2d+beam_target_max)*(1-beam_target_nul)*(1-ring_circ))),clim=(0,numpy.log10(data2d.max())))

def onclick(event):
    print('button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
          (event.button, event.x, event.y, event.xdata, event.ydata))
    if event.button == 3:
        xp.append(event.xdata)
        yp.append(event.ydata)        
        
## Program starts here

# Set some variables
data_folder = os.path.join('..', 'data')
integcenmpp = 'integ_cen.mpp'
integmpp = 'integ.mpp'
cen_fold = 'cen'
angle=45
ang_mask_file = 'angular_mask.tif'

# Set variables that might be given on the command line    
agbe_sn = 0
agbe_tif = ''

d_agbe = 58.38


try:
    expname = '_'.join(os.getcwd().split(os.sep)[-2].split('_')[1:])
except:
    expname = ''

# parse command line arguments, if any
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
            find the beam center using agbe scattering
            """
    )

parser.add_argument('agbe_image', nargs ='?', default=0, type=str, help='agbe filename/series number. Automatic search if 0 or not specified')
parser.add_argument('--interactive','-i', help='interactive version/ ask at each step', action='store_true',default=False)
parser.add_argument('-df','--data_folder',metavar='PATH',default='../data' , help='path to the data folder, used for automatic search')

arg_in = parser.parse_args()

try:
    # specified series number
    agbe_sn = int(arg_in.agbe_image)
    data_folder = arg_in.data_folder
except:
    # specified filename
    agbe_tif = arg_in.agbe_image
    data_folder = os.path.dirname(agbe_tif)
    if not os.path.isfile(agbe_tif):
        sys.exit("File does not exist!")
    

if os.path.isdir(data_folder) == False:
    print('\n Could not find data folder.')
    print('Please select your data folder.')
    root = Tk()
    root.withdraw()
    #root.option_add('*foreground', 'black')
    data_sel = askdirectory(title='Please select your data folder')
    root.destroy()
    data_folder = os.path.relpath(data_sel)



# find the newest agbe file if a series number nor a file name is not specified on the command line
if agbe_sn == 0 and len(agbe_tif) == 0:
    agbe_list=[]
    rrr = re.compile('.*[Aa][Gg][Bb][Ee].*0+1\.(?:tif|cbf)')
    for file in os.listdir(data_folder):
        if rrr.match(file):
            agbe_list.append(os.path.join(data_folder,file))
    
    agbe_list_s = sorted(agbe_list,key=lambda f: os.stat(f).st_mtime,reverse=True)
    if len(agbe_list_s) > 0:
        agbe_tif=agbe_list_s[0]
    else:
        print('Could not find silver behenate file in data folder.')
        print('Please select your silver behenate file.')
        root = Tk()
        root.withdraw()
        #root.option_add('*foreground', 'black')
        new_agbe = askopenfilename(title='Please select agbe file',filetypes=[('Tif/cbf files',('*.tif','*.tiff','*.cbf')),('all files', '*')])
        root.destroy()
        agbe_tif = os.path.relpath(new_agbe)
    
# if a file name is given, use that
elif len(agbe_tif) > 0:
    pass
# or find the file number from a series number
else:
    agbe_list=[]
    rrr = re.compile('.*[S|B|T]'+str(agbe_sn).zfill(3)+'.*_0+1\.(?:tif|cbf)')

    for file in os.listdir(data_folder):
        if rrr.match(file):
            agbe_list.append(os.path.join(data_folder,file))
    if len(agbe_list) > 0:
        agbe_tif = agbe_list[0]
    else:
        print('Could not find silver behenate file in data folder.')
        print('Please select your silver behenate file.')
        root = Tk()
        root.withdraw()
        #root.option_add('*foreground', 'black')
        new_agbe = askopenfilename(title='Please select agbe file',filetypes=[('Tif/cbf files',('*.tif','*.tiff','*.cbf')),('all files', '*')])
        root.destroy()
        agbe_tif = os.path.relpath(new_agbe)

if arg_in.interactive:
    print('Silber behenate file to be used for center determination is:')
    print('\033[0;33;44m\033[1m \033[4m{}\033[0;0m \n'.format(agbe_tif))
    print('Type \'Y\' to continue or \'N\' to choose another file (Y/n).\n')
    agbe_input = raw_input('?')
    #stdscr = curses.initscr()
    #stdscr.addstr('Silber behenate file to be used for center determination is:\n')
    #stdscr.addstr('{}\n'.format(agbe_tif))
    #stdscr.addstr('Type \'Y\' to continue or \'N\' to choose another file (Y/n).\n')
    #c_input = stdscr.getch()
    #agbe_input = chr(c_input)
    #curses.endwin()
    if agbe_input == 'N' or agbe_input =='n':
        root = Tk()
        root.withdraw()
        #root.option_add('*foreground', 'red')
        new_agbe = askopenfilename(title='Please select agbe file',filetypes=[('Tif/cbf files',('*.tif','*.tiff','*.cbf')),('all files', '*')],initialdir = data_folder)
        root.destroy()
        agbe_tif = os.path.relpath(new_agbe)
    
    

print('Opening image {}.'.format(agbe_tif))

# sastool datfile name
agbe_datfile = os.path.basename(agbe_tif)[:-3]+'dat'

# Open agbe file    
#agbe_2d_im = Image.open(agbe_tif).convert('I')
#agbe_2d = numpy.array(agbe_2d_im)
#if agbe_2d.shape == ():
#    print('Opening image using slow method')
#    agbe_2d = numpy.array(agbe_2d_im.getdata(),dtype='int32').reshape(agbe_2d_im.size[0],agbe_2d_im.size[1])

agbe_2d = fabio.open(agbe_tif).data.astype('int32')

agbe_max = agbe_2d.max()

# Get info from prp file

with open(agbe_tif[:-4]+'.prp') as agbe_prp:
    for line in agbe_prp.readlines():
        if 'Beam energy' in line:
            e_kev=float(line.split('=')[1].split(' ')[0])/1000
        if 'Pipe length' in line:
            det_dist=float(line.split("=")[1].split(' ')[0])
        if 'Detector mode' in line:
            try:
                # if detector mode is a number ('0' or '1' in .prp) -> Rayonix
                int(line.split('=')[1][0])
                det='mx'
            except:
                # if detector mode is not a number ('Pilatus' in .prp) -> Pilatus
                det='pil'
        if 'I_2' in line:
            i2 = float(line.split('=')[1].split(' ')[0])
          
            

# Get info from the file
agbe_size_v = int(agbe_2d.shape[0])
agbe_size_h = int(agbe_2d.shape[1])

if det == 'mx':   
    det_bin = int(6144/agbe_size_h)
    px_size = .225/6144*det_bin

if det == 'pil':
    agbe_2d=numpy.where(agbe_2d<0.9*agbe_2d.max(),agbe_2d,1)
    px_size = 172e-6
    det_bin=1

# Calculate approximate position of first agbe ring
agbe_ring_est = ((det_dist+180.)/1000)*numpy.tan(2*numpy.arcsin((12.3984/e_kev)*(2*numpy.pi/d_agbe)/(4*numpy.pi)))/px_size

# if if ring position < 100px, use a different ring
rrn = int(numpy.ceil(100/agbe_ring_est))


# calulate autocorrelation and find the maximum position

cx_crmax, cy_crmax = cen_corr_max(agbe_2d)


# put zeros in the center of the image to lower the background in autocorrelation
# and find the center again
agbe_2d_circ = agbe_2d*(1-circ(agbe_size_v,agbe_size_h,agbe_size_v-cy_crmax,cx_crmax,agbe_ring_est/10,1))

# fit 2d gaussian
if det == 'mx':
    cr_dx = int(80/det_bin)
    cr_dy = int(80/det_bin)
else:
    cr_dx = 20
    cr_dy = 20
    
cx_cg, cy_cg = cen_corr_gauss(agbe_2d_circ,cr_dx,cr_dy)

if numpy.sqrt((cx_crmax-cx_cg)**2+(cy_crmax-cy_cg)**2)>20:
     cx_cg = cx_crmax
     cy_cg = cy_crmax

if arg_in.interactive:
    # check if the approximate center position is correct
    xp=[5]
    while len(xp)>0:
    
        xp = []
        yp = []
        
    
        beam_target = numpy.zeros([agbe_size_v,agbe_size_h])
        beam_target_neg = numpy.zeros([agbe_size_v,agbe_size_h])
    
        beam_target=beam_target + circ(agbe_size_v,agbe_size_h,agbe_size_v-cy_cg,cx_cg,agbe_size_h/100,1)
        beam_target_neg=beam_target_neg + circ(agbe_size_v,agbe_size_h,agbe_size_v-cy_cg,cx_cg,agbe_size_h/250,agbe_max)
        
        
        fig = plt.figure('Please right-click on the center and close the image')
        ax = fig.add_subplot(111)
        ax.imshow(numpy.log10(abs(agbe_2d*(1-beam_target)+beam_target_neg)),clim=(0,numpy.log10(agbe_max)))
        
        
        cid = fig.canvas.mpl_connect('button_press_event', onclick)
        
        print('\n\nSelect a point with a right click')
        print('or close the image to continue with the preselected center.\n')
        print('Last point chosen before closing the image will be used \nas a starting point for center refinement. \n\n')
        
        plt.show()
        
        if len(xp) > 0:
            cx_cg = xp[-1]
            cy_cg = agbe_size_v-yp[-1]

# create a new integcenmpp file

ff = open(integcenmpp,'w')
ff.write('-l '+time.strftime("%Y%m%d")+'_cen.log\n')
ff.write('-c {0:.3f} {1:.3f}\n'.format(cx_cg,cy_cg))
if i2 > 10:
    ff.write('-i yes i2 25000\n')
#ff.write('-r yes 1.5\n')
ff.write('-z yes 3.5\n')
if det == 'pil':
    ff.write('-o 0\n')
ff.write('-m yes {}\n'.format(ang_mask_file))
ff.write('-f '+agbe_tif+'\n')
ff.close()

# set some variables for angular integration

angles = angle + numpy.arange(0,360,90)
ang_rg = 5

a_px = 50

# number of pixel around the estimated peak where to search for a peak
# half way from center to peak (or peak to peak, for rrn>1) or mx -> (160/detector binning), pil -> 40, whichever smaller
if det == 'mx':
    px_peak_search_rg = int(min(160/det_bin,agbe_ring_est/2))
else:
    px_peak_search_rg = int(min(40,agbe_ring_est/2))
    
# number of pixels around the peak to fit a gauss
px_num_g = int(px_peak_search_rg/2)
#px_num_g = int(min(80/det_bin,px_peak_search_rg))

if arg_in.interactive:
    print('How many degrees to integrate +/- for each angle?')
    print('Default value is \033[0;30;43m\033[1m\033[4m+/-{}\033[0;0m'.format(ang_rg))
    print('press ENTER to accept or input another value: \n')
    rg_input = raw_input('?')
    if len(rg_input)>0:
        try:
            ang_rg = float(rg_input)
            print('Angular range is +-{0:g} now.'.format(ang_rg))
        except:
            print('Not a number, continuing with +-{}.'.format(ang_rg))
                
    
    xp=[5]
    while len(xp)>0:
        xp = []
        yp = []
        fig = plt.figure('Angles (right clicks and close)')
        ax = fig.add_subplot(111)
        agbe_ang=numpy.zeros([agbe_size_v,agbe_size_h])
    
    
        xxm, yym = numpy.meshgrid(numpy.arange(agbe_size_h),numpy.arange(agbe_size_v))
        ppp = numpy.arctan2(yym-cy_cg,xxm-cx_cg)
        agbe_ang = numpy.zeros([agbe_size_v,agbe_size_h])
    
        for i in angles:
            agbe_ang = agbe_ang+numpy.where(numpy.logical_or(numpy.logical_and(ppp>numpy.radians(i-ang_rg),ppp<numpy.radians(i+ang_rg)),numpy.logical_and((ppp+2*numpy.pi)>numpy.radians(i-ang_rg),(ppp+2*numpy.pi)<numpy.radians(i+ang_rg))),agbe_max/2000,0)
    
        agbe_ang= numpy.flipud(agbe_ang)+agbe_2d*(1-beam_target)+beam_target_neg
        ax.imshow(numpy.log10(agbe_ang))
        cid = fig.canvas.mpl_connect('button_press_event', onclick)
    
        print('\n\nSelect points (4 or more) with a right click.')
        print('or close the image to continue with preselected angles.\n')
        
        plt.show()
        
        if len(xp) > 0:
            angles = numpy.zeros(len(xp))
            for ll in range(len(xp)):
                angles[ll] = numpy.degrees(math.atan2(agbe_size_v-yp[ll]-cy_cg,xp[ll]-cx_cg))
    


# How many times to perform refinement
repeats = 2

num_of_ang = angles.size

agbe_ring_dir = numpy.zeros([repeats,num_of_ang])
ref_cen = numpy.zeros([repeats+1,2])
ref_cen[0,:] = [cx_cg,cy_cg]
if not os.path.exists(cen_fold):
    os.makedirs(cen_fold)
plt.figure('agbe ring - directions')

for rr in range(1,repeats+1):
    for i in range(num_of_ang):
        for line in fileinput.input(integcenmpp, inplace = 1):
            line = re.sub('^-c \d+(\.\d+)? \d+(\.\d+)?','-c '+str(ref_cen[rr-1,0])+' '+str(ref_cen[rr-1,1]),line)
            sys.stdout.write(line)
        ang_mask(agbe_size_h,agbe_size_v,ref_cen[rr-1,0],ref_cen[rr-1,1],angles[i],ang_rg)
        subprocess.call(['sastool',integcenmpp])
        agbe_1d = numpy.genfromtxt(agbe_datfile)
        shutil.move(agbe_datfile,os.path.join(cen_fold,agbe_datfile[:-4]+'_r'+str(rr)+'_d'+str(i)+'.dat'))
        agbe_max_px = agbe_1d[int(rrn*agbe_ring_est-px_peak_search_rg):int(rrn*agbe_ring_est+px_peak_search_rg),1].argmax() + rrn*agbe_ring_est-px_peak_search_rg
        coeff, var_matrix = fit1dgauss(agbe_1d,agbe_max_px,px_num_g)
        agbe_ring_dir[rr-1,i] = coeff[1]
        if rr == repeats:
            agbe_1d[:,1] = numpy.maximum(agbe_1d[:,1],.1)
            plt.plot(agbe_1d[:,0],numpy.log(agbe_1d[:,1]))
    
    #x_del = 0
    #y_del = 0
    #for j in range(0,4):
    #    x_del = x_del+agbe_ring_dir[rr-1,j]*numpy.cos(numpy.radians(start_ang+90*j))/2
    #    y_del = y_del+agbe_ring_dir[rr-1,j]*numpy.sin(numpy.radians(start_ang+90*j))/2
    ring_points_x = numpy.zeros(len(angles))
    ring_points_y = numpy.zeros(len(angles))
    # set ring positions or circle fitting
    for j in range(len(angles)):
        ring_points_x[j] = ref_cen[rr-1,0]+agbe_ring_dir[rr-1,j]*numpy.cos(numpy.radians(angles[j]))
        ring_points_y[j] = ref_cen[rr-1,1]+agbe_ring_dir[rr-1,j]*numpy.sin(numpy.radians(angles[j]))
    
    rx_m = numpy.mean(ring_points_x)
    ry_m = numpy.mean(ring_points_y)
    
    center_2, ier = leastsq(f_2,(rx_m,ry_m),args=(ring_points_x,ring_points_y))
    
    
    ref_cen[rr,0] = center_2[0]
    ref_cen[rr,1] = center_2[1]
    
shutil.move(integcenmpp,os.path.join(cen_fold,integcenmpp))
shutil.move(ang_mask_file,os.path.join(cen_fold,ang_mask_file))
cx_final = ref_cen[-1,0]
cy_final = ref_cen[-1,1]


# Check if the mask file exists
mask_msk = ''
mask_tif = ''
try:
    mask_tif = max(glob.iglob('mask*.tif'), key=os.path.getctime)
except:
    pass

if mask_tif == '':
    try:
        mask_msk = max(glob.iglob('*msk'), key=os.path.getctime)
    except:
        pass

if mask_msk != '':
    subprocess.call(['msk2tif.py',mask_msk])
    mask_tif = mask_msk[:-3]+'tif'


# Prepare integ.mpp
if os.path.exists(integmpp):
    # change existing file
    for line in fileinput.input(integmpp, inplace = 1):
        line = re.sub('^-c \d+(\.\d+)? \d+(\.\d+)?','-c {0:.3f} {1:.3f}'.format(cx_final,cy_final),line)
        if mask_tif!='':
            #line = re.sub('^#*-m.*$','-m yes '+os.path.join(os.path.realpath('.'),mask_tif),line)
            line = re.sub('^#*-m.*$','-m yes {}'.format(mask_tif),line)
        else:
            line = re.sub('^#*-m.*$','-m yes {}'.format(ang_mask_file),line)
        if expname == '':
            line = re.sub('^-l.*$','-l '+time.strftime("%Y%m%d")+'.log',line)
        else:
            line = re.sub('^-l.*$','-l '+time.strftime("%Y%m%d")+'_'+expname+'.log',line)
        line = re.sub('^-q yes','#-q yes',line)
        line = re.sub('^-q wide','#-q wide',line)
        line = re.sub('^-s','#-s',line)
        line = re.sub('^-f','#-f',line)
        if 'i2' in line:
            if i2 < 10:
                line = re.sub('^-i','#-i',line)
        if (not line.startswith('-a') and not re.match(r'^\s*$', line)):        
            sys.stdout.write(line)

    ff = open(integmpp,'a')
    ff.write('-f '+agbe_tif+'\n')
    ff.close()



        
else:
    # make a new file
    ff = open(integmpp,'w')
    if expname == '':
        ff.write('-l '+time.strftime("%Y%m%d")+'.log\n')
    else:
        ff.write('-l '+time.strftime("%Y%m%d")+'_'+expname+'.log\n')
    ff.write('-c {0:.3f} {1:.3f}\n'.format(cx_final,cy_final))
    ff.write('#-q yes 0 0\n')
    if i2 > 10:
        ff.write('-i yes i2 10000000\n')
    ff.write('-r yes 1.5\n')
    ff.write('#-s yes\n')
    if mask_tif != '':
        ff.write('-m yes {}'.format(mask_tif)+'\n')
    else:
        ff.write('-m yes {0}\n'.format(ang_mask_file))
    ff.write('-z yes 3.5\n')
    if det == 'pil':
        ff.write('-o 0\n')
    ff.write('-f '+agbe_tif+'\n')
    ff.close()

if mask_tif=='':
    ang_mask(agbe_size_h,agbe_size_v,1,1,45,45,ang_mask_file)

# integrate agbe ang load the 1d spectra    
subprocess.call(['sastool',integmpp])

agbe_1d = numpy.genfromtxt(agbe_datfile)
# q=0 is always at 0 px
ring_px = numpy.array([0])

# find the position of the first agbe ring
#rad_max = agbe_1d[int(agbe_ring_dir[-1,2]-50):,1].argmax()
#rad_max = int(agbe_1d[int(agbe_ring_est-px_peak_search_rg):,1].argmax()+agbe_ring_est-px_peak_search_rg)
rad_max = int(agbe_1d[int(agbe_ring_est-px_peak_search_rg):int(agbe_ring_est+2*px_peak_search_rg),1].argmax()+agbe_ring_est-px_peak_search_rg)


# fit gauss to find first peak position
coeff, var_matrix = fit1dgauss(agbe_1d,rad_max,px_num_g)
ring_px = numpy.append(ring_px,coeff[1])


# find positions of other rings

for jj in range(2,6):
    if det == 'mx':
        px_peak_search_rg = int(min(160/det_bin,jj*agbe_ring_est/2))
    else:
        px_peak_search_rg = int(min(40,jj*agbe_ring_est/2))
    px_num_g = int(px_peak_search_rg/2)
    if agbe_1d.shape[0] > int(jj * ring_px[1] + px_num_g):
        try:
            coeff, var_matrix = fit1dgauss(agbe_1d,jj*ring_px[jj-1]/(jj-1),px_num_g)
            ring_px = numpy.append(ring_px,coeff[1])
        except:
            pass
        

# add q info into integmpp
qinfo = ''
for jj in range(len(ring_px)):
    qinfo = qinfo+' '+'{:.2f}'.format(ring_px[jj])+' '+str(jj*0.1076)

for line in fileinput.input(integmpp, inplace = 1):
    line = re.sub('.*-q .*','-q yes'+qinfo,line)
    sys.stdout.write(line)

# run sastool
subprocess.call(['sastool',integmpp])

# print center positions found during search
print('\n\n')
print(ref_cen)
print('\n\n')


# warn if there is no correct mask file
if mask_tif == '':
    print('\n\n\n*************************************************')
    print("\033[0;37;41m{}\033[0;0m".format('!! ** **  There is no correct mask file.  ** ** !!'))
    print("\033[0;37;41m{}\033[0;0m".format('!! Please change \'-m\' option in integ.mpp file. !!'))

agbe_1d = numpy.genfromtxt(agbe_datfile)
agbe_1d[:,1] = numpy.maximum(agbe_1d[:,1],.1)

# print info about beam center and sample-detector distance

print('*****************************************************')
print('Beam center is at ({0:.3f}, {1:.3f})'.format(cx_final,cy_final))
print('*****************************************************')

distance=[]

print('Calculated detector distances are:\n')
for yy in range(1,len(ring_px)):
    dd = ring_px[yy]*px_size/numpy.tan(2*yy*numpy.arcsin((12.3984/e_kev)/(2*d_agbe)))
    distance = numpy.append(distance,dd)
    print('Ring {0:d}:\t pixel {1:.2f}:\t {2:.1f} mm'.format(yy,ring_px[yy],dd*1000))
print('----------------------------------------------------------')
dist_start=0
if len(distance)>3:
    dist_start = 1
    print('Not including first ring in distance average')

dist_mean = 1000*numpy.mean(distance[dist_start:])

# Change -q to wide if we are too close

if dist_mean < 1000:
    for line in fileinput.input(integmpp, inplace = 1):
        if '-q' in line:
            line = '-q wide {0:.0f} {1}\n'.format(e_kev*1000,dist_mean)
        sys.stdout.write(line)
    subprocess.call(['sastool',integmpp])
    agbe_1d = numpy.genfromtxt(agbe_datfile)
    agbe_1d[:,1] = numpy.maximum(agbe_1d[:,1],.1)
    print('\n\n******************************')
    print('Using \'-q wide\' in integ.mpp')
    print('******************************\n\n')

print('Mean distance:\t\t {0:.1f} mm\n'.format(dist_mean))    

# save the same info into a file
info_filename = 'setup_info_{}.txt'.format(time.strftime('%Y%m%d_%H%M%S'))

info_file = open(info_filename,'w')
info_file.write('File used for calibration:\n')
info_file.write('{}\n\n'.format(agbe_tif))
info_file.write('Beam center is at ({0:.3f}, {1:.3f})\n\n'.format(cx_final,cy_final))
info_file.write('Calculated detector distances are:\n')
for yy in range(1,len(ring_px)):
    info_file.write('Ring {0:d}:\t pixel {1:.2f}:\t {2:.1f} mm\n'.format(yy,ring_px[yy],distance[yy-1]*1000))
info_file.write('----------------------------------------------------------\n')
dist_start=0
if len(distance)>3:
    info_file.write('Not including first ring in distance average\n')
if dist_mean < 1000:
    info_file.write('Using \'-q wide\' in integ.mpp\n')
info_file.write('Mean distance:\t\t {0:.1f} mm\n'.format(dist_mean))

info_file.close()



plt.figure('agbe scattering')
plt.plot(agbe_1d[:,0],numpy.log(agbe_1d[:,1]))
for ii in range(1,len(ring_px)):
    plt.axvline(ii*2*numpy.pi/d_agbe,linewidth=2,color='r')
    



# Plot center position and the agbe ring used to find center (add .5 because sastool and python index pixels differently)
plot_center(agbe_2d,cy_final+.5,cx_final+.5,ring_px[rrn])        
plt.show()
