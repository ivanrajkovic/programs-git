#!/usr/bin/env python


from numpy import *
from scipy import signal
import os
from matplotlib import pyplot as plt
from matplotlib.image import imread
from scipy import optimize
import sys
import PIL


def gaussian(height, center_x, center_y, width_x, width_y):
    """Returns a gaussian function with the given parameters"""
    width_x = float(width_x)
    width_y = float(width_y)
    return lambda x,y: height*exp(
                -(((center_x-x)/width_x)**2+((center_y-y)/width_y)**2)/2)

def moments(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution by calculating its
    moments """
    total = data.sum()
    X, Y = indices(data.shape)
    x = (X*data).sum()/total
    y = (Y*data).sum()/total
    col = data[:, int(y)]
    width_x = sqrt(abs((arange(col.size)-y)**2*col).sum()/col.sum())
    row = data[int(x), :]
    width_y = sqrt(abs((arange(row.size)-x)**2*row).sum()/row.sum())
    height = data.max()
    return height, x, y, width_x, width_y

def fitgaussian(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    params = moments(data)
    errorfunction = lambda p: ravel(gaussian(*p)(*indices(data.shape)) -
                                 data)
    p, success = optimize.leastsq(errorfunction, params)
    return p





infile = sys.argv[1]
qq = imread(infile)
#qq = array(PIL.Image.open(infile))
#qq = PIL.Image.open(infile)
#qq = array(qq.getdata(),uint16).reshape(qq.size[1],qq.size[0])

#os.chdir('C:/Users/rajkovic/Documents/programs/center')
#qq = array(PIL.Image.open('agbe_S017_0_01.tif'))

# circle for testing
#cxx, cyy = mgrid[:1000, :1000]
#circle = (cxx - 570) ** 2 + (cyy - 350) ** 2
#donut = logical_and(circle<(90000+500),circle>(90000-500))
#im = PIL.Image.fromstring('L',(1000,1000),donut.tostring())
#im.save('570x350.tif')

#qq=array(PIL.Image.open('570x350.tif'))



corr = signal.fftconvolve(qq,qq)

x, y = unravel_index(argmax(corr), corr.shape)

dx=10
dy=10

#plt.imshow(corr[x-dx:x+dx,y-dy:y+dy])


mf = moments(corr[x-dx:x+dx,y-dy:y+dy])
gf = fitgaussian(corr[x-dx:x+dx,y-dy:y+dy])

mx = x-dx+mf[1]
my = y-dy+mf[2]

gx = x-dx+gf[1]
gy = y-dy+gf[2]

s=qq.shape[0]

print('  max: \t {0:.3f} \t {1:.3f} \n  mom: \t {2:.3f} \t {3:.3f}\n gaus: \t {4:.3f} \t {5:.3f}'.format((1.0*y+1)/2,s-(1.0*x+1)/2,(my+1)/2,s-(mx+1)/2,(gy+1)/2,s-(gx+1)/2))

#plt.imshow(qq,clim=(0,1000))

cs = int(s/128)


ccxx, ccyy = mgrid[:qq.shape[0], :qq.shape[1]]
ccircle = (ccxx - (gx+1)/2) ** 2 + (ccyy - (gy+1)/2) ** 2
ccspot=(ccircle<cs**2)*qq.max()

#ccspot2=(logical_and(ccircle<(cs*0.75 )**2,ccircle>(cs*.5)**2))

ccspot2=logical_or(logical_and(ccircle<(cs*0.75 )**2,ccircle>(cs*0.5)**2),logical_and(ccircle<(cs*.25)**2,ccircle>(cs*.1)**2))


plt.imshow((ccspot+uint32(qq))*(1-ccspot2),clim=(0,qq.max()/40))
plt.show()
