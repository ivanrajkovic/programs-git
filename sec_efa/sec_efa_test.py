#!/usr/bin/env python

'''
@author: rajkovic
'''

import sys
reload(sys)
sys.setdefaultencoding('utf8')


import os
import time
import numpy
import scipy.stats
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
from sklearn.decomposition import TruncatedSVD


ts = time.time()


folder = 'test_data/Fis1_High_60ul_030'
first_file = 250
last_file = 500

file_root = folder.split('/')[-1]

svd_values = 10

bin_size = 20.0

os.chdir(os.path.expanduser('~/bitbucket/programs-git/sec_efa'))
#ttt= time.time()


def get_full_file(fff,rrr,nnn):
    full_name = os.path.join(fff,'sastool','{}_0_{:03}.dat'.format(rrr,nnn))
    return(full_name)

tsvd = TruncatedSVD(n_components=svd_values, n_iter=7, random_state=10)
prefile = get_full_file(folder,file_root,first_file+svd_values-1)

while not os.path.isfile(prefile):
    print('waiting for {}'.format(prefile))
    time.sleep(2)

first_data = numpy.loadtxt(get_full_file(folder,file_root,first_file))

# first non-zero element
fnz =  first_data[:,1].nonzero()[0][0]

data_len = first_data[fnz:,1].size

size_binned=int(numpy.ceil(data_len/bin_size))




int_data = numpy.zeros([last_file-first_file+1,first_data.shape[0]-fnz])
b_int_data = numpy.zeros([last_file-first_file+1,size_binned])
svd_data = numpy.zeros([last_file-first_file-svd_values+1,svd_values+1])
b_svd_data = numpy.zeros([last_file-first_file-svd_values+1,svd_values+1])
r_svd_data = numpy.zeros([last_file-first_file-svd_values+1,svd_values+1])


q_data = first_data[fnz:,0]
b_q_data = q_data[int(bin_size/2)-1::int(bin_size)]

int_data[0,:] = first_data[fnz:,1]
b_int_data[0,:] = scipy.stats.binned_statistic(q_data,first_data[fnz:,1],statistic='mean',bins=size_binned).statistic




for i in range(first_file+1,first_file+svd_values):
    int_data[i-first_file,:] = numpy.loadtxt(get_full_file(folder,file_root,i))[fnz:,1]
    b_int_data[i-first_file,:] = scipy.stats.binned_statistic(q_data,int_data[i-first_file,:],statistic='mean',bins=size_binned).statistic




for i in range(first_file+svd_values,last_file+1):
    int_data[i-first_file,:] = numpy.loadtxt(get_full_file(folder,file_root,i))[fnz:,1]
    b_int_data[i-first_file,:] = scipy.stats.binned_statistic(q_data,int_data[i-first_file,:],statistic='mean',bins=size_binned).statistic
    #u, s, vh = numpy.linalg.svd(int_data[0:i-first_file,:])
    #tsvd.fit(int_data[0:i-first_file,:])

    #svd_data[i-first_file-svd_values,0] = i
    #svd_data[i-first_file-svd_values,1:] = s[0:10]
    #svd_data[i-first_file-svd_values,1:] = tsvd.singular_values_

    tsvd.fit(b_int_data[0:i-first_file,:])
    #b_u, b_s, b_vh = numpy.linalg.svd(b_int_data[0:i-first_file,:])
    b_svd_data[i-first_file-svd_values,0] = i
    #b_svd_data[i-first_file-svd_values,1:] = b_s[0:10]
    b_svd_data[i-first_file-svd_values,1:] = tsvd.singular_values_


    # svd in reverse direction
    if i%100 == 0:
        for j in range(i,first_file+svd_values-1,-1):
            tsvd.fit(b_int_data[j-first_file-svd_values:i-first_file,:])
            r_svd_data[j-first_file-svd_values,0] = j
            r_svd_data[j-first_file-svd_values,1:] = tsvd.singular_values_


        # plt.figure()
        # for k in range(2,7):
        #     plt.plot(b_svd_data[:i,0],numpy.log(b_svd_data[:i,k]))
        #     plt.plot(r_svd_data[:i,0],numpy.log(r_svd_data[:i,k]),'o')
        # plt.show()


saxs_data = numpy.loadtxt(os.path.join(folder,'plots','{}_plot_data.txt'.format('_'.join(file_root.split('_')[:-1]))))

print(time.time()-ts)
#print('')
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
#ax1.plot(saxs_data[:,0],saxs_data[:,2])
for j in range(2,7):
    #ax2.plot(svd_data[:,0],numpy.log(svd_data[:,j]))
    ax1.semilogy(b_svd_data[:,0],b_svd_data[:,j])
    ax2.semilogy(r_svd_data[:,0],r_svd_data[:,j])
plt.show()


