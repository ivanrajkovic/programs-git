#!/usr/bin/env python
#
# avg-dat.py <path to first file> <number of files to average>


'''
@author: rajkovic
'''


from __future__ import division

import sys
import os
import numpy
import glob
from distutils.dir_util import mkpath


if __name__ == "__main__":

    if not (len(sys.argv)==3):

        sys.exit("""\n\n\tHow-to:
        {0} <path to the first sample> <number of files to average>]
        """.format(os.path.basename(sys.argv[0])))

    first_sample = sys.argv[1]
    num_to_avg= int(sys.argv[2])

    sample_name_ser = '_'.join(first_sample.split('_')[:-2])
    sample_name = "_".join(sample_name_ser.split('_')[:-1])
    sample_ser = sample_name_ser.split('_')[-1]
    num_length = len(os.path.splitext(first_sample)[0].split('_')[-1])

    files_list = sorted(glob.glob('{}*.dat'.format(sample_name_ser)))
    # find the last numer of the series
    #end_num=int(files_list[-1].split('.')[0].split('_')[-1])
    end_num = len(files_list)


    mkpath('Average')
    for aa in numpy.arange(1,end_num-num_to_avg+2,num_to_avg):
        try:
            dat_file = sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+".dat"
            avg_data = numpy.genfromtxt(dat_file)
            # use variance instead of stdev
            avg_data[:,2] = avg_data[:,2]**2
            for zz in numpy.arange(1,num_to_avg):
                dat_file = sample_name_ser+"_0_"+str(int(aa+zz)).zfill(num_length)+".dat"
                dat_data = numpy.genfromtxt(dat_file)
                avg_data[:,1]=avg_data[:,1]+dat_data[:,1]
                avg_data[:,2]=avg_data[:,2]+dat_data[:,2]**2

            avg_data[:,1] = avg_data[:,1]/num_to_avg
            # back to stdev form variance
            avg_data[:,2] = numpy.sqrt(avg_data[:,2])/num_to_avg

            numpy.savetxt(os.path.join('Average',"Ave_"+sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+"-"+str(int(aa+num_to_avg-1)).zfill(num_length)+'.dat'),avg_data,delimiter='\t',fmt='%.6e')
        except:
            print("Problem averaging data!")