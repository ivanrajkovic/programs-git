#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author: rajkovic
'''


import numpy
from scipy import signal
from PIL import Image

from matplotlib import pyplot as plt


im1 = 'data/Cheng_NA_03A_B018_0_001.tif'
im2 = 'data/Cheng_NA_03C_S020_0_006.tif'
im3 = 'data/ald_pt_03A_B230_3_0025.tif'
im4 = 'data/ald_pt_03A_B230_0_0045.tif'

im5 = 'data/3A6_12mgml_50ul_011_0_010.tif'

# c1,c2  = 419,840
# c3,c4  =  419,837


# not on the gap
im6 = 'data/s2opt_vert0p94_S012_0_001.tif'

# c = 419 , 780

# agbe
im7 = 'data/agbe_S030_0_001.tif'



i1 = numpy.array(Image.open(im1))


i1[i1>.8*i1.max()] = 0
#i1[i1<0] = 0

i2 = numpy.array(Image.open(im6))


i2[i2>0.8*i2.max()] = 0
#i2[i2<0] = 0

sy , sx = i1.shape

cx = 419
cy = sy- 840


beam = i1[cy-30:cy+30,cx-30:cx+30]
#import time

#tt =time.time()
#w = signal.correlate2d(i2,beam,mode='same')
w = signal.convolve(i2,beam[::-1,::-1],mode='same')
#print(time.time()-tt)
pp = numpy.unravel_index(numpy.argmax(w, axis=None), w.shape)

print(pp[1], sy - pp[0])

i2[pp]=i2[pp[0]-30:pp[0]+30,pp[1]-30:pp[1]+30].max()

plt.imshow(i2[pp[0]-30:pp[0]+30,pp[1]-30:pp[1]+30])
#plt.imshow(i1)
plt.show()