#!/usr/bin/env python
from __future__ import division

import os
import sys
import glob
import fileinput
import numpy
#from matplotlib.image import imread
from scipy.ndimage import imread
from scipy import ndimage
from matplotlib import pyplot as plt

#work_folder='Y:\\Commissioning2017\\20170216_laser_stability_test\\laser_test\\aperture_8b_onpaper_b'

work_folder='/mnt/home/staff/Commissioning2017/20170216_laser_stability_test/laser_test/focus_run_2/'

file_ext='.png'
#file_ext='.pgm'

os.chdir(work_folder)
img_num = len(glob.glob('*'+file_ext))

m_cen=numpy.zeros([img_num,2])

for ii in range(1,img_num+1):
    #print(ii)
    img_name=glob.glob('*_'+str(ii)+file_ext)[0]
    img_temp=imread(img_name,mode='L')
    m_cen[ii-1]=ndimage.measurements.center_of_mass(img_temp)

m_cen_delta=m_cen-m_cen.mean(0)

#plt.figure()
#plt.hold
#plt.plot(range(0,img_num),m_cen_delta[range(0,img_num),0])
#plt.plot(range(0,img_num),m_cen_delta[range(0,img_num),1])
#plt.show()

numpy.savetxt('mass.txt',m_cen_delta)
