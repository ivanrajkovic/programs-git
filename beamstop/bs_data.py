import os
import numpy as np
import matplotlib.pyplot as plt
import os

os.chdir('C:\\Users\\rajkovic\\Documents\\data\\20160114_Beamstop\\data')

# filter transmission

f_11 = np.zeros(4)
f_9 = np.zeros(4)

f_11[0] =  0.60392                            # 100 um      0001
f_11[1] = f_11[0] ** (2.5)                    # 250 um      0010
f_11[2] = f_11[0] ** (5)                      # 500 um      0100
f_11[3] = f_11[0] ** (12)                     # 1200 um     1000

f_9[0] = 0.40186                              # 100 um
f_9[1] = f_9[0] ** (2.5)                      # 250 um
f_9[2] = f_9[0] ** (5)                        # 500 um
f_9[3] = f_9[0] ** (12)                       # 1200 um


mm=0
 
for file in os.listdir("C:\\Users\\rajkovic\\Documents\\data\\20160114_Beamstop\\data"):
    if file.endswith(".scan") and file.startswith("time"):
        f_max = max(mm,int(file[-8:-5]))
        f_last=file


# file info (enerfy, sensitivity (na/V),filter, beam yes/no)

finf = np.zeros(shape=(4,f_max)) 
finf[:,0] = [11,200,0b0000,1]
finf[:,1] = [11,200,0b0000,1]
finf[:,2] = [11,200,0b0000,1]
finf[:,3] = [11,200,0b0000,1]
finf[:,4] = [11,200,0b0001,1]
finf[:,5] = [11,200,0b0010,1]
finf[:,6] = [11,200,0b0011,1]
finf[:,7] = [11,200,0b0100,1]
finf[:,8] = [11,200,0b0101,1]
finf[:,9] = [11,200,0b0110,1]
finf[:,10] = [11,200,0b0111,1]
finf[:,11] = [11,200,0b0000,0]
finf[:,12] = [11,2,0b0000,0]
finf[:,13] = [11,2,0b0111,1]
finf[:,14] = [11,2,0b1000,1]
finf[:,15] = [11,2,0b1100,1]
finf[:,16] = [11,0.05,0b0000,0]
finf[:,17] = [11,0.05,0b0000,0]
finf[:,18] = [11,0.05,0b1100,1]
finf[:,19] = [11,0.05,0b1111,1]
finf[:,20] = [11,0.05,0b1110,1]
finf[:,21] = [11,0.05,0b0000,0]
finf[:,22] = [11,0.05,0b0000,0]
finf[:,23] = [9,50,0b0000,0]
finf[:,24] = [9,50,0b0000,1]
finf[:,25] = [9,50,0b0000,1]
finf[:,26] = [9,50,0b0001,1]
finf[:,27] = [9,50,0b0010,1]
finf[:,28] = [9,50,0b0011,1]
finf[:,29] = [9,50,0b0100,1]
finf[:,30] = [9,0.5,0b0000,0]
finf[:,31] = [9,0.5,0b0100,1]
finf[:,32] = [9,0.5,0b0101,1]
finf[:,33] = [9,0.5,0b0110,1]
finf[:,34] = [9,0.5,0b0111,1]
finf[:,35] = [9,0.05,0b0000,0]
finf[:,36] = [9,0.05,0b0111,1]
finf[:,37] = [9,0.05,0b1000,1]
finf[:,38] = [9,0.05,0b0000,0]
finf[:,39] = [9,0.05,0b1000,1]
finf[:,40] = [9,0.05,0b1100,1]
finf[:,41] = [9,0.01,0b0000,0]
finf[:,42] = [9,0.01,0b1000,1]
finf[:,43] = [9,0.01,0b1100,1]
finf[:,44] = [9,0.01,0b0000,0]
finf[:,45] = [9,0.01,0b1110,1]
finf[:,46] = [9,0.01,0b1101,1]
finf[:,47] = [9,0.01,0b0000,0]
finf[:,48] = [1,4,0b0000,1]
finf[:,49] = [1,4,0b0000,0]


        
(q,w) = np.loadtxt(f_last).shape
data = np.zeros(shape = (q,f_max))
names = [None] * f_max


for file in os.listdir("C:\\Users\\rajkovic\\Documents\\data\\20160114_Beamstop\\data"):
    if file.endswith(".scan") and file.startswith("time"):
        f_num = int(file[-8:-5])
        names[int(file[-8:-5])-1] = file
        data[:,int(file[-8:-5])-1]=np.loadtxt(file)[:,1]
        
data_norm = data/data.mean(axis=0)      

plt.figure('All normalized')        
#[plt.plot(range(q),data_norm[:,x],label=names[x]) for x in range(0,len(data_norm[0,:]))]
[plt.plot(range(q),data_norm[:,x]) for x in range(0,len(data_norm[0,:]))]
plt.ylim(0.92,1.06)
#plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#plt.legend(bbox_to_anchor=(1, 0.4))


int_11 = [[None,None,None]]

for ii in range(f_max-2):
    if (finf[0,ii] == 11 and finf[3,ii] == 1):
#        ftrans_2 = np.prod(([int(d) for d in str(format(int(finf[2,ii]),'#06b'))[:1:-1]]*f_11)[([int(d) for d in str(format(int(finf[2,ii]),'#06b'))[:1:-1]]*f_11).nonzero()])
        ftrans = 1
        for jj in range(4):
            if format(int(finf[2,ii]),'#06b')[-jj-1] == '1':
                ftrans=ftrans*f_11[jj]
        int_11 = np.append(int_11,[[ii,ftrans*finf[1,ii],np.mean(data[:,ii])]],axis=0)
    if (finf[0,ii] == 11 and finf[3,ii] == 0):
        int_11 = np.append(int_11,[[ii,0,np.mean(data[:,ii])]],axis=0)

int_11 = np.delete(int_11,(0),axis=0)

plt.plot(int_11[:,1],int_11[:,2],'ro')


int_9 = [[None, None, None]]

for ii in range(f_max-2):
    if (finf[0,ii] == 9 and finf[3,ii] == 1):
#        ftrans_2 = np.prod(([int(d) for d in str(format(int(finf[2,ii]),'#06b'))[:1:-1]]*f_9)[([int(d) for d in str(format(int(finf[2,ii]),'#06b'))[:1:-1]]*f_9).nonzero()])
        ftrans = 1
        for jj in range(4):
            if format(int(finf[2,ii]),'#06b')[-jj-1] == '1':
                ftrans=ftrans*f_9[jj]
        int_9 = np.append(int_9,[[ii,ftrans*finf[1,ii],np.mean(data[:,ii])]],axis=0)
    if (finf[0,ii] == 9 and finf[3,ii] == 0):
        int_9 = np.append(int_9,[[ii,0,np.mean(data[:,ii])]],axis=0)

int_9 = np.delete(int_9,(0),axis=0)

plt.plot(int_9[:,1],int_9[:,2],'ro')



# 11keV, no beam
int_11_nb = int_11[int_11[:,1] == 0]

# int_11_nb = int_11[int_11[:,1] == 0,:]
# int_11_nb = int_11[np.where(int_11[:,1] == 0)[0],:]

# 9keV, no beam
int_9_nb = int_9[int_9[:,1] == 0]

# find all no_beam filenames
#list(filter(lambda x: re.search('no_beam',x) ,names))
[x for i, x in enumerate(names) if re.search('no_beam',x)]

# filter list of names, get indeces
[i for i, x in enumerate(names) if x[-6]=='0']