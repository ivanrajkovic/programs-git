#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division

import sys
import argparse
import os
import numpy
import glob


if __name__ == "__main__":

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
        Bin tr-saxs data
        """)

    parser.add_argument('dat_fol', metavar='sample_folder', help='path to the sample analysis folder')
    parser.add_argument('-bf','--bin_first',metavar='N', type=int, default=1 , help='first sample for binning')
    parser.add_argument('-bn','--bin_num',metavar='N', type=int,  default=10 , help='number of samples to bin')


    arg_in = parser.parse_args()


    # set averaging varaibles
    sam_fol = os.path.abspath(arg_in.dat_fol)
    dat_fol = os.path.join(sam_fol,'sastool')
    bin_f = arg_in.bin_first
    bin_n = arg_in.bin_num

    # sample name is the same as folder name
    sam_name = os.path.split(sam_fol)[1]



    ## average files
    #

    # get the number of files:
    sam_n = len([ff for ff in os.listdir(dat_fol) if (ff.startswith(sam_name) and ff.endswith('dat'))])

    # get some dat file names:
    dd = glob.glob(os.path.join(dat_fol,'*001.dat'))

    dig_num = len(os.path.splitext(dd[0])[0].split('_')[-1])

    for aa in range(bin_f, sam_n-bin_n+1, bin_n):
        bin_cur = numpy.genfromtxt(os.path.join(dat_fol,'{}_{}.dat'.format(sam_name,str(aa).zfill(dig_num))))
        # sqaure variance for adding the other ones
        bin_cur[:,2] = bin_cur[:,2]**2
        for mm in range(aa+1,aa+bin_n):
            kk = numpy.genfromtxt(os.path.join(dat_fol,'{}_{}.dat'.format(sam_name,str(mm).zfill(dig_num))))
            bin_cur[:,1] = bin_cur[:,1]+kk[:,1]
            bin_cur[:,2] = bin_cur[:,2]+kk[:,2]**2

        bin_cur[:,1] = bin_cur[:,1]/bin_n
        # square root /N for variance
        bin_cur[:,2] = numpy.sqrt(bin_cur[:,2])/bin_n


        numpy.savetxt(os.path.join('./',"Bin_"+sam_name+"_"+str(int(aa)).zfill(dig_num)+"-"+str(int(aa+bin_n-1)).zfill(dig_num)+'.dat'),bin_cur,delimiter='\t',fmt='%.6e')