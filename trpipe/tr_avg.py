#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division

import sys
import argparse
import os
import numpy
import glob
import re

if __name__ == "__main__":

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
        Average tr-saxs data
        """)


    parser.add_argument('avg',metavar='sample series', type=str, help='sample series to average, range or/and list: 30,33-38,40')
    parser.add_argument('-o','--offset_all',metavar='image_offset',default = '', help='image offset for each series: 0,1,1,-2,1,...\n If first offset is negative, you must use long option --offset_all=-1,....')
    parser.add_argument('-oi','--offset_ind', metavar='offset_individual',action='append',default=[], help='image offest for indivudual series, specified as semicolon separated series,offset pairs: \'22,1;25,-1;33,2\' Can be specified multiple times')
    parser.add_argument('-af','--aa_folder', metavar='aa_folder',default = '.', help='path to the automatic analysis folder, default is current folder')


    arg_in = parser.parse_args()
    #print(arg_in)
    aa_fol = os.path.abspath(arg_in.aa_folder)
    ofs_a = arg_in.offset_all
    ofs_i = arg_in.offset_ind

    #print(ofs_i)

    temp_list= list(arg_in.avg.split(','))

    avg_fold = os.path.join(aa_fol,'Avg_'+arg_in.avg.replace(',','_'))


    #print(temp_list)

    alist = []
    for ll in temp_list:
        if not '-' in ll:
            try:
                cs = int(ll)
                alist.append(cs)
            except:
                print('{} is not an integer number'.format(ll))
                sys.exit()
        else:
            try:
                sf,sl = [int(x) for x in ll.split('-')]
                alist.extend(range(sf,sl+1))
            except:
                print('Cannot make sense of {} range'.format(ll))
                sys.exit()


    if ofs_a == '':
        ofs = [0]*len(alist)
    else:
        try:
            ofs = [int(x) for x in ofs_a.split(',')]
        except:
            print('Cannot make sense of offset data {}'.format(ofs_a))
            sys.exit()

    if ofs_i != []:
        try:
            for ol in ofs_i:
                for ooi in ol.split(';'):
                    osf,oo = [int(x) for x in ooi.split(',')]
                    os_i = alist.index(osf)
                    ofs[os_i] = oo
        except:
            print('Cannot make sense of {}'.format(ofs_i))

    if len(ofs) != len(alist):
        print('number of offsets {} is different than number of sample series {}'.format(len(ofs),len(alist)))
        print(alist)
        print(ofs)
        sys.exit()


    print(alist)
    print(ofs)


    ## average files
    #

    # make output folder
    if not os.path.exists(avg_fold):
        os.makedirs(avg_fold)

    # get all folders in automatic_analysis

    aa_sfol = [f for f in os.listdir(aa_fol) if os.path.isdir(f)]

    # get the folder names

    sflist=[]

    for tn in alist:
        pr = re.compile('.*_T0*{}'.format(tn))
        for sf in aa_sfol:
            if pr.match(sf):
                sflist.append(sf)
                break


    print(sflist)



    # get the number of files:
    sam_n = len([ff for ff in os.listdir(os.path.join(sflist[0],'sastool')) if ff.endswith('dat')])

    # get a dat file name, q-axis, number of points and numper of filename digits:
    dd = glob.glob(os.path.join(sflist[0],'sastool','*001.dat'))

    data_z = numpy.genfromtxt(dd[0])

    q_axis = data_z[:,0]

    p_num = len(q_axis)

    dig_num = len(os.path.splitext(dd[0])[0].split('_')[-1])


    print(sam_n,dd,dig_num)
    print(p_num)

    print(min(ofs),sam_n+max(ofs)+1)


    nofs = numpy.array(ofs)

    for jj in range(min(ofs)+1,sam_n+max(ofs)+1):
        cur_ind_l = jj-nofs

        agv_data = numpy.zeros_like(data_z)
        agv_data[:,0] = q_axis[:]
        cn = 0
        for ii in range(len(sflist)):
            if cur_ind_l[ii]>0 and (cur_ind_l[ii]<sam_n+1):
                # read data
                kk = numpy.genfromtxt(os.path.join(sflist[ii],'sastool',sflist[ii]+'_'+str(cur_ind_l[ii]).zfill(dig_num)+'.dat'))
                agv_data[:,1] = agv_data[:,1]+kk[:,1]
                agv_data[:,2] = agv_data[:,2]+kk[:,2]**2
                cn=cn+1

        agv_data[:,1] = agv_data[:,1]/cn
        # square root /N for variance
        agv_data[:,2] = numpy.sqrt(agv_data[:,2])/cn

        # save data
        numpy.savetxt(os.path.join(avg_fold,"Avg_"+str(int(jj)).zfill(dig_num)+'.dat'),agv_data,delimiter='\t',fmt='%.6e')



