#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division

import argparse
import os
import sys
from PIL import Image
from PIL import ImageDraw
import numpy
#import fabio
from matplotlib import pyplot as plt
import glob

import heapq




if __name__ == "__main__":
    # don't show log errors for negative numbers
    numpy.seterr(all='ignore')

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
        Create a movie of time-resolved data.
        Usage:
        trmovie.py <series number> # works only from analysis foloder
        trmovie.py </path/to/integrated/data/folder or ./first_file.dat>

    """
    )

    parser.add_argument('sam', metavar='sample',
                        help='sample file path or series number')
    parser.add_argument('-df','--data_folder',metavar='PATH',default='' , help='path to the data folder, use of different than expected sample/../../../data')
    parser.add_argument('-fps','--frame_per_second',metavar='N', type=int, default=5 , help='movie frames per second, default=5')
    parser.add_argument('-of','--out_file', default='', help='output movie file, default is <sample_name>_FPS.mp4 in sample analysis folder')
    parser.add_argument('-d','--debug', action='store_true', default=False , help='print debugging info')

    arg_in = parser.parse_args()


    if arg_in.debug == True:
        print(arg_in)

    # get sample analysis folder, sample name, data folder.....
    if os.path.exists(os.path.expanduser(arg_in.sam)):
        if os.path.isdir(arg_in.sam):
            int_fol = os.path.realpath(arg_in.sam)
        else:
            int_fol = os.path.realpath(os.path.dirname(arg_in.sam))

    else:
        try:
            sam_num = int(arg_in.sam)
            ffol = glob.glob('./automatic_analysis/*_T*{}'.format(sam_num))
            int_fol = os.path.realpath(ffol[0])
        except:
            sys.exit('cannot find the sample!')



    if arg_in.data_folder == '':
        df = os.path.realpath(os.path.join(int_fol,'../../../data'))
    else:
        df = os.path.realpath(arg_in.data_folder)

    # sample name
    sam_nm = os.path.split(int_fol)[-1]

    # find buffer name:
    all_dat  = glob.glob(os.path.join(int_fol,'sastool','*.dat'))
    for adat in all_dat:
        nm = '_'.join(os.path.split(adat)[-1].split('_')[:-1])
        if nm != sam_nm:
            buf_nm = nm
            break

    # folder to save individual frames
    frame_fol = os.path.join(int_fol,'frames')

    if not os.path.exists(frame_fol):
        os.makedirs(frame_fol)

    # all sample files
    dat_list = sorted(glob.glob(os.path.join(int_fol,'sastool',sam_nm+'*.dat')))

    # number of samples
    num_sm = len(dat_list)

    # sample int
    sam_intf = os.path.join(df,sam_nm+'_s1.int')
    sam_int = numpy.loadtxt(sam_intf)[num_sm:]

    # all buffer images
    buf_2d_list = sorted(glob.glob(os.path.join(df,buf_nm+'*.tif')))

    # number of buffers
    num_buf = len(buf_2d_list)

    # buf int file
    buf_intf = os.path.join(df,buf_nm+'_s1.int')
    buf_int = numpy.loadtxt(buf_intf)[num_buf:]

    # image normalization factor
    i2_max = numpy.max([numpy.max(buf_int),numpy.max(sam_int)])

    # average buffer 2d files, normalized

    for jj, bf in enumerate(buf_2d_list):
        print('buf {} of {}'.format(jj+1, num_buf))
        if jj == 0:
            buf_avg = numpy.array(numpy.clip(Image.open(bf).convert('I'),0,None))*(i2_max/buf_int[jj])
        else:
            buf_avg = buf_avg + numpy.array(numpy.clip(Image.open(bf).convert('I'),0,None))*(i2_max/buf_int[jj])

    buf_avg = buf_avg/num_buf

    # set 1d plot limits (for both linear and semilog plot)

    d1d = numpy.loadtxt(dat_list[0])
    pl_max = heapq.nlargest(5,d1d[:,1])[-1]
    pl_min = heapq.nsmallest(5,d1d[:,1])[-1]
    ld1d = numpy.log10(d1d[:,1])
    ld1d = ld1d[numpy.isfinite(ld1d)]
    if len(ld1d) > 10:
        pl_lmin = heapq.nsmallest(5,ld1d)[-1]
    else:
        pl_lmin = 1

    for dd in dat_list[1:]:
        d1d = numpy.loadtxt(dd)
        ld1d = numpy.log10(d1d[:,1])
        ld1d = ld1d[numpy.isfinite(ld1d)]
        pl_max = numpy.max([pl_max,heapq.nlargest(5,d1d[:,1])[-1]])
        pl_min = numpy.min([pl_min,heapq.nsmallest(5,d1d[:,1])[-1]])
        if len(ld1d) > 10:
            pl_lmin = numpy.min([pl_lmin,heapq.nsmallest(5,ld1d)[-1]])

    pl_lmin = numpy.max([pl_lmin,-5])
    pl_max_plus = pl_max*(1+numpy.sign(pl_max))
    pl_min_minus = pl_min*(1-numpy.sign(pl_min)*.1)
    pl_lmin_minus = 10**(pl_lmin-0.5)


    pl_title = '|{} - <{}>|'.format(sam_nm,buf_nm)

    # get abs(image) min and max for scaling
    for jj,dd in enumerate(dat_list):
        print('1st pass, sam {} of {}'.format(jj+1,num_sm))
        dd_nm = os.path.split(dd)[-1]

        d2d_nm = dd_nm[:-4]+'.tif'
        ii = os.path.join(df,d2d_nm)
        i2d = numpy.array(numpy.clip(Image.open(ii).convert('I'),0,None))*(i2_max/sam_int[jj])-buf_avg

        if jj == 0:
            im_min = numpy.min(numpy.abs(i2d))
            im_max = heapq.nlargest(10,(numpy.abs(i2d).flatten()))[-1]
        else:
            im_min = numpy.min([numpy.min(numpy.abs(i2d)),im_min])
            im_max = numpy.max([heapq.nlargest(10,(numpy.abs(i2d).flatten()))[-1],im_max])

    # don't set minimum too low, it might be zero
    im_min = numpy.max([im_min,1e-5])

    im_lmin = numpy.log10(im_min)
    im_lmax = numpy.log10(im_max)

    for jj,dd in enumerate(dat_list):
        d1d = numpy.loadtxt(dd)

        dd_nm = os.path.split(dd)[-1]

        d2d_nm = dd_nm[:-4]+'.tif'

        ii = os.path.join(df,d2d_nm)
        i2d = numpy.array(numpy.clip(Image.open(ii).convert('I'),0,None))*(i2_max/sam_int[jj])-buf_avg

        # get image max, using first image only for now
        # skip top 10 pixels, might be hot pixels
        # if jj == 0:
        #     temp_2d = 1*i2d
        #     s = temp_2d.flatten()
        #     s.sort()
        #     im_lim = 1.2*numpy.log(s[-10])

        f_num = dd_nm.split('_')[-1].split('.')[0]
        print('2nd pass, sam {} of {}'.format(jj+1,num_sm))
        fig,axs = plt.subplots(1,2,figsize=(12,6))
        im0 = axs[0].imshow(numpy.log10(numpy.clip(numpy.abs(i2d),im_min,None)),cmap = 'nipy_spectral', clim=(im_lmin,im_lmax))
        axs[0].axis('off')
        #im0 = axs[0].imshow(i2d,cmap = 'nipy_spectral', clim=(-1*pl_max,pl_max))
        cbar = plt.colorbar(im0,ax=axs[0],fraction=.03)
        #cbar.ax.yaxis.tick_left()
        cbar.ax.tick_params(axis='both', which='major', labelsize=10)
        cbar.ax.set_yticklabels(cbar.ax.get_yticklabels())    #,rotation=-10)


        # plot ABSOLUTE value of 1d subtracted data, semilog plot
        axs[1].semilogy(d1d[d1d[:,1]>0,0],d1d[d1d[:,1]>0,1],'.',c='b',label='pos')   ,#'-+')    #linewidth=1)
        axs[1].semilogy(d1d[d1d[:,1]<0,0],-1*d1d[d1d[:,1]<0,1],'.',c='r',label='neg')
        axs[1].legend(numpoints=1)
        axs[1].set_xlabel('q [$A^{-1}$]')
        axs[1].set_ylabel('Int')
        #axs[1].set_ylim([pl_min_minus,pl_max_plus])
        axs[1].set_ylim([pl_lmin_minus,pl_max_plus])

        plt.tight_layout(pad=2.5,w_pad=1,h_pad=0)
        axs[0].text(0.1,1.03,f_num,color='red',fontsize=16,transform=axs[0].transAxes)
        #axs[0].text(1.04,.12,'log',fontsize=14,transform=axs[0].transAxes)
        tfs = 20
        if len(pl_title) > 60:
            tfs = 10
        axs[1].text(-0.7,1.03,pl_title,color='magenta',fontsize=tfs,transform=axs[1].transAxes)



        plt.savefig(os.path.join(frame_fol,f_num+'.png'))
        plt.close()


# make movie
    # set movie output file
    if arg_in.out_file == '':
        of = os.path.join(int_fol,'{}_{}fps.mp4'.format(sam_nm,arg_in.frame_per_second))
    else:
        of = arg_in.out_file


    movie_cmd = "ffmpeg -framerate {} -pattern_type glob -i \"{}/*.png\" -y -c:v libx264 -f mp4 {}".format(arg_in.frame_per_second,frame_fol,of)
    os.system(movie_cmd)

    print("\n\n ** movie is saved as:\n {} \n ** \n".format(of))




