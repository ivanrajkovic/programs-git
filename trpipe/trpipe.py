#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division

import time
# starttime = time.time()

import sys
import os
import re
import distutils.spawn
from distutils.dir_util import mkpath
import shutil
import fileinput
#import subprocess
import subprocess32
import glob
import argparse
import getpass


# importing modules later, while sastool is running 

# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt
# import numpy
# from heapq import nlargest, nsmallest
mod_imp = 0


def set_path():
    global autorg
    global datgnom
    global sastool
    
    atsas_path='/mnt/home/staff/SW/ATSAS'
    exe_path='/mnt/home/sw/bin:{}'.format(os.path.join(atsas_path,'bin'))
    ld_path=os.path.join(atsas_path,'lib64/atsas')
    
    try:
        os.environ['LD_LIBRARY_PATH']=os.environ['LD_LIBRARY_PATH']+':'+ld_path
    except:
        os.environ['LD_LIBRARY_PATH']=ld_path
    
    try:
        os.environ['PATH']=os.environ['PATH']+':'+exe_path
    except:
        os.environ['PATH']=exe_path
            
    
    sastool = distutils.spawn.find_executable('sastool')

    autorg = distutils.spawn.find_executable('autorg')
    
    datgnom = distutils.spawn.find_executable('datgnom4')
    if datgnom == None:
        datgnom = distutils.spawn.find_executable('datgnom')
    
    if autorg == None:
        #sys.exit('\'autorg\' not found, please add it to the path.')
        print('\n \033[0;37;41m\033[1m\033[4m \'autorg\' not found, please add it to the path.\033[0;0m \n')
    if datgnom == None:
        #sys.exit('\'datgnom\' not found, please add it to the path.')
        print('\n \033[0;37;41m\033[1m\033[4m \'datgnom\' not found, please add it to the path.\033[0;0m \n')
    if sastool == None:
        sys.exit('\n \033[0;37;41m\033[1m\033[4m \'sastool\' not found, please add it to the path.\033[0;0m \n')
        #print('\'sastool\' not found, please add it to the path.')
    
        
def find_image(folder,num):
    im_list=[]
    # find the image filename for the given series number
    rrr = re.compile('.*_T'+str(num).zfill(3)+'_0+1\.tif')
    for file in os.listdir(folder):
        if rrr.match(file):
            im_list.append(file)
    if len(im_list) == 0:
        sys.exit("\nNo files found!\n")
    elif len(im_list) > 1:
        print("\n\033[0;37;41m\033[1m\033[4m Two or more files found! \033[0;0m")
        sys.exit(im_list)
    else:
        return im_list[0]
        

def set_filenames(bn,sample_file,buffer_file,sastool_sfol,totfiles_sfol,other_sfol):
    if bn != '':
        sub_orig = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'.sub')
        sub_file = os.path.join(totfiles_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-1])+'_'+buffer_file.split('_')[-2]+"_sub.dat")
        btot_orig = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(buffer_file))[0]+'.tot') # same as buf_tot
        btot_file = os.path.join(totfiles_sfol,'_'.join(os.path.basename(buffer_file).split('_')[:-1])+"_tot.dat")
        buffer_log = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(buffer_file))[0]+'.log')
        rg_file_name = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-1])+'_'+buffer_file.split('_')[-2]+"_sub_rg.log")
        dg_file_name = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-1])+'_'+buffer_file.split('_')[-2]+"_sub_datgnom.log")
        dg_out_file = os.path.join(sastool_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-1])+'_'+buffer_file.split('_')[-2]+'_sub.out')
    else:
        sub_orig=''
        sub_file=''
        btot_orig=''
        btot_file=''
        buffer_log=''
        rg_file_name=''
        dg_file_name=''
        dg_out_file=''
    
    stot_orig = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'.tot')
    stot_file = os.path.join(totfiles_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-1])+"_tot.dat")
    sample_log = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'.log')
    sam_tot = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'.tot')
    
    return(sub_orig,sub_file,btot_orig,btot_file,buffer_log,rg_file_name,dg_file_name,dg_out_file,stot_orig,stot_file,sample_log,sam_tot)


def set_plot_names(bn,other_sfol,sample_file,buffer_file):
    if bn != '':
        img_gr = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-1])+'_'+buffer_file.split('_')[-2]+"_image_graphs.png")
        dat_gr = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-1])+'_'+buffer_file.split('_')[-2]+"_data_graphs.png")
        dat_an_gr = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-1])+'_'+buffer_file.split('_')[-2]+"_data_analysis_graphs.png")
        bin_graph = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-1])+'_'+buffer_file.split('_')[-2]+"_bin_graph.png")
        row_an_gr_1 = os.path.join(other_sfol,"_".join(os.path.basename(sample_file).split('_')[:-3])+'_{}'+'_'+buffer_file.split('_')[-2]+"_row_analysis_graphs.png")
    else:
        img_gr = os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+"_image_graphs.png")
        dat_gr = os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+"_data_graphs.png")
        bin_graph = os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+"_bin_graph.png")
        dat_an_gr = os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+"_data_analysis_graphs.png")
        row_an_gr_1 = os.path.join(other_sfol,"_".join(os.path.basename(sample_file).split('_')[:-3])+'_{}'+"_row_analysis_graphs.png")
    
    return(img_gr,dat_gr,dat_an_gr,row_an_gr_1,bin_graph)

    
def get_acq_t(ff):
    # get acquisition time of the first sample file
    #print(os.path.splitext(sample_file)[0]+'.prp')
    f_acq_time = '0000'
    try:
        for line in fileinput.input(os.path.splitext(ff)[0]+'.prp'):
            if line.startswith('Time this file was written'):
                f_acq_time = line.split('written: ')[-1][:-1]
    except:
        try:
            with open('_'.join(ff.split('_')[:-1])+'_.log') as logfile:
                ll = logfile.readlines()[1]
                f_acq_time = ' '.join(ll.split()[2].split('T'))
                
        except:
            print('\n \033[0;37;41m\033[1m\033[4m Cannot get sample file info (.prp or .log file) \033[0;0m \n')
    return(f_acq_time)

def get_exp_t(ff):
    # get exposure time of the first sample file
    f_exp_time = '-'
    try:
        for line in fileinput.input(os.path.splitext(ff)[0]+'.prp'):
            if line.startswith('Exposure time'):
                f_exp_time = float(line.split('=')[-1].strip())
    except:
        try:
            with open('_'.join(ff.split('_')[:-1])+'_.log') as logfile:
                ll = logfile.readlines()[1]
                f_exp_time = float(ll.split()[1])
        except:
            print('\n \033[0;37;41m\033[1m\033[4m Cannot get exposure time info from .prp file \033[0;0m \n')
    
    # write exposure time in s or ms, depending on the value (if found):
    if f_exp_time != '-':
        try:
            if f_exp_time < 0.5:
                f_exp_time = '{:g}ms'.format(f_exp_time*1000)
            else:
                f_exp_time = '{:g}s'.format(f_exp_time)
        except:
            # something failed, f_exp_time might not be a number, just keep it as is 
            pass
    
    return(f_exp_time)
    

def make_image_graphs():
    global sample_file,buffer_file,bn,sample_log,buffer_log,variance_num,var_factor,img_gr, int_comment
    plt.figure(num=None, figsize=(16,4))


    try:
        plt.subplot(245)
        sint_file = sample_file.split('_00')[0]+'_0s1.int'
        sam_num = int(sum(1 for line in open(sint_file))/2)
        with open(sint_file) as ssii:
            sint_str = ssii.readlines()[sam_num:]
        
        sint_data = map(lambda x: float(x.split()[0]), sint_str)
        int_norm = 10**(len(str(int(max(sint_data))))-1)
        plt.title("Trans. Int./{:.0e}".format(int_norm))
        plt.plot(range(1,sam_num+1),numpy.array(sint_data)/int_norm,'bo-')
        plt.locator_params(axis='y',nbins=4)
        ti_min = numpy.min(numpy.array(sint_data)/int_norm)
        ti_max = numpy.max(numpy.array(sint_data)/int_norm)
        
        if bn != '':
            bint_file = buffer_file.split('_00')[0]+'_0s1.int'
            buf_num = int(sum(1 for line in open(bint_file))/2)
            with open(bint_file) as bbii:
                bint_str = bbii.readlines()[buf_num:]
            
            bint_data = map(lambda x: float(x.split()[0]), bint_str)
            plt.plot(range(1,buf_num+1),numpy.array(bint_data)/int_norm,'ro-')
            plt.locator_params(axis='y',nbins=4)
            
            # check if sample intensity is much different than buffer (injection failure?)
            if abs((numpy.array(sint_data).mean()/numpy.array(bint_data).mean())-1) > .2:
                int_comment = "Check for injection failure or bubbles!"
            
            
            ti_min = numpy.min([ti_min,numpy.min(numpy.array(bint_data)/int_norm)])
            ti_max = numpy.max([ti_max,numpy.max(numpy.array(bint_data)/int_norm)])
            
        plt.xlim([0.5,sam_num+.5])
        plt.ylim(ti_min-0.05*ti_max,ti_max+0.05*ti_max)
        plt.subplot(241)
    except:
        plt.subplot(141)
        
        plt.title("Variance")
        if bn != '':
            plt.plot(range(2, len(buf_var)+1), buf_var[1:],'ro-')
        plt.plot(range(2, len(sam_var)+1), sam_var[1:],'bo-')
        plt.plot(range(1, len(sam_var)+1),variance_num*var_factor*numpy.ones(len(sam_var)),'g--')
        plt.xlim([0.5,len(sam_var)+.5])
        plt.locator_params(axis='y',nbins=4)
        
        
        

    
    plt.subplot(142)
    #plt.hold(True)
    plt.title("Tot file(s)")
    plt.locator_params(nbins=4)
    if bn != '':
        plt.semilogy(buf_tot_data[:,0], buf_tot_data[:,1],'r')
    plt.semilogy(sam_tot_data[:,0],sam_tot_data[:,1],'b')
    
    
    plt.subplot(143)
    plt.title(r"Dat files q<0.2*q$_{max}$")
    #plt.hold(True)
    for ii in dat_list:
        jj = numpy.loadtxt(ii)
        plt.plot(jj[:,0],jj[:,1])
        
            
            
    plt.xlim(0,max(jj[:,0]*.2))
    plt.locator_params(nbins=4)
    
    plt.subplot(144)
    plt.title("Image Rg")
    
    if bn != '':
        plt.errorbar(range(1, len(rg_single)+1), rg_single , yerr= rg_single_del)
        plt.locator_params(nbins=4)
   
    
    plt.tight_layout(pad=0.2)
    
    plt.savefig(img_gr)


def make_data_graphs():
    global dd,sub_file,stot_file, dat_gr, other_sfol, dat_gr_html
    # load i vs q data
    try:
        dd = numpy.loadtxt(sub_file,dtype=numpy.float32)
    except:
        dd = numpy.loadtxt(stot_file,dtype=numpy.float32)
    
    plt.figure(num=None, figsize=(12,4))
    
    if max(dd[:,1])>0.00001:
        plt.subplot(131)
        plt.title("log(i) vs. q")
        plt.locator_params(numticks=3)
        plt.semilogy(dd[:,0],dd[:,1],'b')
        #plt.grid(True)
        
        
        plt.subplot(132)
        plt.title("log(i) vs. log(q)")
        plt.loglog(dd[:,0],dd[:,1],'b')
        plt.xlim(dd[numpy.nonzero(dd[:,1])[0][0],0]*0.7,max(dd[:,0])*1.3)
        #plt.grid(True)
        plt.locator_params(numticks=5)
        
    else:
        plt.subplot(131)
        plt.title("i vs. q")
        plt.plot(dd[:,0],dd[:,1],'b')
        plt.locator_params(nbins=4)
        
        plt.subplot(132)
        plt.title("i vs. log(q)")
        plt.semilogx(dd[:,0],dd[:,1],'b')
        plt.locator_params(numticks=4)
    
    
    
    plt.subplot(133)
    plt.title("Kratky")
    plt.plot(dd[:,0],dd[:,1]*(dd[:,0]**2),'b')
    plt.ylim(min(dd[:int(0.9*len(dd)),1]*dd[:int(0.9*len(dd)),0]**2),1.1*max(dd[:int(0.9*len(dd)),1]*dd[:int(0.9*len(dd)),0]**2))
    #plt.grid(True)
    plt.locator_params(nbins=4)
    
    plt.tight_layout(pad=0.2)
    
    plt.savefig(dat_gr)
    
    
    
def make_analysis_graphs():        
    global dd, sub_file,rg_start,rg_end,dg_out_file,other_sfol,sample_file,buffer_file,dat_an_gr,gnom_rg_sub,dmax_sub
    
    plt.figure(num=None, figsize=(8,4))
    plt.subplot(121)
    if rg_sub[0] == 0:
        plt.title('No good Rg')
        plt.plot([0],[0])
        plt.locator_params(nbins=4)
    else:
        plt.title('Guinier plot')
        plt.plot(dd[rg_start:rg_end+1,0]**2,numpy.log10(dd[rg_start:rg_end+1,1]),'bo',markersize=10)
        plt.figtext(0.28,0.7,'Rg: {:.1f} \nI$_0$: {}'.format(rg_sub[0],i0_sub[0]), fontsize=20)
        #plt.plot(dd[rg_start:rg_end+1,0],dd[rg_start:rg_end+1,1])
        try:
            coefs = numpy.lib.polyfit(dd[rg_start:rg_end+1,0]**2, numpy.log10(dd[rg_start:rg_end+1,1]), 1) 
            fit_y = numpy.lib.polyval(coefs, dd[rg_start:rg_end+1,0]**2)
            plt.plot(dd[rg_start:rg_end+1,0]**2, fit_y, 'r--',linewidth=3.0)
        except:
            print('problem plotting coef')
        
        plt.ylim(0.99*min(numpy.log10(dd[rg_start:rg_end+1,1])),1.01*max(numpy.log10(dd[rg_start:rg_end+1,1])))
        plt.xlim(0.9*dd[rg_start,0]**2,1.1*dd[rg_end,0]**2)
        plt.locator_params(nbins=4)
        
        
    plt.subplot(122)
    if gnom_rg_sub == 0:
        plt.title('No good P(r)')
        plt.plot([0],[0])
        plt.locator_params(nbins=4)
        
    else:
        # load Pr from datgnom .out file
        
        regexp = r"(?<!\d)  (\d\.\d+E[+|-]\d+)  (\d\.\d+E[+|-]\d+)\s\s(\d\.\d+E[+|-]\d+)\n"
        pr_xy = numpy.fromregex(dg_out_file,regexp,[('prx', numpy.float32),('pry', numpy.float32),('errpry', numpy.float32)])
                
        
        
        plt.title('P(r) distribution')
        plt.plot(pr_xy['prx'],pr_xy['pry'],'b')
        plt.figtext(0.82,0.7,'Rg: {:.1f} \nD$_m$: {:.0f}'.format(gnom_rg_sub,dmax_sub), fontsize=20)
        plt.locator_params(nbins=4)
        
        # export log(i) vs q^2 and fitting data to txt file
        try:
            exp_rg_txt = [dd[:3*rg_end,0]**2,numpy.log(dd[:3*rg_end,1]),numpy.lib.polyval(coefs, dd[:3*rg_end,0]**2)]
            numpy.savetxt(os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'_'+buffer_file.split('_')[-3]+"_guiner.txt"),numpy.transpose(exp_rg_txt),header='q^2\t\tln(i)\t\tlin_fit')
        except:
            try:
                exp_rg_txt = [dd[:,0]**2,numpy.log(dd[:,1])]
                numpy.savetxt(os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'_'+buffer_file.split('_')[-3]+"_guiner.txt"),numpy.transpose(exp_rg_txt),header='q^2\t\tln(i)')
            except:
                exp_rg_txt = [dd[:,0]**2,numpy.log(dd[:,1])]
                numpy.savetxt(os.path.join(other_sfol,os.path.splitext(os.path.basename(sample_file))[0]+"_guiner.txt"),numpy.transpose(exp_rg_txt),header='q^2\t\tln(i)')
        
        #print(exp_rg_txt)
    
    plt.tight_layout(pad=0.2)
    plt.savefig(dat_an_gr)



def make_bin_graph():
    global avgfiles_sfol,other_sfol,bin_gr
    plt.figure(num=None, figsize=(4,4))
    plt.title('Binned data, q<0.2*q$_{max}$')
    for ff in glob.glob(os.path.join(avgfiles_sfol,'*.dat')):
         ff_dat = numpy.genfromtxt(ff)
         plt.plot(ff_dat[:,0],ff_dat[:,1])
    plt.xlim(0,max(ff_dat[:,0]*.2))
    plt.tight_layout(pad=0.2)
    plt.savefig(bin_gr)

    
    
    


# HTML parts :

html_start = """<!DOCTYPE html>
    <html>
        <head>
            <title>{}</title>"""
            
html_css = """
            <meta charset="UTF-8">
            <style media="screen" type="text/css">
                                    
                table a:link {
                        color: #0000aa;
                        text-decoration:none;
                            }
                table a:visited {
                        color: #0000aa;
                        text-decoration:none;
                            }
                table a:active,
                        table a:hover {
                        color: #bd5a35;
                        text-decoration:underline;
                            }
                table tr:hover td {
                        background: #f2f2f2;  
                            }
                table, td, th {
                        border: 1px solid grey;
                        text-align: center;
                        font-family: arial;
                        font-size: 0.9em;
                        }
                td, th {
                        padding: 10px;
                        }
                thead {
                        font-size: 1.3em;
                        } 
                tfoot td{
                        font-size: 1.3em;
                        text-align: left;
                        }
                .foot_font {
                        font-size: 0.7em;
                    }
                .num_font {
                        font-size: 1.6em;
                    }
                .b_col {
                        color: red;
                    }
                .s_col {
                        color: blue;
                    }
                thead th
                    {
                    position: sticky;
                    top: 21px;
                    background-color: #eeffff;
                    }
                #divfix {
                    top: 0;
                    left: 30;
                    position: fixed;
                    z-index: 30;
                    background-color: white;
                    height: 21px;
                    width: 100%;
                    }
                .hidden { display: none }
            </style>"""
            
html_script = """
    
    <script type="text/javascript">
    $(function() {
    
        $("input:checkbox:not(:checked)").each(function() {
        var checkbox = $(this),
            header = $(checkbox.data("column")),
            table = header.closest("table"),
            index = header.index() + 1, // convert to CSS's 1-based indexing
            selector = "tbody tr td:nth-child(" + index + ")",
            column = table.find(selector).add(header);
    
        column.toggleClass("hidden");
        });
    
    
    
        $("input:checkbox").on("change", function(e) {
        var checkbox = $(this),
            header = $(checkbox.data("column")),
            table = header.closest("table"),
            index = header.index() + 1, // convert to CSS's 1-based indexing
            selector = "tbody tr td:nth-child(" + index + ")",
            column = table.find(selector).add(header);
    
        column.toggleClass("hidden");
        
        });  
    })
    
    </script>
        </head>"""


html_table_header =         """<body>

            <table>
                <thead>
                    <tr>
                        <th id="column-time"> Time </th>
                        <th> Sub (or tot) file </th>
                        <th id="column-expinf"> Nexp*Texp&nbsp;(reject) <br>buf/sam </th>
                        {}
                        <th id="column-imgr"> Image graphs <br> blue=sample, red=background (buffer) </th>
                        <th id="column-avg"> Binned graphs </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                </table>"""

html_atsas_input_full="""<input type="checkbox" data-column="#column-guin" checked="checked" />Guinier
    """

html_atsas_th_full="""    <th id="column-guin"> Guinier R<sub>g</sub>&nbsp;&&nbsp;I<sub>0</sub> </th>
    """

html_input_full ="""<input type="checkbox" data-column="#column-danal" checked="checked" />Data analysis
    """
    
html_th_full="""<th id="column-danal"> Data analysis graphs </th>"""


html_footer = """
                </tbody>
                </table>
                <br>
            This information is meant for a quick data check only.  Please analyse your data carefully by hand!&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='foot_font'>Table generated by Saxspipe using {}{}.</span>
        
        </body>
    </html>
            """



#######################################################
############### main program ##########################
#######################################################

if __name__ == "__main__":
    try:
    # to get command line options in a file:
        aaaa = open("/tmp/trspipe_{}.txt".format(getpass.getuser()), "w")
        aaaa.write(' '.join(sys.argv[:]))
        aaaa.write('\n')
        aaaa.close()
    except:
        pass

    
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
        If only a sample file is given, program will analyze it without a buffer/background subtraction.
        
        
        Sample and buffer can be specified in three different ways:
        
        1) full path to the file: %(prog)s /path/to/my/data/file.tif
        2) relative path to the file: %(prog)s ../path/data/file.tif
        3) series number of the file: %(prog)s 6 4 
            sample number can also be a range: %(prog)s 10-20 4
            for case 3), data are expected to be in ../data/ folder
        
        In case 1), results will be saved in /path/to/my/data/../analysis/automatic_analysis/
        In case 2) and 3), results will be saved in ./automatic_analysis/
    
        If option -m or --mini is specified, trpipe.py will work in 'mini mode' without atsas programs.
        
        If option -u or --micro is specified, trpipe.py will work in 'micro mode' and just run sastool without creating an html table.
        --mini and --micro options are mutualy exclusive.
    """
    )
    
    parser.add_argument('sam', metavar='sample',
                        help='sample file path or series number')
    parser.add_argument('buf', metavar='buffer', nargs='?',
                        help='buffer file path or series number',default='')
    pargr = parser.add_mutually_exclusive_group()
    pargr.add_argument('-m','--mini', action='store_true', default=False , help='no atsas')
    pargr.add_argument('-u','--micro', action='store_true', default=False , help='just run sastool, don\'t create an html table')
    parser.add_argument('-df','--data_folder',metavar='PATH',default='../data' , help='path to the data folder, used only when sample is specified by series number')
    parser.add_argument('-bf','--buffer_folder',metavar='PATH',default=None , help='path to the buffer folder if different than sample/data folder, used only when sample is specified by series number')
    parser.add_argument('-fb','--first_bin',metavar='N', type=int, default=1 , help='first sample for binning, default=1')
    parser.add_argument('-nb','--num_bin',metavar='N', type=int,  default=10 , help='number of samples to bin, default=10')
    parser.add_argument('-d','--debug', action='store_true', default=False , help='print debugging info')
    
    arg_in = parser.parse_args()
    
    
    if arg_in.debug == True:
        print(arg_in)
    
    
    # set averaging varaibles
    bin_f = arg_in.first_bin
    bin_n = arg_in.num_bin
    
    # analysis folder: current if sample file specified as relative path or series number, next to 'data' if sample file is apsolute path 
    
    if os.path.isabs(os.path.expanduser(arg_in.sam)):
        data_fol = os.path.realpath(os.path.dirname(arg_in.sam))
        try:
            analysis_fol = os.path.normpath(os.path.join(data_fol,'../analysis'+data_fol.split('/')[-1].split('data')[1]))
            if not os.path.isdir(analysis_fol):
                analysis_fol = os.path.normpath(os.path.join(data_fol,'../analysis'))
        except:
            analysis_fol = os.path.normpath(os.path.join(data_fol,'../analysis'))
        
    elif os.path.isfile(arg_in.sam):
        data_fol = os.path.realpath(os.path.abspath(os.path.dirname(arg_in.sam)))
        analysis_fol = os.path.abspath('.')
    else:
        data_fol = os.path.abspath(arg_in.data_folder)
        analysis_fol = os.path.abspath('.')
    
    if os.path.isfile(arg_in.buf):
        buf_fol = os.path.realpath(os.path.abspath(os.path.dirname(arg_in.buf)))
    elif arg_in.buffer_folder is None:
        buf_fol = data_fol
    else:
        buf_fol = arg_in.buffer_folder
    
    # set paths and folders
    set_path()
    
    # send commant output to devnull
    FNULL = open(os.devnull, 'w')
    #    
    ## find sample/buffer names
    #
    # deterine if we have numbers, range or strings as sample/buffer arguments
    
    
    # find the sample name; first check if it a series number, range or a file name
    series_range = False
    sn = ''
    bn = ''
    buffer_file = ''
    sample_file = ''
    sam_list = []
    
    
    
    # sam is a number ?
    if re.match('^\d+$',arg_in.sam):
        sn = int(arg_in.sam)
        sn_end = sn
        if arg_in.debug:
            print('\tSpecified sample series number {}\n'.format(sn))
    else:
        if arg_in.debug:
            print('\tSpecified sample is not a number:\n')
        
    
    # sam is a range ?
    if re.match('^\d+-\d+$',arg_in.sam):
        series_range = True
        [sn,sn_end] = [int(x) for x in arg_in.sam.split('-')]
        if arg_in.debug:
            print('\tSpecified sample range {}-{}\n'.format(sn,sn_end))
        
    # if neither of previuos two, try file name:
    
    if sn == '':
        try:
            sn = int(arg_in.sam.split('_')[-2][1:])
            sn_end = sn
            sample_file = os.path.abspath(arg_in.sam)
            if arg_in.debug:
                print('\tSpecified sample name {}\n'.format(sample_file))
                print('\tSample series number from file name {}\n'.format(sn))
        except:
            print('\n \033[0;37;41m\033[1m\033[4m Sample file not found! \033[0;0m \n')
            sys.exit()
    
    
    if arg_in.buf !='':
        # buf is a number ?
        if re.match('^-?\d+$',arg_in.buf):
            bn = int(arg_in.buf)
            buffer_file = os.path.join(buf_fol,find_image(buf_fol,bn))
            if arg_in.debug:
                print('\tSpecified buffer series number {}\n'.format(bn))
                print('\tBuffer file from series number {}\n'.format(buffer_file))
            
        else:
            try:
                bn = int(arg_in.buf.split('_')[-2][1:])
                buffer_file = os.path.realpath(os.path.abspath(arg_in.buf))
                if arg_in.debug:
                    print('\tSpecified buffer name {}\n'.format(buffer_file))
                    print('\tBuffer series number from file name {}\n'.format(bn))
            except:
                print('\n \033[0;37;41m\033[1m\033[4m Buffer file not found! \033[0;0m \n')
                sys.exit()
    
    # create sample list
    
    # if we already know sample filename
    if sample_file != '':
        
        sam_list = [sample_file]
        
    else:
        for sn_i in range(sn,sn_end+1):
            # find sample filename
            try:
                #print(data_fol,sn_i)
                sample_file=os.path.join(data_fol,find_image(data_fol,sn_i))
                if arg_in.debug:
                    print('\tSample name from series number {}\n'.format(sample_file))
                    
                sam_list.append(sample_file)
                
                
            except:
                print('Sample file {} not found!\n'.format(sn_i))
                
    
            
            
    if arg_in.debug:
        print(sam_list,len(sam_list))
        print('\n')
        print(buffer_file)
        print('\n')
        

    
    if len(sam_list) == 0:
        print('\n \033[0;37;41m\033[1m\033[4m No sample files found! \033[0;0m \n')
        sys.exit()
    
    
    timestart = time.time()
    while not os.path.isfile(sam_list[0]):
        if arg_in.debug:
            print("Waiting for first file: {}".format(all_list[0][0]))
        if (time.time()-timestart) > 120:
            if arg_in.debug:
                print("No file after 2 minutes, exiting!")
            sys.exit()
        time.sleep(1)
    
    
    
    
    
    #
    ## prepare folders, integ.mpp, and run sastool
    #
    
    # making subfolders for each sample, running separate sastool in each sample/sastool folder
    
   
    # this is where sample data goes for html table
    html_row=""
   
    for sam_i in sam_list:
        sam_prfx = '_'.join(os.path.splitext(os.path.basename(sam_i))[0].split('_')[:-1])
        
        try:
            sn = int(sample_file.split('_')[-2][1:])
        except:
            print('Something wrong with sample filename!')
            sys.exit()
            
        if arg_in.debug:
            print(sample_file,buffer_file)
            print(sn,bn)
        
        # these will be created if they don't exist
        aa_fol = os.path.join(analysis_fol,'automatic_analysis')
        results_fol = os.path.join(aa_fol,sam_prfx)
        sastool_sfol = os.path.join(results_fol,'sastool')
        other_sfol = os.path.join(results_fol,'other_files')
        totfiles_sfol = os.path.join(aa_fol,'tot_files')
        avgfiles_sfol = os.path.join(results_fol,'bin')
        resf_html = os.path.join(aa_fol,'tr_results.html')
        resm_html = os.path.join(aa_fol,'tr_results-mini.html')
        t_mpp = os.path.join(sastool_sfol,'integ.mpp')
        
    
        if arg_in.debug:
            print('\n\n\tData folder is: '+'\n'+data_fol+'\n')
            print('\tAnalysis folder is: '+'\n'+analysis_fol+'\n')
        
        
        mkpath(sastool_sfol)
        mkpath(other_sfol)
        mkpath(totfiles_sfol)
        mkpath(avgfiles_sfol)
    
    
        # copy integ.mpp to the automatic_analysis folder as template.mpp, remove all lines starting with '-f' or '#', and all empty lines
        # if the buffer is empty comment out '-s'; if not, uncomment '-s' if it was commented out
        # add full path to the mask file, if necessary
       
    
    

    
        print('\nworking on ')
        print(os.path.basename(sam_i))
        print('\n')
        if os.path.isfile(os.path.join(analysis_fol,'integ.mpp')):
            shutil.copy(os.path.join(analysis_fol,'integ.mpp'),t_mpp)
        else:
            print("\nCan't find integ.mpp!\n")
            sys.exit()
        
        for line in fileinput.input(t_mpp, inplace = 1):
            #if line.startswith('-s') or line.startswith('#-s') or line.startswith('# -s'):
            if re.match('^#? *-s',line):
                line = re.sub('# *-s','-s',line)
                if bn=='':
                    line = '#'+line
            if line.startswith('-m'):
                mask_file = line.split()[-1]
                if os.path.isabs(mask_file) == False:
                    line = '-m yes {}\n'.format(os.path.relpath(os.path.join(analysis_fol,mask_file),sastool_sfol))
                else:
                    line = '-m yes {}\n'.format(os.path.relpath(mask_file,sastool_sfol))
            if (not line.endswith('\n')):
                line = line + '\n'
            if (not line.startswith('-f') and not line.startswith('#') and not re.match(r'^\s*$', line)):        
                sys.stdout.write(line)
    
        
    
        # add files to be processed
        ff = open(t_mpp,'a')
        if bn == '':
            ff.write('-f {}\n'.format(sam_i))
        else:
            ff.write('-f {} {}\n'.format(sam_i,buffer_file))
        ff.close()

        # run sastool in sastool subfolder
        #os.chdir(sastool_sfol)
        
        psas = subprocess32.Popen([sastool,t_mpp],cwd=sastool_sfol,stdout=FNULL, stderr=subprocess32.STDOUT)

        #import modules while sastool is working
        if mod_imp == 0:
            import matplotlib
            matplotlib.use('Agg')
            import matplotlib.pyplot as plt
            import numpy
            from heapq import nlargest, nsmallest
            
            mod_imp=1
            
    
        psas.communicate()
    
        print('sastool finished \n')
        # delete zeros from the top of the file
        for ff in (glob.glob(os.path.join(sastool_sfol,'*.dat')) +  glob.glob(os.path.join(sastool_sfol,'*.tot')) + glob.glob(os.path.join(sastool_sfol,'*.sub'))):
            top_zeros = 0
            try:
                for line in fileinput.input(ff, inplace = 1):
                    #if (not re.match('.* 0\.000000e\+00 0\.000000e\+00',line) or top_zeros == 1):
                    if (not float(line.split()[1]) == float(line.split()[2]) == 0  or top_zeros == 1):
                        sys.stdout.write(line)
                        top_zeros = 1
            except:
                pass
        
        
        
        
            # get filenames
        sub_orig,sub_file,btot_orig,btot_file,buffer_log,rg_file_name,dg_file_name,dg_out_file,stot_orig,stot_file,sample_log,sam_tot=set_filenames(bn,sample_file,buffer_file,sastool_sfol,totfiles_sfol,other_sfol)
        
        # for (sample - buffer), copy/rename .sub file and buffer .tot to tot_files subfolder
            
        if bn != '':
            
            shutil.copyfile(sub_orig,sub_file)
            shutil.copyfile(btot_orig,btot_file)
            
            
    
        # copy tot file to tot_files subfolder
        shutil.copyfile(stot_orig,stot_file)
    
        
       
        # sastool log file for this run
        
        if bn != '':
            buffer_log = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(buffer_file))[0]+'.log')
    
        #
        ## Get info from processed files (to do: if not --micro)!!!
        #
        
        # get the number of processed and rejected sample/buffer files; get variance
        sam_var=[]
        if bn != '':
            tot_bf = -1
            rej_bf = -1
        for line in fileinput.input(sample_log):
            if bn != '':
                if line.startswith('Buffer'):
                    tot_bf =  int(line.split(' ')[1].split(':')[1])
                    rej_bf =  int(line.split(' ')[2].split(':')[1])
            if line.startswith('Sample'):
                tot_sf =  int(line.split(' ')[1].split(':')[1])
                rej_sf =  int(line.split(' ')[2].split(':')[1])
            
            if line.startswith('frame'):
                sam_var.append(float(line.split()[4]))
            if line.startswith('Variance used'):
                variance_num = float(line.split(' ')[-1])
            if line.startswith('Factor'):
                var_factor = float(line.split(' ')[-1])
        if bn != '':
            buf_var=[]
            for line in fileinput.input(buffer_log):
                if line.startswith('frame'):
                    buf_var.append(float(line.split()[4]))
                
        if arg_in.debug:
            if bn != '':
                print('\nBuffer files: total = {0}, rejected = {1}'.format(tot_bf,rej_bf))
                print('\nBuffer variance: {}'.format(buf_var))
            print('\nSample files: total = {0}, rejected = {1}\n'.format(tot_sf,rej_sf))
            print('\nSample variance: {}\n'.format(sam_var))
        
        # get .tot files data
        #sam_tot=os.path.join(sastool_sfol,os.path.splitext(os.path.basename(sample_file))[0]+'.tot')
        sam_tot_data = numpy.loadtxt(sam_tot)
        if bn != '':
            buf_tot_data = numpy.loadtxt(btot_orig)
    
        #
        ## bin files
        #
        
        # get the number of files:
        print('binning data \n')
        sam_name = '_'.join(os.path.splitext(os.path.split(sample_file)[1])[0].split('_')[:-1])
        sam_n = len([ff for ff in os.listdir(sastool_sfol) if (ff.startswith(sam_name) and ff.endswith('dat'))])
        dig_num = len(os.path.splitext(sample_file)[0].split('_')[-1])
        for aa in range(bin_f, sam_n-bin_n+2, bin_n):
            bin_cur = numpy.genfromtxt(os.path.join(sastool_sfol,'{}_{}.dat'.format(sam_name,str(aa).zfill(dig_num))))
            # sqaure variance for adding the other ones
            bin_cur[:,2] = bin_cur[:,2]**2
            for mm in range(aa+1,aa+bin_n):
                kk = numpy.genfromtxt(os.path.join(sastool_sfol,'{}_{}.dat'.format(sam_name,str(mm).zfill(dig_num))))
                bin_cur[:,1] = bin_cur[:,1]+kk[:,1]
                bin_cur[:,2] = bin_cur[:,2]+kk[:,2]**2
            
            bin_cur[:,1] = bin_cur[:,1]/bin_n
            # square root /N for variance
            bin_cur[:,2] = numpy.sqrt(bin_cur[:,2])/bin_n
            
            
            if bn != '':
                bin_sav = os.path.join(avgfiles_sfol,"Bin_"+sam_name+"-T"+str(int(bn)).zfill(dig_num)+"_"+str(int(aa)).zfill(dig_num)+"-"+str(int(aa+bin_n-1)).zfill(dig_num)+'.dat')
            else:
                bin_sav=os.path.join(avgfiles_sfol,"Bin_"+sam_name+"_"+str(int(aa)).zfill(dig_num)+"-"+str(int(aa+bin_n-1)).zfill(dig_num)+'.dat')
            
            numpy.savetxt(bin_sav,bin_cur,delimiter='\t',fmt='%.6e')
            
            
        #
        ## run autorg and datgnom (if found) (to do: if not --mini or --micro)!!!
        #
        
        # create list of .dat files
        dat_list = sorted(glob.glob(os.path.join(sastool_sfol,"_".join(os.path.basename(sample_file).split("_")[:-1])+'*.dat')))
        
        # only run if buffer-subtracted sample
        if bn != '':
            
            # check if we have autorg
            if autorg != None:
                rg_single = []
                rg_single_del = []
                i0_single = []
                i0_single_del = []
                
                # autorg on .dat files
                for ii in dat_list:
                    try:
                        rg_out = subprocess32.check_output([autorg,ii],stderr=subprocess32.STDOUT)
                        rg_file = open(os.path.join(other_sfol,os.path.splitext(os.path.basename(ii))[0]+'_'+buffer_file.split('_')[-2]+"_rg.log"),'w')
                        rg_file.write(rg_out)
                        rg_file.close()
                        rg_single.append(float(rg_out.split()[2]))
                        rg_single_del.append(float(rg_out.split()[4]))
                        i0_single.append(float(rg_out.split()[8]))
                        i0_single_del.append(float(rg_out.split()[10]))
                    except:
                        #print('No Rg found for {}'.format(os.path.basename(ii)))
                        rg_single.append(0)
                        rg_single_del.append(0)
                        i0_single.append(0)
                        i0_single_del.append(0)
                # autorg on the .sub file
                #rg_file_name = os.path.join(other_sfol,'_'.join(os.path.basename(sample_file).split('_')[:-2])+'_'+buffer_file.split('_')[-3]+"_sub_rg.log")
                try:
                    rg_out = subprocess32.check_output([autorg,sub_file],stderr=subprocess32.STDOUT)
                    rg_file=open(rg_file_name,'w')
                    rg_file.write(rg_out)
                    rg_file.close()
                    rg_sub = [float(rg_out.split()[2])]
                    rg_sub_del = [float(rg_out.split()[4])]
                    i0_sub = [float(rg_out.split()[8])]
                    i0_sub_del = [float(rg_out.split()[10])]
                    rg_range = " ".join(rg_out.split()[12:17])
                    rg_start = int(rg_range.split()[0])
                    rg_end = int(rg_range.split()[2])
                except:
                    rg_sub = [0]
                    rg_sub_del = [0]
                    i0_sub = [0]
                    i0_sub_del = [0]
                    rg_range=''
            
                    
            # check if we have datgnom    
            # if datgnom != None:
            #     
            #     #   Don't run datgnom on individual .dat files, only on .sub  
            #     
            #     # dmax_single=[]
            #     # guin_rg_single=[]
            #     # gnom_rg_single=[]
            #     # for ii in dat_list:
            #     #     try:
            #     #         datgnom_out = subprocess32.check_output([datgnom,ii])
            #     #         dmax_single.append(float(datgnom_out.split()[6]))
            #     #         guin_rg_single.append(float(datgnom_out.split()[12]))
            #     #         gnom_rg_single.append(float(datgnom_out.split()[15]))
            #     #     except:
            #     #         print('No datgnom results for {}'.format(os.path.basename(ii)))
            #     #         dmax_single.append(0)
            #     #         guin_rg_single.append(0)
            #     #         gnom_rg_single.append(0)
            #     
            #     # datgnom on .sub file
            #     
            #     try:
            #         datgnom_out = subprocess32.check_output([datgnom,sub_file,'-o',dg_out_file],timeout=15,stdout=FNULL, stderr=subprocess32.STDOUT)
            #         dg_file=open(dg_file_name,'w')
            #         dg_file.write(datgnom_out)
            #         dg_file.close()
            #         dmax_sub = float(datgnom_out.split()[6])
            #         guin_rg_sub = float(datgnom_out.split()[12])
            #         gnom_rg_sub = float(datgnom_out.split()[15])
            #     except:
            #         print('No datgnom for {}'.format(os.path.splitext(os.path.basename(sample_file))[0]+"_sub.dat"))
            #         gnom_rg_sub=0
            #         guin_rg_sub=0
            #         dmax_sub=0
            #         
            # 
            # # check if rg values are similar (within 5%)
            # 
            # if rg_sub[0]*gnom_rg_sub != 0:
            #     if abs((rg_sub[0]-gnom_rg_sub)/rg_sub[0])> 0.05:
            #         data_quality = "Check data - real and reciporocal Rgs differ for more than 5%"
            #     else:
            #         data_quality = "--"
            # else:
            #     data_quality = "No usable Rg"
        
            
        
        #
        ## make graphs (to do: if not --micro, select which ones if --mini)!!!
        #
        if not arg_in.micro:
            font = {'family' : 'serif',
                    'weight' : 'normal',
                    'size'   : 16}
            
            matplotlib.rc('font', **font)
            
            if arg_in.debug:
                print('making plots')
            
            img_gr,dat_gr,dat_an_gr,row_an_gr_1, bin_gr= set_plot_names(bn,other_sfol,sample_file,buffer_file)
            
            print('making graphs \n')
            # make image graphs
            int_comment = ''
            make_image_graphs()


            # set row_an_gr value to '.' so we have something to put in an array for html look-up
            row_an_gr='.'
            dd=''
            # make data graphs, even if not buffer subtracted
            #make_data_graphs()
            
            # make average graphs
            make_bin_graph()
            
            # if bn != '':
            #     # make data analysis graphs, only if buffer subtracted samples (previous IF)
            #     #  and not '--mini'
            #     if (not arg_in.mini) and autorg != None and datgnom != None :
            #     #and len(os.path.basename(sample_file).split('_'))>4  and re.match('[0-1]\d[A-H]',os.path.basename(sample_file).split('_')[-4]):      
            #         make_analysis_graphs()
   
   
            
        # make html table for current sample/buffer
            
            
            
            if buffer_file != '':
                b_exp_time = get_exp_t(buffer_file)
                
            
            
            acq_time = get_acq_t(sample_file)
            exp_time = get_exp_t(sample_file)
            
            
            
            if bn != '':
                # in case of data with buffer subtraction
                if arg_in.mini:
                    rg_cell = '<td>--</td>'
                    #datgnom_cell = ''
                    #data_analysis_row = ''
                    #row_analysis=''
                else:
                    rg_cell ="<td><a title=\"reciprocal Rg&I0\" href=\"{}\"><span class='num_font'>{}&plusmn;{}<br>{}&plusmn;{}</span><br>{}</a></td>".format(os.path.relpath(rg_file_name,aa_fol),rg_sub[0],rg_sub_del[0],i0_sub[0],i0_sub_del[0], rg_range)
                    #datgnom_cell = "<td><a title=\"datgnom realspace Rg&Dmax\" href=\"{}\"><span class='num_font'>{:.1f}<br>{:.1f}</span></a></td>".format(os.path.relpath(dg_file_name,aa_fol),gnom_rg_sub,dmax_sub)
                    #data_analysis_row = "<td><a title=\"data analysis graphs\" href=\"{}\"><img src=\'{}\' alt=\'analysis graphs\' height=\"200\"></a></td>".format(os.path.relpath(dat_an_gr,aa_fol),os.path.relpath(dat_an_gr,aa_fol))
                    
                #if int_comment != '':
                #data_quality = int_comment#+'\n'+data_quality
                
                html_row_t = """
                        <tr>
                            <td> <p title="Acquisition time">{}</p></td>
                            <td> <a title="sub file S{}-B{}" href="{}"><span class='num_font'>{}<br>{}</span></a> </td>
                            <td> <span class='num_font'><span class="b_col">b:</span>&nbsp;<a title="buffer (reject) pos" href="{}">{}x{}&nbsp;({})</a><br><span class="s_col">s:&nbsp;</span><a title="sample (reject) pos" href="{}">{}x{}&nbsp;({})</a><span class="s_col">&nbsp;&nbsp;</span></span></td>
                            {}
                            <td> <a title="image graphs" href="{}"><img src='{}' alt='image graphs' height="200"></a> </td>
                            <td> <a title="avg graphs" href="{}"><img src='{}' alt='avg graphs' height="200"></a> </td>
                            
                        </tr>
                """.format(acq_time,\
                str(sn).zfill(3),str(bn).zfill(3),os.path.relpath(sub_file,aa_fol),'_'.join(os.path.basename(sub_file).split('_')[:-3]),os.path.basename(sub_file).split('_')[-3],\
                os.path.relpath(buffer_log,aa_fol),tot_bf,b_exp_time,rej_bf,os.path.relpath(sample_log,aa_fol),tot_sf,exp_time,rej_sf,\
                rg_cell,\
                os.path.relpath(img_gr,aa_fol),os.path.relpath(img_gr,aa_fol),\
                os.path.relpath(bin_gr,aa_fol), os.path.relpath(bin_gr,aa_fol),\
               
                )
                
                
            else:
                data_gr_row = "<td><a title=\"avg graphs\" href=\"{0}\"><img src='{1}' height=\"200px\"></a></td>".format(os.path.relpath(bin_gr,aa_fol), os.path.relpath(bin_gr,aa_fol))
                rg_cell = '<td></td>'
                        
                # if arg_in.mini:
                #     data_analysis_row=''
                # else:
                #     data_analysis_row="<td>No data analysis graphs</td>"
                    
                html_row_t = """
                        <tr align="center">
                            <td> <p title="Acquisition time">{}</p></td>
                            <td> <a title="tot file" href="{}"><span class='num_font'>{}<br>{}</span></a> </td>
                            <td> <span class='num_font'><a title="sample (reject) [pos]" href="{}">{}x{}&nbsp;({})</a></span> </td>
                            {}
                            <td> <a title="image graphs" href="{}"><img src='{}' alt='image graphs' height="200"></a> </td>
                            {} 
                            
                              
                        </tr>
                """.format(acq_time,\
                os.path.relpath(sam_tot,aa_fol),'_'.join(os.path.basename(sam_tot).split('_')[:-3]),os.path.basename(sam_tot).split('_')[-3],\
                
                os.path.relpath(sample_log,aa_fol),tot_sf,exp_time,rej_sf,\
                rg_cell,\
                os.path.relpath(img_gr,aa_fol),os.path.relpath(img_gr,aa_fol),\
                data_gr_row,\
               
                
                )
                
                
            html_row=html_row+html_row_t
        
    #
    ## make html (remove Rg/datgnom columns if --mini)
    #
    if arg_in.micro == False:
        if arg_in.debug:
            print('making html')
        
        # read the existing html file or start with the basic template
        #os.chdir(results_fol)
    
        sastool_ver = subprocess32.check_output(sastool).split(',')[0]
        if autorg != '' and arg_in.mini == False:
            atsas_ver = " and autorg and datgnom from {}".format(subprocess32.check_output([autorg,'-v']).split('\n')[0].split(',')[-1].strip())
        else:
            atsas_ver = ''
        
        if arg_in.mini == True:
            res_html = resm_html
        else:
            res_html = resf_html
        
        if os.path.isfile(res_html):
            if arg_in.debug:
                print('\nHtml file exists\n')
            html_base = file(res_html).read()
        else:
            try:
                #html_title = os.getcwd().split('/')[-3]+' SAXS Analysis'
                html_title = getpass.getuser()+' SAXS Analysis'
            except:
                html_title = 'SAXS Analysis' 
           
            if arg_in.mini:
                #html_base = html_start.format(html_title)+html_css+html_script+html_table_header.format('','')
                html_base = html_start.format(html_title)+html_css+html_table_header.format('')
            else:
                #html_base = html_start.format(html_title)+html_css+html_script+html_table_header.format(html_atsas_input_full,html_atsas_th_full)
                html_base = html_start.format(html_title)+html_css+html_table_header.format(html_atsas_th_full)
            
        # compile a new table row with  current sample
        
        
            
        
        html_end = html_footer.format(sastool_ver,atsas_ver)
        hh=open(res_html,'w')
        hh.write(html_base.split('</tbody>')[0]+html_row+html_end)
        hh.close()
        
        
        
#print("\n\nFinished in {} seconds.\n".format(time.time()-starttime))    
    
