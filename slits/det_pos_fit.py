#!/usr/bin/env python

'''
@author: rajkovic

'''

import numpy

dd = numpy.loadtxt('det_pos_beam.txt')

x_fit = numpy.polyfit(dd[:,0],dd[:,2],1)

y_fit = numpy.polyfit(dd[:,1],dd[:,3],1)

print('x fit: {}'.format(x_fit))
print('y fit: {}'.format(y_fit))

