#!/usr/bin/env python

'''
@author: rajkovic
'''

import numpy
import argparse
import os
from scipy.interpolate import UnivariateSpline
from scipy.ndimage.filters import uniform_filter1d
import matplotlib.pyplot as plt


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
            slit data fitting"""
    )

    parser.add_argument('csv_file', type=str, help='csv file with slit data')

    arg_in = parser.parse_args()

    data = numpy.loadtxt(arg_in.csv_file)

    #print(data)

    x_data = data[:,0]
    if arg_in.csv_file.split('.')[-1] == 'csv':
        y_data=data[::,2]
    else:
        y_data=data[::,1]
    y_data = y_data/max(y_data)

    sl_spline3 = UnivariateSpline(x_data,y_data,w=1/numpy.sqrt(y_data),k=3)
    sl_spline5 = UnivariateSpline(x_data,y_data,w=1/numpy.sqrt(y_data),k=5)
    x_spl = numpy.linspace(x_data[0],x_data[-1],1000)
    y_spl3 = sl_spline3(x_spl)
    y_spl5 = sl_spline5(x_spl)
    # window average
    y_wa = uniform_filter1d(y_data,3)
    print(x_data[y_data.argmin()],y_data.min())
    print(x_data[y_wa.argmin()],y_wa.min())

    plt.figure()
    plt.title(os.path.basename(arg_in.csv_file))
    plt.plot(x_data, y_data, 'bo',label='raw')
    plt.plot(x_data,y_wa,'g-',label='smooth')
    plt.plot(x_spl,y_spl3,'c-',label='spline3')
    plt.plot(x_spl,y_spl5,'k-',label='spline5')
    plt.plot(x_data[y_data.argmin()],y_data.min(),'y+',label='raw min')
    plt.plot(x_data[y_wa.argmin()],y_wa.min(),'rx',label='smooth min')
    plt.plot(x_spl[y_spl3.argmin()],y_spl3.min(),'mx',label='spline3 min')
    plt.plot(x_spl[y_spl5.argmin()],y_spl5.min(),'bx',label='spline5 min')
    plt.legend()
    plt.figtext(0.1,0.02,'raw: {0:2.4g}, smooth: {1:2.4g}, spline3: {2:2.4g}, spline5: {3:2.4g}'.format(x_data[y_data.argmin()],x_data[y_wa.argmin()],x_spl[y_spl3.argmin()],x_spl[y_spl5.argmin()] ), fontsize=12)
    #plt.savefig(os.path.splitext(arg_in.csv_file)[0]+'.png')
    plt.show()

    #plt.figure()
    #plt.plot(x_data,y_data)
    #plt.show()