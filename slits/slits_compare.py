#!/usr/bin/env python

'''
@author: rajkovic
'''

import numpy
import argparse
import os
from scipy.interpolate import UnivariateSpline
from scipy.ndimage.filters import uniform_filter1d
import matplotlib.pyplot as plt
import glob
from distutils.dir_util import mkpath

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
            slit data fitting"""
    )

    parser.add_argument('folder', type=str, help='folder with slit data')

    arg_in = parser.parse_args()

    pl_path = os.path.join(arg_in.folder,'fit_compare')
    mkpath(pl_path)

    com_res = os.path.join(pl_path,'results.txt')
    com_res_html = os.path.join(pl_path,'results.html')

    with open(com_res,'w') as cr:
        cr.write("name \t raw \t smooth \t spline3 \t spline5\n")


    with open(com_res_html,'w') as crh:
        crh.write("""<!DOCTYPE html>
    <html>
        <head>
            <title>compare slits fitting</title>
            <meta charset="UTF-8">
            <style media="screen" type="text/css">

                table a:link {
                        color: #0000aa;
                        text-decoration:none;
                            }
                table a:visited {
                        color: #0000aa;
                        text-decoration:none;
                            }
                table a:active,
                        table a:hover {
                        color: #bd5a35;
                        text-decoration:underline;
                            }
                table tr:hover td {
                        background: #f2f2f2;
                            }
                table, td, th {
                        border: 1px solid grey;
                        text-align: center;
                        font-family: arial;
                        font-size: 0.9em;
                        }
                td, th {
                        padding: 10px;
                        }
                thead {
                        font-size: 1.3em;
                        }
                tfoot td{
                        font-size: 1.3em;
                        text-align: left;
                        }
                .foot_font {
                        font-size: 0.7em;
                    }
                .num_font {
                        font-size: 1.6em;
                    }
                .b_col {
                        color: red;
                    }
                .s_col {
                        color: blue;
                    }
                thead th
                    {
                    position: sticky;
                    top: 21px;
                    background-color: #eeffff;
                    }
                </style>
        </head>
        <body>
            <table>
                <thead>
                    <tr>
                        <th> name </th>
                        <th> raw </th>
                        <th> smooth </th>
                        <th> spline3 </th>
                        <th> spline5 </th>
                        <th> Image</th>
                    </tr>
                </thead>
                <tbody>
        """)


    for fff in glob.glob(os.path.join(arg_in.folder,'*.cnt')):

        data = numpy.loadtxt(fff)

        #print(data)

        x_data = data[:,0]
        #if arg_in.csv_file.split('.')[-1] == 'csv':
        #    y_data=data[::,2]
        #else:
        y_data=data[:,1]
        y_data = y_data/max(y_data)
        #print(fff,x_data,y_data)

        sl_spline3 = UnivariateSpline(x_data,y_data,w=1/numpy.sqrt(y_data),k=3)
        sl_spline5 = UnivariateSpline(x_data,y_data,w=1/numpy.sqrt(y_data),k=5)
        x_spl = numpy.linspace(x_data[0],x_data[-1],1000)
        y_spl3 = sl_spline3(x_spl)
        y_spl5 = sl_spline5(x_spl)
        # window average
        y_wa = uniform_filter1d(y_data,3)
        #print(x_data[y_data.argmin()],y_data.min())
        #print(x_data[y_wa.argmin()],y_wa.min())

        image_name = os.path.splitext(os.path.basename(fff))[0]+'.png'

        with open(com_res,'a') as cr:
            cr.write("{} \t {} \t {} \t {} \t {}\n".format(os.path.basename(fff),x_data[y_data.argmin()],x_data[y_wa.argmin()],x_spl[y_spl3.argmin()],x_spl[y_spl5.argmin()]))

        with open(com_res_html,'a') as crh:
            crh.write("""<tr>
            <td>{} </td>
             <td>{} </td>
             <td>{} </td>
             <td>{} </td>
             <td>{} </td>
             <td><a title="image" href="{}"><img src='{}' alt='image' height="200"></a> </td>""".format(os.path.basename(fff),x_data[y_data.argmin()],x_data[y_wa.argmin()],x_spl[y_spl3.argmin()],x_spl[y_spl5.argmin()],image_name,image_name))

        plt.figure(fff)
        plt.title(os.path.basename(fff))
        plt.plot(x_data, y_data, 'bo',label='raw')
        plt.plot(x_data,y_wa,'g-',label='smooth')
        plt.plot(x_spl,y_spl3,'r-',label='spline3')
        plt.plot(x_spl,y_spl5,'k-',label='spline5')
        plt.scatter(x_data[y_data.argmin()],y_data.min(),marker = 'x', color='g',s=100,linewidth=3, label='raw min')
        plt.scatter(x_data[y_wa.argmin()],y_wa.min(),marker='x',color='c',s=100,linewidth=3,label='smooth min')
        plt.scatter(x_spl[y_spl3.argmin()],y_spl3.min(),marker='x',color='r',s=100,linewidth=3,label='spline3 min')
        plt.scatter(x_spl[y_spl5.argmin()],y_spl5.min(),marker='x',color='k',s=100,linewidth=3,label='spline5 min')
        plt.xlim(plt.xlim()[0],1.3*plt.xlim()[1])
        plt.legend()
        plt.figtext(0.1,0.02,'raw: {0:2.4g}, smooth: {1:2.4g}, spline3: {2:2.4g}, spline5: {3:2.4g}'.format(x_data[y_data.argmin()],x_data[y_wa.argmin()],x_spl[y_spl3.argmin()],x_spl[y_spl5.argmin()] ), fontsize=12)
        #plt.savefig(os.path.splitext(arg_in.csv_file)[0]+'.png')
        plt.savefig(os.path.join(pl_path,image_name))

        #plt.figure()
        #plt.plot(x_data,y_data)
        #plt.show()

    with open(com_res_html,'a') as crh:
        crh.write("""
                </tbody>
                </table>
                </body>
                </html>
    """)