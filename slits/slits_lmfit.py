#!/usr/bin/env python

'''
@author: rajkovic
'''

import numpy
import argparse

from scipy import optimize
from lmfit import Model
from lmfit.models import LinearModel, LorentzianModel,GaussianModel,ConstantModel

import matplotlib.pyplot as plt


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
            slit data fitting"""
    )

    parser.add_argument('csv_file', type=str, help='csv file with slit data')

    arg_in = parser.parse_args()

    data = numpy.loadtxt(arg_in.csv_file)

    #print(data)

    x_data = data[:,0]
    if arg_in.csv_file.split('.')[-1] == 'csv':
        y_data=5-data[::,2]
    else:
        y_data=5-data[::,1]


    #fitfunc = lambda p, x: p[0]+numpy.exp(-(x-p[1])/p[2])+numpy.exp((x-p[3])/p[4])+p[5]*x#+p[6]*(x**2)

    #def fitfunc2(x,a,b,c,d,e,f):
    #    return a+numpy.exp(-(x-b)/c)+numpy.exp((x-d)/e)+f*x

    #def fitfunc2(x,a,b,c):
    #    return a+b*x+c*x**2

    #gmodel=Model(fitfunc2)

    #peak = LorentzianModel()
    peak = GaussianModel()
    background = LinearModel()
    offset = ConstantModel()
    gmodel = peak  + offset

    #params = background.make_params()
    params = offset.make_params(c=numpy.median(y_data))
    params+=peak.guess(y_data, x=x_data, amplitude=1)
    #fitfunc = lambda p, x: p[0] + p[1]*x+ p[7]*numpy.exp(-numpy.sign(x-p[2])*((numpy.abs(x-p[2]))**p[3])/p[4])+numpy.exp((x-p[5])/p[6])#+p[7]*(x**2)
    #errfunc = lambda p, x, y: fitfunc(p, x) - y # Distance to the target function
    #p0 = [y_data.min(),x_data.min(),1,x_data.max(),1,0] # Initial guess for the parameters
    #p0 = [y_data.min(),0,x_data.min(),1,1,x_data.max(),1,1]
    #p1, success = optimize.leastsq(errfunc, p0[:], args=(x_data, y_data),maxfev=5000)
    #p0 = [0,0,0]

    #p2, s2 = optimize.curve_fit(fitfunc2,x_data,y_data,p0,maxfev=5000)

    #print(p1,success)
    #print(p2)

    xfit = numpy.linspace(x_data.min(), x_data.max(), len(x_data))

    yfit=gmodel.eval(params,x=xfit)

    result=gmodel.fit(y_data,params,x=x_data)

    print(result.fit_report())
    #yfit=fitfunc(p1,xfit)
    #y2fit=fitfunc2(xfit,*p2)
    print(x_data[y_data.argmin()],y_data.min())

    plt.figure()
    plt.plot(x_data, y_data, 'bo')
    #plt.plot(xfit, result.init_fit, 'k--', label='initial fit')
    plt.plot(xfit, result.best_fit, 'r-', label='best fit')
    plt.legend(loc='best')
    plt.show()

    #print(xfit[yfit.argmin()],yfit.min())
    #print(xfit[y2fit.argmin()],y2fit.min())
    #plt.figure()
    #plt.plot(x_data, y_data, "bo", xfit,y2fit,'g-')
    #plt.show()

    #plt.figure()
    #plt.plot(x_data,y_data)
    #plt.show()