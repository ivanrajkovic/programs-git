#!/usr/bin/env python

'''
@author: rajkovic
'''

import numpy
import argparse
import os
#import matplotlib.pyplot as plt

#import matplotlib.pyplot as plt
#from scipy.interpolate import UnivariateSpline
#from scipy.ndimage.filters import uniform_filter1d

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
            slit data fitting"""
    )

    parser.add_argument('scan_file', type=str, help='scan file with slit data')
    parser.add_argument('--out_folder','-of', type=str, help='output folder for result file, default is the same as the scan file')
    parser.add_argument('--plot','-p', help='show plot (default=no)',action='store_true', default=False)
    parser.add_argument('--method','-m', type=str, help='fitting method: smooth3 (default), smooth5, spline3, or spline5', default='smooth3',choices=['smooth3', 'smooth5', 'spline3','spline5'])

    arg_in = parser.parse_args()

    data = numpy.loadtxt(arg_in.scan_file)

    #print(data)

    x_data = data[:,0]
    y_data = data[:,1]
    y_max = max(y_data)


    if arg_in.method == 'smooth3':
        from scipy.ndimage.filters import uniform_filter1d
        y_fit = uniform_filter1d(y_data,3)
        x_fit = x_data[:]

    elif arg_in.method == 'smooth5':
        from scipy.ndimage.filters import uniform_filter1d
        y_fit = uniform_filter1d(y_data,5)
        x_fit = x_data[:]

    elif arg_in.method == 'spline3':
        from scipy.interpolate import UnivariateSpline
        sl_spline3 = UnivariateSpline(x_data,y_data/y_max,k=3)
        x_fit = numpy.linspace(x_data[0],x_data[-1],1000)
        y_fit = y_max*sl_spline3(x_fit)

    elif arg_in.method == 'spline5':
        from scipy.interpolate import UnivariateSpline
        sl_spline5 = UnivariateSpline(x_data,y_data/y_max,k=5)
        x_fit = numpy.linspace(x_data[0],x_data[-1],1000)
        y_fit = y_max*sl_spline5(x_fit)

    x_min = x_fit[y_fit.argmin()]

    if y_fit.argmin() in [0,y_fit.size-1]:
        out_text = 'BAD FIT - minimum at start/end of the scan\n'
    else:
        out_text = 'GOOD FIT\n'

    #print(x_min,y_fit.min())

    if arg_in.out_folder == None:
        outfol = os.path.dirname(arg_in.scan_file)
    else:
        outfol = arg_in.out_folder

    res_file = os.path.join(outfol,os.path.splitext(os.path.basename(arg_in.scan_file))[0]+'.sft')
    with open(res_file,'w') as rs:
        rs.write(out_text)
        rs.write('{:.2f}\n'.format(x_min))


    disp_true = 'DISPLAY' in os.environ

    if not disp_true:
        # no DISPLAY set
        import matplotlib
        matplotlib.use('Agg')

    import matplotlib.pyplot as plt



    plt.figure(os.path.splitext(os.path.basename(arg_in.scan_file))[0])
    plt.title(os.path.splitext(os.path.basename(arg_in.scan_file))[0])
    plt.plot(x_data, y_data, "bo", x_fit,y_fit,'g-')
    plt.scatter(x_min,y_fit.min(),marker = 'x', color='r',s=100,linewidth=3)
    plt_file = os.path.join(outfol,os.path.splitext(os.path.basename(arg_in.scan_file))[0]+'.png')
    plt.savefig(plt_file)
    plt.close()

    if arg_in.plot :
        if disp_true:
            plt.show()
        else:
            print('DISPLAY is not set!')

