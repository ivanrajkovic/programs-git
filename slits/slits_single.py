#!/usr/bin/env python

'''
@author: rajkovic
'''
#from __future__ import division
import time
#tt = time.time()

import sys
import numpy
#from PIL import Image
import os.path
import argparse
import fileinput
from fabio import open as fabopen

def find_beam(im):
    #from scipy import ndimage
    dd = numpy.copy(im)
    #s = dd.flatten()
    #s.sort()

    # remove hot pixels
    dd[dd>500]=0

    dd_l,dd_w = dd.shape

    # bin bb x bb
    bb = 4
    dd_l_c = dd_l//bb
    dd_w_c = dd_w//bb

    gg = dd[:dd_l_c*bb,:dd_w_c*bb].reshape(dd_l_c,bb,dd_w_c,bb).sum(3).sum(1)


    # set background to zero
    #sm = dd.mean()
    #dd[dd<sm*20]=0
    #dd[dd<s[-2000]]=0
    #cm_y,cm_x = ndimage.measurements.center_of_mass(dd)
    #return(int(cm_x),int(cm_y))

    cm_y, cm_x = numpy.where(gg == numpy.amax(gg))

    return(bb*cm_x[0],bb*cm_y[0])



if __name__ == "__main__":
    # parse command line arguments

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
            Calulates sum of intenisty in the box of size +- size_x/2 and size_y/2 centered at (center_x,center_y).

            If center_x or center_y is zero or not given center position is calculated

            If not specified, size_x and size_y are 100 (+-50)

            """
    )

    parser.add_argument('image', type=str, help='image to analyize')
    parser.add_argument('--center_x','-cx', metavar = 'pix', type=float, help='center x, (in pixels, default 0)', default=0)
    parser.add_argument('--center_y','-cy', metavar = 'pix', type=float, help='center y, (in pixels, default 0)', default=0)
    parser.add_argument('--size_x','-sx', metavar = 'pix', type=float, help='box size x, (in pixels, default 100)', default=100)
    parser.add_argument('--size_y','-sy', metavar = 'pix', type=float, help='box size y, (in pixels, default 100)', default=100)


    arg_in = parser.parse_args()


    im_timer=60
    while not os.path.isfile(arg_in.image):
        #print("Waiting for the image, {}s before giving up".format(im_timer))
        if im_timer == 1:
            sys.exit('No image found')
        im_timer-=1
        time.sleep(1)

    cen_x = int(arg_in.center_x)
    cen_y = int(arg_in.center_y)
    roi_w = int(arg_in.size_x)
    roi_h = int(arg_in.size_y)

    im_file = arg_in.image
    cnt_file = "_".join(im_file.split("_")[:-2])+'.cnt'

    scan_nr = int(im_file.split("_")[-2])

    #im_data = numpy.flipud(Image.open(arg_in.image))
    im_data = numpy.flipud(fabopen(im_file).data)
    im_l, im_w = im_data.shape

    if os.path.exists(cnt_file):
        try:
            # read the center info from the cnt file
            with open(cnt_file,'r') as f:
                cen_info = f.readline()
                cen_x = int(cen_info.split()[2])
                cen_y = int(cen_info.split()[4])
        except:
            # couldn't read cnt file
            pass

    # find center if not specified or not read from cnt file
    if cen_x == 0 or cen_y == 0:
        try:
            # calculate position from prp file
            rr = tt*5
            for line in fileinput.input(os.path.splitext(arg_in.image)[0]+'.prp'):
                if line.startswith('detector_vert'):
                    dv = float(line.split('=')[1].split()[0])
                elif line.startswith('detector_horz'):
                    dh = float(line.split('=')[1].split()[0])

            # coeff should be 5.8 (1/.172), need more data for better fit; also, det_vert loses steps, could be the cause of this issue
            cen_x = int(5.8*dh-1417)
            cen_y = int(-5.7*dv+1781)



        except:
            cen_x,cen_y = find_beam(im_data)


        #print('x: {}\t y: {}'.format(cen_x,cen_y))
        #cen_y = im_l - cen_y - 1

    #print(cen_x,cen_y)

    # check if the ROI is inside the image, if not limit to the image size

    y_start = max(0,cen_y-int(roi_h/2))
    y_end = min(im_l,cen_y+int(roi_h/2))
    x_start = max(0,cen_x-int(roi_w/2))
    x_end = min(im_w,cen_x+int(roi_w/2))

    #print('{}:{}\t{}:{}'.format(x_start,x_end,y_start,y_end))
    #roi_sum = numpy.sum(im_data[cen_y-int(roi_h/2):cen_y+int(roi_h/2),cen_x-int(roi_w/2):cen_x+int(roi_w/2)].clip(0))

    # get i2 normalization
    try:
        for line in fileinput.input(os.path.splitext(arg_in.image)[0]+'.prp'):
            if line.startswith('I_2'):
                i2 = float(line.split('=')[1].split()[0])
    except:
        i2=1

    # calculate roi sum, normalize with i2
    roi_sum = im_data[y_start:y_end,x_start:x_end].clip(0).sum()/i2

    if not os.path.exists(cnt_file):
        with open(cnt_file,'w+') as f:
            # write center info
            f.write("# cen_x {} cen_y {}\n".format(cen_x,cen_y))

    # append the roi data
    with open(cnt_file,'a') as f:
        f.write("{} {}\n".format(scan_nr,roi_sum))



    #roi_sum_all.append(roi_sum)
    #rr = numpy.append(rr,roi_sum)
    # if arg_in.center_x == 0 or arg_in.center_y == 0:
    #     sys.stdout.write('{}\t{}\t{}\n'.format(roi_sum,cen_x,cen_y))
    # else:
    #     sys.stdout.write('{}\n'.format(roi_sum))
    # sys.stdout.flush

    #print(time.time()-tt)
