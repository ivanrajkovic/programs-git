#!/usr/bin/env python

'''
@author: rajkovic
'''

import numpy
import argparse

from scipy import optimize

import matplotlib.pyplot as plt


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
            slit data fitting"""
    )

    parser.add_argument('csv_file', type=str, help='csv file with slit data')

    arg_in = parser.parse_args()

    data = numpy.loadtxt(arg_in.csv_file)

    #print(data)

    x_data = data[:,0]
    if arg_in.csv_file.split('.')[-1] == 'csv':
        y_data=data[::,2]
    else:
        y_data=data[::,1]



    # find y_data min and max, keep just some values of y_data
    # ymin = y_data.min()
    # ymax = y_data.max()
    # y_limit = ymin+(ymax-ymin)*.1
    # xx = x_data[y_data<=y_limit]
    # yy = y_data[y_data<=y_limit]
    # print(len(xx),len(x_data))
    #
    #
    # if len(xx) < len(x_data)/3:
    #     print('data/3')
    #     # too little points, use at least lowest 1/3 of the array for fit
    #     int_num = int(numpy.ceil(len(x_data)/3.))
    #     y_mm=max(y_data[numpy.argpartition(y_data,int_num)][:int_num])
    #     xx = x_data[y_data<=y_mm]
    #     yy = y_data[y_data<=y_mm]
    #
    #
    # if len(xx) > 2*len(x_data)/3:
    #     print('2*data/3')
    #     #too many points, use at most lowest 2/3 of the array for fit
    #     int_num = int(2*len(x_data)/3.)
    #     y_mm=max(y_data[numpy.argpartition(y_data,int_num)][:int_num])
    #     xx = x_data[y_data<=y_mm]
    #     yy = y_data[y_data<=y_mm]

        #xx = x_data[y_data.argsort()][:int_num]
        #yy = y_data[y_data.argsort()][:int_num]

    # use 1/2 of data around the minimum
    # yargmin = y_data.argmin()
    # d_range = int(len(x_data)/2)
    # rmin = max(0,yargmin-d_range)
    # rmax= min(len(y_data),yargmin+d_range)
    #xx = x_data[rmin:rmax]
    #yy = y_data[rmin:rmax]

    # use just some data from lower intensities
    int_num = int(numpy.ceil(3*len(x_data)/5.))
    # get y_data value of int_num's position in (partially) sorted data (low to high)
    y_mm=max(y_data[numpy.argpartition(y_data,int_num)][:int_num])

    # use only y_data lower than y_mm
    xx = x_data[y_data<=y_mm]
    yy = y_data[y_data<=y_mm]
    #print(len(xx),len(x_data))


    # fit 3rd order polynomial
    pp = numpy.polyfit(xx,yy,3)
    #print(pp)

    # fitting twice, leastsq gives info if the fitting is successful ; using polyfit result as a starting point
    # although leastsq also always fits polynomial with success

    def fitfunc(p,x):
        return p[3]+p[2]*x+p[1]*(x**2)+p[0]*(x**3)

    def errfunc(p,x,y):
        return fitfunc(p,x)-y

    p1, success = optimize.leastsq(errfunc, pp, args=(xx, yy))
    #print(p1,success)
    if not  (0 < success < 5):
        print('Fit not good!')
    #

    # create points from fitted polynomial, 10x oversample

    rr = numpy.poly1d(p1)

    xfit = numpy.linspace(xx.min(), xx.max(), len(x_data)*10)
    yfit = rr(xfit)
    # find minimums of exp data and fitted function
    print('data minimum at x={}, y={}'.format(x_data[y_data.argmin()],y_data.min()))
    print('for minimum at x={}, y={}'.format(xfit[yfit.argmin()],yfit.min()))

    # check if the minimum is at the fitting range border
    if min(numpy.abs(numpy.array([xx[0],xx[-1]])-xfit[yfit.argmin()])) < 0.02:
        print(xx[0],xfit[yfit.argmin()])
        print('lowest value at the range border!')

    # delta x_min
    #print(x_data[y_data.argmin()]-xfit[yfit.argmin()])

    # plot a figure with all data
    plt.figure()
    plt.plot(x_data, y_data,'ro',xx,yy,'g-',xfit,yfit,'b*')
    plt.xlim([xx.min(),xx.max()])
    plt.ylim([0.98*yy.min(),1.02*yy.max()])
    plt.show()

