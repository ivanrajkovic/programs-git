#!/usr/bin/env python

'''
@author: rajkovic
'''
#from __future__ import division
#import time
#tt = time.time()

import sys
import numpy
import os.path
import argparse
from PIL import Image
import rasterio
import warnings
import imagesize
warnings.filterwarnings("ignore")
import matplotlib.pyplot as plt

#import fileinput

def find_beam(im):
    from scipy import ndimage
    dd = 1*im
    s = dd.flatten()
    s.sort()
    
    # remove hot pixels
    dd[dd>s[-10]]=0
    
    # set background to zero 
    #sm = dd.mean()
    #dd[dd<sm*20]=0
    dd[dd<s[-1500]]=0
    
    cm_y,cm_x = ndimage.measurements.center_of_mass(dd)
    return(int(cm_x),int(cm_y))
    

bip_text = """ _graph_title.text "Int vs slit gap"
  
 _graph_axes.xLabel "slit gap"
 _graph_axes.yLabel "Int"
 
 _graph_background.showGrid 1
  
 data_
 _trace.name Data
 _trace.xLabels "{slit gap}"
 _trace.hide 0
  
 loop_
 _sub_trace.name
 _sub_trace.yLabels
 _sub_trace.color
 _sub_trace.width
 _sub_trace.symbol
 _sub_trace.symbolSize
 Int "{Int}" blue 0 square 2
 
 loop_
 _sub_trace.x
"""


if __name__ == "__main__":
    # parse command line arguments
    
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
            If center_x or center_y is zero or not given center position is calculated
            
            If not specified, size_x and size_y are 100 (+-50)"""
    )
    
    parser.add_argument('first_image', type=str, help='first image to analyize')
    parser.add_argument('num_images', type = int, help='number of images to analyze')
    parser.add_argument('--first_slit','-fs', metavar = 'mm', type=float, help='slit size for fist image (in mm, default 1.0)', default=1.0)  
    parser.add_argument('--delta_slit','-ds', metavar = 'mm', type=float, help='slit size difference between images (in mm, default 0.05)', default=-.05)
    parser.add_argument('--center_x','-cx', metavar = 'pix', type=float, help='center x, (in pixels, default 0)', default=0)  
    parser.add_argument('--center_y','-cy', metavar = 'pix', type=float, help='center y, (in pixels, default 0)', default=0)
    parser.add_argument('--size_x','-sx', metavar = 'pix', type=float, help='box size x, (in pixels, default 100)', default=100)  
    parser.add_argument('--size_y','-sy', metavar = 'pix', type=float, help='box size y, (in pixels, default 100)', default=100)                  
    
    
    arg_in = parser.parse_args()
    
    
    if not os.path.isfile(arg_in.first_image):
        sys.exit("Image list does not exist")
    
    cen_x = int(arg_in.center_x)
    cen_y = int(arg_in.center_y)
    roi_w = int(arg_in.size_x)
    roi_h = int(arg_in.size_y)
    
    
    # prepare array for results
    #roi_sum_all = []
    #rr = numpy.array([],numpy.int32)
    
    # get image info
    #data_fol = os.path.realpath(os.path.dirname(arg_in.first_image))
    sn = int(arg_in.first_image.split('_')[-3][1:])
    sl = len(arg_in.first_image.split('_')[-3][1:])
    sb = arg_in.first_image.split('_')[-3][0]
    #print(sn,sl,sb)
    
    #prepare image list
    im_list = [arg_in.first_image]
    for snum in numpy.arange(sn+1,sn+arg_in.num_images):
        # prepare the list
        # either create the list by increasing the series number
        # or search for the tif/cbf files with increasing series numbers
        # or try first, see if file exists, if not do second
        next_im = '_'.join(['_'.join(arg_in.first_image.split('_')[:-3]),sb+str(snum).zfill(sl),'_'.join(arg_in.first_image.split('_')[-2:])])
        if not os.path.isfile(next_im):
            import re
            im_pat = re.compile('.*'+sb+str(snum).zfill(sl)+'_0_0+1\.tif|cbf')
            temp_im_list=[]
            for file in os.listdir(os.path.dirname(arg_in.first_image)):
                if im_pat.match(file):
                    temp_im_list.append(file)
            if len(temp_im_list) == 0:
                sys.exit("\nNo files found!\n")
            elif len(temp_im_list) > 1:
                print("\n\033[0;37;41m\033[1m\033[4m Two or more files found! \033[0;0m")
                sys.exit(temp_im_list)
            else:
                next_im = os.path.join(os.path.dirname(arg_in.first_image),temp_im_list[0])
        
        im_list.append(next_im)
    
    
    hh = imagesize.get(im_list[0])[1]
    if cen_y != 0:
        cen_y = hh - cen_y
    
    if cen_x == 0 or cen_y == 0:
        im_data = numpy.array(Image.open(im_list[0]))
        cen_x,cen_y = find_beam(im_data)
        #print('x: {}\t y: {}'.format(cen_x,hh - cen_y))
        
    # go through the list
    for ll in numpy.arange(len(im_list)):
        try:
            # open image
            #im_data = numpy.array(Image.open(line.strip()))
            #im_data = numpy.flipud(numpy.array(Image.open(im_list[ll])))
            
            
            #hh = im_data.shape[0] 
            
            # find center if not specified
            # if cen_x == 0 or cen_y == 0:
            #     cen_x,cen_y = find_beam(im_data)
            #     print('x: {}\t y: {}'.format(cen_x,cen_y))
            
            
            
            # use rasterio to open only a window
            with rasterio.open(im_list[ll]) as src:
                im_data_window = src.read(1, window=rasterio.windows.Window(cen_x-int(roi_w/2), cen_y-int(roi_h/2), int(roi_w), int(roi_h)))
            
            
            #roi_sum = numpy.sum(im_data[cen_y-int(roi_h/2):cen_y+int(roi_h/2),cen_x-int(roi_w/2):cen_x+int(roi_w/2)].clip(0))
            #roi_sum = im_data[cen_y-int(roi_h/2):cen_y+int(roi_h/2),cen_x-int(roi_w/2):cen_x+int(roi_w/2)].clip(0).sum()
            roi_sum = im_data_window.clip(0).sum()
            
            #roi_sum_all.append(roi_sum)
            #rr = numpy.append(rr,roi_sum)
            #print("{} \t {}".format(roi_sum,roi_sum_w))
            print(roi_sum)
            bip_text = bip_text + '{} {}\n'.format(arg_in.first_slit+ll*arg_in.delta_slit,roi_sum)
            
            # plt.figure(ll)
            # plt.imshow(im_data_window)
            # wm = plt.get_current_fig_manager()
            # wm.window.wm_geometry("500x500+350+350")
            
            
        except:
            print('\'{}\' : not an image or something else went wrong'.format(im_list[ll]))

    
    
    
    #print(time.time()-tt)
    
    #sys.stdout.write(str(", ".join(str(x) for x in roi_sum_all))+'\n')
    #sys.stdout.write(str(", ".join(map(str,roi_sum_all)))+'\n')
    #sys.stdout.flush()
    
    #print(roi_sum_all)
    #numpy.savetxt('res_'+arg_in.im_list,rr)
    #with open(os.path.splitext(arg_in.first_image)[0]+'.bip','w') as lg:
    #    lg.write(bip_text)
    
    #print(time.time()-tt)
    
    # plt.show()
       
        