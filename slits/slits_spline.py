#!/usr/bin/env python

'''
@author: rajkovic
'''

import numpy
import argparse
import os

from scipy.interpolate import UnivariateSpline
import matplotlib.pyplot as plt


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
            slit data fitting"""
    )

    parser.add_argument('csv_file', type=str, help='csv file with slit data')

    arg_in = parser.parse_args()

    data = numpy.loadtxt(arg_in.csv_file)

    #print(data)

    x_data = data[:,0]
    if arg_in.csv_file.split('.')[-1] == 'csv':
        y_data=data[::,2]
    else:
        y_data=data[::,1]


    y_data = y_data/max(y_data)
    # get spline
    sl_spline = UnivariateSpline(x_data,y_data,w=1/numpy.sqrt(y_data))
    #sl_spline.set_smoothing_factor(1)

    x_spl = numpy.linspace(x_data[0],x_data[-1],1000)

    plt.figure()
    plt.title(os.path.basename(arg_in.csv_file))
    plt.plot(x_data, y_data, 'bo',label='raw')
    plt.plot(x_spl,sl_spline(x_spl),'g-',label='spline')
    #plt.plot(x_data[y_data.argmin()],y_data.min(),'y+',label='raw min')
    #plt.plot(x_data[y_wa.argmin()],y_wa.min(),'rx',label='smooth min')
    #plt.legend()
    #plt.figtext(0.1,0.02,'raw: x={0:.3g}, y={1:.2g}'.format(x_data[y_data.argmin()],y_data.min()), fontsize=12)
    #plt.figtext(0.5,0.02,'smooth: x={0:.3g}, y={1:.2g}'.format(x_data[y_wa.argmin()],y_wa.min()), fontsize=12)
    #plt.savefig(os.path.splitext(arg_in.csv_file)[0]+'.png')
    plt.show()

    #plt.figure()
    #plt.plot(x_data,y_data)
    #plt.show()