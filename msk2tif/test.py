import numpy
import sys
import struct
import os
import scipy.misc
import re

os.chdir('c:\\Users\\rajkovic\\Documents\\programs\\msk2tif\\')

f = open('mask_may28.msk', 'rb')

header = f.read(1024)

ttt = numpy.fromstring(header,numpy.uint32)
d1 = ttt[4]
d2 = ttt[5]

num_ints = (d1 + 31) // 32

total = d2 * num_ints * 4 
data = f.read()
f.close()



data = numpy.fromstring(data, numpy.uint8)
data = numpy.reshape(data, (d2, num_ints * 4))
result = numpy.zeros((d2, num_ints * 4 * 8), numpy.uint8)

bits = numpy.ones((1), numpy.uint8)
for i in range(8):
    temp = numpy.bitwise_and(bits, data)
    result[:, i::8] = temp.astype(numpy.uint8)
    bits = bits * 2

spares = num_ints * 4 * 8 - d1
if spares == 0:
    data = numpy.where(result == 0, 0, 1)
else:
    data = numpy.where(result[:, :-spares] == 0, 0, 1)
    
data = numpy.reshape(data.astype(numpy.uint16),(d2, d1))
data = numpy.flipud(data)

scipy.misc.toimage(data, cmin=0.0, cmax=255.0).save('may28.tif')