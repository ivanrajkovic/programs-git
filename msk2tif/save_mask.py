# Copyright 2010-2014 Gunthard Benecke, DESY, MPIKG.
#
# This file is part of DPDAK.
#
# DPDAK is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DPDAK is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with DPDAK. If not, see <http://www.gnu.org/licenses/>.

import os
import time
import tempfile

import numpy
import fabio
from base.dpdak import *
from base import dpdak_image

### big part cut from here, just mask writing part left
def write_fit2d_mask(fname, data):
    # open file in binary mode
    f = open(fname, 'wb')
    
    # write 'magic numbers' for fit2d mask file format
    f.write('M\x00\x00\x00A\x00\x00\x00S\x00\x00\x00K\x00\x00\x00')
    
    # write header with dimensions, fit2d writes more..., but it works anyway
    head = numpy.zeros(252, dtype=numpy.int32)
    head[0] = data.shape[1]
    head[1] = data.shape[0] 
    f.write(head.tostring())
    
    # convert input data to 'binary'
    data = numpy.where(data, 1, 0).astype(numpy.uint8)
    
    # pad with 0's to n*32 bit if necessary
    if data.shape[1]%32:
        p = numpy.zeros((data.shape[0], 32-data.shape[1]%32), dtype=numpy.uint8)
        data = numpy.append(data, p, axis=1)

    # compress to bytes 
    for i in range(1, 8):
        data[:,::8] |= data[:,i::8] * 2**i

    # write resulting byte array
    f.write(data[:,::8].tostring())
    f.close()