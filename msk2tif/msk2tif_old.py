#!/usr/bin/python
# coding: utf-8

"""
mask reading routine:
Author: Andy Hammersley, ESRF
Translation into python/fabio: Jon Wright, ESRF.
Writer: Jérôme Kieffer
https://pypi.python.org/pypi/fabio

Adapted to msk2tif.py: Ivan Rajkovic
"""

import sys
import numpy
#import struct
#import os
#import scipy.misc
from PIL import Image

if len(sys.argv) == 1:
	sys.exit("\nUsage: "+ sys.argv[0] + " <infile.msk> [outfile.tif]\n")

infile = sys.argv[1]
#print infile
#print infile[0:-4]
if len(sys.argv)<3:
	outfile = infile[0:-4]+'.tif'
else:
	outfile = sys.argv[2]

if outfile[-3:]!='tif':
	outfile = outfile.replace(".","")+'.tif'

#print outfile
f = open(infile, 'rb')

header = f.read(1024)

ttt = numpy.fromstring(header,numpy.uint32)
d1 = ttt[4]
d2 = ttt[5]

#d1 = header[16]*16**3+header[17]*16**2+header[18]*16+header[19]
#d2 = header[20]*16**3+header[21]*16**2+header[22]*16+header[23]

num_ints = (d1 + 31) // 32

total = d2 * num_ints * 4 
data = f.read()
f.close()



data = numpy.fromstring(data, numpy.uint8)
data = numpy.reshape(data, (d2, num_ints * 4))
result = numpy.zeros((d2, num_ints * 4 * 8), numpy.uint8)

bits = numpy.ones((1), numpy.uint8)
for i in range(8):
    temp = numpy.bitwise_and(bits, data)
    result[:, i::8] = temp.astype(numpy.uint8)
    bits = bits * 2

spares = num_ints * 4 * 8 - d1
if spares == 0:
    data = numpy.where(result == 0, 1, 0)
else:
    data = numpy.where(result[:, :-spares] == 0, 1, 0)
    
data = numpy.reshape(data.astype(numpy.uint16),(d2, d1))
data = numpy.flipud(data)
#data = numpy.fliplr(data)

#scipy.misc.toimage(data).save('mask_a.tif')
#scipy.misc.imsave('may28_b.tiff',data)

im = Image.fromstring('I;16',(d1,d2),data.tostring())
im.save(outfile)

#tifim = scipy.misc.toimage(data,high=2**16,cmax=1)
#tifim.save('aaaa.tif')
