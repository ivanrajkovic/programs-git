import pyvirtualcam
from pyvirtualcam import PixelFormat
import numpy
import os

from thorlabs_tsi_sdk.tl_camera import TLCameraSDK, OPERATION_MODE
from thorlabs_tsi_sdk.tl_mono_to_color_processor import MonoToColorProcessorSDK
from thorlabs_tsi_sdk.tl_camera_enums import SENSOR_TYPE

#os.environ['PATH'] = 'C:\\Users\\b_tsuruta\\Documents\\programs\\python\\dlls' + os.pathsep + os.environ['PATH']
#os.add_dll_directory('C:\\Users\\b_tsuruta\\Documents\\programs\\python\\dlls')

os.environ['PATH'] = 'C:\\Users\\rajkovic\\Documents\\programs-git\\thor_cam\\dlls' + os.pathsep + os.environ['PATH']
os.add_dll_directory('C:\\Users\\rajkovic\\Documents\\programs-git\\thor_cam\\dlls')




with pyvirtualcam.Camera(width=2448, height=2048, fps=10) as cam:
    print(f'Using virtual camera: {cam.device}')
    frame = numpy.zeros((cam.height, cam.width, 3), numpy.uint8)  # RGB

    with TLCameraSDK() as sdk:
        available_cameras = sdk.discover_available_cameras()
        if len(available_cameras) < 1:
            print("no cameras detected")

        with sdk.open_camera(available_cameras[0]) as camera:
            camera.exposure_time_us = int(.1*(10**6))  # set exposure in us
            camera.frames_per_trigger_zero_for_unlimited = 0  # start camera in continuous mode
            camera.image_poll_timeout_ms = 2500 # 2.5 second polling timeout

            # set gain (up to 48)
            db_gain = 20.0
            gain_index = camera.convert_decibels_to_gain(db_gain)
            camera.gain = gain_index
            print(f"Set camera gain to {camera.convert_gain_to_decibels(camera.gain)}")


            # set ROI, doesn't work with different size pyvirtualcam
            #old_roi = camera.roi  # store the current roi
            #camera.roi = (100, 100, 600, 600)  # set roi to be at origin point (100, 100) with a width & height of 500


            image_width = camera.image_width_pixels
            image_height = camera.image_height_pixels

            mono_to_color_sdk = MonoToColorProcessorSDK()
            mono_to_color_processor = mono_to_color_sdk.create_mono_to_color_processor(
                camera.camera_sensor_type,
                camera.color_filter_array_phase,
                camera.get_color_correction_matrix(),
                camera.get_default_white_balance_matrix(),
                camera.bit_depth
            )


            camera.arm(2)

            camera.issue_software_trigger()

            while True:
                frame = camera.get_pending_frame_or_null()
                if frame is not None:
                    #print("frame #{} received!".format(frame.frame_count))
                    image_data = numpy.copy(frame.image_buffer)
                    image_data = mono_to_color_processor.transform_to_24(image_data, image_width, image_height)
                    image_data = image_data.reshape(image_height, image_width, 3)


                    cam.send(image_data)
                    cam.sleep_until_next_frame()


                else:
                    print("timeout reached during polling, program exiting...")
                    break



