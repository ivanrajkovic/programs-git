#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@author: rajkovic
'''

import sys

import serial.tools.list_ports

from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QStyleFactory, QMessageBox, QWidget
from PyQt5.QtCore import QTimer, QEvent, Qt

from srs_hv_gui import Ui_MainWindow

class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # set theme: Windows', 'WindowsXP', 'WindowsVista', 'Fusion'
        QApplication.setStyle(QStyleFactory.create("Fusion"))
        self.show()

        self.port_fill_list()
        if len(self.plist) > 0:
            for i,p in enumerate(self.plist):
                if 'USB' in p[1]:
                    self.ui.port_list.setCurrentIndex(i)
                    # HV is on first USB-serial, stop as soon as we find one
                    break

        self.timer=QTimer()
        self.timer.timeout.connect(self.srs_update)

        self.installEventFilter(self)

        ## set variable starting values
        self.sel_port=''
        self.srs_info=''

        self.ui.hv_on.setEnabled(False)
        self.ui.b_set_v.setEnabled(False)
        self.ui.b_set_i.setEnabled(False)


        ## button connections
        self.ui.b_set_v.clicked.connect(self.srs_set_v)
        self.ui.b_set_i.clicked.connect(self.srs_set_i)
        self.ui.port_reload.clicked.connect(self.port_fill_list)
        self.ui.srs_connect.toggled.connect(self.srs_con)
        self.ui.hv_on.toggled.connect(self.hv_on_off)
        self.ui.enable_limit.toggled.connect(self.enable_limit)

    def eventFilter(self, source, event):
        if  self.ui.srs_connect.isChecked() and event.type() == QEvent.KeyPress:
            if event.key() == Qt.Key_Return:
                if QApplication.focusWidget().objectName() == 'set_i':
                    self.srs_set_i()
                elif QApplication.focusWidget().objectName() == 'set_v':
                    self.srs_set_v()


        return QWidget.eventFilter(self, source, event)


    def err_msg_pop(self,etitle,etext):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(etext)
        msg.setWindowTitle(etitle)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def enable_limit(self):
        if not self.ui.enable_limit.isChecked():
            self.err_msg_pop('10kV Enabled','Please check that the correct high voltage cable is connected!')

    def hv_on_off(self):
        if self.ui.hv_on.isChecked():
            to_send='HVON\n'
        else:
            to_send='HVOF\n'
        with serial.Serial(self.sel_port, 9600, timeout=.2) as srs:
                srs.write(to_send.encode())


    def srs_set_v(self):
        if self.ui.srs_connect.isChecked():
            if self.ui.set_v.value() > 5000 and self.ui.enable_limit.isChecked():
                self.err_msg_pop('Voltage limit','Voltage is limited to 5kV')
            else:
                with serial.Serial(self.sel_port, 9600, timeout=.2) as srs:
                    to_send = 'VSET {}\n'.format(self.ui.set_v.value())
                    srs.write(to_send.encode())

    def srs_set_i(self):
        if self.ui.srs_connect.isChecked():
            with serial.Serial(self.sel_port, 9600, timeout=.2) as srs:
                to_send = 'ILIM {}\n'.format(self.ui.set_i.value()/1000)
                srs.write(to_send.encode())


    def port_fill_list(self):
        self.ui.port_list.clear()

        self.plist = sorted(serial.tools.list_ports.comports())

        if len(self.plist) == 0:
            self.err_msg_pop('Port error','No ports found')
        else:
            for port, desc, hwid in self.plist:
                self.ui.port_list.addItem('{} - {}'.format(port,desc))

    def srs_con(self):
        if self.ui.srs_connect.isChecked():
            if len(self.plist) == 0:
                self.err_msg_pop('Port error','No ports found')
                self.ui.srs_connect.setChecked(False)
            else:
                self.sel_port = self.plist[self.ui.port_list.currentIndex()][0]
                self.ui.port_list.setEnabled(False)
                self.ui.port_reload.setEnabled(False)
                with serial.Serial(self.sel_port, 9600, timeout=.2) as srs:
                    srs.write(b'*IDN?\n')
                    self.srs_info = srs.readline().decode()

                if len(self.srs_info.split(','))>1:
                    dd = self.srs_info.split(',')[1]
                    if dd != 'PS365':
                        self.err_msg_pop('Connection error','Wrong SRS device ({}) found on port {} !\nLooking for PS365'.format(dd, self.sel_port))
                        self.ui.srs_connect.setChecked(False)
                    else:
                        self.ui.srs_status.showMessage('Connected to {}'.format(' '.join(self.srs_info.split(',')[0:2])))
                        self.timer.start(1000)
                        self.ui.hv_on.setEnabled(True)
                        self.ui.b_set_v.setEnabled(True)
                        self.ui.b_set_i.setEnabled(True)
                else:
                    self.err_msg_pop('Connection error','No SRS HV supply found on port {} !'.format(self.sel_port))
                    self.ui.srs_connect.setChecked(False)


        else:
            self.ui.srs_status.showMessage(' ')
            self.timer.stop()
            self.ui.hv_on.setEnabled(False)
            self.ui.b_set_v.setEnabled(False)
            self.ui.b_set_i.setEnabled(False)
            self.ui.port_list.setEnabled(True)
            self.ui.port_reload.setEnabled(True)

    def srs_update(self):
        with serial.Serial(self.sel_port, 9600, timeout=.2) as srs:
            srs.write(b'IOUT?\n')
            iout = srs.readline()
            self.ui.i_out.display(float(iout))


            srs.write(b'VOUT?\n')
            vout = srs.readline()
            self.ui.v_out.display(float(vout))


            srs.write(b'ILIM?\n')
            iset = srs.readline()
            self.ui.i_set_to.display(1000*float(iset))


            srs.write(b'VSET?\n')
            vset = srs.readline()
            self.ui.v_set_to.display(float(vset))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWindow()
    sys.exit(app.exec_())