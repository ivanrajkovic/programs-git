#!/usr/bin/env python

from PIL import Image
import numpy as np
import sys
#sys.ps1='block'
import pyinotify
import os
import time
from math import *
#import matplotlib
#matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt

if len(sys.argv) == 1:
     sys.exit("\nUsage:" + sys.argv[0] + " how to\n")
    
first_file = sys.argv[1]
 
if len(sys.argv) > 2:
     step_size = sys.argv[2]
else:
     step_size=1000

folwatch = os.getcwd()
plt.ion()

plt.figure()
plt.show()
    
wm = pyinotify.WatchManager()  # Watch Manager
mask = pyinotify.IN_CREATE 

class EventHandler(pyinotify.ProcessEvent):
     def process_IN_CREATE(self, event):
          nnn = os.path.basename(event.pathname) 
          if nnn.split('.')[-1]=='tif' and (int(nnn[-8:-4])%step_size)==int(first_file):
               print(nnn)
               time.sleep(2)
               im = Image.open(nnn)
               imarray=np.array(im)
               temp=np.partition(-imarray.flatten(),4)
               plt.imshow(imarray,vmax=-temp[4])
               plt.draw()


handler = EventHandler()
notifier = pyinotify.Notifier(wm, handler)
wdd = wm.add_watch(folwatch, mask, rec=True)

notifier.loop()

