import numpy

Cv = 4.184 # J/(g*K)
ro = .001 # g/(mm^3)

A = 0.2 * 0.2 #(mm^2)
l= 1 # mm

Nph = 1*10**13  # 1/second
Eph = 11000 * 1.602 * 10**(-19)  # 11keV -> J
t = 0.1  # second

att = 2.625 # mm , for water at 11keV

T = t * Eph * Nph *( 1 - numpy.exp(-l/att))/(ro*Cv*A*l)   # K

print("Temparature change in {0:.1g} seconds is {1:.2g} K".format(t,T))


#Tlin = t * Eph * Nph /(ro*Cv*A*att)  # linear approx. for absorption