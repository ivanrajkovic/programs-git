#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 10:18:00 2015

@author: rajkovic
"""

import sys
import os
import glob
import numpy

if len(sys.argv) < 3:
    sys.exit("\nUsage: "+ sys.argv[0] + " <first scan> <last scan> \n\nShould be run from the analysis folder.\nIt will add '-f sample buffer' lines to 'integ_manual.mpp' file.")
 
integmpp = './integ_manual.mpp' 
data_folder = '../data/'

start_scan = sys.argv[1]
end_scan = sys.argv[2]

def _find_files(sn):
     
    full_sample_file = glob.glob(data_folder+'*[SB]'+str(sn).zfill(3)+'*01.tif')
    if full_sample_file == []:
        return ['skip']
    else:
        sample_file=os.path.basename(full_sample_file[0])
    sample_name = '_'.join(sample_file.split('_')[:-4])
    raw = sample_file.split('_')[-4][0:2]
    all_buffer_files = glob.glob(data_folder+sample_name+'_'+raw+'A_[B]*01.tif')
    bn = numpy.array([])
    for n in xrange(0,len(all_buffer_files)):
          bn = numpy.append(bn,[int(all_buffer_files[n].split('_')[3][1:])])
    buffer_file=os.path.basename(all_buffer_files[bn.argmin()])
    if sample_file != buffer_file:
        return sample_file, buffer_file
    else:
        return ['skip']

outfile = open(integmpp, 'a')

for xx in xrange(int(start_scan),int(end_scan)+1):
    aa = _find_files(xx)
    if len(aa)==2:
        outfile.write('\n-f ../data/'+aa[0]+" ../data/"+aa[1])
        
outfile.close()


    
