#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division
import sys
import os
import distutils.spawn
from distutils.dir_util import mkpath
import shutil
import fileinput
import subprocess
import re



if __name__ == "__main__":
    
    # get the path for sastool
    exe_path='/mnt/home/sw/bin'
    try:
        os.environ['PATH']=os.environ['PATH']+':'+exe_path
    except:
        os.environ['PATH']=exe_path
    sastool = distutils.spawn.find_executable('sastool')
    
    # set sample and buffer file names from input parameters
    sample_file = sys.argv[1]
    buffer_file = sys.argv[2]
    
    
    # set data and analysis folders from the sample file path
    data_fol = os.path.realpath(os.path.dirname(sample_file))
    analysis_fol = os.path.normpath(os.path.join(data_fol,'../analysis/'))
    
 
    # prepare a temporary folder for pushthrough analysis
    
    pt_analysis = os.path.join(analysis_fol,'.pt_temp')
    mkpath(pt_analysis)
    
    # new .mpp file name/ location
    pt_mpp = os.path.join(pt_analysis,'integ_pt.mpp')
    
    # copy integ.mpp to the pt_analysis folder as integ_pt.mpp, remove all lines starting with '-f' or '#', and all empty lines
    # uncomment '-s' if it was commented out
    # add full path to the mask file, if necessary
    
    
    shutil.copy(os.path.join(analysis_fol,'integ.mpp'),pt_mpp)
    
    for line in fileinput.input(pt_mpp, inplace = 1):
        if line.startswith('#-s') or line.startswith('# -s'):
            line = re.sub('# *-s','-s',line)
        if line.startswith('-m'):
            mask_file = line.split(' ')[-1]
            if os.path.isabs(mask_file) == False:
                line = '-m yes {}'.format(os.path.abspath(os.path.join(analysis_fol,mask_file)))
        if (not line.startswith('-f') and not line.startswith('#') and not re.match(r'^\s*$', line)):        
            sys.stdout.write(line)
    

    # add files to be processed
    ff = open(pt_mpp,'a')
    ff.write('-f '+sample_file+' '+buffer_file)
    ff.close()
    
    # run sastool and get output
    sastool_out = subprocess.check_output([sastool,'integ_pt.mpp'],cwd=pt_analysis)

    # check sastool output, see if there are any rejected frames between 'Transmission high' rejections
    start_high = 0
    end_high = 0
    status = 0 # 0 -> no damage ; 1 -> damage
    
    #for z in sastool_out.split('Use intensity from *.int file')[-1].split('\n'):
    for z in sastool_out.split('frame #1:')[-1].split('\n'):    
        # set 'start_high = 1' once we find first line with 'Transm high!!' 
        if(start_high == 0 and z.endswith('Transm high!!')):
            start_high = 1
        
        # if start_high is 1, find first line that is not 'Transm high!!' and set end_high = 0
        if (end_high == 0 and start_high == 1 and not z.endswith('Transm high!!')):
            end_high = 1
        
        
        # if end_high is 1 and ther eis 'Frame rejected' line before 'Transm high' line, there was sample damage (or other sample problem)
        # set status to 1, exit loop
        if (end_high == 1 and z.endswith('Frame rejected!!')):
            status = 1
            print(z)
            break
    
    
        # if end_high is 1 and there is another line with 'Transm high', there was no sample damage
        # exit loop
        if (end_high == 1 and z.endswith('Transm high!!')):
            print(z)
            break

    # if we came to the end without finding 'Transm high' or 'Frame rejected', something is wrong
    else:
        status = 1

    # write status to the stdout
    
    sys.stdout.write('{}\n'.format(status))
    sys.stdout.flush()

        


    
    

    