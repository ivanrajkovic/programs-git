#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division

import time
# starttime = time.time()

import sys
import os
import re
import fileinput
import subprocess32
import glob
import argparse
import numpy
from distutils.dir_util import mkpath
import distutils.spawn

def set_path():
    global sastool

    sastool = distutils.spawn.find_executable('sastool')

    if sastool == None:
        sys.exit('\n \033[0;37;41m\033[1m\033[4m \'sastool\' not found, please add it to the path.\033[0;0m \n')
        #print('\'sastool\' not found, please add it to the path.')



def find_image(folder,num):
    im_list=[]
    # find the image filename for the given series number
    rrr = re.compile('.*[SBT]'+str(num).zfill(3)+'_0_0+1\.tif')
    for file in os.listdir(folder):
        if rrr.match(file):
            im_list.append(file)
    if len(im_list) == 0:
        sys.exit("\nNo files found!\n")
    elif len(im_list) > 1:
        print("\n\033[0;37;41m\033[1m\033[4m Two or more files found! \033[0;0m")
        sys.exit(im_list)
    else:
        return im_list[0]


def get_series(in_fol,in_exp):
    if os.path.isfile(str(in_exp)):
        exp_name = in_exp
    else:
        try:
            exp_name=os.path.join(in_fol,find_image(in_fol,int(in_exp)))
        except:
            exp_name = ""
    return(exp_name)



if __name__ == "__main__":

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
     calculates (sam-cap_1) - (buf-cap_2)

     all parameters can be given as their series number or path to the first file of the series
     """)
    parser.add_argument('sam', help='sample file path of series number')
    parser.add_argument('cap_1', help='cap1 file path of series number')
    parser.add_argument('buf', help='buffer file path or series number')
    parser.add_argument('cap_2', help='cap2 file path of series number')
    parser.add_argument('-df','--data_folder', default='../data' , help='path to the data folder, default is ../data. used only when sample is specified by series number')

    arg_in = parser.parse_args()

    set_path()

    sam_name = get_series(arg_in.data_folder,arg_in.sam)
    if sam_name == "":
        sys.exit('Sample not found')

    cap1_name = get_series(arg_in.data_folder,arg_in.cap_1)
    if cap1_name == "":
        sys.exit('Cap_1 not found')

    buf_name = get_series(arg_in.data_folder,arg_in.buf)
    if buf_name == "":
        sys.exit('Buffer not found')

    cap2_name = get_series(arg_in.data_folder,arg_in.cap_2)
    if cap2_name == "":
        sys.exit('Cap_2 not found')

    # remove -f lines from integ.mpp
    for line in fileinput.input('integ.mpp', inplace = 1):
        #if line.startswith('-s') or line.startswith('#-s') or line.startswith('# -s'):
        if re.match('^#? *-s',line):
            line = re.sub('# *-s','-s',line)
        if (not line.endswith('\n')):
            line = line + '\n'
        if (not line.startswith('-f') and not line.startswith('#') and not re.match(r'^\s*$', line)):
            sys.stdout.write(line)

    # add -f lines
    ff = open('integ.mpp','a')
    ff.write('-f {} {}\n'.format(sam_name,cap1_name))
    ff.write('-f {} {}\n'.format(buf_name,cap2_name))
    ff.close()

    # run sastool in
    psas = subprocess32.Popen([sastool,'integ.mpp'],cwd=os.getcwd())
    psas.communicate()

    samsub = os.path.join(os.path.splitext(os.path.basename(sam_name))[0]+'.sub')
    bufsum =  os.path.join(os.path.splitext(os.path.basename(buf_name))[0]+'.sub')


    samres = numpy.loadtxt(samsub)
    bufres = numpy.loadtxt(bufsum)

    sambuf = numpy.full_like(samres,0)

    sambuf[:,0] = samres[:,0]
    sambuf[:,1] = samres[:,1] - bufres[:,1]
    sambuf[:,2] = numpy.sqrt(samres[:,2]**2+bufres[:,2]**2)

    mkpath('./subs')
    savename = '({}-{})-({}-{}).dat'.format(arg_in.sam,arg_in.cap_1,arg_in.buf,arg_in.cap_2)

    numpy.savetxt(os.path.join('./subs',savename),sambuf)


