#!/usr/bin/env python
# 
# secpipe.py <path to first file> <num_buf> <end_num> <wait_time[s]> [<shut_cl_start> <shut_cl_end>]


'''
@author: rajkovic
'''

from __future__ import division

import sys
import os
import re
import shutil
import time
import distutils.spawn
from distutils.dir_util import mkpath
import subprocess
import glob
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.rcParams['savefig.dpi'] = 200
plt.rcParams['lines.markersize'] = 7.0
plt.rcParams['lines.markeredgewidth'] = 1.5
plt.rcParams['lines.linewidth'] = 3.0
import matplotlib.patheffects as pe
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
from heapq import nlargest
import fileinput

from scipy.signal import find_peaks, peak_widths
from scipy.optimize import curve_fit
from scipy.stats import pearsonr
from matplotlib import gridspec

import getpass
import logging

def file_len(fname):
    try:
        i = -1
        with open(fname) as f:
            for i, l in enumerate(f):
                pass
        return(i + 1)
    except IOError:
        print('File not found!')
        return(0)

def get_hl(bf):
    # number of header lines ion bip file
    hl = 0
    # regex for multiple numbers separated by space. 
    nn = re.compile('^(?:[-+]?(?:\d*\.)?\d+(?:[eE][-+]?\d+)?\ ?)+$')
    
    with open(bf) as ff:
        for line in ff:
            if re.match(nn,line):
                break
            else:
                hl+=1
    return(hl)

def func(x, *params):
    global bkg_dg
    y = numpy.zeros_like(x)
    for i in range(2, len(params), 3):
        ctr = params[i]
        amp = params[i+1]
        wid = params[i+2]

        if i == 2:
            nn=bkg_dg
            y = y + (params[0]+params[1]*x)*amp * numpy.exp( -(numpy.abs(x - ctr)/wid)**nn)
        else:
            nn=2
            y = y + amp * numpy.exp( -(numpy.abs(x - ctr)/wid)**nn)

    return(y)


def finish_analysis():
    global bkg_dg
    os.remove(log_file_tmp)
    
    # save data
    numpy.savetxt(os.path.join(plots_sfol,'{}_plot_data.txt'.format(sample_name_ser)),numpy.transpose([im_num_ar,rg_ar,i0_ar,iqmin_ar]),fmt='%.8G',delimiter='\t',header='im.num. \tRg \tI0 \tIqL')
    # read UV data, if exists
    uv_bip = os.path.join(data_fol,sample_name_ser+'uv.bip')
    try:
        header_lines = get_hl(uv_bip)
        uv_data = numpy.genfromtxt(uv_bip,skip_header=header_lines)
        
    except Exception as e:
        print(e)
        print('No UV data')
        uv_data=[]
    
    offset_uv_x = 0
    uv1w = 1
    uv2w = 2 
    try:
        uv_setup = os.path.join(data_fol,'SecSetup_'+sample_name_ser)+'.txt'
        with open(uv_setup) as uvs:
            for ll in uvs:
                if ll.startswith('Shutter Delay:'):
                    offset_uv_x = int(ll.split(':')[-1])
                elif ll.startswith('Ch8 wavelength(nm):'):
                    uv1w = int(ll.split(':')[-1])
                elif ll.startswith('Ch9 wavelength(nm):'):
                    uv2w = int(ll.split(':')[-1])
    except Exception as e:
        print(e)
        print('No UV extra info')
    
    if uv_data!=[]:
        uv_data[:,0] = uv_data[:,0] + offset_uv_x
        numpy.savetxt(os.path.join(plots_sfol,'{}_UV_data.txt'.format(sample_name_ser)),uv_data,fmt='%.8G',delimiter='\t',header='im.num. \tUV1 \tUV2 \tC(mg/ml) \tC(mg/ml)')
    
    
    # make graphs
    
    # I0 and Iqmin graph
    
    plt.figure(num=None)
    plt.title('{}'.format(sample_name_ser),fontsize=22)
    plt.grid(axis='both')
    plt.plot(im_num_ar,i0_ar,'o',markeredgecolor=sec_col[1],markerfacecolor=sec_col[1],label='I$_0$')
    plt.plot(im_num_ar,iqmin_ar,'o',markeredgecolor=sec_col[2],markerfacecolor='none',label='I$_{q_L}$ '+'($q_L$={:.4f}'.format(sub_d[5,0])+'A$^{-1}$)')
    plt.xlabel('Image number',fontsize=18)
    plt.ylabel('I$_0$, I$_{q_L}$',fontsize=18)
    yy_max=nlargest(10,i0_ar)[-1]
    try:
        plt.ylim(0,yy_max*1.2)
        plt.yticks(numpy.arange(0,yy_max*1.2,1.2*yy_max/6)," ")
    except Exception as e:
        print(e)
    if shut_cl_end < end_num:
        plt.xlim(xmin=shut_cl_end-20)
    plt.xlim(xmax=end_num+7)
    
    plt.legend(numpoints=1,loc=2,fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
    plt.tight_layout()
    plt.savefig(os.path.join(plots_sfol,'i0_iql_{}.png'.format(sample_name_ser)))
    # try to make zoomed-in graphs
    peak_pos=numpy.argmax(iqmin_ar)
    try:
        plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
        plt.savefig(os.path.join(plots_sfol,'zoom1_i0_iql_{}.png'.format(sample_name_ser)))
    except Exception as e:
        print(e)
        print('Could not make zoomed-in graph')
    # zoom-in part two
    try:
        xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
        xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
        plt.xlim(xxmin-20,xxmax+10)
        plt.savefig(os.path.join(plots_sfol,'zoom2_i0_iql_{}.png'.format(sample_name_ser)))
    except Exception as e:
        print(e)
        print('Could not make zoomed-in graph')
    
    # Rg graph
    
    plt.figure(num=None)
    plt.title('{}'.format(sample_name_ser),fontsize=22)
    plt.grid(axis='both')
    plt.plot(im_num_ar,rg_ar,'o',markeredgecolor=sec_col[0],markerfacecolor='none',label='R$_g$')
    plt.xlabel('Image number',fontsize=18)
    plt.ylabel('R$_g$ [$\AA$]',fontsize=18)
    try:
        plt.ylim(0,1.4*nlargest(10,rg_ar)[-1])
    except Exception as e:
        print(e)
    if shut_cl_end < end_num:
        plt.xlim(xmin=shut_cl_end-20)
    plt.xlim(xmax=end_num+7)
    plt.legend(numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
    plt.tight_layout()
    plt.savefig(os.path.join(plots_sfol,'rg_{}.png'.format(sample_name_ser)))
    #plt.savefig(os.path.join(plots_sfol,'rg_{}.pdf'.format(sample_name)))
    # try to make zoomed-in graphs
    try:
        plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
        plt.savefig(os.path.join(plots_sfol,'zoom1_rg_{}.png'.format(sample_name_ser)))
        #plt.savefig(os.path.join(plots_sfol,'zoom1_rg_{}.pdf'.format(sample_name)))
    except Exception as e:
        print(e)
        print('Could not make zoomed-in graph')   
    # zoom-in part two
    try:
        xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
        xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
        plt.xlim(xxmin-20,xxmax+10)
        plt.savefig(os.path.join(plots_sfol,'zoom2_rg_{}.png'.format(sample_name_ser)))
        #plt.savefig(os.path.join(plots_sfol,'zoom2_rg_{}.pdf'.format(sample_name)))
    except Exception as e:
        print(e)
        print('Could not make zoomed-in graph')  
    # I0, Iqmin and Rg graph
    
    fig = plt.figure(num=None)
    plt.title('{}'.format(sample_name_ser),fontsize=22)
    plt.xlabel('Image number',fontsize=18)
    plt.grid(axis='x')
    ax1 = fig.add_subplot(111)
    sc1 = ax1.plot(im_num_ar,i0_ar,'o',markeredgecolor=sec_col[1],markerfacecolor=sec_col[1],label='I$_0$')
    sc2 = ax1.plot(im_num_ar,iqmin_ar,'o',markeredgecolor=sec_col[2],markerfacecolor='none',label='I$_{q_L}$ '+'($q_L$={:.4f}'.format(sub_d[5,0])+'A$^{-1}$)')
    ax1.set_ylabel('I$_{0}$, I$_{q_L}$',fontsize=18)
    ax1.set_yticklabels([])
    ax2 = ax1.twinx()
    sc3 = ax2.plot(im_num_ar,rg_ar,'o',markeredgecolor=sec_col[0],markerfacecolor='none',label='R$_g$')
    ax2.set_ylabel('R$_g$ [$\AA$]',color='r',fontsize=18)
    try:
        ax2_max = int(1.4*nlargest(10,rg_ar)[-1])
        ax2.set_ylim(0,ax2_max)
        # ticks for the Rg y-axis
        rg_ticks = max(5,int(ax2_max/6)-numpy.mod(int(ax2_max/6),5))
        ax2.yaxis.set_ticks(numpy.arange(0,ax2_max,rg_ticks))
    except Exception as e:
        print(e)
    ax2.grid(axis='y')
    try:
        ax1_max = 1.2*nlargest(10,i0_ar)[-1]
        ax1.set_ylim(0,ax1_max)
        # try to make y-ticks at the same place on both sides
        ax1.yaxis.set_ticks(numpy.arange(0,ax1_max,(ax1_max/ax2_max)*rg_ticks))
    except Exception as e:
        print(e)
    for tl in ax2.get_yticklabels():
        tl.set_color('r')
    if shut_cl_end < end_num:
        plt.xlim(xmin=shut_cl_end-20)
    plt.xlim(xmax=end_num+7)
    # put labels together
    sc = sc1+sc2+sc3
    labels = [l.get_label() for l in sc]
    ax2.legend(sc,labels,numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
    plt.savefig(os.path.join(plots_sfol,'i0_iql_rg_{}.png'.format(sample_name_ser)))
    # try to make zoomed-in graphs
    try:
    
        plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
        plt.savefig(os.path.join(plots_sfol,'zoom1_i0_iql_rg_{}.png'.format(sample_name_ser)))
    except Exception as e:
        print(e)
        print('Could not make zoomed-in graph')
    # zoom-in part two
    try:
        xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
        xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
        plt.xlim(xxmin-20,xxmax+10)
        plt.savefig(os.path.join(plots_sfol,'zoom2_i0_iql_rg_{}.png'.format(sample_name_ser)))
    except Exception as e:
        print(e)
        print('Could not make zoomed-in graph')
    
    
    
    if uv_data!=[]:
        
        # UV and rg graph
        
        # labels
        uv1l = 'UV$_{'+'{}'.format(uv1w)+'}$'
        uv2l = 'UV$_{'+'{}'.format(uv2w)+'}$'
        
        # uv ratio and Rg
        fig=plt.figure(num=None)
        plt.title('{}'.format(sample_name_ser),fontsize=22)
        plt.xlabel('Image number',fontsize=18)
        plt.grid(axis='x')
        ax1 = fig.add_subplot(111)
        sc1 = ax1.plot(uv_data[:,0],uv_data[:,1]/uv_data[:,2],'-',color=sec_col[3],label=uv1l+'/'+uv2l)
        ax1.set_ylabel(uv1l+'/'+uv2l,fontsize=18)
        ax1.set_ylim(max(0,-0.5+min(uv_data[:,1]/uv_data[:,2])),min(5,0.5+max(uv_data[:,1]/uv_data[:,2])))
        ax2 = ax1.twinx()
        sc2 = ax2.plot(im_num_ar,rg_ar,'o',markeredgecolor=sec_col[0],markerfacecolor='none',label='R$_g$')
        ax2.set_ylabel('R$_g$ [$\AA$]',color=sec_col[0],fontsize=18)
        ax2_max = int(1.4*nlargest(10,rg_ar)[-1])
        ax2.set_ylim(0,ax2_max)
        sc = sc1+sc2
        labels = [l.get_label() for l in sc]
        ax2.legend(sc,labels,numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
        ax2.grid(axis='y')
        #ax1.yaxis.set_ticks((10*numpy.arange(0,5.,(5./ax2_max)*rg_ticks)).astype('int')/10.)
        plt.savefig(os.path.join(plots_sfol,'uv_rg_{}.png'.format(sample_name_ser)))
        #plt.savefig(os.path.join(plots_sfol,'uv_rg_{}.pdf'.format(sample_name)))
        try:
            plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
            plt.savefig(os.path.join(plots_sfol,'zoom1_uv_rg_{}.png'.format(sample_name_ser)))
        except:
            print('Could not make zoomed-in graph')
        try:
            xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
            xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
            plt.xlim(xxmin-20,xxmax+10)
            plt.savefig(os.path.join(plots_sfol,'zoom2_uv_rg_{}.png'.format(sample_name_ser)))
        except:
            print('Could not make zoomed-in graph')
        
        # UV1 and UV2 and conc
        fig=plt.figure(num=None)
        plt.title('{}'.format(sample_name_ser),fontsize=22)
        plt.grid(axis='x')
        plt.xlabel('Image number',fontsize=18)
        
        ax1 = fig.add_subplot(111)
        ax1.set_ylabel('UV',fontsize=18)
        sc1a = ax1.plot(uv_data[:,0],uv_data[:,1],'-',color=sec_col[3],label=uv1l, zorder=5)
        sc1b = ax1.plot(uv_data[:,0],uv_data[:,2],'-',color=sec_col[4],label=uv2l, zorder=4)
        
        ax2 = ax1.twinx()
        ax2.set_ylabel('Concentration [mg/ml]',color=sec_col[3],fontsize=18,path_effects=[pe.withStroke(linewidth=2, foreground="black")])
        sc2a = ax2.plot(uv_data[:,0],uv_data[:,3],'-',color=sec_col[3])
        
        #sc2b = ax2.plot(uv_data[:,0],uv_data[:,4],'mo',label='C$_{wce}$')
        
        # plot uv1 and conc graph one over another
        yyll = ax1.get_ylim()
        ax2.set_ylim(yyll[0]*uv_data[:,3].max()/uv_data[:,1].max(),yyll[1]*uv_data[:,3].max()/uv_data[:,1].max())
        
        sc = sc1a+sc1b  #+sc2a+sc2b
        labels = [l.get_label() for l in sc]
        ax2.legend(sc,labels,numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
        
        ax2.grid(axis='y')
        plt.tight_layout()
        
        plt.savefig(os.path.join(plots_sfol,'uv_conc_{}.png'.format(sample_name_ser)))
        
        try:
            plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
            plt.savefig(os.path.join(plots_sfol,'zoom1_uv_conc_{}.png'.format(sample_name_ser)))
        except Exception as e:
            print(e)
            print('Could not make zoomed-in graph')
            
        try:
            xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
            xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
            plt.xlim(xxmin-20,xxmax+10)
            plt.savefig(os.path.join(plots_sfol,'zoom2_uv_conc_{}.png'.format(sample_name_ser)))
        except Exception as e:
            print(e)
            print('Could not make zoomed-in graph')
    
        # fit gaussians, use 'try' since a lot of things can go wrong
        try:
            # find peaks
            pp = find_peaks(uv_data[:,1],prominence=numpy.max(uv_data[:,1])/50)
            logging.info('found {} UV peaks'.format(len(pp[0])))
            if len(pp[0]) < 8:
                # peak height and width
                c_height = uv_data[pp[0],1] - pp[1]['prominences']
                w_half = peak_widths(uv_data[:,1], pp[0], rel_height=.5)
                
                # initial gauss fitting parameters
                guess = numpy.ravel([[uv_data[pp[0][i],0], pp[1]['prominences'][i], w_half[0][i]] for i in range(len(pp[0]))])
                # prepend background
                guess = numpy.append([1.0,0.0,numpy.mean([pp[0][0],pp[0][-1]]),50,(pp[0][-1]-pp[0][0])*2],guess)
                
                #limit to positive values
                bl = numpy.zeros_like(guess)
                bu = numpy.ones_like(guess)*numpy.inf
                # linear part can be negative
                bl[:2] = -numpy.inf
                
                # limit first peak position
                #bl[5] = guess[5]-1
                #bu[5] = guess[5]+1
                # limit first peak width
                bl[7] = guess[7]-1
                bu[7] = guess[7]+1
                
                # degree for background flat-top gauss
                bkg_dg = 6
                
                # function fit
                popt, pcov = curve_fit(func, uv_data[:,0], uv_data[:,1], p0=guess, bounds=(bl,bu))
                fit = func(uv_data[:,0], *popt)
                
                # make plots
                plt.subplots(2,1)
                plt.suptitle('{}'.format(sample_name_ser),fontsize=22)
                gs = gridspec.GridSpec(2, 1, height_ratios=[5, 1], wspace=0.0, hspace=0.0)
                ax0 = plt.subplot(gs[0])
                ax0.plot(uv_data[:,0], uv_data[:,1],color=sec_col[3])
                ax0.plot(uv_data[:,0], fit , ':m')
                
                for l in range(2,len(popt),3):
                    ctr = popt[l]
                    amp = popt[l+1]
                    wid = popt[l+2]
                
                    if l == 2 :
                        nn=bkg_dg
                        dd = (popt[0]+popt[1]*uv_data[:,0])*amp * numpy.exp( -(numpy.abs(uv_data[:,0] - ctr)/wid)**nn)
                    else:
                        nn=2
                        dd = amp * numpy.exp( -(numpy.abs(uv_data[:,0] - ctr)/wid)**nn)
                
                    ax0.plot(uv_data[:,0],dd,'--')
                
                # peak positions from initial find
                #ax0.plot(uv_data[pp[0],0],uv_data[pp[0],1],'xr')
                # remove numbers for x-axis
                ax0.set_xticklabels([])
    
                ax1 = plt.subplot(gs[1])
                ax1.plot(uv_data[:,0],(uv_data[:,1]-fit),'k')
                ax1.yaxis.set_major_locator(plt.MaxNLocator(3))
                ax1.yaxis.set_label_position("right")
                ax1.yaxis.tick_right()
                ax0.set_xlim(uv_data[0,0],uv_data[-1,0])
                ax1.set_xlim(uv_data[0,0],uv_data[-1,0])
                plt.savefig(os.path.join(plots_sfol,'uv_fit_{}.png'.format(sample_name_ser)))
                try:
                    ax0.axvline(numpy.ceil(popt[-6]+3*popt[-4]),color='red')
                    ax0.minorticks_on()
                    ax0.grid(axis='x',which='major',color='#111111')
                    ax0.grid(axis='x',which='minor',color='#999999')
                    ax1.tick_params(which='minor', bottom=False, left=False)
                    ax0.set_xlim([popt[-6]-1.5*popt[-4],popt[-3]+1.5*popt[-1]])
                    ax1.set_xlim([popt[-6]-1.5*popt[-4],popt[-3]+1.5*popt[-1]])
                    ax1.yaxis.set_major_locator(plt.MaxNLocator(3))
                    plt.savefig(os.path.join(plots_sfol,'zoom1_uv_fit_{}.png'.format(sample_name_ser)))
                except Exception as e:
                    print(e)
                    print('Could not make zoomed-in uv fit graph')
        except Exception as e:
            print(e)
            print('Could not fut gaussians')


        # all-in-one graph: I0,Iql,Rg and UV1
        plt.figure()
        # this works for matplotlib 2, version 3 has a different way
        # using axisartist to move the third y-axis to the side
        ax1=host_subplot(111, axes_class=AA.Axes)
        plt.subplots_adjust(right=0.75)
        ax2 = ax1.twinx()
        ax3 = ax1.twinx()
        
        offset = 60
        new_fixed_axis = ax3.get_grid_helper().new_fixed_axis
        ax3.axis["right"] = new_fixed_axis(loc="right",axes=ax3,offset=(offset, 0))
        
        ax2.axis["right"].toggle(all=True)
        ax3.axis["right"].toggle(all=True)
        
        plt.title('{}'.format(sample_name_ser),fontsize=22)
        plt.xlabel('Image number',fontsize=18)
        plt.grid(axis='x')
        
        
        sc1 = ax1.plot(im_num_ar,rg_ar,'o',markeredgecolor=sec_col[0],markerfacecolor='none',label='R$_g$',zorder=4)
        
        sc2a = ax2.plot(im_num_ar,i0_ar,'o',markeredgecolor=sec_col[1],markerfacecolor=sec_col[1],label='I$_{0}$',zorder=3)
        
        sc2b = ax2.plot(im_num_ar,iqmin_ar,'o',markeredgecolor=sec_col[2],markerfacecolor='none',label='I$_{q_L}$ '+'($q_L$={:.4f}'.format(sub_d[5,0])+'A$^{-1}$)',zorder=2)
        
        sc3 = ax3.plot(uv_data[:,0],uv_data[:,1],'-',color=sec_col[3],label=uv1l,zorder=1)
        ax3.set_ylim(bottom=0)
        
        ax1.axis["left"].label.set(text='R$_g$  [$\AA$]',color=sec_col[0],size=18)
        ax2.axis["right"].label.set(text='I$_{0}$, I$_{q_L}$',color=sec_col[1],size=18)
        ax3.axis["right"].label.set(text='UV',color=sec_col[3],size=18,path_effects=[pe.withStroke(linewidth=2, foreground="black")])
        
        sc = sc1+sc2a+sc2b+sc3
        labels = [l.get_label() for l in sc]
        ax2.legend(sc,labels,numpoints=1,loc=2, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
        ax2.grid(axis='y')
        plt.tight_layout()
        #ax1.yaxis.set_ticks((10*numpy.arange(0,5.,(5./ax2_max)*rg_ticks)).astype('int')/10.)
        plt.savefig(os.path.join(plots_sfol,'all_{}.png'.format(sample_name_ser)))
        try:
            plt.xlim(im_num_ar[peak_pos]-70,im_num_ar[peak_pos]+70)
            plt.savefig(os.path.join(plots_sfol,'zoom1_all_{}.png'.format(sample_name_ser)))
        except:
            print('Could not make zoomed-in graph')
        try:
            xxmin = im_num_ar[numpy.where(numpy.array(i0_ar)>(0.2*max(i0_ar)))[0][0]]
            xxmax = im_num_ar[len(i0_ar)-1-numpy.where(numpy.array(i0_ar)[::-1]>(0.2*max(i0_ar)))[0][0]]
            plt.xlim(xxmin-20,xxmax+10)
            plt.savefig(os.path.join(plots_sfol,'zoom2_all_{}.png'.format(sample_name_ser)))
        except:
            print('Could not make zoomed-in graph')
    
        
    # average sub files
    if shut_cl_start > end_num:
        av_start = numpy.ceil((num_buf+1)/5.)*5
    else:
        av_start=numpy.ceil((shut_cl_end+1)/5.)*5
    
    for aa in numpy.arange(av_start,end_num-4,5):
        try:
            sub_file = os.path.join(sastool_sfol,sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+".sub")
            avg_data = numpy.genfromtxt(sub_file)
            # use variance instead of stdev
            avg_data[:,2] = avg_data[:,2]**2
            for zz in numpy.arange(1,5):
                sub_file = os.path.join(sastool_sfol,sample_name_ser+"_0_"+str(int(aa+zz)).zfill(num_length)+".sub")
                sub_data = numpy.genfromtxt(sub_file)
                avg_data[:,1]=avg_data[:,1]+sub_data[:,1]
                avg_data[:,2]=avg_data[:,2]+sub_data[:,2]**2
            
            avg_data[:,1] = avg_data[:,1]/5
            # back to stdev form variance
            avg_data[:,2] = numpy.sqrt(avg_data[:,2])/5
                
            numpy.savetxt(os.path.join(avg_sfol,"Ave_"+sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+"-"+str(int(aa+4)).zfill(num_length)+'.dat'),avg_data,delimiter='\t',fmt='%.6e')
        except Exception as e:
            print(e)
            print("Problem averaging data!")
    
    # move data file
    # get list of tif, prp and int files from buf_name, shutil.move to subfolder in data folder
    ff_to_move=glob.glob('_'.join(buf_name.split('_')[:-1])+'*tif')+glob.glob('_'.join(buf_name.split('_')[:-1])+'*prp')+glob.glob('_'.join(buf_name.split('_')[:-1])+'*int')
    fol_to_move = os.path.join(data_fol,sample_name_ser)
    mkpath(fol_to_move)
    for ff in ff_to_move:
        shutil.move(ff,os.path.join(fol_to_move,os.path.basename(ff)))
    
    # copy bip and SecSetup file since it might be still used by BluIce
    ff_to_copy=glob.glob('_'.join(buf_name.split('_')[:-2])+'*bip')+glob.glob(os.path.join(data_fol,'SecSetup_'+sample_name_ser)+'*.txt')
    logging.info('Copying {}'.format(ff_to_copy))
    for ff in ff_to_copy:
        shutil.copy(ff,os.path.join(fol_to_move,os.path.basename(ff)))    
    
    
    sys.exit('Done!')

## Program start

if __name__ == "__main__":
    
    try:
        logfile = "/tmp/secpipe_{}.txt".format(getpass.getuser())
    except:
        logfile = "/tmp/secpipe_{}.txt".format(str(time.time()).split('.')[0])
    
    logging.basicConfig(filename=logfile,filemode='w',level=logging.DEBUG,format='%(asctime)s - %(levelname)s: %(message)s')
    
    logging.debug(' '.join(sys.argv[:]))
    
    if not (len(sys.argv)==7 or len(sys.argv)==5):
         
        sys.exit("""\n\n\tHow-to:
        
        {0} <path to the first sample> <num buf> <end num> <wait time [s]> [<shut cl start> <shut cl end>]
        
        """.format(os.path.basename(sys.argv[0])))
    
    # set debug level
    debug = 1
    
    # plot settings
    sec_col = ['#ff0000','#0000FF','#00ff5d','#fff000','#e8b218']
    mar_size = 8.0
    mar_edge_wd = 1.5
    ln_wd = 3.0
    
    # set paths
    atsas_path='/mnt/home/staff/SW/ATSAS'
    exe_path='/mnt/home/sw/bin:{}'.format(os.path.join(atsas_path,'bin'))
    ld_path=os.path.join(atsas_path,'lib64/atsas')
    
    try:
        os.environ['LD_LIBRARY_PATH']=os.environ['LD_LIBRARY_PATH']+':'+ld_path
    except:
        os.environ['LD_LIBRARY_PATH']=ld_path
    
    try:
        os.environ['PATH']=os.environ['PATH']+':'+exe_path
    except:
        os.environ['PATH']=exe_path
            
    
    # set some variables from arguments 
    first_sample_path = sys.argv[1]
    first_sample_no = int(first_sample_path.split('_')[-1].split('.')[0])
    num_buf = int(sys.argv[2])
    last_buf = num_buf+first_sample_no-1
    end_num = int(sys.argv[3])
    wait_time = int(sys.argv[4])
    if len(sys.argv) == 7:
        shut_cl_start = int(sys.argv[5])
        shut_cl_end = int(sys.argv[6])
    else:
        shut_cl_start = end_num+1
        shut_cl_end = end_num+2
    
    if debug == 1:
        print('Setting variables.\n')
    
    first_sample = os.path.basename(first_sample_path)
    data_fol = os.path.abspath(os.path.dirname(first_sample_path))
    sample_name_ser = '_'.join(first_sample.split('_')[:-2])
    sample_name = "_".join(sample_name_ser.split('_')[:-1])
    sample_ser = sample_name_ser.split('_')[-1]
    num_length = len(os.path.splitext(first_sample)[0].split('_')[-1])
    int_file = os.path.join(data_fol,first_sample.split('_0_')[0]+'_0s1.int')
    
    # prepare folders
    if debug == 1:
        print('Making folders.\n')
    try:
        analysis_fol = os.path.normpath(os.path.join(data_fol,'../analysis'+data_fol.split('/')[-1].split('data')[1]))
        if not os.path.isdir(analysis_fol):
            analysis_fol = os.path.normpath(os.path.join(data_fol,'../analysis'))
    except:
        analysis_fol = os.path.abspath(os.path.join(data_fol,'../analysis/'))
    sample_sfol = os.path.join(analysis_fol,sample_name_ser)
    sastool_sfol = os.path.join(sample_sfol,'sastool')
    plots_sfol = os.path.join(sample_sfol,'plots')
    avg_sfol = os.path.join(sample_sfol,'average')
    
    #mkpath(analysis_fol)
    mkpath(sastool_sfol)
    mkpath(plots_sfol)
    mkpath(avg_sfol)
    
    buf_name = os.path.join(data_fol,first_sample)
    
    # location of the integ.mpp
    integ_mpp = os.path.join(analysis_fol,'integ.mpp')
    
    # set sastool and autorg paths
    autorg = distutils.spawn.find_executable('autorg')
    sastool = distutils.spawn.find_executable('sastool')
    
    if autorg == None:
        sys.exit('\'autorg\' not found, please add it to the path.')
    if sastool == None:
        sys.exit('\'sastool\' not found, please add it to the path.')
    
    # header for a log file
    log_header = """ _graph_title.text "Rg, I0 and IqL vs Image Number"
  
 _graph_axes.xLabel "Image Number"
 _graph_axes.yLabel "Rg"
 _graph_axes.x2Label ""
 _graph_axes.y2Label "I0,IqL"
  
 _graph_background.showGrid 1
  
 data_
 _trace.name Data
 _trace.xLabels "{Image Number}"
 _trace.hide 0
  
 loop_
 _sub_trace.name
 _sub_trace.yLabels
 _sub_trace.color
 _sub_trace.width
 _sub_trace.symbol
 _sub_trace.symbolSize
 Rg "{Rg}" red 0 square 2
 I0 "{I0,IqL}" blue 0 circle 2
 IqL "{I0,IqL}" darkgreen 0 circle 2
  
 loop_
 _sub_trace.x
 _sub_trace.y1
 _sub_trace.y2
 _sub_trace.y3
"""
    
    
    # create log file and tmp log file
    if debug == 1:
        print('Creating log file.\n')
    log_file = os.path.join(sample_sfol,sample_name_ser+'.bip')
    log_file_tmp = log_file+'.tmp'
    with open(log_file_tmp,'w') as lg:
        lg.write(log_header)
    
    shutil.copy(log_file_tmp,log_file)
       
    #file_list=[]
    spmpp = os.path.join(sastool_sfol,'secpipe.mpp')
    
    # prepare secpipe.mpp
    spmpp_text=[]
    t_switch = 0
    
    if debug == 1:
        print('Reading integ.mpp.\n')
    
    os.chdir(analysis_fol)
    with open(integ_mpp, "r") as impp:
        for line in impp:
            if line.startswith('-s') or line.startswith('#-s') or line.startswith('# -s'):
                line = re.sub('# *-s','-s',line)
            if not line.startswith('#') and not line.startswith('-f') and not re.match(r'^\s*$', line):
                if line.startswith('-m'):
                    mask_file = line.split()[-1]
                    if os.path.isabs(mask_file) == False:
                        line = '-m yes {}\n'.format(os.path.relpath(os.path.join(analysis_fol,mask_file),sastool_sfol))
                    else:
                        line = '-m yes {}\n'.format(os.path.relpath(mask_file,sastool_sfol))
                if not line.endswith('\n'):
                    line=line+'\n'
                spmpp_text.append(line)
            if line.startswith('-t'):
                t_switch = 1
    
    # append '-t yes' line if missing in integ.mpp
    if t_switch==0:
        spmpp_text.append('-t yes\n')
        
    # set last_file_time to 20 mins in future, to allow some time before the first image
    last_file_time = time.time() + 1200
        
# change folder to sastool
    os.chdir(sastool_sfol)
    
    # empty arrays, used for plotting later
    im_num_ar = []
    rg_ar = []
    i0_ar = []
    iqmin_ar = []
    
    # set current image number, start with 1
    im_num = 1
    
    while 1:
        if time.time() - last_file_time > 120+wait_time:
            if debug == 1:
                print('Too much time since last image.\n')
            finish_analysis()
        
        # next image file name to analyze
        next_filename = re.sub('(.*)_(\d+)\.ti(f+)','\\1_{}.ti\\3'.format(str(im_num).zfill(num_length)),buf_name)
        
        if debug == 1:
            print('Looking for {}.\n'.format(next_filename))
        
        # check if the file exists:
        if not os.path.isfile(next_filename):
            if debug == 1:
                print('File not found, trying again in 2 seconds.\n')
            # if not, wait 2 seconds and try again
            time.sleep(2)
        else:
            # set the read time as last image time
            last_file_time = time.time()
            # check if the .int file is updated
            while file_len(int_file) < 2*im_num:
                #print('Waiting for int file for image number {}.\n'.format(im_num))
                time.sleep(.5)
            
            if last_buf < im_num < shut_cl_start or im_num > shut_cl_end-1:
                # write new secpipe.mpp
                with open(spmpp, 'w') as mpp_file:
                    for item in spmpp_text:
                        mpp_file.write("{}".format(item))
                    mpp_file.write('-f {} 1 {} {}'.format(os.path.join(data_fol,next_filename), buf_name, num_buf))
                
                # run sastool, but wait a bit for the int file to be written
                time.sleep(.5)
                subprocess.call([sastool,spmpp])
                
                #delete zeros from sub file:
                sub_file = os.path.join(sastool_sfol,os.path.splitext(os.path.basename(next_filename))[0]+".sub")
                top_zeros = 0
                try:
                    for line in fileinput.input(sub_file, inplace = 1):
                        if (not float(line.split()[1]) == float(line.split()[2]) == 0 or top_zeros == 1):
                            sys.stdout.write(line)
                            top_zeros = 1
                except:
                    pass
                # run autorg
                try:
                    #rg_out = subprocess.check_output([autorg,'--sminrg=1','--smaxrg=1.3',sub_file])
                    rg_out = subprocess.check_output([autorg,sub_file])
                    rg=float(rg_out.split()[2])
                    #rg_stdev=rg_out.split()[4])
                    i0=float(rg_out.split()[8])
                    #i0_stdev=rg_out.split()[10]
                except:
                    rg = 0
                    i0 = 0
                
                if rg<0:
                    rg = 0
                if i0<0:
                    i0=0

                # read i_qmin value
                sub_d = numpy.genfromtxt(sub_file)
                iqmin = max(sub_d[5,1],0)

                # put the values in arrayes for printing later
                im_num_ar.append(im_num)
                rg_ar.append(rg)
                i0_ar.append(i0)
                iqmin_ar.append(iqmin)
                
                # write temp file with rg and i0, move to permenant one
                with open(log_file_tmp,'a') as lg:
                    lg.write('{} {} {} {}\n'.format(im_num,rg,i0,iqmin))
                
                shutil.copy(log_file_tmp,log_file) 
                
            if im_num == end_num:
                if debug == 1:
                    print('Finished all images.\n')
                finish_analysis()
                
            im_num = im_num+1
            if debug == 1:
                print('\n\nNext image number {}.\n'.format(im_num))
                
            
        
