#!/usr/bin/env python
#

'''
@author: rajkovic
'''


import numpy
from scipy.signal import find_peaks, peak_widths
from scipy.optimize import curve_fit
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
from matplotlib import gridspec


#uv_data = numpy.loadtxt('/fs/home/rajkovic/bitbucket/programs-git/secpipe/uv/BSA-4_010_UV_data.txt')
#uv_data = numpy.loadtxt('/fs/home/rajkovic/bitbucket/programs-git/secpipe/uv/test_BSA_PBS_3_UV_data.txt')
#xr_data = numpy.loadtxt('/fs/home/rajkovic/bitbucket/programs-git/secpipe/uv/test_BSA_PBS_3_plot_data.txt')
uv_data = numpy.genfromtxt(fname = '/fs/home/rajkovic/bitbucket/programs-git/secpipe/test/data/BSA_10ul_011uv.bip',skip_header = 31,delimiter = ' ')


# add zero so x-axis is the same as index
#uv_data=numpy.append([[0,uv_data[0,1],uv_data[0,2]]],uv_data,axis=0)

pp = find_peaks(uv_data[:,1],prominence=numpy.max(uv_data[:,1])/50)

# find peaks in saxs intensity data
#xp = find_peaks(xr_data[:,3],prominence=numpy.max(xr_data[:,3])/10)

# uv offset from peak diff,
#uv_off = int(xr_data[xp[0][-1],0] - uv_data[pp[0][-1],0])

# image offset from pearsons correlation

# # length of shorter data
# ml = numpy.min([uv_data.shape[0],xr_data.shape[0]])
#
# # first image number in xray data
# fi = int(xr_data[0,0])
#
# pc = []
# for jj in range(20):
#     ppc = pearsonr(numpy.roll(uv_data[fi:ml+fi,1],jj),xr_data[:ml,3])
#     pc.append(ppc[0])
#
# uv_offset = numpy.argmax(pc)


c_height = uv_data[pp[0],1] - pp[1]['prominences']

w_half = peak_widths(uv_data[:,1], pp[0], rel_height=.5)


# plt.figure()
# plt.plot(uv_data[:,0],uv_data[:,1])
# #plt.plot(uv_data[:,0,],uv_data[:,2])
# plt.scatter(pp[0],uv_data[pp[0],1],color='red')
# plt.vlines(x=pp[0], ymin=c_height, ymax=uv_data[pp[0],1])
# #plt.vlines(int(numpy.ceil(w_half[-1][-2]+w_half[0][-2])),0,uv_data[int(numpy.ceil(w_half[-1][-2]+2*w_half[0][-2])),1],color='green')
# plt.hlines(*w_half[1:], color="C3")
# plt.show()




# initial gauss fitting parameters
#guess = numpy.ravel([[i, uv_data[i,1], 10] for i in pp[0]])
guess = numpy.ravel([[uv_data[pp[0][i],0], pp[1]['prominences'][i], w_half[0][i]] for i in range(len(pp[0]))])

# prepend background
guess = numpy.append([1.0,0.0,numpy.mean([pp[0][0],pp[0][-1]]),50,(pp[0][-1]-pp[0][0])*2],guess)
#guess = numpy.append(guess,[pp[0][-1]+50,20,(pp[0][-1]-pp[0][0])])

#limit to positive values
bl = numpy.zeros_like(guess)
bu = numpy.ones_like(guess)*numpy.inf
# linear part can be negative
bl[:2] = -numpy.inf
# limit first peak position
#bl[5] = guess[5]-1
#bu[5] = guess[5]+1

# limit first peak width
bl[7] = guess[7]-1
bu[7] = guess[7]+1


# degree for background flat-top gauss
bkg_dg = 6


def func(x, *params):
    y = numpy.zeros_like(x)
    for i in range(2, len(params), 3):
        ctr = params[i]
        amp = params[i+1]
        wid = params[i+2]

        if i == 2:
            nn=bkg_dg
            y = y + (params[0]+params[1]*x)*amp * numpy.exp( -(numpy.abs(x - ctr)/wid)**nn)
        else:
            nn=2
            y = y + amp * numpy.exp( -(numpy.abs(x - ctr)/wid)**nn)

    return(y)




popt, pcov = curve_fit(func, uv_data[:,0], uv_data[:,1], p0=guess,bounds=(bl,bu))
#print(popt)
fit = func(uv_data[:,0], *popt)



#print('end of second to last peak: {}'.format(numpy.ceil(pp[0][-2]+1.5*w_half[0][-2])))
print('end of second to last peak: {}'.format(numpy.ceil(popt[-6]+3*popt[-4])))



plt.subplots(2,1)
gs = gridspec.GridSpec(2, 1, height_ratios=[5, 1], wspace=0.0, hspace=0.0)
ax0 = plt.subplot(gs[0])
ax0.plot(uv_data[:,0], uv_data[:,1])
ax0.plot(uv_data[:,0], fit , ':m')

for l in range(2,len(popt),3):
    ctr = popt[l]
    amp = popt[l+1]
    wid = popt[l+2]

    if l == 2 :
        nn=bkg_dg
        dd = (popt[0]+popt[1]*uv_data[:,0])*amp * numpy.exp( -(numpy.abs(uv_data[:,0] - ctr)/wid)**nn)
    else:
        nn=2
        dd = amp * numpy.exp( -(numpy.abs(uv_data[:,0] - ctr)/wid)**nn)


    ax0.plot(uv_data[:,0],dd,'--')


#ax0.plot(uv_data[pp[0],0],uv_data[pp[0],1],'xr')

ax1 = plt.subplot(gs[1]) # ,sharex=ax0
ax1.plot(uv_data[:,0],(uv_data[:,1]-fit),'k')


plt.subplots(2,1)
gs = gridspec.GridSpec(2, 1, height_ratios=[5, 1], wspace=0.0, hspace=0.0)
ax0 = plt.subplot(gs[0])
ax0.plot(uv_data[:,0], uv_data[:,1])
ax0.plot(uv_data[:,0], fit , ':m')

for l in range(2,len(popt),3):
    ctr = popt[l]
    amp = popt[l+1]
    wid = popt[l+2]

    if l == 2 :
        nn=bkg_dg
        dd = (popt[0]+popt[1]*uv_data[:,0])*amp * numpy.exp( -(numpy.abs(uv_data[:,0] - ctr)/wid)**nn)
    else:
        nn=2
        dd = amp * numpy.exp( -(numpy.abs(uv_data[:,0] - ctr)/wid)**nn)


    ax0.plot(uv_data[:,0],dd,'--')


#ax0.plot(uv_data[pp[0],0],uv_data[pp[0],1],'xr')

ax0.axvline(numpy.ceil(popt[-6]+3*popt[-4]),color='red')


ax1 = plt.subplot(gs[1]) # ,sharex=ax0
ax1.plot(uv_data[:,0],(uv_data[:,1]-fit),'k')
ax1.yaxis.set_major_locator(plt.MaxNLocator(3))
ax0.set_xticklabels([])
ax0.minorticks_on()
ax0.grid(axis='x',which='major',color='#777777')
ax0.grid(axis='x',which='minor',color='#dddddd')
#ax0.set_xticks([])
ax1.tick_params(which='minor', bottom=False, left=False)
ax1.set_xlim([popt[-6]-1.5*popt[-4],popt[-3]+1.5*popt[-1]])
ax0.set_xlim([popt[-6]-1.5*popt[-4],popt[-3]+1.5*popt[-1]])


plt.show()


