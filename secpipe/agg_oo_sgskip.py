"""
=============================
The object-oriented interface
=============================

A pure OO (no pyplot!) example using the agg backend.
"""

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

fig = Figure(figsize=(16,4))
# A canvas must be manually attached to the figure (pyplot would automatically
# do it).  This is done by instanciating the canvas with the figure as
# argument.
FigureCanvas(fig)
ax = fig.add_subplot(121)
ax.plot([1,5,10],[1, 2, 3])
ax.plot([4,7,10])
ax.set_title('hi')
ax.grid(True)
ax.locator_params(nbins=4)
ax.set_xlabel('time')
ax.set_ylabel('volts')

ax = fig.add_subplot(122)
ax.plot([1, 2, 3])
ax.set_title('hi2')
ax.grid(True)
ax.set_xlabel('time2')
ax.set_ylabel('volts2')
ax.text(0.5,0.5,'text text testttttt',fontsize=20,transform=ax.transAxes)

fig.tight_layout(pad=0.2)

fig.savefig('test.png')
