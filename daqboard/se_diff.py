import numpy
import matplotlib.pyplot as plt
import os





se = numpy.loadtxt('C:\\Users\\rajkovic\\Documents\\programs-git\\daqboard\\ASCII\\SE7.TXT',skiprows=2)
diff = numpy.loadtxt('C:\\Users\\rajkovic\\Documents\\programs-git\\daqboard\\ASCII\\DIFF7.TXT',skiprows=2)


se_av = numpy.mean(se)
se_std = numpy.std(se)
se_min=se.min()
se_max=se.max()


diff_av = numpy.mean(diff)
diff_std = numpy.std(diff)
diff_min=diff.min()
diff_max=diff.max()

bins=numpy.linspace(4.49,4.505,45)

fig,ax=plt.subplots()

plt.hist(se,bins,alpha=0.5,color='b',label='SE')
plt.hist(diff,bins,alpha=0.5,color='g',label='DIFF')
plt.axvline(x=4.498,color='r',linestyle=(0,(1,2)))
plt.axvline(x=4.499,color='r',linestyle=(0,(1,4)))
plt.legend()


plt.text(0.03,.92,f'V_fluke=4.498(-4.499)',transform=ax.transAxes)
plt.text(0.03,.77,f'SE: {se_av:.4f}+-{se_std:.4f} \n range: {se_min:.4f}-{se_max:.4f}',transform=ax.transAxes)
plt.text(0.03,0.60,f'DIFF:  {diff_av:.4f}+-{diff_std:.4f} \n range: {diff_min:.4f}-{diff_max:.4f}',transform=ax.transAxes)

plt.show()


