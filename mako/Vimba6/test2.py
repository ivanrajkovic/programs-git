from vimba import *

def print_preamble():
    print('///////////////////////////////////////////')
    print('/// Vimba Python No Broadcast Discovery ///')
    print('///////////////////////////////////////////\n')


def print_camera(cam: Camera):
    print('/// Camera Name   : {}'.format(cam.get_name()))
    print('/// Model Name    : {}'.format(cam.get_model()))
    print('/// Camera ID     : {}'.format(cam.get_id()))
    print('/// Serial Number : {}'.format(cam.get_serial()))
    print('/// Interface ID  : {}\n'.format(cam.get_interface_id()))

def print_feature(feature):
    try:
        value = feature.get()

    except (AttributeError, VimbaFeatureError):
        value = None

    print('/// Feature name   : {}'.format(feature.get_name()))
    print('/// Display name   : {}'.format(feature.get_display_name()))
    print('/// Tooltip        : {}'.format(feature.get_tooltip()))
    print('/// Description    : {}'.format(feature.get_description()))
    print('/// SFNC Namespace : {}'.format(feature.get_sfnc_namespace()))
    print('/// Unit           : {}'.format(feature.get_unit()))
    print('/// Value          : {}\n'.format(str(value)))


def main():
    print_preamble()
    vmb = Vimba.get_instance()
    #comment out if trace log is needed:
    #vmb.enable_log(LOG_CONFIG_TRACE_FILE_ONLY)
    vmb.set_network_discovery(False)
    with vmb:
        cameras = vmb.get_all_cameras()
        if not cameras:
            print("/// Camera list from broadcast discovery is empty.\n")

        cam = vmb.get_camera_by_id("169.254.234.12") #Put IP-address here as string
        if cam:
            print("/// Camera was found through IP-address:")
            print_camera(cam)
            with cam:
                print_camera(cam) #Check if the camera can be opened and if it streams one frame
                print('Print all features of camera \'{}\':'.format(cam.get_id()))
                for feature in cam.get_all_features():
                    print_feature(feature)

if __name__ == '__main__':
    main()