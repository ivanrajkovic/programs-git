from vimba import *
from vimba import camera



id='DEV_000F315C28CA'
ip = '169.254.234.12'


with Vimba.get_instance() as vimba:

    #i = vimba.get_all_interfaces()
    #for ii in i:
    #    print(ii)

    #cam = camera.discover_camera(ip)
    #print(cam)

    #cams = vimba.get_all_cameras()
    #print(cams)

    cc = vimba.get_camera_by_id(ip)
    print(cc)




with Vimba.get_instance() as vmb:
    is_gige_tl_present = vmb.GeVTLIsPresent.get()



def get_camera(camera_id: str) -> Camera:
    with Vimba.get_instance() as vimba:
        try:
            return vimba.get_camera_by_id(camera_id)

        except VimbaCameraError:
            abort('Failed to access Camera {}. Abort.'.format(camera_id))


def print_feature(feature):
    try:
        value = feature.get()

    except (AttributeError, VimbaFeatureError):
        value = None

    print('/// Feature name   : {}'.format(feature.get_name()))
    print('/// Display name   : {}'.format(feature.get_display_name()))
    print('/// Tooltip        : {}'.format(feature.get_tooltip()))
    print('/// Description    : {}'.format(feature.get_description()))
    print('/// SFNC Namespace : {}'.format(feature.get_sfnc_namespace()))
    print('/// Unit           : {}'.format(feature.get_unit()))
    print('/// Value          : {}\n'.format(str(value)))

with Vimba.get_instance() as vimba:
    #cam = camera.discover_camera(ip)
    #cam = vimba.get_camera_by_id(ip)
    cam = get_camera(ip)
    print(cam.get_id())
    print(cam.get_serial())


    print('Print all features of camera \'{}\':'.format(cam.get_id()))
    print(cam.get_all_features())


with Vimba.get_instance():
    with get_camera(ip) as cam:
        print(cam)
        print('Print all features of camera \'{}\':'.format(cam.get_id()))
        for feature in cam.get_all_features():
            print_feature(feature)