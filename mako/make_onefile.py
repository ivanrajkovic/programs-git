#!/usr/bin/env python
import os

with open('mako_gui2.py','r') as f1:
    data_1 = f1.read()
    
    
with open('gui2.py','r') as f2:
    data_2 = f2.read()
    


data_1s = data_1.split('from gui2 import Ui_MainWindow')

data_2s = data_2.split('if __name__ == "__main__":')

if os.path.isfile('mako_gui'):
    os.remove('mako_gui')

with open('mako_gui','w') as out_file:
    out_file.write(data_1s[0]+data_2s[0]+data_1s[1])
