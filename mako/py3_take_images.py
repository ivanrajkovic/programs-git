from pymba import Vimba, Frame
import numpy
import time
import os
import warnings
import binascii

from skimage import io
#io.use_plugin('freeimage')

################################
##### Change these variables ###
################################

base_folder = 'c:\\users\\b_tsuruta.ssrlad\\Desktop\\camera\\'

img_folder = 'test'

file_name = 'img12_'

total_frames = 10   # max 2300 for Mono8 with save_at_end = 'Yes'

exp_time = 250000 # in microseconds

cam_gain=20

px_format = 'Mono12'  # Mono8, Mono12 or Mono12Packed

trigger_mode = 'Off'  # 'Off' o 'On'

save_at_end = 'No' # 'Yes' or 'No'; if 'Yes' images are kept in memory until all images are aquired and saved afterwards
                    # if 'No', save at most 2 frames per second  - either with exp_time> 0.5s or using trigger
                    # Don't use 'No' with Mono12Packed


############################################
###Don't change below this line ############
############################################

dig = len(str(total_frames))

if px_format == 'Mono12':
    img_type = numpy.uint16
else:
    img_type = numpy.uint8

def hex2bin(chain):
    return ''.join((bin(int(chain[i:i+2], 16))[2:].zfill(8) for i in range(0, len(chain), 2)))

i=0
ww = [0]
def get_data(frame):
    global i, aaa, ts, ww
    if i<total_frames:
        ts.append(time.clock())
        ww.append(frame.data.timestamp)
        aaa[i,:,:]=numpy.ndarray(buffer=frame.buffer_data(), dtype=img_type, shape=(f_h,f_w))
        i=i+1

def save_data(frame):
    global i, ts, ww
    if i<total_frames:
        ts.append(time.clock())
        ww.append(frame.data.timestamp)
        with open(os.path.join(base_folder,img_folder,file_name+'timing.txt'),'a') as qq:
            qq.write('{:d}'.format(i+1)+'\t'+'{:f}'.format(ts[i]-ts[0])+'\t'+'{:f}'.format(ts[i+1]-ts[i])+'\t'+'{}'.format((ww[i+1]-ww[i])/1e9)+'\t'+file_name+str(i+1).zfill(dig)+'.tif'+'\n')
        if not px_format == "Mono12Packed":
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                io.imsave(os.path.join(base_folder,img_folder,file_name+str(i+1).zfill(dig)+'.tif'),numpy.ndarray(buffer=frame.buffer_data(), dtype=img_type, shape=(f_h,f_w)) )
                i=i+1


with open(os.path.join(base_folder,img_folder,file_name+'timing.txt'),'w') as qq:
    qq.write('exposure: {}us\t\tgain: {}\n'.format(exp_time,cam_gain))
    qq.write('img\trelative time\ttime delta\tframe time\timage name\n')



with Vimba() as vimba:
    
    system = vimba.system()
    #vimba.startup()
    #for feature_name in system.feature_names():
    #    print(feature_name)
    
    system.run_feature_command("GeVDiscoveryAllAuto")
    time.sleep(.2)
    print(vimba.camera_ids())
    camera = vimba.camera(0)
    camera.open()
    
    frame_width = camera.Width
    frame_height = camera.Height
    
    if px_format == 'Mono12Packed':
        f_h=int(frame_height*frame_width*1.5)
        f_w=1
    else:
        f_h = frame_height
        f_w = frame_width
    
    if save_at_end == 'Yes' or save_at_end == 'yes':
        aaa=numpy.zeros([total_frames,f_h,f_w],dtype=img_type)
    
    camera.GVSPPacketSize = 8999
    camera.StreamBytesPerSecond = 124000000 
    
    camera.PixelFormat=px_format
    camera.ExposureTimeAbs=exp_time
    camera.AcquisitionFrameRateAbs=camera.AcquisitionFrameRateLimit
    #print(camera.AcquisitionFrameRateLimit)
    camera.Gain = cam_gain
    
    # set trigger
    camera.TriggerSelector='FrameStart'
    camera.TriggerSource='Line1'
    camera.TriggerMode=trigger_mode


    if save_at_end == 'Yes' or save_at_end == 'yes':
        camera.arm('Continuous', get_data)
    else:
        camera.arm('Continuous', save_data)
    camera.start_frame_acquisition()
    # time.clock() has better precision than time.time(), but this works only on windows
    # on linux, time.clock() returns cpu clock ; on windows, wall clock
    ts=[time.clock()]
    while i<total_frames:
        time.sleep(0.1)


    camera.stop_frame_acquisition()
    camera.disarm()
    
    # switch back to non-triggered mode
    if trigger_mode == 'On':
        camera.TriggerMode='Off'

    
    camera.close()

# finished acquisitions

tot_t = ts[-1]-ts[0]
fps = total_frames/tot_t

if tot_t < 1:
    print('Total time:\t{:.2f} s'.format(tot_t))
elif tot_t< 60:
    print('Total time:\t{:.1f} s'.format(tot_t))
elif tot_t < 3600:
    print('Total time:\t{:.1f} min'.format(tot_t/60))
else:
    print('Total time:\t{:.1f} hrs'.format(tot_t/3600))

if fps > 50:
    print('Speed:\t\t{:.0f} fps'.format(fps))
elif fps >= 1:
    print('Speed:\t\t{:.1f} fps'.format(fps))
elif fps > 0.015:
    print('Speed:\t\t{:.1f} frame/min'.format(fps*60))
else:
    print('Speed:\t\t{:.1f} frame/hr'.format(fps*3600))


# Save images, if save_at_end = 'Yes'
if save_at_end == 'Yes' or save_at_end == 'yes':  
    with open(os.path.join(base_folder,img_folder,file_name+'timing.txt'),'a') as qq:


        for ff in range(total_frames):
            if px_format == 'Mono12Packed':
                data=aaa[ff].flatten()
                ii=numpy.empty(int(2*len(data)/3))
                ic = 0
                for oo in range(0,int(len(data)/3)):
                    bb = hex2bin(binascii.b2a_hex(data[3*oo:3*oo+3]))
                    ii[ic] = int(bb[0:8],2)*16+int(bb[12:16],2)
                    ii[ic+1] = int(bb[16:24],2)*16+int(bb[8:12],2)
                    ic=ic+2
                img = (numpy.reshape(ii.astype(numpy.uint16),(frame_height,frame_width)))
            else:
                img = aaa[ff]
                
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                io.imsave(os.path.join(base_folder,img_folder,file_name+str(ff+1).zfill(dig)+'.tif'), img)
                
            qq.write('{:d}'.format(ff+1)+'\t'+'{:f}'.format(ts[ff]-ts[0])+'\t'+'{:f}'.format(ts[ff+1]-ts[ff])+'\t'+'{}'.format((ww[ff+1]-ww[ff])/1e9)+'\t'+file_name+str(ff+1).zfill(dig)+'.tif'+'\n')
        qq.write('\t'+'{:f}'.format(ts[-1]-ts[0]))
        
    del aaa    
    
# add end time if save_at_end = 'No'
with open(os.path.join(base_folder,img_folder,file_name+'timing.txt'),'a') as qq:
    qq.write('\t'+'{:f}'.format(ts[-1]-ts[0]))

del ts, ww, vimba
