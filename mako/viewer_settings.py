from pymba import *

with Vimba() as vimba:
    system = vimba.getSystem()

    system.runFeatureCommand("GeVDiscoveryAllOnce")
    time.sleep(0.2)

    camera_ids = vimba.getCameraIds()

    for cam_id in camera_ids:
        print "Camera found: ", cam_id
        
    c0 = vimba.getCamera(camera_ids[0])
    c0.openCamera()
    c0.TriggerSource='Freerun'
    c0.TriggerMode='Off'
    c0.AcquisitionMode='Continuous'
    c0.PixelFormat='Mono8'
    #c0.StreamBytesPerSecond = 124000000
    c0.AcquisitionFrameRateAbs=c0.AcquisitionFrameRateLimit