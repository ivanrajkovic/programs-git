#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
import os
import numpy
import warnings
import binascii
from skimage import io
import ipaddress


#from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QStyleFactory, QFileDialog, QMessageBox


from pymba import Vimba, Frame


from gui import Ui_MainWindow


def hex2bin(chain):
    return ''.join((bin(int(chain[i:i+2], 16))[2:].zfill(8) for i in range(0, len(chain), 2)))



class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # set theme: Windows', 'WindowsXP', 'WindowsVista', 'Fusion'
        QApplication.setStyle(QStyleFactory.create("Fusion"))
        self.show()
        
        ## set variable starting values
        self.img_folder = os.path.abspath('.')
        
        self.ui.set_folder.setText(self.img_folder)
        #self.ww=[0]
        
        ## button connections
        self.ui.find_camera.clicked.connect(self.f_find_cameras)
        self.ui.set_folder_2.clicked.connect(self.f_set_folder)
        self.ui.take_images.clicked.connect(self.f_take_images)
        self.ui.set_folder.textEdited.connect(self.folder_changed)
        
        

        
    ## functions
    def get_data(self,frame):
        if self.cur_frame<self.ui.total_frames.value():
            self.ts.append(time.clock())
            self.ww.append(frame.data.timestamp)
            self.aaa[self.cur_frame,:,:]=numpy.ndarray(buffer=frame.buffer_data(), dtype=self.img_type, shape=(self.f_h,self.f_w))
            self.ui.cur_img_label.setText('Current image number: {}'.format(self.cur_frame+1))
            self.cur_frame = self.cur_frame+1
    
    def save_data(self,frame):
        if self.cur_frame<self.ui.total_frames.value():
            self.ts.append(time.clock())
            self.ww.append(frame.data.timestamp)
            with open(os.path.join(self.img_folder,self.ui.file_name.text()+'_timing.txt'),'a') as qq:
                qq.write('{:d}'.format(self.cur_frame+1)+'\t'+'{:f}'.format(self.ts[self.cur_frame]-self.ts[0])+'\t'+'{:f}'.format(self.ts[self.cur_frame+1]-self.ts[self.cur_frame])+'\t'+'{}'.format((self.ww[self.cur_frame+1]-self.ww[self.cur_frame])/1e9)+'\t'+self.ui.file_name.text()+str(self.cur_frame+1).zfill(self.dig)+'.tif'+'\n')
            if not self.ui.px_format.currentText() == "Mono12Packed":
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    io.imsave(os.path.join(self.img_folder,self.ui.file_name.text()+'_'+str(self.cur_frame+1).zfill(self.dig)+'.tif'),numpy.ndarray(buffer=frame.buffer_data(), dtype=self.img_type, shape=(self.f_h,self.f_w)) )
                    self.ui.cur_img_label.setText('Current image number: {}'.format(self.cur_frame+1))
                    self.cur_frame=self.cur_frame+1
        
    
    
        
    def f_find_cameras(self):
        self.ui.camera_list.clear()
        with Vimba() as vimba:
            system = vimba.system()
            system.run_feature_command("GeVDiscoveryAllAuto")
            time.sleep(.2)
        
            for ii in range(len(vimba.camera_ids())):
                try:
                    camera=vimba.camera(ii)
                    camera.open()
                    #cam_info='{} - {}'.format(camera.feature('DeviceModelName').value,ipaddress.ip_address(camera.feature('GevCurrentIPAddress').value.__str__()))
                    s=camera.feature('DeviceModelName').value
                    d=ipaddress.ip_address(camera.feature('GevCurrentIPAddress').value).__str__()
                    d='.'.join(d.split('.')[::-1])
                    cam_info = '{} - {}'.format(s,d)
                    
                except:
                    cam_info=str(vimba.camera_ids()[ii])
                    
                self.ui.camera_list.addItem(cam_info)
        
        
    def f_set_folder(self):
        options = QFileDialog.Options()
        fol = str(QFileDialog.getExistingDirectory(self,'Select folder to save images',self.img_folder,QFileDialog.ShowDirsOnly))
        if fol != '':
            self.img_folder = fol
            self.ui.set_folder.setText(self.img_folder)
        
    
    def folder_changed(self):
        if os.path.isdir(self.ui.set_folder.text()):
            self.img_folder = self.ui.set_folder.text()
        
    
        
    def f_take_images(self):
        self.cur_frame=0
        self.ui.cur_img_label.setText('Current image number: {}'.format(self.cur_frame))
        self.dig = len(str(int(self.ui.total_frames.value())))
        self.ww=[0]
        app.processEvents()
        with open(os.path.join(self.img_folder,self.ui.file_name.text()+'_timing.txt'),'w') as qq:
            qq.write('exposure: {}us\t\tgain: {}\n'.format(self.ui.exp_time.value(),self.ui.gain_spin.value()))
            qq.write('img\trelative time\ttime delta\tframe time\timage name\n')
        with Vimba() as vimba:
            system = vimba.system()
            system.run_feature_command("GeVDiscoveryAllAuto")
            time.sleep(.2)
            try:
                # open camera
                camera=vimba.camera(self.ui.camera_list.currentIndex())
                camera.open()
                
                self.frame_width = camera.Width
                self.frame_height = camera.Height
                
                if str(self.ui.px_format.currentText()) == 'Mono12Packed':
                    self.f_h=int(self.frame_height*self.frame_width*1.5)
                    self.f_w=1
                else:
                    self.f_h = self.frame_height
                    self.f_w = self.frame_width
                
                
                # set speed
                camera.GVSPPacketSize = 8999
                camera.StreamBytesPerSecond = 124000000 
                
                # set camera parameters
                camera.PixelFormat=str(self.ui.px_format.currentText())
                camera.ExposureTimeAbs=self.ui.exp_time.value()
                camera.AcquisitionFrameRateAbs=camera.AcquisitionFrameRateLimit
                camera.Gain=self.ui.gain_spin.value()
                
                
                if str(self.ui.px_format.currentText()) == 'Mono12':
                    self.img_type = numpy.uint16
                else:
                    self.img_type = numpy.uint8
                
                # set trigger mode
                camera.TriggerSelector='FrameStart'
                camera.TriggerSource='Line1'
                camera.TriggerMode=str(self.ui.trigger_mode.currentText())
                
                
                if str(self.ui.save_at_end.currentText()) == 'Yes':
                    self.aaa=numpy.zeros([int(self.ui.total_frames.value()),self.f_h,self.f_w],dtype=self.img_type)
                    camera.arm('Continuous', self.get_data)
                else:
                    camera.arm('Continuous', self.save_data)
                
                camera.start_frame_acquisition()
                self.ts=[time.clock()]
                while self.cur_frame<self.ui.total_frames.value():
                    app.processEvents()
                    time.sleep(.1)
                    
                    
            
                camera.stop_frame_acquisition()
                camera.disarm()
                
                # switch back to non-triggered mode
                camera.TriggerMode='Off'
                
                camera.close()
                
                if str(self.ui.save_at_end.currentText()) == 'Yes':
                    
                    with open(os.path.join(self.img_folder,self.ui.file_name.text()+'_timing.txt'),'a') as qq:
                        
                        for ff in range(int(self.ui.total_frames.value())):
                            if str(self.ui.px_format.currentText()) == 'Mono12Packed':
                                data=self.aaa[ff].flatten()
                                ii=numpy.empty(int(2*len(data)/3))
                                ic = 0
                                for oo in range(0,int(len(data)/3)):
                                    bb = hex2bin(binascii.b2a_hex(data[3*oo:3*oo+3]))
                                    ii[ic] = int(bb[0:8],2)*16+int(bb[12:16],2)
                                    ii[ic+1] = int(bb[16:24],2)*16+int(bb[8:12],2)
                                    ic=ic+2
                                img = (numpy.reshape(ii.astype(numpy.uint16),(self.frame_height,self.frame_width)))
                            else:
                                img = self.aaa[ff]
                                
                            
                            with warnings.catch_warnings():
                                warnings.simplefilter("ignore")
                                io.imsave(os.path.join(self.img_folder,self.ui.file_name.text()+'_'+str(ff+1).zfill(self.dig)+'.tif'), img)
                                
                            qq.write('{:d}'.format(ff+1)+'\t'+'{:f}'.format(self.ts[ff]-self.ts[0])+'\t'+'{:f}'.format(self.ts[ff+1]-self.ts[ff])+'\t'+'{}'.format((self.ww[ff+1]-self.ww[ff])/1e9)+'\t'+self.ui.file_name.text()+str(ff+1).zfill(self.dig)+'.tif'+'\n')
                        qq.write('\t'+'{:f}'.format(self.ts[-1]-self.ts[0]))
                        
                    del self.aaa    
                    
                QMessageBox.about(self, "Done", "All images are saved.")
                            
                                
            except:
                QMessageBox.about(self, "Error", "Something went wrong.")
            
        
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWindow()
    sys.exit(app.exec_())