import os
os.chdir('C:\\Users\\b_tsuruta.SSRLAD\\Documents')

from pymba import *
import time
import numpy as np
with Vimba() as vimba:
    print(vimba.getVersion())
    
    
# start Vimba
with Vimba() as vimba:
    # get system object
    system = vimba.getSystem()
    if system.GeVTLIsPresent:
        system.runFeatureCommand("GeVDiscoveryAllOnce")
        time.sleep(0.2)
    cameraIds = vimba.getCameraIds()
    for cameraId in cameraIds:
        print('Camera ID:', cameraId)
        
    camera0 = vimba.getCamera(cameraIds[0])
    camera0.openCamera()
    
    cameraFeatureNames = camera0.getFeatureNames()
    for name in cameraFeatureNames:
        print('Camera feature:', name)
        
    featInfo = camera0.getFeatureInfo('EventFrameTrigger')
    for field in featInfo.getFieldNames():
        print(field, '--', getattr(featInfo, field))