#! /sw/bin/python

import numpy as np
import matplotlib.pyplot as plt
import os, argparse

parser = argparse.ArgumentParser(description='Calculate SAXS data quality statistics.')
parser.add_argument("-f", metavar="FILES", help="CSV formatted SAXS Data Quality table.")
args = parser.parse_args()

filename, ext = os.path.splitext(args.f)

table = np.loadtxt(open(args.f),delimiter=",",skiprows=9,usecols=(-2,-1))
labels = np.loadtxt(open(args.f),delimiter=",",skiprows=9,usecols=(0,),dtype=str)

fig, ax = plt.subplots()
cm = plt.cm.get_cmap('RdYlGn')
ax.bar(np.arange(len(table))+.5,table[:,1],color=cm(table[:,0]*5))
ax.set_xticks(np.arange(len(table[:,1]))+1)
ax.set_xticklabels(labels,rotation=45)
ymax = 5.
ymin = -5.
if table[:,1].max() > ymax:
    ymax = table[:,1].max()
if table[:,1].min() < ymin:
    ymin = table[:,1].min()
ax.set_ylim([ymin,ymax])
ax.grid()
ax.set_axisbelow(True)
plt.title(filename)
plt.ylabel("Impact (% per mg/ml)")
plt.tight_layout()
plt.savefig(filename,ext="png")