#! /sw/bin/python

import os, sys, argparse, subprocess, itertools
from scipy import stats
import numpy as np
#import HTML

parser = argparse.ArgumentParser(description='Calculate SAXS data quality statistics.')
parser.add_argument("-f", metavar="FILES", action="append", nargs="+", help="List of files (space delimited).")
parser.add_argument("-c", metavar="CONC", type=float, help="Concentration of first file in mg/ml. (optional)")
parser.add_argument("-m", metavar="MW", type=float, help="Molecular weight of protein in kDa. (optional)")
parser.add_argument("-M", metavar="MWSTD", type=float, help="Molecular weight of standard in kDa. (optional)")
parser.add_argument("-C", metavar="CONCSTD", type=float, help="Concentration of standard in mg/ml. (optional)")
parser.add_argument("-I", metavar="I0STD", type=float, help="I(0) of standard. (optional)")
parser.add_argument("-q", metavar="VCQMAX", type=float, default=0.3, help="Maximum q to use for Vcorr calculation. (default=0.3)")
parser.add_argument("-s", metavar="QSCALE", type=float, default=0.07, help="Starting q to use for scaling region. (default=0.07)")
parser.add_argument("-o", action="store_true", help="Enable outlier rejection. (optional)")
parser.add_argument("-r", action="store_true", default=False, help="Check for radiation damage, not concentration dependence.")
args = parser.parse_args()


usestd=False
numconc = 0
files = []
numpts = []
conc = 0
names = []
data = []
q = []
I = []
gnom = []
rgpr = []
dmax = []
I0pr_unscaled = []
I0pr = []
porodv = []
porodmw = []
vcorr = []
Qr = []
vcorrmw = []
vcorrmwratio = []
gupper = []
gupperp = []
glower = []
glowerp = []
guinier = []
rg = []
rgavg = []
rgslppc = []
I0 = []
I0avg = []
I0slppc = []
glowers = []
pts = []
rgs = []
I0s = []
scalingdata = []
scalefactor = []
keepers = []

files=args.f[0]

def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return idx

def mad(a):
    return np.median(np.fabs(a - np.median(a)))
    
def modz(a, i, axis=0):
    return 0.6745 * (a[i] - np.median(a)) / mad(a)

def func(x, b):
    return x**(-4) + b

conc_list = ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

for file in files:
    filename, ext = os.path.splitext(file)
    names.append(filename)
    #read in data
    data.append(np.transpose(np.loadtxt(file, usecols = (0, 1, 2), unpack=True)))

dataarray = np.asarray(data)

sample = str.split(filename,"_")[0]

if not os.path.exists(sample):
    os.makedirs(sample)

conc = 0

for file in files:
    os.chdir(sample)
    np.savetxt(file,dataarray[conc,:,:],delimiter=" ",fmt="%.5e")
    os.chdir(os.pardir)
    conc+=1

os.chdir(sample)

conc = 0

for file in files:
    #calculate P(r) using a first estimate of Rg = 20
    #the selection of 20 has little affect on the final Rg, but is
    #chosen as an approximate starting point to avoid gross outliers
    print file
    gnom = str.split(subprocess.check_output(["datgnom", "-r", "20", file]))
    tmprgpr = gnom[11]
    #recalculate P(r) using the Rg calculated from the previously estimated P(r)
    gnom = str.split(subprocess.check_output(["datgnom", "-r", tmprgpr, file]))
    #store Rg and Dmax values
    rgpr.append(float(gnom[11]))
    dmax.append(float(gnom[2]))
    numpts.append(len(data[conc][:]))
    gnomfile = names[conc] + '.out'
    save=0
    prfile = []
    with open(gnomfile) as g:
        for line in g:
            if save==1:
                prfile.append(line.rstrip())
            if "P(R)" in line:
                save=1
    np.savetxt(names[conc]+'_pr.dat',prfile[:-2],delimiter=" ",fmt="%s")
    I0pr_unscaled.append(float(str.split(line)[9]))
    porodv.append(float(str.split(subprocess.check_output(["datporod", gnomfile]))[0]))
    conc += 1

numconc = len(files)

minnumpts = min(numpts)
#print minnumpts

for j in range(len(numpts)-1):
    k = j + 1
    if numpts[k] != numpts[j]:
        print names[k], 'has a different number of data points!'
        print names[j] + ':', numpts[j]
        print names[k] + ':', numpts[k]
        exit()

maxqpt = find_nearest(dataarray[0, :, 0], args.q)
dq = dataarray[0, 1, 0] - dataarray[0, 0, 0]
numpts = minnumpts
q = np.zeros((numconc, numpts))
I = np.zeros((numconc, numpts))
qI = np.zeros((numconc, maxqpt, 2))
sumqI = np.zeros((numconc, maxqpt, 2))

scalingdata = np.zeros((numconc, 50, 2))
gps = np.zeros((numconc, 3))
gsigns = np.zeros((numconc, 3))
rgavg = np.zeros((3))
rgslp = np.zeros((3))
rgyint = np.zeros((3))
rgp = np.zeros((3))
rgerr = np.zeros((3))
rgslppc = np.zeros((3))
I0avg = np.zeros((3))
I0slp = np.zeros((3))
I0yint = np.zeros((3))
I0p = np.zeros((3))
I0err = np.zeros((3))
I0slppc = np.zeros((3))
r = []
sumqIdq = []

for conc in range(numconc):
    if args.o:
        zmod = np.fabs(modz(rgpr, conc))
        if zmod < 3.5:
            keepers.append(conc)
    else:
        keepers.append(conc)

q = np.zeros((numconc, numpts))
I = np.zeros((numconc, numpts))
qI = np.zeros((numconc, maxqpt, 2))

for conc in range(numconc):
    scalemin = find_nearest(dataarray[conc, :, 0], args.s)
    scalemax = find_nearest(dataarray[conc, :, 0], 1)
    scalefactor_tmp = np.linalg.lstsq(dataarray[0, scalemin:scalemax, :2], dataarray[conc, scalemin:scalemax, :2])[0][1, 1]
    if args.r:
        scalefactor.append(conc+1)
    else:
        scalefactor.append(scalefactor_tmp)
    #convert data from array into list of lists and separate into q and I lists
    q[conc] = dataarray[conc, :, 0]
    I[conc] = dataarray[conc, :, 1]
    qI[conc] = np.transpose(np.array([dataarray[conc, :maxqpt, 0], np.multiply(dataarray[conc, :maxqpt, 0], dataarray[conc, :maxqpt, 1])]))
    j = qI[conc, 0, 1]
    for i in range(maxqpt):
        sumqI[conc, i, :] = [qI[conc, i, 0], j]
        j = j + qI[conc, i, 1]
    sumqIdq.append(np.sum(qI[conc, :, 1])*dq)
    vcorr.append(np.divide(I0pr_unscaled[conc], sumqIdq[conc]))
    Qr.append(np.divide(np.square(vcorr[conc]), rgpr[conc]))
    vcorrmw.append(np.divide(Qr[conc], 0.1231*1000))
    #print vcorrmw
    vcorrmwratio.append(np.divide(vcorrmw[conc], vcorrmw[0]))
    #calculate the upper limit of all three Guinier regions as 1.3/Rg (P(r))
    gupper.append(1.3/rgpr[conc])
    #determine the data point of the upper limit of the Guinier Region
    gupperp.append(len([x for x in q[conc] if x < gupper[conc]]))
    #declare new lists for each additional concentration
    glower.append([])
    glowerp.append([])
    guinier.append([])
    rg.append([])
    I0.append([])
    glowers.append([])
    pts.append([])
    rgs.append([])
    I0s.append([])
    conc += 1

dataextrap = np.zeros((numpts, 2))
dataextrapregp = []
datascaled = np.zeros((numconc, numpts, 3))

#scalefactor = []
I0pr_scaledbyvcorrmw = []

#scale data by I(0), taking account of mw changes using the vcorr mw
#(using ratio of vcorr mw to first concentration, rather than absolute vcorr mw)
for conc in range(numconc):
    I0pr_scaledbyvcorrmw.append(np.divide(I0pr_unscaled[conc], vcorrmwratio[conc]))
    #scalefactor.append(np.divide(I0pr_scaledbyvcorrmw[conc], I0pr_scaledbyvcorrmw[0]))
    porodmw.append(np.divide(porodv[conc], 1.66*1000))

#print scaled data sets to file
for conc in range(numconc):
    datascaled[conc, :, 0] = dataarray[conc, :, 0]
    datascaled[conc, :, 1] = np.divide(dataarray[conc, :, 1], scalefactor[conc])
    datascaled[conc, :, 2] = dataarray[conc, :, 2]
    np.savetxt(names[conc] + '_scaled.dat', datascaled[conc], delimiter=' ', fmt='%.5f')

#extrapolate data using scale factors
for i in range(numpts):
    dataextrap[i, 0] = dataarray[0, i, 0]
    dataextrap[i, 1] = stats.linregress(scalefactor, dataarray[:, i, 1])[1]

#output extrapolated data to file
np.savetxt(sample + '_0extrapolated.dat', dataextrap, delimiter=' ', fmt='%.5f')

#if concentration is given, scale all scalefactors by that amount, otherwise
#if options for standards are given, adjust scale factors for absolute concentration
#can use either porodmw or vcorrmw for calculating.  Just reverse the commented line.
if args.c is not None:
    concfactor = args.c
    for conc in range(numconc):
        scalefactor[conc] = scalefactor[conc] * concfactor
elif args.M is not None and args.C is not None and args.I is not None:
    usestd = True
    #concfactor = (np.divide(I0pr_unscaled[0], porodmw[conc])*(np.divide((args.M*args.C), args.I)))
    concfactor = (np.divide(I0pr_unscaled[0], vcorrmw[conc])*(np.divide((args.M*args.C), args.I)))
    for conc in range(numconc):
        scalefactor[conc] = scalefactor[conc] * concfactor    

glower = np.zeros((numconc, 3))
glowerp = np.zeros((numconc, 3), dtype=int)
gupper = np.zeros((numconc))
gupperp = np.zeros((numconc), dtype=int)
guinier = np.zeros((numconc, numpts, 2))
I0 = np.zeros((numconc, 3))
rg = np.zeros((numconc, 3))
gsigns = np.zeros((numconc, 3))
gps = np.zeros((numconc, 3))
I0pr = np.zeros((numconc))
glowers = np.zeros((numconc, 3))
pts = np.zeros((numconc, 3))
rgs = np.zeros((numconc, 3))
I0s = np.zeros((numconc, 3))

Iq = dataarray

for conc in range(numconc):
    qi = dataarray[conc,:,0]
    #fill new lists with values for each of the three Guinier regions:
    #Guinier region 1: 1st pt  < q < 1.3/Rg
    #Guinier region 2: 0.65/Rg < q < 1.3/Rg
    #Guinier region 3: pi/Dmax < q < 1.3/Rg
    gupper[conc] = 1.3/rgpr[conc]
    if gupper[conc] <= qi[6]:
        gupperp[conc] = 6
    else:
        gupperp[conc] = find_nearest(qi, gupper[conc])
    for i in range(3):
        if i == 0:
            glower[conc, i] = qi[0]
        if i == 1:
            glower[conc, i] = 0.65/rgpr[conc]
        if i == 2:
            glower[conc, i] = np.pi/dmax[conc]
        #determine the first point of the Guinier region
        glowerp[conc, i] = find_nearest(qi, glower[conc, i])
        if gupperp[conc] - glowerp[conc, i] < 6:
            glowerp[conc, i] = 0
        #create the Guinier plot, i.e. ln(I(q)) vs. q^2 in the Guinier region
        if i == 0:
            q2 = np.square(qi[glowerp[conc, i]:gupperp[conc]])
            lnI = np.log(np.abs(Iq[conc, glowerp[conc, i]:gupperp[conc], 1]))
            guinier[conc, glowerp[conc, i]:gupperp[conc], :] = np.transpose(np.array([q2, lnI]))
        #this calculates the linear regression for the Guinier plot
        slope, intercept, r, p, err = stats.linregress(guinier[conc, glowerp[conc, i]:gupperp[conc], :])
        #this calculates and stores the Rg and I(0) values from the Guinier plot
        if slope < 0:
            rg[conc, i] = (-3*slope)**(0.5)
        else:
            rg[conc, i] = rgpr[conc]
        if args.r:
            I0[conc, i] = np.exp(intercept)
        else:
            I0[conc, i] = np.exp(intercept) / scalefactor[conc]
        gpts = int(gupperp[conc] - glowerp[conc, i] - 3)
        gslps = []
        for j in range(glowerp[conc, i], glowerp[conc, i] + gpts):
            k = j + 3
            gslps.append(stats.linregress(guinier[conc, j:k, :])[0])
        gsigns[conc, i], yint, r, gps[conc, i], err = stats.linregress(guinier[conc, glowerp[conc, i]:gupperp[conc]-3, 0], gslps)
        if gsigns[conc, i] < 0:
            gps[conc, i] = (-1)*gps[conc, i]
    if args.r:
        I0pr[conc] = I0pr_unscaled[conc]
    else:
        I0pr[conc] = I0pr_unscaled[conc] / scalefactor[conc]

for conc in range(numconc):
    for i in range(3):
        #collect lists of rounded values for convenient display
        glowers[conc, i] = round(glower[conc, i], 3)
        pts[conc, i] = glowerp[conc, i]
        rgs[conc, i] = round(rg[conc, i],2)
        I0s[conc, i] = round(I0[conc, i],2)

for i in range(3):
    rgavg[i] = np.average(np.array(rg)[:,i])
    I0avg[i] = np.average(np.array(I0)[:,i])
    rgslp[i], rgyint[i], r, rgp[i], rgerr[i] = stats.linregress(np.array([scalefactor, np.array(rg)[:,i]]))
    I0slp[i], I0yint[i], r, I0p[i], I0err[i] = stats.linregress(np.array([scalefactor, np.array(I0)[:,i]]))
    rgslppc[i] = np.divide(rgslp[i],rgyint[i])*100
    I0slppc[i] = np.divide(I0slp[i],I0yint[i])*100

rgpravg = np.average(rgpr)
dmaxavg = np.average(dmax)
I0pravg = np.average(I0pr)
porodmwavg = np.average(porodmw)
vcorrmwavg = np.average(vcorrmw)
rgprslp, rgpryint, r, rgprp, rgprerr = stats.linregress([scalefactor, rgpr])
dmaxslp, dmaxyint, r, dmaxp, dmaxerr = stats.linregress([scalefactor, dmax])
I0prslp, I0pryint, r, I0prp, I0prerr = stats.linregress([scalefactor, I0pr])
porodmwslp, porodmwyint, r, porodmwp, porodmwerr = stats.linregress([scalefactor, porodmw])
vcorrmwslp, vcorrmwyint, r, vcorrmwp, vcorrmwerr = stats.linregress([scalefactor, vcorrmw])
rgprslppc = np.divide(rgprslp, rgpryint)*100
dmaxslppc = np.divide(dmaxslp, dmaxyint)*100
I0prslppc = np.divide(I0prslp, I0pryint)*100
porodmwslppc = np.divide(porodmwslp, porodmwyint)*100
vcorrmwslppc = np.divide(vcorrmwslp, vcorrmwyint)*100

outputarray = np.zeros((19,len(keepers)+7))

for conc in keepers:
    column = conc+1
    outputarray[0][column] = round(scalefactor[conc],3)
    outputarray[1][column] = glowerp[conc, 0]+1
    outputarray[2][column] = glowerp[conc, 1]+1
    outputarray[3][column] = glowerp[conc, 2]+1
    outputarray[4][column] = gupperp[conc]+1
    outputarray[5][column] = gps[conc, 0]
    outputarray[6][column] = gps[conc, 1]
    outputarray[7][column] = gps[conc, 2]
    outputarray[8][column] = rgs[conc, 0]
    outputarray[9][column] = rgs[conc, 1]
    outputarray[10][column] = rgs[conc, 2]
    outputarray[11][column] = rgpr[conc]
    outputarray[12][column] = I0[conc, 0]
    outputarray[13][column] = I0[conc, 1]
    outputarray[14][column] = I0[conc, 2]
    outputarray[15][column] = I0pr[conc]
    outputarray[16][column] = dmax[conc]
    outputarray[17][column] = porodmw[conc]
    outputarray[18][column] = vcorrmw[conc]

#collect averages
column = len(keepers) + 1
outputarray[8][column] = rgavg[0]
outputarray[9][column] = rgavg[1]
outputarray[10][column] = rgavg[2]
outputarray[11][column] = rgpravg
outputarray[12][column] = I0avg[0]
outputarray[13][column] = I0avg[1]
outputarray[14][column] = I0avg[2]
outputarray[15][column] = I0pravg
outputarray[16][column] = dmaxavg
outputarray[17][column] = porodmwavg
outputarray[18][column] = vcorrmwavg

#collect slopes
column = len(keepers) + 2
outputarray[8][column] = rgslp[0]
outputarray[9][column] = rgslp[1]
outputarray[10][column] = rgslp[2]
outputarray[11][column] = rgprslp
outputarray[12][column] = I0slp[0]
outputarray[13][column] = I0slp[1]
outputarray[14][column] = I0slp[2]
outputarray[15][column] = I0prslp
outputarray[16][column] = dmaxslp
outputarray[17][column] = porodmwslp
outputarray[18][column] = vcorrmwslp

#collect y-intercepts
column = len(keepers) + 3
outputarray[8][column] = rgyint[0]
outputarray[9][column] = rgyint[1]
outputarray[10][column] = rgyint[2]
outputarray[11][column] = rgpryint
outputarray[12][column] = I0yint[0]
outputarray[13][column] = I0yint[1]
outputarray[14][column] = I0yint[2]
outputarray[15][column] = I0pryint
outputarray[16][column] = dmaxyint
outputarray[17][column] = porodmwyint
outputarray[18][column] = vcorrmwyint

#collect standard errors
column = len(keepers) + 4
outputarray[8][column] = rgerr[0]
outputarray[9][column] = rgerr[1]
outputarray[10][column] = rgerr[2]
outputarray[11][column] = rgprerr
outputarray[12][column] = I0err[0]
outputarray[13][column] = I0err[1]
outputarray[14][column] = I0err[2]
outputarray[15][column] = I0prerr
outputarray[16][column] = dmaxerr
outputarray[17][column] = porodmwerr
outputarray[18][column] = vcorrmwerr

#collect p-values
column = len(keepers) + 5
outputarray[8][column] = rgp[0]
outputarray[9][column] = rgp[1]
outputarray[10][column] = rgp[2]
outputarray[11][column] = rgprp
outputarray[12][column] = I0p[0]
outputarray[13][column] = I0p[1]
outputarray[14][column] = I0p[2]
outputarray[15][column] = I0prp
outputarray[16][column] = dmaxp
outputarray[17][column] = porodmwp
outputarray[18][column] = vcorrmwp

#collect slopes as percentage of y-intercept
column = len(keepers) + 6
outputarray[8][column] = rgslppc[0]
outputarray[9][column] = rgslppc[1]
outputarray[10][column] = rgslppc[2]
outputarray[11][column] = rgprslppc
outputarray[12][column] = I0slppc[0]
outputarray[13][column] = I0slppc[1]
outputarray[14][column] = I0slppc[2]
outputarray[15][column] = I0prslppc
outputarray[16][column] = dmaxslppc
outputarray[17][column] = porodmwslppc
outputarray[18][column] = vcorrmwslppc

outputrounded = np.around(outputarray, 3)

outputlist = outputrounded.tolist()

if usestd is True:
    outputlist[0][0] = 'mg/ml'
else:
    outputlist[0][0] = 'Scale-Factor'
outputlist[1][0] = 'BegP-1'
outputlist[2][0] = 'BegP-2'
outputlist[3][0] = 'BegP-3'
outputlist[4][0] = 'EndP'
outputlist[5][0] = 'G-1_p'
outputlist[6][0] = 'G-2_p'
outputlist[7][0] = 'G-3_p'
outputlist[8][0] = 'Rg-1'
outputlist[9][0] = 'Rg-2'
outputlist[10][0] = 'Rg-3'
outputlist[11][0] = 'Rg-Pr'
outputlist[12][0] = 'I(0)/c-1'
outputlist[13][0] = 'I(0)/c-2'
outputlist[14][0] = 'I(0)/c-3'
outputlist[15][0] = 'I(0)/c-Pr'
outputlist[16][0] = 'Dmax'
outputlist[17][0] = 'Porod-MW'
outputlist[18][0] = 'Vcorr-MW'

concentrations = [x for y in keepers for x in conc_list[y]]
header_row_tmp = [sample, concentrations, 'Avg', 'Slope', 'Extrap', 'Error', 'P-value', 'Slope(%)']
header_row = list(itertools.chain.from_iterable(itertools.repeat(x,1) if isinstance(x,str) else x for x in header_row_tmp))
numcolumns = len(header_row)
alignment = ['center'] * numcolumns
'''
if args.r:
    HTMLFILE = sample + '_rdam_stats.html'
else:
    HTMLFILE = sample + '_conc_stats.html'
f = open(HTMLFILE, 'w')
htmlcode = HTML.table(outputlist, header_row = header_row, col_align = alignment)
f.write(htmlcode + '<p>\n')
f.close()
'''
outputarray2 = np.vstack((np.array(header_row), np.array(outputlist)))

if args.r:
    np.savetxt(sample + '_rdam_stats.csv', outputarray2, delimiter=',', fmt='%s')
else:
    np.savetxt(sample + '_conc_stats.csv', outputarray2, delimiter=',', fmt='%s')

























