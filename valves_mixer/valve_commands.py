import serial
import time

vv = serial.Serial('COM7', baudrate=9600, timeout=1)

def send_com(aa):
    global vv
    vv.write(str.encode(aa+'\r\n'))
    time.sleep(.1)
    response = (vv.readline())
    print(response)

# query (No ending R)
command = '/1&'  # get firmware info

command = '/1?0'  # get commanded motor position
command = '/1?1'  # get Start Velocity
command = '/1?2'  # get Max Speed
command = '/1?3'  # get Stop Speed

command = '/1?5'  # get Velocity mode Speed
command = '/1?6'  # get current step size microsteps/step

command = '/1?8' # get encoder position

command = '/1Q' # query current status

command = '/1T' # terminate current move


# execute

command ='/1Z400R' # home/initialize (with speed 'v')

command ='/1z400R' # set this  position without moving

command = '/1v200R' # set start speed (200-2500, 200)

command = '/1V1000R' # set max velocity, microsteps per second (50-10000, 3700)

command = '/1c200R' # set start speed (200-2500, 200)

command = '/1A800R' # move absolute (microsteps)

command = '/1P800R' # move relative positive (microsteps)
command = '/1D800R' # move relative negative (microsteps)




command = '/1j2R'  # set microstep size

send_com(command)


def send_com(aa):
    vv.write(str.encode(aa+'\r\n'))
    time.sleep(.1)
    response = vv.readline()

    print(response)