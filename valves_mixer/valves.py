#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@author: rajkovic
'''
import sys
import serial
import os
import serial.tools.list_ports
import pickle
import time

from PyQt5 import QtGui, uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QStyleFactory, QMessageBox, QWidget, QLineEdit, QComboBox, QPushButton, QSpinBox, QFileDialog, QDial
from PyQt5.QtCore import QTimer, QEvent, Qt


class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__() # Call the inherited classes __init__ method
        uic.loadUi('valves.ui', self) # Load the .ui file
        self.show() # Show the GUI

        # for testing purposes, without serial connection
        self.testing = 1

        self.port_fill_list()
        if len(self.plist) > 0:
            for i,p in enumerate(self.plist):
                 if 'USB' in p[1]:
                     self.serport_list.setCurrentIndex(i)

        # get names of GUI elements

        self.qle_list = self.findChildren(QLineEdit)
        self.qsb_list = self.findChildren(QSpinBox)
        self.qcb_list = self.findChildren(QComboBox)
        self.qpb_list = self.findChildren(QPushButton)
        self.qdi_list = self.findChildren(QDial)


        self.settings_file = 'valves.cfg'

        # variables to load/save

        self.to_save = []
        for jj in self.qle_list:
            jn = jj.objectName()
            if jn.startswith('l_') or jn.startswith('pn_'):
                self.to_save.append(jn)

        for jj in self.qsb_list:
            jn = jj.objectName()
            if jn.startswith('v_'):
                self.to_save.append(jn)

        for jj in self.qcb_list:
            jn = jj.objectName()
            if jn.startswith('pr'):
                self.to_save.append(jn)



        # load settings and rename fields
        self.init_load_settings()



        # set some variables

        self.left = 2
        self.right = 1

        self.statusbar.showMessage('not connected')

        # button connections

        for jj in self.qle_list:
            jn = jj.objectName()
            if jn.startswith('l_'):
                jj.editingFinished.connect(self.position_names_single)
            elif jn.startswith('pn_'):
                jj.editingFinished.connect(self.pb_names_single)

        for jj in self.qpb_list:
            if jj.objectName().startswith('bp_'):
                jj.clicked.connect(self.run_program)
            elif jj.objectName().startswith('mv_a'):
                jj.clicked.connect(self.move_abs)
            elif jj.objectName().startswith('mv_b'):
                jj.clicked.connect(self.move_rel)
            elif jj.objectName().startswith('set_'):
                jj.clicked.connect(self.set_pos)


        for jj in self.qdi_list:
            jj.sliderReleased.connect(self.move_valve)


        self.find_serial.clicked.connect(self.port_fill_list)

        self.serial_connect.clicked.connect(self.s_conn)
        self.serial_disconnect.clicked.connect(self.s_disconn)
        self.home_l.clicked.connect(self.home_init_l)
        self.home_r.clicked.connect(self.home_init_r)

        self.load_info.clicked.connect(self.load_settings)
        self.save_info.clicked.connect(self.save_settings)

        self.dial_l.installEventFilter(self)
        self.dial_r.installEventFilter(self)

    def eventFilter(self, source, event):
        #if (((source is self.dial_l) or (source is self.dial_r)) and isinstance(event, (
        if ((source in [self.dial_l, self.dial_r]) and isinstance(event, (
            QtGui.QWheelEvent, QtGui.QKeyEvent))):
            return(True)
        return(QWidget.eventFilter(self, source, event))


    def port_fill_list(self):
        self.serport_list.clear()

        self.plist = sorted(serial.tools.list_ports.comports())

        if len(self.plist) == 0:
            self.err_msg_pop('Port error','No ports found')
        else:
            for port, desc, hwid in self.plist:
                self.serport_list.addItem('{} - {}'.format(port,desc))

    def position_names_fill_list(self):
        for jj in self.qle_list:
            jn = jj.objectName()
            if jn.startswith('l_'):
                for ii in range(1,5):
                    qcb_name = 'pr{}_l{}'.format(ii,jn[2])
                    qcb = self.findChild(QComboBox,qcb_name)
                    qcb_i = int(jn[3])-1
                    qcb.setItemText(qcb_i,jj.text())

    def position_names_single(self):
        jj = self.sender()
        jn = jj.objectName()
        for ii in range(1,5):
            qcb_name = 'pr{}_l{}'.format(ii,jn[2])
            qcb = self.findChild(QComboBox,qcb_name)
            qcb_i = int(jn[3])-1
            qcb.setItemText(qcb_i,jj.text())

    def pb_names(self):
        for jj in self.qle_list:
            jn = jj.objectName()
            if jn.startswith('pn_'):
                but_name = 'bp_{}'.format(jn[3])
                but = self.findChild(QPushButton,but_name)
                but.setText(jj.text())

    def pb_names_single(self):
        jj =  self.sender()
        jn = jj.objectName()
        but_name = 'bp_{}'.format(jn[3])
        but = self.findChild(QPushButton,but_name)
        but.setText(jj.text())

    def s_conn(self):
        if len(self.plist) == 0:
            self.err_msg_pop('Port error','No ports found')
        else:
            sel_port = self.serport_list.currentText().split(' - ')[0]
            self.valves_con = serial.Serial(sel_port, baudrate=9600, timeout=1)

            # change microstepping setting

            self.valves_con.write('/1j16R\r\n'.encode())
            time.sleep(.1)
            response = self.valves_con.readline()

            self.valves_con.write('/2j16R\r\n'.encode())
            time.sleep(.1)
            response = self.valves_con.readline()

            # set speed

            self.valves_con.write('/1V2400R\r\n'.encode())
            time.sleep(.1)
            response = self.valves_con.readline()

            self.valves_con.write('/2V2400R\r\n'.encode())
            time.sleep(.1)
            response = self.valves_con.readline()

            self.statusbar.showMessage('Connected')
            self.serial_connect.setEnabled(False)
            self.serial_disconnect.setEnabled(True)

    def s_disconn(self):
        self.valves_con.close()
        self.statusbar.showMessage('Not connected')
        self.serial_connect.setEnabled(True)
        self.serial_disconnect.setEnabled(False)

    def home_init_l(self):

        cts = '/{}Z400R\r\n'.format(self.left)
        if not self.testing:
            self.valves_con.write(cts.encode())
            time.sleep(.1)
            response = self.valves_con.readline()
        else:
            print(cts)

    def home_init_r(self):

        cts = '/{}Z400R\r\n'.format(self.right)
        if not self.testing:
            self.valves_con.write(cts.encode())
            time.sleep(.1)
            response = self.valves_con.readline()
        else:
            print(cts)


    def run_program(self):
        pnum = self.sender().objectName()[-1]

        # get commands to run
        lv_sp_name = 'pr{}_ll'.format(pnum)
        rv_sp_name = 'pr{}_lr'.format(pnum)

        lv_togo_pos = 1+self.findChild(QComboBox,lv_sp_name).currentIndex()
        rv_togo_pos = 1+self.findChild(QComboBox,rv_sp_name).currentIndex()

        vl_name = 'v_l{}'.format(lv_togo_pos)
        vr_name = 'v_r{}'.format(rv_togo_pos)

        left_pos = self.findChild(QSpinBox,vl_name).value()
        right_pos = self.findChild(QSpinBox,vr_name).value()

        l_cts = '/{}A{}\r\n'.format(self.left,left_pos)
        r_cts = '/{}A{}\r\n'.format(self.right,right_pos)

        # move dials
        self.dial_l.setValue(lv_togo_pos)
        self.dial_r.setValue(rv_togo_pos)

        if not self.testing:

            self.valves_con.write(l_cts.encode())
            time.sleep(.1)
            self.valves_con.write(r_cts.encode())
            time.sleep(.1)
            self.valves_con.write('/AR\r\n'.encode())
            time.sleep(.1)
            response = self.valves_con.readline()
        else:
            print(l_cts,r_cts)




    def move_valve(self):
        # determine which valve to move
        v_side = self.sender().objectName()[-1]
        if v_side == 'l':
            v_num = self.left
        else:
            v_num = self.right

        # get dial value
        dial_pos = self.sender().value()

        sp_name = 'v_{}{}'.format(v_side,dial_pos)
        sp_w = self.findChild(QSpinBox,sp_name)

        position = sp_w.value()

        # prepare command

        cts = '/{}A{}R\r\n'.format(v_num,position)

        if not self.testing:
            self.valves_con.write(cts.encode())
            time.sleep(.1)
            response = self.valves_con.readline()
        else:
            print(cts)


    def move_abs(self):
        # determine which valve to move
        v_side = self.sender().objectName()[-1]
        if v_side == 'l':
            v_num = self.left
        else:
            v_num = self.right

        pos = self.findChild(QSpinBox,'b'+self.sender().objectName()).value()

        cts = '/{}A{}R\r\n'.format(v_num,pos)

        if not self.testing:
            self.valves_con.write(cts.encode())
            time.sleep(.1)
            response = self.valves_con.readline()
        else:
            print(cts)


    def move_rel(self):
        # determine which valve to move
        v_side = self.sender().objectName()[-1]
        if v_side == 'l':
            v_num = self.left
        else:
            v_num = self.right

        pos = self.findChild(QSpinBox,'b'+self.sender().objectName()).value()

        if pos > 0:
            cc = 'P'
        elif pos < 0:
            cc = 'D'
        else:
            print('Not moving by 0')

        if pos != 0:
            cts = '/{}{}{}R\r\n'.format(v_num,cc,abs(pos))
            if not self.testing:
                self.valves_con.write(cts.encode())
                time.sleep(.1)
                response = self.valves_con.readline()
            else:
                print(cts)

    def set_pos(self):
        # determine which valve to move
        v_side = self.sender().objectName()[-1]

        if v_side == 'l':
            v_num = self.left
        else:
            v_num = self.right

        pos = self.findChild(QSpinBox,'b'+self.sender().objectName()).value()
        cts = '/{}z{}R\r\n'.format(v_num,pos)

        if not self.testing:


            self.valves_con.write(cts.encode())
            time.sleep(.1)
            response = self.valves_con.readline()
        else:
            print(cts)



    def init_load_settings(self):
        # load file if exists, fill lineedit values
        if os.path.isfile(str(self.settings_file)):
            # load settings

            with open(self.settings_file,'rb') as f:
                dd = pickle.load(f)

            for kk in range(len(dd)):
                if dd[kk][0].startswith('v_'):
                    le = self.findChild(QSpinBox,dd[kk][0])
                    le.setValue(dd[kk][1])
                elif dd[kk][0].startswith('l_') or dd[kk][0].startswith('pn_'):
                    le = self.findChild(QLineEdit,dd[kk][0])
                    le.setText(dd[kk][1])
                elif dd[kk][0].startswith('pr'):
                    le = self.findChild(QComboBox,dd[kk][0])
                    le.setCurrentIndex(dd[kk][1])


        else:
            print('cannot find the settings file')

        # rename buttons/comboboxes
        self.position_names_fill_list()
        self.pb_names()

    def load_settings(self):
        # select file using gui
        options = QFileDialog.Options()
        self.settings_file = QFileDialog.getOpenFileName(self,"Select settings file", '.', ".cfg files (*.cfg);; All Files (*)", options=options)[0]

        self.init_load_settings()


    def save_settings(self):
        # save settings, use gui for file select
        options = QFileDialog.Options()
        save_set_file = QFileDialog.getSaveFileName(self, 'Save .cfg File','.',".cfg files (*.cfg);;All Files (*)", options=options)[0]


        save_var= []

        for oo in self.to_save:
            if oo.startswith('v_'):
                le = self.findChild(QSpinBox,oo)
                save_var.append([oo,le.value()])
            elif oo.startswith('pn_') or oo.startswith('l_'):
                le = self.findChild(QLineEdit,oo)
                save_var.append([oo,le.text()])
            elif oo.startswith('pr'):
                le = self.findChild(QComboBox,oo)
                save_var.append([oo,le.currentIndex()])




        #print(save_var)
        with open(save_set_file, 'wb') as sf:
            pickle.dump(save_var,sf)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWindow()
    sys.exit(app.exec_())






####
# first thing, set microstepping to 16
# after each homing reset max speed to 2400?
# output positions are at +800 steps (with 16 microsteps/step)
##########

