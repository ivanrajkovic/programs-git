#!/usr/bin/env python

'''
@author: rajkovic
'''

import time
import os
from distutils.dir_util import mkpath
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import getpass
import urllib
from subprocess import check_output
import multiprocessing

log_name = os.path.join('/tmp','gecko_'+getpass.getuser()+'.log')

cam_in_place = 0
first = True

#fbinary = FirefoxBinary('/usr/bin/firefox')

URL= 'http://134.79.35.33/view/index2.shtml?newstyle=One&cam=1'
img1_url = 'http://134.79.35.33/jpg/1/image.jpg'
img2_url = 'http://134.79.35.121/jpg/1/image.jpg'
img3_url = 'http://134.79.35.121/jpg/2/image.jpg'
img4_url = 'http://134.79.35.121/jpg/3/image.jpg'
img5_url = 'http://134.79.35.121/jpg/4/image.jpg'

options = webdriver.FirefoxOptions()
options.set_headless(True)



def move_cam():
    ff = webdriver.Firefox(options=options,log_path=log_name)
    ff.get(URL)

    #aa = Select(ff.find_element_by_name('conf_PRESET_PresetName'))
    #aa.select_by_visible_text('LVDT')
    Select(ff.find_element_by_name('conf_PRESET_PresetName')).select_by_visible_text('LVDT')
    #time.sleep(1)
    print('camera moved')
    ff.quit()




while True:
    if time.strftime("%I%M",time.localtime()) == '0730' or first:
        first = False
        print("It is time!\n")
        print("{}".format(time.strftime("%I:%M:%S",time.localtime())))
        lpath = os.path.join(os.path.expanduser('~'),'setup_data/LVDT',time.strftime("%Y",time.localtime()),time.strftime("%m",time.localtime()))
        cpath = os.path.join(os.path.expanduser('~'),'setup_data/cameras',time.strftime("%Y",time.localtime()),time.strftime("%m",time.localtime()))
        chilpath = os.path.join(os.path.expanduser('~'),'setup_data/chiller',time.strftime("%Y",time.localtime()),time.strftime("%m",time.localtime()))


        mkpath(lpath)
        mkpath(cpath)


        img_suffix = '_{}.jpg'.format(time.strftime("%Y%m%d-%H%M",time.localtime()))
        chiller_name = 'chiller_info'+img_suffix[:-4]+".txt"



        while cam_in_place == 0:
            pp = multiprocessing.Process(target=move_cam)
            pp.start()

            pp.join(15)

            if pp.is_alive():
                print('camera not moved, stopping.')
                pp.terminate()
                pp.join()
            else:
                cam_in_place = 1



        urllib.urlretrieve(img1_url,os.path.join(lpath,'LVDT'+img_suffix))

        urllib.urlretrieve(img2_url,os.path.join(cpath,'C1'+img_suffix))
        urllib.urlretrieve(img3_url,os.path.join(cpath,'C2'+img_suffix))
        urllib.urlretrieve(img4_url,os.path.join(cpath,'C3'+img_suffix))
        urllib.urlretrieve(img5_url,os.path.join(cpath,'C4'+img_suffix))

        print('Images taken!\n')
        try:
            out = check_output(["chiller_info.py"])
        except:
            out = 'No connection!\n'

        if out[:3] != 'No ':
            mkpath(chilpath)
            with open(os.path.join(chilpath,chiller_name),'w') as cinf:
                cinf.write(out)

        cam_in_place = 0

    else:
        print('Waiting {}'.format(time.strftime("%I:%M:%S",time.localtime())))

    time.sleep(55)