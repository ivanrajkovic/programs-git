#!/usr/bin/env python

'''
@author: rajkovic
'''
import sys
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import urllib
import time
import datetime
import getpass
import os

path = '.'

if len(sys.argv) == 2:
    if sys.argv[1] == '--help':
        print("""\n\n\tHow-to:

        get_LVDT.py [folder]

        This will take an image of the LVDT panel and create a LVDT-<date>_<time>.jpg image in 'folder'.
        If 'folder' is not specified, the image will be saved in the current folder.

        """)
        sys.exit()
    else:
        path = sys.argv[1]

URL= 'http://134.79.35.33/view/index2.shtml?newstyle=One&cam=1'
img_url = 'http://134.79.35.33/jpg/1/image.jpg'
#URL = 'http://134.79.35.33/view/indexFrame.shtml'

options = webdriver.FirefoxOptions()
options.set_headless(True)


if getpass.getuser() == 'staff':
    fp = webdriver.FirefoxProfile('/mnt/home/staff/.mozilla/firefox/vx3s3y6p.lvdt')
    ff = webdriver.Firefox(options=options,firefox_profile=fp)
else:
    ff = webdriver.Firefox(options=options)

ff.get(URL)


aa = Select(ff.find_element_by_name('conf_PRESET_PresetName'))
aa.select_by_visible_text('LVDT')
time.sleep(1)
ff.quit()

img_name = 'LVDT_{}.jpg'.format(datetime.datetime.now().strftime("%Y%m%d-%H%M"))

urllib.urlretrieve(img_url,os.path.join(path,img_name))

print('Saved image {}'.format(os.path.abspath(os.path.join(path,img_name))))

