#!/usr/bin/env python

# change python path when using from cron
# /mnt/home/staff/SW/python/bin/python
'''
@author: rajkovic
'''

import time
import os
from distutils.dir_util import mkpath
#from selenium import webdriver
#from selenium.webdriver.support.ui import Select
#from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import getpass
import urllib
#from subprocess import check_output
#import multiprocessing

#log_name = os.path.join('/tmp','gecko_'+getpass.getuser()+'.log')

# cam_in_place = 0


#fbinary = FirefoxBinary('/usr/bin/firefox')

# URL= 'http://134.79.35.33/view/index2.shtml?newstyle=One&cam=1'

IPa = '172.22.246.56'
IPb = '172.22.246.57'

img1_url = 'http://{}/jpg/1/image.jpg'.format(IPa)
img2_url = 'http://{}/jpg/1/image.jpg'.format(IPb)
img3_url = 'http://{}/jpg/2/image.jpg'.format(IPb)
img4_url = 'http://{}/jpg/3/image.jpg'.format(IPb)
img5_url = 'http://{}/jpg/4/image.jpg'.format(IPb)


#img1_url = 'http://134.79.35.33/jpg/1/image.jpg'
#img2_url = 'http://134.79.35.121/jpg/1/image.jpg'
#img3_url = 'http://134.79.35.121/jpg/2/image.jpg'
#img4_url = 'http://134.79.35.121/jpg/3/image.jpg'
#img5_url = 'http://134.79.35.121/jpg/4/image.jpg'

#options = webdriver.FirefoxOptions()
#options.set_headless(True)



#def move_cam():
#    ff = webdriver.Firefox(executable_path='/mnt/home/staff/SW/bin/geckodriver',options=options,log_path=log_name)
#    ff.get(URL)

    #aa = Select(ff.find_element_by_name('conf_PRESET_PresetName'))
    #aa.select_by_visible_text('LVDT')
#    Select(ff.find_element_by_name('conf_PRESET_PresetName')).select_by_visible_text('LVDT')
    #time.sleep(1)
#    print('Camera moved\n')
#    ff.quit()



#lpath = os.path.join(os.path.expanduser('~'),'setup_data/LVDT',time.strftime("%Y",time.localtime()),time.strftime("%m",time.localtime()))
#cpath = os.path.join(os.path.expanduser('~'),'setup_data/cameras',time.strftime("%Y",time.localtime()),time.strftime("%m",time.localtime()))
#chilpath = os.path.join(os.path.expanduser('~'),'setup_data/chiller',time.strftime("%Y",time.localtime()),time.strftime("%m",time.localtime()))


[lpath,cpath,chilpath] = map(lambda x: os.path.join(os.path.expanduser('~'),'setup_data',x,time.strftime("%Y",time.localtime()),time.strftime("%m",time.localtime())), ['LVDT','cameras','chiller'])

mkpath(lpath)
mkpath(cpath)

img_suffix = '_{}.jpg'.format(time.strftime("%Y%m%d-%H%M",time.localtime()))
chiller_name = 'chiller_info'+img_suffix[:-4]+".txt"



#while cam_in_place == 0:
#    pp = multiprocessing.Process(target=move_cam)
#    pp.start()
#
#    pp.join(30)
#
#    if pp.is_alive():
#        print('camera not moved, retrying.')
#       pp.terminate()
#        pp.join()
#    else:
#        cam_in_place = 1

# move camera to preset LVDT
urllib.urlretrieve('http://{}/axis-cgi/com/ptz.cgi?gotoserverpresetname=LVDT&camera=1'.format(IPa))

# wait frot he camera to move
time.sleep(3)


urllib.urlretrieve(img2_url,os.path.join(cpath,'C1'+img_suffix))
urllib.urlretrieve(img3_url,os.path.join(cpath,'C2'+img_suffix))
urllib.urlretrieve(img4_url,os.path.join(cpath,'C3'+img_suffix))
urllib.urlretrieve(img5_url,os.path.join(cpath,'C4'+img_suffix))

urllib.urlretrieve(img1_url,os.path.join(lpath,'LVDT'+img_suffix))

print('Images taken!\n')
try:
    out = check_output(["chiller_info.py"])
except:
    print('No connection to the chiller!\n')
    out = 'No conn'

if out[:3] != 'No ':
    mkpath(chilpath)
    with open(os.path.join(chilpath,chiller_name),'w') as cinf:
        cinf.write(out)
    print('Chiller info taken!\n')
elif out != 'No conn':
    print('No connection to the chiller!\n')
