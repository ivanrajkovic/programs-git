import pytesseract
from PIL import Image
from PIL import ImageFilter
#from skimage.transform import warp, AffineTransform
import numpy
import PIL.ImageOps   
import os

#pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files/Tesseract-OCR/tesseract'

print(pytesseract.get_tesseract_version())

#print(pytesseract.image_to_string(Image.open('Desktop/image.jpg')))

#ii = Image.open('Desktop\image.jpg')
os.chdir("bitbucket/programs/axis")
ii = Image.open('image.jpg')

jj = ii.rotate(-24,expand=1)



ll = [PIL.ImageOps.invert(jj.crop([240,340,323,370]).convert('L'))]
ll.append(PIL.ImageOps.invert(jj.crop([340,340,460,370]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([460,340,575,370]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([580,340,700,370]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([230,380,320,410]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([340,380,460,410]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([460,380,550,410]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([560,380,650,410]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([190,430,290,460]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([300,433,420,460]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([420,433,530,460]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([540,433,620,460]).convert('L')))
ll.append(PIL.ImageOps.invert(jj.crop([180,470,280,495]).convert('L')))



# threshold = 170  
# aa = ll.point(lambda p: p > threshold and 255)  
# aa.show()

for n in range(len(ll)):
    
    bb = ll[n].resize([ll[n].size[0]*10,ll[n].size[1]*10],resample=PIL.Image.BICUBIC).filter(ImageFilter.GaussianBlur(7))
    
    
    dd = numpy.array(bb)
    
    dd[dd>150] = 255
    dd[dd<150] = 0
    
    for qq in range(int(dd.shape[0])):
        dd[qq,:] = numpy.roll(dd[qq,:],int(qq/2.5))
    
    gg = Image.fromarray(dd).filter(ImageFilter.GaussianBlur(3))
    #gg.show()
    
    yy = gg.resize([int(gg.size[0]/5),int(gg.size[1]/5)],resample=PIL.Image.LANCZOS)
    yy = yy.resize([yy.size[0]*2,yy.size[1]*2],resample=PIL.Image.BICUBIC)
    yy.show()
    
    print(pytesseract.image_to_string(yy,config=' --psm 7 -c tessedit_char_whitelist=-0123456789'))


