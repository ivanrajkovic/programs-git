import serial
from binascii import unhexlify

## this works
# tts = b'\x02\x07DIA\x2e\xdc\x03'
# ser.write(tts)
# ser.read_until(b'\x03')

# \x02 - start sending
# \x07 - length of bytes to come (1+3+2+1) (l(1)+ command(3) + crc(2) + end(1))
# DIA - command
# \x2e\xdc - crc16 (0x2edc)
# \x03 - end
## yep



def crc16(data: bytes):
    xor_in = 0x0000  # initial value
    xor_out = 0x0000  # final XOR value
    poly = 0x1021  # generator polinom (normal form)

    reg = xor_in
    for octet in data:
        # reflect in
        for i in range(8):
            topbit = reg & 0x8000
            if octet & (0x80 >> i):
                topbit ^= 0x8000
            reg <<= 1
            if topbit:
                reg ^= poly
        reg &= 0xFFFF
        # reflect out
    return reg ^ xor_out

ser=serial.Serial(port='COM3', baudrate=19200,timeout=10)

#ser.write('00DIA\x0d'.encode('UTF-8'))
ser.write('01DIA\r'.encode('UTF-8'))
aa = ser.read_until(b'\x03')

bb = aa.decode('UTF-8')


# try safe protocol

to_send = '1VOL'

ts_l = len(to_send)+4

ts_crc = crc16(to_send.encode())



tts = b'\x02' + unhexlify('{0:02x}'.format(ts_l)) + to_send.encode() + unhexlify('{0:04x}'.format(ts_crc))  + b'\x03'





ser.write(tts)
aa = ser.read_until(b'\x03')

bb = aa.decode('UTF-8')



# pump number
pn = bb[1:3]

# status/alarm
sa = bb[3]
if sa == 'A':
    sa = sa + bb[4]

# return data
rd = bb[3+len(sa):-1]


print(' pump: {} \n status: {} \n reply: {}'.format(pn,sa,rd))

ser.close()