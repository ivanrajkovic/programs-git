#!/usr/bin/env python

'''
@author: rajkovic
'''

import serial
from binascii import unhexlify
import argparse


def crc16(data):
    xor_in = 0x0000  # initial value
    xor_out = 0x0000  # final XOR value
    poly = 0x1021  # generator polinom (normal form)

    reg = xor_in
    for octet in data:
        # reflect in
        for i in range(8):
            topbit = reg & 0x8000
            if octet & (0x80 >> i):
                topbit ^= 0x8000
            reg <<= 1
            if topbit:
                reg ^= poly
        reg &= 0xFFFF
        # reflect out
    return reg ^ xor_out


if __name__ == "__main__":

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
    Send a program from a file to the syringe pump.
    """)
    parser.add_argument('prog_file', help='program file to send to the pump')
    parser.add_argument('-c','--com', default=4, type=int, help='com port to use [n], default 4')
    parser.add_argument('-s','--safe_mode', default=False,  action='store_true', help='use safe mode for communication, default: no safe mode')

    arg_in = parser.parse_args()

    cp = 'COM{}'.format(arg_in.com)
    ser=serial.Serial(port=cp, baudrate=19200,timeout=10)
    with open(arg_in.prog_file) as fp:
        for line in fp:
            if not (line =="\n" or line.startswith('#')):
                to_send = line.encode()
                if arg_in.safe_mode:
                    ts_l = len(to_send)+4
                    ts_crc = crc16(to_send)
                    tts = b'\x02' + unhexlify('{0:02x}'.format(ts_l)) + to_send + unhexlify('{0:04x}'.format(ts_crc))  + b'\x03'
                else:
                    tts = to_send + b'\x0d'

                ser.write(tts)
                pump_return = ser.read_until(b'\x03').decode('UTF-8')

    ser.close()

