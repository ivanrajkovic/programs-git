#!/usr/bin/env python

'''
@author: rajkovic
'''

import serial
from binascii import unhexlify
import argparse

def crc16(data):
    xor_in = 0x0000  # initial value
    xor_out = 0x0000  # final XOR value
    poly = 0x1021  # generator polinom (normal form)

    reg = xor_in
    for octet in data:
        # reflect in
        for i in range(8):
            topbit = reg & 0x8000
            if octet & (0x80 >> i):
                topbit ^= 0x8000
            reg <<= 1
            if topbit:
                reg ^= poly
        reg &= 0xFFFF
        # reflect out
    return reg ^ xor_out



if __name__ == "__main__":

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
    Syringe pump control program for NE- pumps from syringepump.com.

    """)
    parser.add_argument('command', help='command to send to the pump')
    parser.add_argument('-n','--pump_number', default=0, type=int, help='pump number [n], default 0')
    parser.add_argument('-s','--safe_mode', default=False,  action='store_true', help='use safe mode for communication, default: no safe mode')
    parser.add_argument('-c','--com', default=3, type=int, help='com port to use [n], default 3')

    arg_in = parser.parse_args()

    to_send = '{}{}'.format(arg_in.pump_number,arg_in.command).encode()

    cp = 'COM{}'.format(arg_in.com)

    if arg_in.safe_mode:
        ts_l = len(to_send)+4
        ts_crc = crc16(to_send)
        tts = b'\x02' + unhexlify('{0:02x}'.format(ts_l)) + to_send + unhexlify('{0:04x}'.format(ts_crc))  + b'\x03'
    else:
        tts = to_send + b'\x0d'
    #print(tts)
    ser=serial.Serial(port=cp, baudrate=19200,timeout=10)
    ser.write(tts)
    pump_return = ser.read_until(b'\x03').decode('UTF-8')
    ser.close()
    # pump number
    pn = pump_return[1:3]

    # status/alarm
    sa = pump_return[3]
    if sa == 'A':
        sa = sa + pump_return[4]

    # return data
    rd = pump_return[3+len(sa):-1]


    print(' pump: {} \n status: {} \n reply: {}'.format(pn,sa,rd))




