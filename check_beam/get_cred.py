#!/usr/bin/env python2

import httplib2
import os
import oauth2client
from oauth2client import client, tools
from apiclient import errors, discovery

home_dir = os.path.expanduser('~')
credential_dir = os.path.join(home_dir, '.credentials')
if not os.path.exists(credential_dir):
    os.makedirs(credential_dir)
credential_path = os.path.join(credential_dir, 'gmail-python-email-send.json')
store = oauth2client.file.Storage(credential_path)
#flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
flow = client.OAuth2WebServerFlow(client_id="579024895352-ms746a3qfj5422lfbg5vj47v1mjhmvc2.apps.googleusercontent.com", client_secret="EjpsR1yfvpefhB0uCPBbHqBJ", scope='https://www.googleapis.com/auth/gmail.send')
flow.user_agent = 'check_beam'
credentials = tools.run_flow(flow, store)