#!/usr/bin/env python2
import smtplib
#import httplib2
import os


import requests
import time
import datetime
import sys

import socket


from cryptography.fernet import Fernet

def SendMessage(sender, pp, ff, to, subject, msgPlain):
    connection = smtplib.SMTP(host='smtp.office365.com', port=587)
    connection.starttls()
    connection.login(sender,ff.decrypt(pp))
    
    message = """From: %s\nBCC: %s\nSubject: %s\n\n%s
    """ % (sender, ", ".join(to), subject, msgPlain)
    
    #emsg = CreateMessage(sender,to,subject,msgPlain)
    #connection.send_message(emsg)
    connection.sendmail(sender, to ,message)
    connection.quit()
    print('Email sent to {}'.format(to))

if len(sys.argv)!=4:
    sys.exit("\nUsage: "+ os.path.basename(sys.argv[0]) + " <email1,email2,...> <wait time [min]> <total time [hrs]>\n\nIt will check for the change in the beam status every 'wait time' minutes for the 'total time' hours and send an email to all of the 'emailx' addresses.")
    
try:
    send_to_email = sys.argv[1]
    repeat_min = float(sys.argv[2])
    total_hrs = float(sys.argv[3])
except:
    print('Something wrong with command arguments')
    sys.exit("\nUsage: "+ os.path.basename(sys.argv[0]) + " <email1,email2,...> <wait time [min]> <total time [hrs]>\n\nIt will check for the change in the beam status every 'wait time' minutes for the 'total time' hours and send an email to all of the 'emailx' addresses.")


send_to_email = list(send_to_email.split(','))

sender = "bl42.ssrl@outlook.com"


fk = 'o0fXs2cndaOuwP5jOApaLl8VFbmGmsiwZqpZ8osLQps='
fernet = Fernet(fk)

epass = 'gAAAAABin-ZAljVxon4JH7tgjy8HPtPhmPUbk95aSgq-ixPt-cktw687N5I0QNDbo06rrx9TyYAB-Ejs1jOiqBHsa_ItpX5evg=='


start_time = time.time()
end_time = start_time + total_hrs*3600
beam_status = 'NA'
beam_current = 'NA' 
bl4_status_new = 'NA'
op_mes = ''


url1 = "https://www-ssrl.slac.stanford.edu/spear_status/getpvs.php?pvs=talk_display"
url2 = "https://www-ssrl.slac.stanford.edu/spear_status/getmsgs.php"

print('--------------------\n')


if socket.gethostname().split('.')[0] == 'bl42nxs1':
    print('This program will not run on bl42nxs1 (smtp port is firewalled)! \nPlease run in on bl42cpu.\n')
    print('--------------------\n')
    sys.exit()

while time.time() < end_time:
    
    
    
    r1 = requests.get(url1).json()
    r2 = requests.get(url2).json()
    
    
    try:
        for ll in r1['data']:
            if ll['name'] == "SPEAR:BeamCurrAvg":
                beam_current_new  = round(float(ll['value']),2)
            elif ll['name'] == "SPEAR:State":
                beam_status_new  = ll['value']
            elif ll['name'] == "BL04:OpenState":
                bl4_status_new = ll['value']
        
        
    except:
        print('no info')
        beam_status_new = 'NA'
        beam_current_new = 'NA'
       
    
    
    try:
        op_mes_new = '\n'.join(' - '.join([d['activationDate'],d['message']]) for d in r2['messages'])
        if op_mes_new =='':
            op_mes_new = 'No operator message'
    except:
        op_mes_new = 'NA'
            
            
    
    
    
   
    print('{0} - {1} - {2} mA \n \t {3}'.format(time.strftime('%Y%m%d %H:%M:%S'),beam_status_new,beam_current_new,op_mes_new))

    if (beam_status_new != 'NA'):
        om_len = min(len(op_mes),len(op_mes_new))
        if ((beam_status_new != beam_status) or  ((op_mes[-om_len:] != op_mes_new[-om_len:]) and op_mes_new != 'No operator message')):
            subject = "Beam: {0}, {1} mA".format(beam_status_new,beam_current_new)
            msg = "Beam status is {0}.\nBeam current is {1} mA.\n{2}\n--------\nchecking beam until {3:%Y-%m-%d %H:%M:%S} ({4:.1f} more hours)".format(beam_status_new,beam_current_new,op_mes_new,datetime.datetime.fromtimestamp(end_time),(end_time-time.time())/3600)
            
            
            SendMessage(sender, epass, fernet, send_to_email, subject, msg)
            
        beam_status = beam_status_new
        beam_current = beam_current_new
        op_mes=op_mes_new
        print('--------------------\n')
        print('checking beam until {:%Y-%m-%d %H:%M:%S} ({:.1f} more hours)'.format(datetime.datetime.fromtimestamp(end_time),((end_time-time.time())/3600)))
        print('--------------------\n')
        time.sleep(repeat_min*60)
        
    else:
        time.sleep(60)
    
