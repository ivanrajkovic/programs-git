#!/usr/bin/env python2
# -*- coding: utf-8 -*- 

import httplib2
import os
import oauth2client
from oauth2client import client, tools
import base64
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from apiclient import errors, discovery
#import mimetypes
#from email.mime.image import MIMEImage
#from email.mime.audio import MIMEAudio
#from email.mime.base import MIMEBase
import requests
import time
import datetime
import sys
import subprocess
#from bs4 import BeautifulSoup

def get_credentials():
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir, 'gmail-python-email-send.json')
    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        # or credentials.token_expiry < datetime.datetime.now():
        # or credentials.token_expiry < datetime.datetime.utcnow()
        #subprocess.call(['get_cred.py'])
        #credentials = get_credentials()
        flow = client.OAuth2WebServerFlow(client_id="579024895352-ms746a3qfj5422lfbg5vj47v1mjhmvc2.apps.googleusercontent.com", client_secret="EjpsR1yfvpefhB0uCPBbHqBJ", scope='https://www.googleapis.com/auth/gmail.send')
        flow.user_agent = 'check_beam'
        pargs = tools.argparser.parse_args('')
        credentials = tools.run_flow(flow, store,pargs)
        
    return credentials
    
def SendMessage(sender, to, subject, msgPlain):
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http, cache_discovery=False)
    message1 = CreateMessage(sender, to, subject, msgPlain)
    result = SendMessageInternal(service, "me", message1)
    print('Email sent to {}'.format(to.split(',')))
    return result

def SendMessageInternal(service, user_id, message):
    try:
        message = (service.users().messages().send(userId=user_id, body=message).execute())
        #print('Message Id: %s' % message['id'])
        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)
        return "Error"
    return "OK"

def CreateMessage(sender, to, subject, msgPlain):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = sender
    msg['Bcc'] = to
    msg.attach(MIMEText(msgPlain, 'html'))
    return {'raw': base64.urlsafe_b64encode(msg.as_string())}


if len(sys.argv)!=4:
    sys.exit("\nUsage: "+ os.path.basename(sys.argv[0]) + " <email1,email2,...> <wait time [min]> <total time [hrs]>\n\nIt will check for the change in the beam status every 'wait time' minutes for the 'total time' hours and send an email to all of the 'emailx' addresses.")
    
try:
    send_to_email = sys.argv[1]
    repeat_min = float(sys.argv[2])
    total_hrs = float(sys.argv[3])
except:
    print('Something wrong with command arguments')
    sys.exit("\nUsage: "+ os.path.basename(sys.argv[0]) + " <email1,email2,...> <wait time [min]> <total time [hrs]>\n\nIt will check for the change in the beam status every 'wait time' minutes for the 'total time' hours and send an email to all of the 'emailx' addresses.")


#SCOPES = 'https://www.googleapis.com/auth/gmail.send'
#CLIENT_SECRET_FILE = 'client_secret.json'
#APPLICATION_NAME = 'check_beam'

sender = "bl42.staff@gmail.com"

start_time = time.time()
end_time = start_time + total_hrs*3600
beam_status = 'NA'
beam_current = 'NA' 
bl4_status_new = 'NA'
op_mes = ''
#url ='https://www-ssrl.slac.stanford.edu/talk_display.html'

url1 = "https://www-ssrl.slac.stanford.edu/spear_status/getpvs.php?pvs=talk_display"
url2 = "https://www-ssrl.slac.stanford.edu/spear_status/getmsgs.php"

get_credentials()

print('--------------------\n')
while time.time() < end_time:
    #get.credentials()
    #r = requests.get(url)    
    #a = r.text.split('Beam Status: ')
    #b = a[0].split('Beam Current: ')
    
    
    r1 = requests.get(url1).json()
    r2 = requests.get(url2).json()
    
    
    try:
        # beam_status_new = str(a[1][:50]).split()[0]
        # 'Beams', 'Inject', 'Down', 'AccPhy'
        # beam_current_new = float(str(b[1][:50]).split()[0])
        # ttt=BeautifulSoup(r.text,'lxml').find_all('table')    
        # beam_info = ttt[0].find_all('td')
        # beam_current_new = float(beam_info[0].get_text().split()[2])
        # beam_status_new = beam_info[1].get_text().split()[2]
        # op_mes_new = ttt[1].get_text().strip()
        
        #ttt=BeautifulSoup(r.text,'lxml').find_all('div',class_='webmsg')
        #op_mes_new = ttt[0].find_all('table')[0].get_text().strip()
        
        #ttt=BeautifulSoup(r.text,'lxml').find_all('table',class_='plot_hdr')
        #beam_current_new = float(ttt[0].find_all('td',class_='left')[0].get_text().split()[2])
        #beam_status_new = ttt[0].find_all('td',class_='middle')[0].get_text().split()[2]
        
        for ll in r1['data']:
            if ll['name'] == "SPEAR:BeamCurrAvg":
                beam_current_new  = round(float(ll['value']),2)
            elif ll['name'] == "SPEAR:State":
                beam_status_new  = ll['value']
            elif ll['name'] == "BL04:OpenState":
                bl4_status_new = ll['value']
        
        
    except:
        print('no info')
        beam_status_new = 'NA'
        beam_current_new = 'NA'
        #op_mes_new = 'NA'
    
    
    try:
        #if len(r2['messages']) > 0:
        op_mes_new = '<br>'.join(' - '.join([d['activationDate'],d['message']]) for d in r2['messages'])
        if op_mes_new =='':
            op_mes_new = 'No operator message'
    except:
        op_mes_new = 'NA'
            
            
    
    
    
    # try:
    #     c=a[1].split('SPEAR Operator Messages')
    #     d=c[1].split('date\">')
    #     op_mes_new=d[1].split('<')[0]
    #     e=c[1].split('msg\">')
    #     f=e[1].split('</td')
    #     op_mes_new=str(op_mes_new)+':\t'+str(f[0])
    # except:
    #     try:
    #         c=a[1].split('SPEAR Operator Messages')
    #         d=c[1].split("<td>")
    #         e=d[1].split("</td>")
    #         op_mes_new=e[0]
    #     except:
    #         op_mes_new=''
    

    
    print('{0} - {1} - {2} mA \n \t {3}'.format(time.strftime('%Y%m%d %H:%M:%S'),beam_status_new,beam_current_new,op_mes_new.replace('<br>','\n')))

    if (beam_status_new != 'NA'):
        #if ((beam_status_new != beam_status) and not(beam_status_new == 'Injecting' and (beam_status == 'Beams'or beam_status == 'AccPhy'))) or (op_mes != op_mes_new):
        om_len = min(len(op_mes),len(op_mes_new))
        if ((beam_status_new != beam_status) or  ((op_mes[-om_len:] != op_mes_new[-om_len:]) and op_mes_new != 'No operator message')):
            subject = "Beam: {0}, {1} mA".format(beam_status_new,beam_current_new)
            msg = "Beam status is {0}.<br>Beam current is {1} mA.<br>{2}<br>--------<br>checking beam until {3:%Y-%m-%d %H:%M:%S} ({4:.1f} more hours)".format(beam_status_new,beam_current_new,op_mes_new,datetime.datetime.fromtimestamp(end_time),(end_time-time.time())/3600)
            
            
            SendMessage(sender, send_to_email, subject, msg)
            
        #if not(beam_status_new == 'Injecting' and (beam_status == 'Beams' or beam_status == 'AccPhy')):
        #    beam_status = beam_status_new
        
        beam_status = beam_status_new
        beam_current = beam_current_new
        op_mes=op_mes_new
        print('--------------------\n')
        print('checking beam until {:%Y-%m-%d %H:%M:%S} ({:.1f} more hours)'.format(datetime.datetime.fromtimestamp(end_time),((end_time-time.time())/3600)))
        print('--------------------\n')
        time.sleep(repeat_min*60)
        
    else:
        # if there is no info (website problem) try again after a minute
        time.sleep(60)
    
