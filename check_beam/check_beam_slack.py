#!/usr/bin/env python2

import os
import requests
from requests.structures import CaseInsensitiveDict
import time
import datetime
import sys
import getpass
import subprocess32


if len(sys.argv)!=3:
    sys.exit("\nUsage: "+ os.path.basename(sys.argv[0]) + "<wait time [min]> <total time [hrs]>\n\nIt will check for the change in the beam status every 'wait time' minutes for the 'total time' hours and send a slack message to ssrl-bl4-2 channel. If <total time> is zero it will work untill manually stopped (CTRL+C).")
    
try:
    repeat_min = float(sys.argv[1])
    total_hrs = float(sys.argv[2])
except:
    print('Something wrong with command arguments')
    sys.exit("\nUsage: "+ os.path.basename(sys.argv[0]) + "<wait time [min]> <total time [hrs]>\n\nIt will check for the change in the beam status every 'wait time' minutes for the 'total time' hours and send a slack message to ssrl-bl4-2 channel. If <total time> is zero it will work untill manually stopped (CTRL+C).")


# lock_file = '/tmp/check_beam_{}.lck'.format(getpass.getuser())
# 
# if os.path.isfile(lock_file):
#     if ((time.time() - os.path.getmtime(lock_file)) < 310):
#         sys.exit('check_beam already running!')
#     else:
#         os.utime(lock_file,None)
# else:
#     open(lock_file, 'a').close()
#     os.chmod(lock_file,0666)

# ps -U staff -o cmd | grep check_beam_slack.py

ps_text = subprocess32.check_output(['ps', '-U',getpass.getuser(),'-o','pid,cmd'])

pid_list = []
for ps_line in ps_text.split('\n'):
    if 'check_beam_slack.py' in ps_line:
        pid_list.append(int(ps_line.split()[0]))

for pp in pid_list:
    if pp != os.getpid():
        print('already running on pid {}'.format(pp))
        sys.exit(0)

webhook = []

# ssrl sbl4-2
webhook.append('https://hooks.slack.com/services/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')

# rajkovic
#webhook.append('https://hooks.slack.com/services/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')

headers = CaseInsensitiveDict()
headers["Content-Type"] = "application/json"

start_time = time.time()
end_time = start_time + total_hrs*3600
beam_status = 'NA'
beam_current = 'NA' 
bl4_status = 'NA'
op_mes = ''


url1 = "https://www-ssrl.slac.stanford.edu/spear_status/getpvs.php?pvs=talk_display"
url2 = "https://www-ssrl.slac.stanford.edu/spear_status/getmsgs.php"

print('--------------------\n')
while (time.time() < end_time or total_hrs == 0):
    
    
    
    #r1 = requests.get(url1).json()
    #r2 = requests.get(url2).json()
    
    beam_status_new = 'NA'
    beam_current_new = 'NA'
    bl4_status_new = 'NA'
    op_mes_new = 'NA'
    
    
    try:
        r1 = requests.get(url1).json()
        for ll in r1['data']:
            if ll['name'] == "SPEAR:BeamCurrAvg":
                beam_current_new  = round(float(ll['value']),2)
            elif ll['name'] == "SPEAR:State":
                beam_status_new  = ll['value']
            elif ll['name'] == "BL04:OpenState":
                bl4_status_new = ll['value']
        
        
    except:
        print('No SPEAR info.')
        
    
    
    try:
        r2 = requests.get(url2).json()
        op_mes_new = '\n'.join(' - '.join([d['activationDate'],d['message']]) for d in r2['messages'])
        if op_mes_new =='':
            op_mes_new = 'No operator message'
    except:
        print('No message info.')
            
            
    
    
    
   
    print('{0} - {1} - {2} mA \n\tBL4-2 is {3}\n\t{4}'.format(time.strftime('%Y%m%d %H:%M:%S'),beam_status_new,beam_current_new,bl4_status_new,op_mes_new))

    if ('NA' not in [beam_status_new,beam_current_new,bl4_status_new,op_mes_new]):
        om_len = min(len(op_mes),len(op_mes_new))
        if ((beam_status_new != beam_status) or  ((op_mes[-om_len:] != op_mes_new[-om_len:]) and op_mes_new != 'No operator message') or (bl4_status_new != bl4_status)):
            
            if total_hrs != 0:
               ttt_end = "--------\nchecking beam until {0:%Y-%m-%d %H:%M:%S} ({1:.1f} more hours).".format(datetime.datetime.fromtimestamp(end_time),(end_time-time.time())/3600)
            else:
                ttt_end=""
            
            msg = "Beam status is {0}.\nBeam current is {1} mA.\nBL4-2 is {2}\n{3}\n{4} ".format(beam_status_new,beam_current_new,bl4_status_new,op_mes_new,ttt_end)
            
            msg=msg.replace('\r','')
            msg=msg.replace('<br>','')
            
            
            
            data_send = '{"text":"'+msg+'"}'
            
            for ww in webhook:
                resp = requests.post(ww, headers=headers, data=data_send)
            
                print('\n Response code: {}\n'.format(resp.status_code))
            
            
        beam_status = beam_status_new
        beam_current = beam_current_new
        bl4_status=bl4_status_new
        op_mes=op_mes_new
        print('--------------------\n')
        if total_hrs !=0:
            print('checking beam until {:%Y-%m-%d %H:%M:%S} ({:.1f} more hours)'.format(datetime.datetime.fromtimestamp(end_time),((end_time-time.time())/3600)))
            print('--------------------\n')
        #else:
        #    print('checking beam until manually stopped.')
        #print('--------------------\n')
        time.sleep(repeat_min*60)
        
    else:
        time.sleep(60)
    
