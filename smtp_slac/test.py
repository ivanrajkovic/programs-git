import smtplib

import getpass


from email.message import EmailMessage

username = input("Enter username:")
password = getpass.getpass(prompt='Password: ', stream=None)
# sometimes not the same, sat a separate variable
mail_from = username
mail_to = "rajkovic@stanford.edu"
mail_subject = "Test Subject"
mail_body = "This is a test message"


msg = EmailMessage()
msg.set_content(mail_body)
msg['Subject'] = mail_subject
msg['From'] = mail_from
msg['To'] = mail_to

#connection = smtplib.SMTP(host='smtp.gmail.com', port=587)
connection = smtplib.SMTP(host='smtp.office365.com', port=587)
connection.starttls()
connection.login(username,password)
connection.send_message(msg)
connection.quit()




