#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division
import sys
import fileinput
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
            Absolute scaling factor for SAXS data"""
    )

    parser.add_argument('water_int', type=float, help='current water intensity')
    parser.add_argument('integmpp', type=str, nargs='?', default='integ.mpp', help='integ.mpp file, default=integ.mpp')
    parser.add_argument('--use_absolute','-ua', action='store_true', default=False, help='Use absolute scale, default=False, just add comment to the integ.mpp file')

    arg_in = parser.parse_args()

    abs_factor = 0.01632/arg_in.water_int

    for line in fileinput.input(arg_in.integmpp, inplace = 1):
        if line.startswith('-i'):
            scale = float(line.split()[-1])
            new_scale = abs_factor*scale
            if arg_in.use_absolute:
                if new_scale > 100:
                    line = "#"+ line+" ".join(line.split()[:-1]) + " {:d}\n".format(int(new_scale))
                else:
                    line = "#"+line+" ".join(line.split()[:-1]) +' {:f}\n'.format(new_scale)

            else:
                if new_scale > 100:
                    line = line+"#"+" ".join(line.split()[:-1]) + " {:d}\n".format(int(new_scale))
                else:
                    line = line+"#"+" ".join(line.split()[:-1]) +' {:f}\n'.format(new_scale)
                line = line + "# multiply data by {:f} for absolute scale\n".format(abs_factor)
        sys.stdout.write(line)