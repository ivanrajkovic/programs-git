#!/usr/bin/env python3

'''
@author: rajkovic
'''

import sys
import glob
import numpy
import os

from scipy import integrate
from tkinter import Tk
from tkinter.filedialog import askdirectory

root = Tk()
root.withdraw()
pp=os.path.normpath(askdirectory())

pp.split(os.path.sep)[-1]

files = glob.glob(os.path.join(pp,'*model.txt'))


#outname = '{}-invariant.txt'.format(os.path.split(files[0])[:])

#file\Level1G\Level1GError\Level1Rg\Level1RgError\Level1B\Level1BError\Level1P\Level1PError\Level2G\Level2GError\Level2Rg\Level2RgError\Level2B\Level2BError\Level2P\Level2PError\Level2Invariant

outfol = os.path.normpath(os.path.join(pp,'..','igor_invariant'))

if not os.path.exists(outfol):
    os.makedirs(outfol)

outname = os.path.join(outfol,pp.split(os.path.sep)[-1]+'.txt')



with open(outname,'w+') as outfile:
    outfile.write('#file\tLevel1G\tLevel1GError\tLevel1Rg\tLevel1RgError\tLevel1B\tLevel1BError\tLevel1P\tLevel1PError\tLevel2G\tLevel2GError\tLevel2Rg\tLevel2RgError\tLevel2B\tLevel2BError\tLevel2P\tLevel2PError\tLevel2Invariant\n')
    for ff in files:
        l1g = 0
        l1ge = 0
        l1rg = 0
        l1rge = 0
        l1b = 0
        l1be = 0
        l1p = 0
        l1pe = 0
        l2g = 0
        l2ge = 0
        l2rg = 0
        l2rge = 0
        l2b = 0
        l2be = 0
        l2p = 0
        l2pe = 0
        l2inv = 0
        fn = ff.split('_')[-3]
        with open(ff, "r") as mf:
            for line in mf:
                if 'Level1G=' in line:
                    l1g = float(line.split('=')[-1])
                elif 'Level1GError' in line:
                    l1ge = float(line.split('=')[-1])
                elif 'Level1Rg=' in line:
                    l1rg = float(line.split('=')[-1])
                elif 'Level1RgError' in line:
                    l1rge = float(line.split('=')[-1])
                elif 'Level1B=' in line:
                    l1b = float(line.split('=')[-1])
                elif 'Level1BError' in line:
                    l1be = float(line.split('=')[-1])
                elif 'Level1P=' in line:
                    l1p = float(line.split('=')[-1])
                elif 'Level1PError' in line:
                    l1pe = float(line.split('=')[-1])
                elif 'Level2G=' in line:
                    l2g =  float(line.split('=')[-1])
                elif 'Level2GError' in line:
                    l2ge = float(line.split('=')[-1])
                elif 'Level2Rg=' in line:
                    l2rg = float(line.split('=')[-1])
                elif 'Level2RgError' in line:
                    l2rge = float(line.split('=')[-1])
                elif 'Level2B=' in line:
                    l2b = float(line.split('=')[-1])
                elif 'Level2BError' in line:
                    l2be = float(line.split('=')[-1])
                elif 'Level2P=' in line:
                    l2p = float(line.split('=')[-1])
                elif 'Level2PError' in line:
                    l2pe = float(line.split('=')[-1])
                elif 'Level2Invariant' in line:
                    l2inv = float(line.split('=')[-1])

        outfile.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(fn, l1g, l1ge, l1rg, l1rge, l1b, l1be, l1p, l1pe, l2g, l2ge, l2rg, l2rge, l2b, l2be, l2p, l2pe, l2inv))