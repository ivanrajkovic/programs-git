#!/usr/bin/env python3

'''
@author: rajkovic
'''

import sys
import glob
import numpy
import os

from scipy import integrate
from tkinter import Tk
from tkinter.filedialog import askdirectory

from distutils.dir_util import mkpath




if __name__ == "__main__":
    # get the main data folder:
    root = Tk()
    root.withdraw()
    mdf=os.path.normpath(askdirectory())

    #save folder
    sf = os.path.join(mdf,'inv_out')
    mkpath(sf)

    # go through data subfolders:
    for x in os.listdir(mdf):
        # only if it is a folder not named 'inv_out'
        if os.path.isdir(os.path.join(mdf,x)) and x != 'inv_out':
            print('Working on {}'.format(x))
            # get serie(s) number(s)
            sntxt = x.split('_')[-1][1:]
            if '-' in sntxt:
                # if multiple series in a folder
                snf,sne = [int(x) for x in sntxt.split('-')]
            else:
                snf=int(sntxt)
                sne = snf

            # current file number for folders with multiple series
            current_file=0

            # output file
            with open(os.path.join(sf,x+'.txt'),'w+') as outfile:
                outfile.write('# current file\tI*dq\t\t\tq^2*I*dq\tseries\timage\n')

                # go through each serie in a folder
                for sn in range(snf,sne+1):

                    # go through all dat files in a given serie
                    for ff in sorted(glob.glob(os.path.join(mdf,x,'{}*_S{}_*.dat'.format(x.split('_')[0],str(sn).zfill(3))))):
                        # get info from the file name
                        ss = ff.split('_')[-3][1:]
                        nn = ff.split('_')[-1].split('.')[0]
                        # dont use data that have '-' in their image number
                        if '-' not in nn:
                            # increase current file counter
                            current_file+=1
                            # load data
                            dd = numpy.loadtxt(ff)
                            #  i*dq
                            q1 = integrate.simpson(dd[:,1],dd[:,0])
                            # q^2*i*dq
                            q2 = integrate.simpson(dd[:,0]**2*dd[:,1],dd[:,0])
                            # write to file
                            outfile.write('{}\t{}\t{}\t{}\t{}\n'.format(current_file,q1,q2,ss,nn))







