#!/usr/bin/env python3

'''
@author: rajkovic
'''

import sys
import glob
import numpy
import os

from scipy import integrate
from tkinter import Tk
from tkinter.filedialog import askdirectory

root = Tk()
root.withdraw()
pp=os.path.normpath(askdirectory())

files = glob.glob(os.path.join(pp,'*.dat'))

#cc = 1/(2*(numpy.pi**2))

a = 1
#print('\n First data row to integrate:')
# a= int(input())


print('\n Last data row to integrate:')
b = int(input())

outname = '{}-invariant.txt'.format(os.path.split(pp)[-1])

with open(os.path.join(pp,outname),'w+') as outfile:
    outfile.write('# series\timage\tI*dq\t\t\tq^2*I*dq\n')
    for ff in files:
        ss = ff.split('_')[-3][1:]
        nn = ff.split('_')[-1].split('.')[0]
        dd = numpy.loadtxt(ff)
        q1 = integrate.simpson(dd[a-1:b,1],dd[a-1:b,0])
        q2 = integrate.simpson(dd[a-1:b,0]**2*dd[a-1:b,1],dd[a-1:b,0])
        outfile.write('{}\t\t{}\t{}\t{}\n'.format(ss,nn,q1,q2))





