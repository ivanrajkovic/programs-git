#!/usr/bin/env python

'''
@author: rajkovic
'''

import serial
import serial.tools.list_ports


def find_port():
    pl = []
    sl = sorted(serial.tools.list_ports.comports())
    for port, desc, hwid in sl:
        if hwid.startswith('USB'):
            pl.append(port)

    if len(pl) > 1:
        for spl in pl:
            try:
                with serial.Serial(spl, 9600, timeout=2) as ser:
                    ser.write(b'*IDN?\n')
                    srs_info = ser.readline().decode()
                    if srs_info.split(',')[1] == 'SR630':
                        sp = spl
                        break
                    else:
                        print('{} on port {} is not the SRS device we are looking for'.format(srs_info.split(',')[1],spl))
            except serial.SerialException:
                print('try some other port, {} is not the one'.format(spl))


    else:
        sp = pl[0]

    return(sp)



if __name__ == "__main__":
    ser_port = find_port()
    print('found SR630 on port {}'.format(ser_port))