#!/usr/bin/env python

'''
@author: rajkovic
'''

import serial
import serial.tools.list_ports

from http.server import BaseHTTPRequestHandler, HTTPServer
import time

import threading

hostName = "bl42serv"
serverPort = 8080

srs_tcm = 'SR630'

#com_port = 'COM5'

rrr = 0

# try:
#     ser = serial.Serial(...)
# except SerialException:
#     print 'port already open'


# try:
#     #ser=serial.Serial(port='COM5', baudrate=9600 ,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_TWO,timeout=2)
#     ser=serial.Serial('COM5', 9600, timeout=.2)
# except serial.SerialException:
#     print('try some other port')
# #ser.write(b'*IDN?\n')
# ser.write(b'MEAS? 2\n')
# pump_return = ser.readline()
# print(pump_return.decode())
# ser.close()

def find_port():
    pl = []
    sl = sorted(serial.tools.list_ports.comports())
    for port, desc, hwid in sl:
        if hwid.startswith('USB'):
            pl.append(port)

    if len(pl) > 1:
        for spl in pl:
            try:
                with serial.Serial(spl, 9600, timeout=2) as ser:
                    ser.write(b'*IDN?\n')
                    srs_info = ser.readline().decode()
                    if srs_info.split(',')[1] == srs_tcm:
                        print('found {} on port {}'.format(srs_tcm,spl))
                        sp = spl
                        break
                    else:
                        print('{} on port {} is not the SRS device we are looking for'.format(srs_info.split(',')[1],spl))
            except serial.SerialException:
                print('try some other port, {} is not the one'.format(spl))


    else:
        sp = pl[0]

    return(sp)


def get_temp(com_port):
    global temp_1,temp_2,upd_time
    try:
        upd_time = time.ctime()
        with serial.Serial(com_port, 9600, timeout=2) as ser:
            ser.write(b'MEAS? 2\n')
            pump_return = ser.readline()
            temp_2 = pump_return.decode().strip()
            ser.write(b'MEAS? 1\n')
            pump_return = ser.readline()
            temp_1 = pump_return.decode().strip()
            print('t1: {}\nt2: {}\n{}\n'.format(temp_1,temp_2,upd_time))

    except serial.SerialException:
        return('try some other port')


def do_loop(stth,com_port):
    while True:
        #thisSecond = int(time.time())
        #if  thisSecond % 10 == 0:
        #if time.localtime().tm_sec % 10 == 0:
        if int(time.time()) % 10 == 0:
            get_temp(com_port)
            time.sleep(1)
        if stth():
            break



class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        global temp_1,temp_2,upd_time
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes("<!DOCTYPE html>\n", "utf-8"))
        self.wfile.write(bytes("<html>\n\t<head>\n\t\t<title>bl42serv</title>\n", "utf-8"))
        self.wfile.write(bytes("\t\t<meta http-equiv=\"refresh\" content=\"60\">\n\t</head>\n", "utf-8"))
        self.wfile.write(bytes("\t<body>\n", "utf-8"))
        self.wfile.write(bytes('\t\ttemp 1 (new shutter): {}&#8451; <br>\n'.format(temp_1),'utf-8'))
        self.wfile.write(bytes('\t\ttemp 2 (old shutter): {}&#8451; <br>\n'.format(temp_2),'utf-8'))
        self.wfile.write(bytes('\t\t<br>\n\t\tLast updated on {}<br>\n'.format(upd_time),'utf-8'))
        self.wfile.write(bytes("\t</body>\n</html>\n", "utf-8"))

if __name__ == "__main__":
    com_port=find_port()
    get_temp(com_port)
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    # update temperatures is a separate thread
    stop_threads = False
    d = threading.Thread(target=do_loop, name='temperature', args =(lambda : stop_threads,com_port, ))
    d.setDaemon(True)
    d.start()


    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    # stop daemon
    stop_threads = True
    d.join()
    webServer.server_close()
    print("Server stopped.")