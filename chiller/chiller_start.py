#!/usr/bin/env python

import serial 
import sys
import time
import binascii

binary = lambda x: " ".join(reversed( [i+j for i,j in zip( *[ ["{0:04b}".format(int(c,16)) for c in reversed("0"+x)][n::2] for n in [1,0] ] ) ] ))

# open usb connection
try:
    bath = serial.Serial('/dev/ttyUSB0', baudrate=19200, timeout=1)
    #print(bath.name)
    print('')
except:
    print('No connection!')
    sys.exit()
    
bath.write(binascii.unhexlify('CA00018101017B'))

time.sleep(.1)
response = binascii.hexlify(bath.readline())


bath.write(binascii.unhexlify('CA00010900F5'))
time.sleep(.1)
response = binascii.hexlify(bath.readline())

iinfo = binary(response[-6:-4])
print('----------------------------\n')

if iinfo[4] == '0':
    print('Unit off\n')
else:
    print('Unit on\n')
    
if iinfo[5] == '0':
    print('Pump off\n')
else:
    print('Pump on\n')

if iinfo[6] == '0':
    print('Compressor off\n')
else:
    print('Compressor on\n')

print('----------------------------\n')

    
bath.close()
    