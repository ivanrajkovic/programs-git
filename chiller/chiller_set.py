#!/usr/bin/env python

import serial
import sys
import time
import binascii

if len(sys.argv) < 2 or len(sys.argv) > 3:
    sys.exit("\nUsage: "+ sys.argv[0] + " [temp. in C]\nEx: \'"+sys.argv[0]+ " 15.7\'")

set_temp = int(float(sys.argv[1])*10)

if set_temp <40 or set_temp> 800:
    sys.exit('temperarure out of range [5 C,80 C]')

set_temp_hex = '{:04x}'.format(set_temp)

#open usb connection
try:
    bath = serial.Serial('/dev/ttyUSB0', baudrate=19200, timeout=1)
    #print(bath.name)
    print('')
except:
    print('No connection!')
    sys.exit()

#calculate checksum:
#cs = hex(255 ^ 0x00+0x01+0xf0+0x02+int(set_temp_hex[:2],16)+int(set_temp_hex[2:],16))
cs = '{:02x}'.format(255 ^ 0x00+0x01+0xf0+0x02+int(set_temp_hex[:2],16)+int(set_temp_hex[2:],16))[-2:]


# if len(cs) > 3:
#     cs = cs[-2:]
# else:
#     cs = '0'+cs[-1]

# prepare string to send:
chiller_command = 'CA0001F002'+set_temp_hex+cs


bath.write(binascii.unhexlify(chiller_command))

time.sleep(.1)
response = binascii.hexlify(bath.readline())
print('----------------------------\n')
print('Temperature setpoint 1: {} C\n'.format(int(response[-6:-2],16)/10.))
print('----------------------------\n')
bath.close()