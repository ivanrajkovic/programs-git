#!/usr/bin/env python

import serial
import sys
import time
import binascii

debug = 0
if len(sys.argv) > 1:
    debug = 1
    from textwrap import wrap

binary = lambda x: " ".join(reversed( [i+j for i,j in zip( *[ ["{0:04b}".format(int(c,16)) for c in reversed("0"+x)][n::2] for n in [1,0] ] ) ] ))

# open usb connection
try:
    bath = serial.Serial('/dev/ttyUSB0', baudrate=19200, timeout=1)
    #print(bath.name)
except:
    print('No connection!')
    sys.exit()

bath.write(binascii.unhexlify('CA00010900F5'))
time.sleep(.1)
response = binascii.hexlify(bath.readline())
iinfo = binary(response[-6:-4])
if iinfo == '':
    print('No response from the chiller!')
    sys.exit()

if debug == 1:
    print('----------------------------\n')
    print(wrap(response,2))
    print('')
    print(iinfo)
    print('')

print('----------------------------\n')

if iinfo[4] == '0':
    print('Unit off\n')
else:
    print('Unit on\n')

if iinfo[5] == '0':
    print('Pump off\n')
else:
    print('Pump on\n')

if iinfo[6] == '0':
    print('Compressor off\n')
else:
    print('Compressor on\n')

print('----------------------------\n')
#read setpoint one
bath.write(binascii.unhexlify('CA000170008E'))
time.sleep(.1)
response = binascii.hexlify(bath.readline())

print('Temperature setpoint 1: {} C\n'.format(int(response[-6:-2],16)/10.))

print('----------------------------\n')
#read internal temp
bath.write(binascii.unhexlify('CA00012000DE'))
time.sleep(.1)
response = binascii.hexlify(bath.readline())

print('Chiller temperature: {} C\n'.format(int(response[-6:-2],16)/10.))

print('----------------------------\n')

bath.close()

