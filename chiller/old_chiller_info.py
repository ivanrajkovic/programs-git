#!/usr/bin/env python

import serial 
import sys
import time
import binascii

# open usb connection
try:
    bath = serial.Serial('/dev/ttyUSB0', baudrate=19200, timeout=1)
    #print(bath.name)
    print('')
except:
    print('No connection!')
    sys.exit()

# check if the chiller is running
bath.write(binascii.unhexlify('CA00018101027A'))
time.sleep(.1)
response = binascii.hexlify(bath.readline())
print(response)

if response[-4:-2] == '00':
    print('Chiller is not running\n')
elif response[-4:-2] == '01':
    print('Chiller is running\n')
else:
    print('Response error!\n')


#read setpoint one 
bath.write(binascii.unhexlify('CA000170008E'))
time.sleep(.1)
response = binascii.hexlify(bath.readline())

print('Temperature setpoint 1: {} C\n'.format(int(response[-6:-2],16)/10.))


#read internal temp
bath.write(binascii.unhexlify('CA00012000DE'))
time.sleep(.1)
response = binascii.hexlify(bath.readline())

print('Chiller temperature: {} C\n'.format(int(response[-6:-2],16)/10.))

bath.close()
    