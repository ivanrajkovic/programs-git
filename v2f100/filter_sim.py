#!/usr/bin/env python

'''
@author: rajkovic
'''
from __future__ import division

import numpy
import matplotlib
#matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
#import pandas as pd
#from collections import OrderedDict

#pd.set_option('display.width', 1000)
#pd.set_option('display.max_columns', 50)



def time_to_text(tt):
    if tt<1000:
        tx = str(tt)+'us'
    else:
        #tx = str(int(tt/1000))+'ms'
        tx='{:.3g}ms'.format(tt/1000)
    return(tx)

def running_mean(x, N):
    cumsum = numpy.cumsum(numpy.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N])/N

# simulated photodiode signal: intmean intensity with st_dev[%] standard deviation, 10000 time points with 2us sampling-> 20ms
intmean=100.0
st_dev = [.5,1,5,10]
pt_num = 10000
timing = [50,100,500,1000,5000] # number of points to integrate, 2us spacing

# how many times to repeat the simulation
repeat = 10000

# prepare the variable to hold the results
results  = numpy.zeros([len(timing),len(st_dev),repeat])


for j,tt in enumerate(timing):
    for i,sd in enumerate(st_dev):
        print(tt,sd)
        for rr in numpy.arange(repeat):

            # create raw data with 'st_dev' standard deviation


            data = numpy.random.normal(intmean,sd*intmean/100,pt_num)

            # filtered data is a running mean over 51 raw data points (51 points -> 50x2us intervals = 100us window)
            filt_data = running_mean(data,51)


            # calculate the relative difference (in %)
            results[j,i,rr] = 100 * (1 - (numpy.mean(filt_data[:tt])/numpy.mean(data[50:50+tt])))
            #results[j,i,rr] = 100 * (numpy.mean(data[50:50+tt]) - numpy.mean(filt_data[:tt]))/intmean


kk=[]
# put everything in one figure
fig,ax = plt.subplots(nrows = len(timing),ncols=len(st_dev),sharex=True, sharey=True,figsize=[12,13])
fig.text(0.5,0.05, '(mean - filtered mean)/mean [%] ({} repeats)'.format(repeat) ,ha='center',size=22)

for j,tt in enumerate(timing):
    for i,sd in enumerate(st_dev):
        print(tt,sd)
        ax = plt.subplot(len(timing),len(st_dev),1+j*len(st_dev)+i)
        # make the histogram
        plt.hist(results[j,i],bins=25)
        plt.title(r'$I_{sd}$'+'={}%, time={}'.format(sd,time_to_text(tt*2)))
        # calcualate the standard deviation of the results
        plt.text(0.01,0.9,'$\sigma$={:.2g}%'.format(numpy.std(results[j,i])),transform=ax.transAxes)
        kk.append(numpy.std(results[j,i])*(tt*2/1000)/sd)
        #plt.text(0.65,0.9,'k={:.2g}'.format(numpy.std(results[j,i])*(tt*2/1000)/sd),transform=ax.transAxes)
        # don't put too many ticks on the graphs
        ax.xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(3))
        ax.yaxis.set_major_locator(matplotlib.ticker.MaxNLocator(3))

fig.text(0.5,0.005, r'$\sigma$[%] = {:.3g}'.format(numpy.mean(kk))+r' * $I_{sd}$[%] / t[ms]',ha='center',size=22)
plt.tight_layout()
matplotlib.pyplot.subplots_adjust(bottom=.1)
plt.show()



# create raw data with .5%, 1% and 2% standard deviation
# data corresponds to 40ms (20k points with 2us sampling)

#data = OrderedDict()
#filt_data = OrderedDict()

#data['.5%'] = numpy.random.normal(100,.5,20000)

#data['1%'] = numpy.random.normal(100,1,20000)

#data['2%'] = numpy.random.normal(100,2,20000)

#data['lin'] = numpy.linspace(95,105,20000)

# filtered data is a running mean over 100 raw data points


#for kk in data.keys():
#    filt_data[kk+'_f'] = running_mean(data[kk],100)

#timing = numpy.array([100,200,500,1000,2000,5000,10000])

#timing_text = [[time_to_text(x),time_to_text(x)+'_f','Delta'+time_to_text(x)] for x in timing[:]]

#timing_text = [i for s in timing_text for i in s]

#results = pd.DataFrame(index=data.keys(),columns = timing_text)


# for nn in timing:
#     for kk in data.keys():
#         results.loc[kk][time_to_text(nn)] = numpy.mean(data[kk][100:100+nn])
#         results.loc[kk][time_to_text(nn)+'_f'] = numpy.mean(filt_data[kk+'_f'][:nn])
#         results.loc[kk]['Delta'+time_to_text(nn)] = (results.loc[kk][time_to_text(nn)] - results.loc[kk][time_to_text(nn)+'_f'])

#results.columns.get_loc('100us_f')
#results.inser(loc,column,calue)

#print(results.iloc[:,2::3])