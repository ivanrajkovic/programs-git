#!/usr/bin/env python

'''
@author: rajkovic
'''



import os
import numpy
import glob
import pandas

import matplotlib.pyplot as plt


folder = '/mnt/home/staff/BioSAXSGroup/People/Ivan/v2f100/data/ASCII'
fig_fol = '/mnt/home/rajkovic/bitbucket/programs/v2f100/plots/'



aa = numpy.loadtxt(os.path.join(folder,'100us_no_gate.txt'),skiprows=3)
no_gate_stat = [numpy.mean(aa[:,0]+aa[:,1]*(2**16)),numpy.std(aa[:,0]+aa[:,1]*(2**16)),numpy.mean(aa[:,2]+aa[:,3]*(2**16)),numpy.std(aa[:,2]+aa[:,3]*(2**16))]
ch_diff = no_gate_stat[0] - no_gate_stat[2]

plt.figure()
plt.title('100us, 5V, both channels not gated')
plt.plot(aa[:,0],'b',label='non-gated')
plt.plot(aa[:,2],'r',label='normaly gated')
plt.legend()
plt.text(100,489,'$\Delta$Cmean={:.2f} ct'.format(ch_diff))
#plt.show()
plt.savefig(os.path.join(fig_fol,'non-gated.png'))
###################################






pulse_duration_list = glob.glob(os.path.join(folder,'*s.txt'))
results=numpy.zeros([len(pulse_duration_list),4])

names = []

for ff in range(len(pulse_duration_list)):
    aa = numpy.loadtxt(pulse_duration_list[ff],skiprows=3)
    #results[ff]=[numpy.mean(aa[:,0]),numpy.std(aa[:,0]),numpy.mean(aa[:,2]),numpy.std(aa[:,2])]
    results[ff]=[numpy.mean(aa[:,0]+aa[:,1]*(2**16)),numpy.std(aa[:,0]+aa[:,1]*(2**16)),numpy.mean(aa[:,2]+aa[:,3]*(2**16)),numpy.std(aa[:,2]+aa[:,3]*(2**16))]
    names.append(os.path.splitext(os.path.basename(pulse_duration_list[ff]))[0])



pd=pandas.DataFrame(index=names,data=results,columns=['C0_mean','C0_stdev','C1_mean','C2_stdev'])

pd = pd.assign(C_diff= (pd['C0_mean'] - pd['C1_mean']))




# get exposure time from the names:

ttime = []

for nn in names:
    tt = nn.split('_')[0]
    suf = tt.lstrip('0123456789')
    num = tt[:-len(suf)]
    if suf[0]=='u':
        ttime.append(int(num))
    elif suf[0]=='m':
        ttime.append(int(num)*1000)
    elif suf[0]=='s':
        ttime.append(int(num)*1000000)
    else:
        print('Error!')

pd = pd.assign(exp_time=ttime)


pd = pd.sort_values(by=['exp_time'],ascending=True)

fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.set_title('5V signal, v2f max = 10MHz')
ax1.semilogx(pd['exp_time'],pd['C_diff'],'bo-')
ax1.set_xlabel(r'Exp time [$\mu s$]')
ax1.set_ylabel(r'$\Delta $C [ct]',color='b')
ax2=ax1.twinx()
ax2.semilogx(pd['exp_time'],100*pd['C_diff']/pd['C0_mean'],'ro-')
ax2.set_ylabel(r'$\Delta $C/C [%]',color='r')
#plt.show()
plt.savefig(os.path.join(fig_fol,'5V_10MHz.png'))

plt.figure(figsize=(12,20))

for ff in range(len(pulse_duration_list)):
    aa = numpy.loadtxt(os.path.join(folder,pd.index[ff]+'.txt'),skiprows=3)
    ct = aa[:,0]+aa[:,1]*(2**16)
    ct2= aa[:,2]+aa[:,3]*(2**16)
    plt.subplot(6,3,ff+1)
    plt.title(pd.index[ff])
    plt.plot(ct/ct.max())
    plt.plot(ct2/ct.max())
    plt.subplots_adjust(hspace=0.3)

#plt.show()
plt.savefig(os.path.join(fig_fol,'all_traces_duration.png'))

# plt.figure()
# plt.title('5V signal, v2f max = 10MHz')
# plt.semilogx(pd['exp_time'],100*pd['C_diff']/pd['C0_mean'],'o-')
# plt.xlabel('Exp time [$\mu s$]')
# plt.ylabel(r'$\Delta $C/C [%]')
# plt.show()


################################################

pulse_level_list = glob.glob(os.path.join(folder,'*V.txt'))
results_level=numpy.zeros([len(pulse_level_list),4])

names_level = []

for ff in range(len(pulse_level_list)):
    aa = numpy.loadtxt(pulse_level_list[ff],skiprows=3)
    results_level[ff]=[numpy.mean(aa[:,0]+aa[:,1]*(2**16)),numpy.std(aa[:,0]+aa[:,1]*(2**16)),numpy.mean(aa[:,2]+aa[:,3]*(2**16)),numpy.std(aa[:,2]+aa[:,3]*(2**16))]
    names_level.append(os.path.splitext(os.path.basename(pulse_level_list[ff]))[0])

pl=pandas.DataFrame(index=names_level,data=results_level,columns=['C0_mean','C0_stdev','C1_mean','C2_stdev'])

pl = pl.assign(C_diff= (pl['C0_mean'] - pl['C1_mean']))

llevel = []
for nn in names_level:
    tt = nn.split('_')[0]
    suf = tt.lstrip('0123456789')
    num = tt[:-len(suf)]
    llevel.append(int(num))

pl = pl.assign(pulse_level=llevel)

pl = pl.sort_values(by=['pulse_level'],ascending=True)

fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.set_title('1ms signal, v2f max = 10MHz')
ax1.plot(pl['pulse_level'],pl['C_diff'],'bo-')
ax1.set_xlabel(r'Pulse level [V]')
ax1.set_ylabel(r'$\Delta $C [ct]',color='b')
ax2=ax1.twinx()
ax2.plot(pl['pulse_level'],100*pl['C_diff']/pl['C0_mean'],'ro-')
ax2.set_ylabel(r'$\Delta $C/C [%]',color='r')
#plt.show()
plt.savefig(os.path.join(fig_fol,'1ms_10MHz.png'))

# plt.figure()
# plt.title('5V signal, v2f max = 10MHz')
# plt.plot(pl['pulse_level'],100*pl['C_diff']/pl['C0_mean'],'o-')
# plt.xlabel('Exp time [$\mu s$]')
# plt.ylabel(r'$\Delta $C/C [%]')
# plt.show()

plt.figure(figsize=(15,20))

for ff in range(len(pulse_level_list)):
    aa = numpy.loadtxt(os.path.join(folder,pl.index[ff]+'.txt'),skiprows=3)
    ct = aa[:,0]+aa[:,1]*(2**16)
    ct2= aa[:,2]+aa[:,3]*(2**16)
    plt.subplot(4,3,ff+1)
    plt.title(pl.index[ff])
    plt.plot(ct/ct.max())
    plt.plot(ct2/ct.max())
    plt.subplots_adjust(hspace=0.3)

#plt.show()
plt.savefig(os.path.join(fig_fol,'all_traces_level.png'))



######################################################

freq_list = glob.glob(os.path.join(folder,'*z.txt'))
results_freq=numpy.zeros([len(freq_list),4])

names_freq = []

for ff in range(len(freq_list)):
    aa = numpy.loadtxt(freq_list[ff],skiprows=3)
    results_freq[ff]=[numpy.mean(aa[:,0]+aa[:,1]*(2**16)),numpy.std(aa[:,0]+aa[:,1]*(2**16)),numpy.mean(aa[:,2]+aa[:,3]*(2**16)),numpy.std(aa[:,2]+aa[:,3]*(2**16))]
    names_freq.append(os.path.splitext(os.path.basename(freq_list[ff]))[0])

v2f=pandas.DataFrame(index=names_freq,data=results_freq,columns=['C0_mean','C0_stdev','C1_mean','C2_stdev'])

v2f = v2f.assign(C_diff= (v2f['C0_mean'] - v2f['C1_mean']))

ffreq = []
for nn in names_freq:
    tt = nn.split('_')[0]
    suf = tt.lstrip('0123456789')
    num = tt[:-len(suf)]
    ffreq.append(int(num))

v2f = v2f.assign(v2f_freq=ffreq)

v2f = v2f.sort_values(by=['v2f_freq'],ascending=True)


fig = plt.figure()
ax1 = fig.add_subplot(111)
ax2=ax1.twinx()
ax1.set_title('2V, 1ms signal')
ax1.plot(v2f['v2f_freq'],v2f['C_diff'],'bo-')
ax2.plot(v2f['v2f_freq'],100*v2f['C_diff']/v2f['C0_mean'],'ro-')
ax1.set_xlabel(r'v2f100 max freq [MHz]')
ax1.set_ylabel(r'$\Delta $C [ct]',color='b')
ax2.set_ylabel(r'$\Delta $C/C [%]',color='r')
#plt.show()
plt.savefig(os.path.join(fig_fol,'2V_1ms.png'))

plt.figure()
# plt.title('5V signal, v2f max = 10MHz')
# plt.plot(v2f['v2f_freq'],100*v2f['C_diff']/v2f['C0_mean'],'o-')
# plt.xlabel('v2f100 max freq [MHz]')
# plt.ylabel(r'$\Delta $C/C [%]')
# plt.show()



##################################################


