# for autosampler, file name includes well-plate position part
def name_parts_AS(img_name):
    all_parts = img_name.split('_')
    
    ext_str = all_parts[-1].split('.')[1]
    num_str = all_parts[-1].split('.')[0]
    xx_str = all_parts[-2]
    ser_str = all_parts[-3]
    pos_str = all_parts[-4]
    nam_str = '_'.join(all_parts[0:-4])
 
    return [nam_str, pos_str, ser_str, xx_str, num_str, ext_str]

# for other file names, without the position part
def name_parts(img_name):
    all_parts = img_name.split('_')
    
    ext_str = all_parts[-1].split('.')[1]
    num_str = all_parts[-1].split('.')[0]
    xx_str = all_parts[-2]
    ser_str = all_parts[-3]
    nam_str = '_'.join(all_parts[0:-3])
 
    return [nam_str, ser_str, xx_str, num_str, ext_str]