import sys
import math
from scipy.constants import codata
#from scipy.constants import c, e, Planck

# pixel size [m] (no binning)
#ray_px = .225/6144 
# For pilatus:
ray_px = .000172  

# e_const = 12.398419300923944 (planck [eV*s] * c[m/s] = [eV*m] = 10^7 [keV*A])
#e_const = codata.value('Planck constant in eV s')*codata.value('speed of light in vacuum')*1e7
#e_const = codata.value('Planck constant over 2 pi times c in MeV fm')*2*math.pi*1e-2
#e_const = c*codata.value('hertz-electron volt relationship')*1e7
#e_const = (Planck/e) * c * 1e7
e_const=1e7/codata.value('electron volt-inverse meter relationship')


# using pint
# from pint import UnitRegistry
# ureg = UnitRegistry()
# hh = codata.value('Planck constant')* ureg.parse_expression(codata.unit('Planck constant'))
# cc = codata.value('speed of light in vacuum')* ureg.parse_expression(codata.unit('speed of light in vacuum'))
# ee = (hh*cc).to(ureg.keV * ureg.angstrom)
# ee.ito(ureg.keV * ureg.angstrom)
# e_const = ee.magnitude


# convert keV to A (and vice-versa):
def e_keV_A(e_1):
    return (e_const / e_1)
    
def e_keV_A_help():
    print('\nConverts keV to A (and vice-versa).\n')
    print('Usage: e_keV_A(energy)')


# calculate x-ray scattering radius at the detector for a given detector distance, x-ray energy[keV] and scattering vector q[A^-1]
# radius is in the same unit as given distance
def radius(distance,energy,q):
    radius = distance*math.tan(2*math.asin(e_keV_A(energy)*q/(4*math.pi)))
    return radius

def radius_help():
    print('\nCalculates x-ray scattering radius at the detector for a given detector distance, x-ray energy[keV]\nand scattering vector q[A^-1]. Radius is in the same unit as given distance.\n')
    print('Usage: radius(distance,energy[keV],q[A^-1])')

# calculate x-ray scattering radius [px] at the detector for a given detector distance[m], x-ray energy[keV], scattering vector q[A^-1] and detector binning

def radius_px(distance,energy,q,dbin):
    radius_px = radius(distance,energy,q)/(ray_px*dbin)
    return radius_px
    
def radius_px_help():
    print('\nCalculates x-ray scattering radius [px] at the detector for a given detector distance[m],\nx-ray energy[keV], scattering vector q[A^-1] and detector binning[n]\n')
    print('Usage: radius_px(distance[m],energy[keV],q[A^-1],binning[n])\n')

# calculate x-ray scattering vector[A^-1] for a given detector distance, x-ray energy[keV] and scattering radius at the detector
# radius and distance are in same units
def qq(distance,energy,rrad):
    qq = (4*math.pi/e_keV_A(energy))*math.sin(math.atan(rrad/distance)/2)
    return qq

def qq_help():
    print('\nCalculates x-ray scattering vector[A^-1] for a given detector distance, x-ray energy[keV]\nand scattering radius at the detector.\nRadius and distance are in same units\n')
    print('Usage: qq(distance,energy[keV],radius)\n')


# calculate x-ray scattering vector[A^-1] for a given detector distance[m], x-ray energy[keV], pixel distance from the beam center at the detector and detector binning.
def qq_px(distance,energy,px_n,dbin):
    qq_px = (4*math.pi/e_keV_A(energy))*math.sin(math.atan(px_n*ray_px*dbin/distance)/2)
    return qq_px

def qq_px_help():
    print('\nCalculates x-ray scattering vector[A^-1] for a given detector distance[m], x-ray energy[keV],\npixel distance from the beam center at the detector[n] and detector binning[n].\n')
    print('Usage: qq_px(distance[m],energy[keV],pixel number[n],binning[n])\n')
    
# calculate detector distance [m] from x-ray energy [keV] and AgBe scattering[ring order, ring position, detector binning]
def distance(energy,ring_n,px_n,dbin):
    distance = px_n*dbin*ray_px/math.tan(2*ring_n*math.asin(e_keV_A(energy)/(2*58.38)))
    return distance

def distance_help():
    print('\nCalculates detector distance [m] from x-ray energy[keV] and AgBe scattering[ring order, ring position, detector binning]\n')
    print('Usage: distance(energy[keV],ring order, pixel number, binning)\n')