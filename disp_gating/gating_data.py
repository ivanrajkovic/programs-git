#!/usr/bin/env python


'''
@author: rajkovic
'''

# works with python3

import glob
import numpy
import os

import matplotlib.pyplot as plt

# list of different parameters
desc = []
# list of arrays, one array per parameter set
int_data_list = []

# intensity per second (approximate)

#run with itsybitsy
#ips = 5222222

# run with caen
ips = 5030000

# load data
for intfile in sorted(glob.glob('./ints_caen/*0s1.int')):
    pp = os.path.split(intfile)[-1].split('_')

    f_desc = "_".join(pp[0:4])

    #time = float(pp[0][:-1].replace('p','.'))
    #osc_vol = int(pp[1][3:])
    #osc_rat = int(pp[2])
    nf = numpy.loadtxt(intfile)
    # use only second half of the data and add one dimension to the array, for easier appending later
    nf_2 = numpy.expand_dims(nf[int(nf.size/2):],axis=0)

    # initialize lists with first int file
    if len(desc) == 0:
        desc = [f_desc]
        #int_data = numpy.array([[nf_2]])
        #int_data = numpy.expand_dims(numpy.expand_dims(nf_2,0),0)
        int_data_list = [nf_2]
    else:
        # see if f_desc already exists is desc; if yes, append data array to the exisiting array
        if f_desc in desc:
            pos = desc.index(f_desc)
            int_data_list[pos] = numpy.append(int_data_list[pos],nf_2,axis=0)
        # if not, append new parameter set and a new data array
        else:
            desc.append(f_desc)
            int_data_list.append(nf_2)


for ii in range(len(desc)):
    desc_part = desc[ii].split('_')
    acq_t = float(desc_part[0][:-1].replace('p','.'))
    acq_tt = desc_part[0]
    #osc_vol = int(desc_part[1][3:])
    osc_vol = int(desc_part[2])
    osc_rat = int(desc_part[3])

    #int_max = int_data_list[ii].max()
    int_max = max(ips*acq_t,int_data_list[ii].max())
    sample_int = int_data_list[ii].sum(axis=1)
    im_num = int_data_list[ii].shape[1]
    id_mean = int_data_list[ii].mean()
    #print('acquisition time was {} s, oscillation volume was {} ul and flow was {} ul/s'.format(acq_t,osc_vol,osc_rat))

    plt.figure()
    for tt in range(int_data_list[ii].shape[0]):
        plt.plot(numpy.arange(16)+1,int_data_list[ii][tt,:])
    plt.plot(numpy.arange(16)+1,acq_t*ips*numpy.ones(16),'g--')
    plt.title("{} s,{} ul".format(acq_t,osc_vol))
    plt.savefig("plots_caen/{}_{}ul_intplot.png".format(acq_tt,osc_vol))
    plt.close()

    plt.hist(int_data_list[ii].flatten(),bins=25)
    plt.title("{} s,{} ul".format(acq_t,osc_vol))
    plt.text(.05,.95,"mean: {:.2g}, {:.0f}%".format(id_mean,100*id_mean/(int_max)),transform = plt.gca().transAxes)
    plt.savefig("plots_caen/{}_{}ul_hist_single.png".format(acq_tt,osc_vol))
    plt.close()

    plt.figure()
    plt.plot(int_data_list[ii].flatten())
    plt.title("{} s,{} ul".format(acq_t,osc_vol))
    plt.text(.05,.95,"mean: {:.2g}, {:.0f}%".format(id_mean,100*id_mean/(int_max)),transform = plt.gca().transAxes)
    plt.savefig("plots_caen/{}_{}ul_all_single.png".format(acq_tt,osc_vol))
    plt.close()

    plt.figure()
    plt.plot(sample_int)
    plt.title("{} s,{} ul".format(acq_t,osc_vol))
    plt.text(.05,.95,"mean: {:.2g}, {:.0f}%".format(sample_int.mean(),100*sample_int.mean()/(im_num*int_max)),transform = plt.gca().transAxes)
    plt.savefig("plots_caen/{}_{}ul_all_sample.png".format(acq_tt,osc_vol))
    plt.close()


    plt.hist(sample_int,bins=25)
    plt.title("{} s,{} ul".format(acq_t,osc_vol))
    plt.text(.05,.95,"mean: {:.2g}, {:.0f}%".format(sample_int.mean(),100*sample_int.mean()/(im_num*int_max)),transform = plt.gca().transAxes)
    plt.savefig("plots_caen/{}_{}ul_hist_sample.png".format(acq_tt,osc_vol))
    plt.close()













