#!/usr/bin/env python

'''
@author: rajkovic
'''

import socket
import sys
import time

## BNC outputs:
bncT0 = 0
bncAB = 1
bncCD = 2
bncEF = 3
bncGH = 4


## channels

chT0 = 0
chT1 = 1

chA = 2
chB = 3

chC = 4
chD = 5

chE = 6
chF = 7

chG = 8
chH = 9
#-------------------------------------------------------------------------

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect(("192.168.42.63",5025))
s.settimeout(10)


s.send("DLAY 3,2,1\n".encode())
time.sleep(.1)

s.send("DLAY?3\n".encode())
result=s.recv(1024)
print(result)

s.close()
