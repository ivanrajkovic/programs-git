##
## info at https://lima1.readthedocs.io/en/latest/
##         https://lima1.readthedocs.io/en/latest/camera/eiger/doc/index.html
##

## pyzo help: F9 executes currently active line ; SHIFT+F9 execute and advance line

from Lima import Eiger
from Lima import Core

import os

cam = Eiger.Camera('bl42eiger.bl42')

hwint = Eiger.Interface(cam)
ct = Core.CtControl(hwint)

acq = ct.acquisition()

# set hardware configuration
# refer to the Dectris Eiger documentation for more information
cam.setCountrateCorrection(True)
cam.setFlatfieldCorrection(True)
cam.setAutoSummation(True)
#cam.setEfficiencyCorrection(True)
cam.setVirtualPixelCorrection(True)
cam.setPixelMask(True)

# read some parameters
print (cam.getTemperature())
print (cam.getHumidity())


# set energy threshold in eV
cam.setThresholdEnergy(5500)
cam.setPhotonEnergy(11000)

# setting new file parameters and autosaving mode
saving=ct.saving()

pars=saving.getParameters()
pars.directory='/mnt/home/staff/Commissioning2020/20200923_Eiger/data_TR/'
pars.prefix='eiger_Si_500x1ms_'
pars.suffix='.tif'
pars.nextNumber=1
pars.fileFormat=Core.CtSaving.TIFFFormat
#pars.suffix='.cbf'
#pars.fileFormat=Core.CtSaving.CBFFormat
pars.savingMode=Core.CtSaving.AutoFrame
# Save parameters again after changing any of them!!
saving.setParameters(pars)

# set accumulation mode

acq_pars= acq.getPars()


# read-out time in seconds
acq.serLatencyTime(0.001)

# now ask for X sec exposure and Y frames
acq.setAcqExpoTime(.1)
acq.setAcqNbFrames(500)

#external gate
acq.setTriggerMode(4)
# internal trigger - default
# acq.setTriggerMode(0)


ct.prepareAcq()
ct.startAcq()

# wait for last image (#9) ready
# lastimg = ct.getStatus().ImageCounters.LastImageReady
# while lastimg !=9:
#   time.sleep(1)
#   lastimg = ct.getStatus().ImageCounters.LastImageReady

# read the first image
#im0 = ct.ReadImage(0)