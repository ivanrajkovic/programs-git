#!/mnt/home/staff/SW/mambaforge/envs/py3-eiger/bin/python3
'''
@author: rajkovic
'''

from Lima import Eiger
from Lima import Core

import os
import sys
import argparse
import time


#
# Lima reported trigger modes:
#IntTrig   = ints  = setTriggerMode(0)
#IntTrigMult = inte
#ExtTrigSingle = exts
#ExtTrigMult = ??
#ExtGate = exte = setTriggerMode(4)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
    Trigger mode:
       ints: internal series - one software trigger for whole series
       inte: internal enable - one software trigger for each frame
       exts: external series - one external trigger for whole series
       extm: external multiple - one external trigger for each frame (not official Eiger mode, extra feature)
       exte: external enable (gate) - one external trigger per frame, frame duration equal to trigger duration
    """)
    parser.add_argument('filename', help=' full path to the file')
    parser.add_argument('-et','--exposure_time', type=float, default=1 , help='exposure time in seconds, default is 1s')
    parser.add_argument('-rt','--readout_time', type=float, default=0.001, help='Readout time in seconds, default is 0.001s')
    parser.add_argument('-nof','--number_of_frames', type=int, default=1 , help='number of frames, default is 1 frame')
    parser.add_argument('-tm','--trigger_mode', default='ints', choices=['ints', 'inte', 'exts', 'extm', 'exte'], help='trigger mode  (ints, inte, exts, extm, exte), default is ints')
    parser.add_argument('-xe','--xray_energy', default=11, type=float, help='X-ray energy in keV (for setting thresholds at 50%% and 140%%), default is 11')
    parser.add_argument('-te','--threshold_energy', type=float, help='First threshold energy in keV, default is 50%% of X-ray energy')


    arg_in = parser.parse_args()
    ts = time.time()
    #print(arg_in)

    #if arg_in.threshold_energy:
    #    print('{}'.format(arg_in.threshold_energy))

    data_folder, first_frame = os.path.split(arg_in.filename)

    if data_folder == "":
        data_folder="."

    first_frame_base, frame_ext = os.path.splitext(first_frame)
    frame_base = '_'.join(first_frame_base.split('_')[:-1])+'_'

    cam = Eiger.Camera('bl42eiger.bl42')

    hwint = Eiger.Interface(cam)
    ct = Core.CtControl(hwint)
    acq = ct.acquisition()

    #print('initialized {}s'.format(time.time()-ts))

    # set hardware configuration
    #cam.setCountrateCorrection(True)
    #cam.setFlatfieldCorrection(True)
    #cam.setAutoSummation(True)
    #cam.setVirtualPixelCorrection(True)
    #cam.setPixelMask(True)

    # set energy and threshold in eV
    #cam.setPhotonEnergy(arg_in.xray_energy*1000)
    # explicitly set first threshold if specified (if not automatically set to xray_energy/2)
    #if arg_in.threshold_energy:
    #    cam.setThresholdEnergy(arg_in.threshold_energy*1000)

    #print('pre pars saved {}s'.format(time.time()-ts))
    # setting new file parameters and autosaving mode
    saving=ct.saving()

    pars=saving.getParameters()
    pars.directory=data_folder
    pars.prefix=frame_base
    pars.suffix=frame_ext
    pars.nextNumber=1
    if frame_ext in ['.tif','.tiff']:
        pars.fileFormat=Core.CtSaving.TIFFFormat
    if frame_ext == '.cbf':
        pars.fileFormat=Core.CtSaving.CBFFormat
    pars.savingMode=Core.CtSaving.AutoFrame
    # Save parameters again after changing any of them!!
    saving.setParameters(pars)

    #print('pars saved {}s'.format(time.time()-ts))
    # setting acquisition parameters

    #acq_pars= acq.getPars()

    # read-out time in seconds
    acq.setLatencyTime(arg_in.readout_time)

    # now ask for X sec exposure and Y frames
    acq.setAcqExpoTime(arg_in.exposure_time)
    acq.setAcqNbFrames(arg_in.number_of_frames)

    # set trigger mode (Lima gives one extra mode (extm)
    tr_mode = ['ints','inte','exts','extm','exte'].index(arg_in.trigger_mode)
    acq.setTriggerMode(tr_mode)

    # prepare/start acquisition
    ct.prepareAcq()
    ct.startAcq()

    sys.stdout.write('\nReady {}s\n'.format(time.time()-ts))

    # wait for last image to be ready
    lastimg = ct.getStatus().ImageCounters.LastImageReady
    while lastimg != (arg_in.number_of_frames-1):
        time.sleep(1)
        lastimg = ct.getStatus().ImageCounters.LastImageReady
        if arg_in.exposure_time > .5:
            print('Last image number {}\n'.format(lastimg+1))

    sys.stdout.write('\nDone\n')









