#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author: rajkovic
based on
https://pyimagesearch.com/2020/10/05/object-detection-bounding-box-regression-with-keras-tensorflow-and-deep-learning/

'''
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.models import load_model
import numpy as np
import cv2
import os

## config parameters
#BASE_PATH = '/home/ivan/bitbucket/programs-git/ml_find_center/'
BASE_PATH = '/mnt/home/rajkovic/bitbucket/programs-git/ml_find_center'


BASE_OUTPUT = os.path.join(BASE_PATH,'output')

MODEL_PATH = os.path.join(BASE_OUTPUT, "detector.h5")
TEST_FILENAMES = os.path.join(BASE_OUTPUT, "test_images.txt")




## end of config


## image to test

targ_s = 224*4

imagePath = os.path.join(BASE_PATH,'testing_data','04.tif')

image = load_img(imagePath, target_size=(targ_s, targ_s))
image = img_to_array(image) / 255.0
image = np.expand_dims(image, axis=0)

print("[INFO] loading object detector...")
model = load_model(MODEL_PATH)

preds = model.predict(image)[0]
(startX, startY, endX, endY) = preds



image = cv2.imread(imagePath)
(h, w) = image.shape[:2]

startX = int(startX * w)
startY = int(startY * h)
endX = int(endX * w)
endY = int(endY * h)

print(startX,endX,startY,endY)


cv2.rectangle(image, (startX, startY), (endX, endY),(255, 255, 255), 10)
# show the output image
cv2.imshow("Output", image)
cv2.waitKey(0)