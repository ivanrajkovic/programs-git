#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author: rajkovic
'''


import numpy
from PIL import Image
#import matplotlib.pyplot as plt

import os

def create_empty(x,y):
    e = numpy.zeros([x,y])
    return(e)

def add_2dgauss(a,x,y,i,wd):

    q = numpy.arange(0,a.shape[1])
    w = numpy.arange(0,a.shape[0])
    w=w[:,numpy.newaxis]

    g = a + i*numpy.exp(-((q-y)**2 + (w-x)**2) / wd**2)

    return(g)



def cut_center(a,x,y,r):
    q = numpy.arange(0,a.shape[1])
    w = numpy.arange(0,a.shape[0])
    w=w[:,numpy.newaxis]

    mask = (q-y)**2 + (w-x)**2 < r**2

    a[mask] = 0

    return(a)

def add_bkg(a,j):
    s = a + j*numpy.random.rand(a.shape[0],a.shape[1])
    return(s)


def add_noise(a,n):
    m = 1+ n*numpy.random.rand(a.shape[0],a.shape[1])
    s = a*m

    return(s)



if __name__ == "__main__":

    save_fol = 'testing_data'
    #save_fol = 'training_data'
    num_im = 10
    num_l = len(str(num_im))
    size_x = 981
    size_y = 1043

    p = create_empty(size_x,size_y)






    for z in range(1,num_im+1):

        xx = numpy.random.randint(size_x)
        yy = numpy.random.randint(size_y)

        ii  = 100*(1+numpy.random.randint(1))

        ww = 20 + numpy.random.randint(10)

        o = add_bkg(p,5)

        o = add_2dgauss(o,xx,yy,ii,ww)

        #o = cut_center(o,xx,yy,5)

        o = add_noise(o,0.1)

        im_name = os.path.join(save_fol,f'{z:0{num_l}d}.tif')

        Image.fromarray(o.astype('uint8'),mode='L').save(im_name)

        with open(os.path.join(save_fol,'cc.txt'),"a") as cc:
            cc.write(f'{os.path.split(im_name)[1]}\t{xx}\t{yy}\n')










