#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author: rajkovic
based on
https://pyimagesearch.com/2020/10/05/object-detection-bounding-box-regression-with-keras-tensorflow-and-deep-learning/

'''

import os

from tensorflow.keras.applications import VGG16
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import cv2
from PIL import Image


## config parameters
#BASE_PATH = '/home/ivan/bitbucket/programs-git/ml_find_center/'
BASE_PATH = '/mnt/home/rajkovic/bitbucket/programs-git/ml_find_center'

IMAGES_PATH = os.path.join(BASE_PATH, "training_data")
ANNOTS_PATH = os.path.join(IMAGES_PATH, "cc.txt")

BASE_OUTPUT = os.path.join(BASE_PATH,'output')

MODEL_PATH = os.path.join(BASE_OUTPUT, "detector.h5")
PLOT_PATH = os.path.join(BASE_OUTPUT, "plot.png")
TEST_FILENAMES = os.path.join(BASE_OUTPUT, "test_images.txt")


INIT_LR = 1e-4
NUM_EPOCHS = 25
BATCH_SIZE = 4

targ_s = 224*4

## end of config

rows = open(ANNOTS_PATH).read().strip().split("\n")

data = []
targets = []
filenames = []

for row in rows:
    (filename, YY, XX) = row.split('\t')

    imagePath = os.path.join(IMAGES_PATH, filename)
    image = cv2.imread(imagePath,cv2.IMREAD_GRAYSCALE)
    #image = Image.open(imagePath)
    (h, w) = image.shape


    # scale the center coordinates relative to the spatial
    # dimensions of the input image
    XXA = (float(XX)-20) / w
    YYA = (float(YY)-20) / h
    XXB = (float(XX)+20) / w
    YYB = (float(YY)+20) / h

    # print(row)
    # cv2.rectangle(image, (int(XXA*w), int(YYA*h)), (int(XXB*w), int(YYB*h)),(255, 255, 255), 10)
    # # show the output image
    # cv2.imshow("Output", image)
    # cv2.waitKey(0)


    image = load_img(imagePath, target_size=(targ_s, targ_s))
    image = img_to_array(image)
    data.append(image/255.0)
    targets.append((XXA,YYA,XXB,YYB))
    filenames.append(filename)

data = np.array(data, dtype="float32")
targets = np.array(targets, dtype="float32")

# partition the data into training and testing splits using 90% of
# the data for training and the remaining 10% for testing
split = train_test_split(data, targets, filenames, test_size=0.10, random_state=42)

# unpack the data split
(trainImages, testImages) = split[:2]
(trainTargets, testTargets) = split[2:4]
(trainFilenames, testFilenames) = split[4:]


print("[INFO] saving testing filenames...")
f = open(TEST_FILENAMES, "w")
f.write("\n".join(testFilenames))
f.close()


# load the VGG16 network, ensuring the head FC layers are left off
vgg = VGG16(weights="imagenet", include_top=False, input_tensor=Input(shape=(targ_s, targ_s, 3)))
# freeze all VGG layers so they will *not* be updated during the
# training process
vgg.trainable = False
# flatten the max-pooling output of VGG
flatten = vgg.output
flatten = Flatten()(flatten)

# construct a fully-connected layer header to output the predicted
# bounding box coordinates
ccc = Dense(128, activation="relu")(flatten)
ccc = Dense(64, activation="relu")(ccc)
ccc = Dense(32, activation="relu")(ccc)
ccc = Dense(4, activation="sigmoid")(ccc)
# construct the model we will fine-tune for bounding box regression
model = Model(inputs=vgg.input, outputs=ccc)



# initialize the optimizer, compile the model, and show the model
# summary
opt = Adam(lr=INIT_LR)
model.compile(loss="mse", optimizer=opt)
print(model.summary())
# train the network for bounding box regression
print("[INFO] training bounding box regressor...")
H = model.fit(trainImages, trainTargets,validation_data=(testImages, testTargets),batch_size=BATCH_SIZE,epochs=NUM_EPOCHS,verbose=1)

# serialize the model to disk
print("[INFO] saving object detector model...")
model.save(MODEL_PATH, save_format="h5")
# plot the model training history
N = NUM_EPOCHS
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.title("CCC Regression Loss on Training Set")
plt.xlabel("Epoch #")
plt.ylabel("Loss")
plt.legend(loc="lower left")
plt.savefig(PLOT_PATH)





