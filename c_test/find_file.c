#include <stdlib.h>
#include <stdio.h>
#include <pcre.h>
#include <string.h>
#include <dirent.h>


int main(int argc, char *argv[]) {

	if (argc<2) return 1; 	
	
	char *find_reg = malloc(30);
	//char *ptr;
	pcre *reCompiled;
	pcre_extra *pcreExtra;
	int pcreExecRet;
	const char *pcreErrorStr;
	int pcreErrorOffset;
	int i = 0;
	char *s_name;
	


	// linked list
	struct list {
    char *string;
    struct list *next;
	};

	typedef struct list LIST;
	
	
	
	//sprintf(find_reg,"^.*[SBT]%03d.*_0+1.tif$",strtol(argv[1],&ptr,10));
	sprintf(find_reg,"^.*[SBT]%03ld.*_0+1.tif$",strtoul(argv[1],NULL,10));
	printf("\nSearching for: %s \n\n",find_reg);
	
	reCompiled = pcre_compile(find_reg, 0, &pcreErrorStr, &pcreErrorOffset, NULL);
	pcreExtra = pcre_study(reCompiled, 0, &pcreErrorStr);
	free(find_reg);
	
    struct dirent *entry;
    DIR *dir = opendir("/mnt/home/rajkovic/bitbucket/programs/autosas/test/data");
    //DIR *dir = opendir(".");
    if (dir == NULL) {
        return 1;
    }

	LIST *current, *head;
    head = current = NULL;

    while ((entry = readdir(dir)) != NULL) {

		pcreExecRet = pcre_exec(reCompiled, pcreExtra, entry->d_name, strlen(entry->d_name), 0, 0, NULL, 0);
		if (pcreExecRet == 0) {
			// number of files counter
			i++;
	
			LIST *node = malloc(sizeof(LIST));
			// either malloc+strcpy or strdup:			
			//node->string = malloc(strlen(entry->d_name)+1);
			//strcpy(node->string,entry->d_name);			
			node->string = strdup(entry->d_name);
			node->next =NULL;
			if(head == NULL){
    			current = head = node;
			} else {
		    	current = current->next = node;
    		}
		}
    }

    closedir(dir);
	free(reCompiled);
	free(pcreExtra);
	
	
	// exit if more than one file found
	
	if (i>1) {
		printf("more than one file found! \n");
		for(current = head; current ; current=current->next){
    	   printf("%s \n", current->string);
   		 }
		printf("number of files: %d \n\n",i);
		return 1;
	} else {
			s_name = strdup(head->string);
	}

	printf("using sample : %s\n\n",s_name);
	
	

	// (print found files and) delete list/free memory at the same time	
	while ((current = head) != NULL) {	
	head = head->next;
	//printf("%s \n", current->string);
	free(current->string);
	free(current); 
	}
	head =NULL;
	free(s_name);
	printf("number of files: %d \n",i);
	return 0;

}
