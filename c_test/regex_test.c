#include <stdlib.h>
#include <stdio.h>
#include <regex.h>

int main(int argc, char *argv[]) {

	regex_t regex;
    int reti;
    char msgbuf[100];
	

	// \d is not working in POSIX, use [0-9] or [[:digit:]]
	reti = regcomp(&regex, "^.*S[0-9]{3}.*$", REG_EXTENDED);

	if( reti )
            { 
                fprintf(stderr, "Could not compile regex\n");
                exit(1);
            }

	reti = regexec(&regex, argv[1], 0, NULL, 0);

	if( !reti ){

                printf("YEP \n");
               }
     else if( reti == REG_NOMATCH ){

                printf("NOPE \n");                    

            }
     else{
                regerror(reti, &regex, msgbuf, sizeof(msgbuf));
                fprintf(stderr, "Regex match failed: %s\n", msgbuf);
            }

	regfree(&regex);
}

