#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "gnuplot_i.h"

#define NPOINTS     50

int main(void) {
    gnuplot_ctrl    *   h1 ;

    double              x[NPOINTS] ;
    double              y[NPOINTS] ;
	double              z[NPOINTS] ;
	
    int                 i ;

	h1 = gnuplot_init() ;

	
	
	//gnuplot_setstyle(h1, "lines");

	//gnuplot_plot_slope(h1, 3.0, 1.0, "unity slope");

	// setting png here will only get the first plot in the file
	gnuplot_cmd(h1,"set terminal unknown");
	for (i=0 ; i<NPOINTS ; i++) {
		    x[i] = (double)i ;
       	 	y[i] = (double)i  - sqrt(100*i) ;
			z[i] = (double)i - 10;
			
		}
	gnuplot_setstyle(h1, "points") ;
	gnuplot_cmd(h1,"set yrange [-30:-5]");    
	gnuplot_cmd(h1,"set xrange [-25:70]");	
	gnuplot_plot_xy(h1, x, y, NPOINTS, "user-defined points") ;
	gnuplot_plot_xy(h1, z, y, NPOINTS, "user-defined points 2") ;

	// set output to png to get all the plots with replot
	gnuplot_cmd(h1,"set terminal png size 800,600");
	gnuplot_cmd(h1,"set output \"test.png\" ");
	gnuplot_cmd(h1,"set yrange [-30:15]");
	gnuplot_cmd(h1,"set xrange [-25:70]");
	gnuplot_cmd(h1,"replot");

    gnuplot_close(h1);

}

