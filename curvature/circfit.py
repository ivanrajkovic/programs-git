import os
from pylab import *
import numpy
# import locale
#from scipy.optimize import curve_fit
import matplotlib as mpl
rc('font', size=22)
import matplotlib.pyplot as plt
import tkinter as tk
import tkinter.simpledialog as tksd
import tkinter.filedialog as tkfd
from scipy.optimize import curve_fit

#  load the existing fit
root = tk.Tk()
root.withdraw()
fitfile = tkfd.askopenfilename(title="Choose a saved fit:", filetypes=[('npz', '.npz'), ('all files', '.*')])
fit_load = numpy.load(fitfile)
fit = fit_load['fit']
x_m = fit_load['x_m']
y_cor = fit_load['y_cor']


def fitcirc(x, xc, yc, R, n):
           return yc+sqrt(R**2-n*(x-xc)**2)
            
p0 = [-7900,100,-7000,1.08];

popt, pcov = curve_fit(fitcirc,x_m,y_cor,p0, maxfev=100000)

ff=zeros(3)
ff[0]=-7500
ff[1]=100
ff[2]=-10000
ff[3]=5
xfun=arange(60,800)
yfun=(ff[0]+sqrt(ff[2]**2-ff[3]*(xfun-ff[1])**2))

plot(xfun,yfun)
plot(x_m,y_cor)
show()