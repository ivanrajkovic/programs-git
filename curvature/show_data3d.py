import os
from pylab import *
import numpy
# import locale
#from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import tkinter as tk
import tkinter.simpledialog as tksd
import tkinter.filedialog as tkfd
from mpl_toolkits.mplot3d import axes3d

folder='d:\\data\\azobenzene - 2012 jan\\treated\\try1\\';
filename_t='tab_map_24_25m.dat';
filename_c='cab_map_24_25.dat';
os.chdir(folder);

tmap = numpy.genfromtxt(filename_t, filling_values = '0', invalid_raise = False);
cmap = numpy.genfromtxt(filename_c, filling_values = '0', invalid_raise = False);

chmin = 500;
chmax = 800;

v = arange(0,9,0.1);
u = arange(-3,5,0.5);

xxx= arange(chmin,chmax);
yyy=tmap[0,0:tmap.shape[1]-1];
plt.figure('tab')
contourf(xxx,yyy,tmap[chmin:chmax,0:tmap.shape[1]-1].T,v)

plt.figure('cab')
contourf(xxx,yyy,cmap[chmin:chmax,0:cmap.shape[1]-1].T,v)

plt.figure('tab - cab')
c = plt.contourf(xxx,yyy,(tmap[chmin:chmax,0:tmap.shape[1]-1]-cmap[chmin:chmax,0:cmap.shape[1]-1]).T,u)
b = plt.colorbar(c, orientation='vertical')

lx = plt.xlabel("x")
ly = plt.ylabel("y")
#ax = plt.axis([600,750,3972,3985])

plt.show()

