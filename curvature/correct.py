# correct raw spectra
# from pylab import *
#import os
from pylab import *
import numpy
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import read_raw_spectra

filename = 'water_540ev_1.txt'
folder = 'd:\\data\\BESSY_2012_jan\\12-01-21\\'

(x, y) = read_raw_spectra.read_spectra(folder,  filename)

# image resolution:
x_max = round(max(x))
y_max = round(max(y))

# how many slices?
slice_num = 20

Int=zeros((slice_num, x_max))

y_step = y_max/slice_num
y_cor=numpy.linspace(y_step/2, y_max-y_step/2, num=slice_num)


# how many +- y_rows to integrate?
#rows_int = 50
y_ind=[len(y)]*(slice_num+1)




# sort by y:
temp = list(zip(y, x))
temp.sort()

(y,  x) = zip(*temp)

# find slices borders:
for k in range(0, slice_num):
    y_ind[k] = next(i for i,v in enumerate(y) if v > y_step*k)

# integrate in y-direction in each slice:
for k in range(0, slice_num):
#    print(k, y_ind[k], y_ind[k+1]);
    for o in range(y_ind[k], y_ind[k+1]):
        Int[k][round(x[o])-1]=Int[k][round(x[o])-1]+1;


# find points to fit in some x range:
x_m=zeros(slice_num)
x_md=zeros(slice_num)
x_m_min=600
x_m_max=800

# just finding maximum, change to gaussian or similar!!!
#for k in range(0, slice_num):
#    x_m[k]=Int[k].argmax()

# fitting gaussian, use this or maximum, not both!!
def dgauss(x, A,  B,  mu,  sigma, C,  nu,  delta):
       return A+B*numpy.exp(-(x-mu)**2/(2*sigma**2))+C*numpy.exp(-(x-nu)**2/(2*delta**2))

def gauss(x, A,  B,  mu,  sigma):
       return A+B*numpy.exp(-(x-mu)**2/(2*sigma**2))

for k in range(0, slice_num):
    p0 = [10,  100,  x_m_min+Int[k][x_m_min:x_m_max].argmax(),  10]
    popt, pcov = curve_fit(gauss, arange(x_m_min, x_m_max), Int[k][x_m_min:x_m_max], p0)
    x_m[k]=popt[2]

    #or double gauss:
#    p0 = [10,  600, 600+Int[k][600:800].argmax() ,  10,  200,  550+Int[k][600:800].argmax(), 15]
#    popt, pcov = curve_fit(dgauss, arange(x_m_min, x_m_max), Int[k][x_m_min:x_m_max], p0)
#    x_m[k]=max(popt[2],  popt[5])
#    x_md[k]=min(popt[2],  popt[5])





# fit curvature:
fit=numpy.polyfit(y_cor, x_m, 2)
#fitd=numpy.polyfit(y_cor, x_md, 2)
#x_fixed=min(x_m)

#
figure(1)
#H,  xx,  yy = numpy.histogram2d(x, y, bins=(1360, 1040))
#plt.imshow(H)
plt.scatter(x, y, s=1)
plt.scatter(x_m, y_cor,  c='r')
#plt.scatter(x_md, y_cor,  c='m')

#scatter(x,  y, s=1)
#scatter(x_m, y_cor, s=50,  c='m')
#scatter(x_md, y_cor, s=50, c='r')
yf=arange(0,y_max)
xf=fit[2]+fit[1]*yf+fit[0]*yf**2
plt.plot(xf, yf, c='r')




# fix curvature

fitname=filename.replace(".txt", "_fit.txt")
savetxt(fitname, fit)


x_new=zeros(len(x))
for i in range(len(x)):
    #x_new[i]=x[i]-(fit[2]+fit[1]*y[i]+fit[0]*y[i]**2-x_fixed)
    x_new[i]=x[i]-(fit[1]*y[i]+fit[0]*y[i]**2)



x_new_max=max(x_new)

figure(2)
plt.scatter(x_new, y, s=1)
#J,  xx,  yy = numpy.histogram2d(x_new, y, bins=(round(x_new_max), 1040))



# integrate spectra in y direction:
# prec = split pixel in #parts
prec = 1
Int_correct=zeros(round(x_new_max*prec))

for i in range(len(x)):
    ind=int(round(x_new[i]*prec));
    Int_correct[ind-1]= Int_correct[ind-1]+1;

chan = arange(1/prec, x_new_max+1/prec, 1/prec)

filename_save = filename.replace(".txt","_cor_prec"+str(prec)+"_slices"+str(slice_num)+".txt")


savetxt(filename_save,list(zip(chan, Int_correct)))

figure(3)
plt.plot(numpy.linspace(0, x_new_max, len(Int_correct)), Int_correct/max(Int_correct))
plt.show()
