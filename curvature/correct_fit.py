import os
os.chdir('d:\\data\\python\\curvature')
from pylab import *
import numpy
#from scipy.optimize import curve_fit
import read_raw_spectra
import matplotlib.pyplot as plt
import find_curvature

filename = 'water_540ev_1.txt'
if os.name == 'nt':
    folder = 'd:\\data\\azobenzene - 2012 jan\\BESSY_2012_jan\\12-01-21\\'
else:
    folder = '/opt/data/work/python'

# Correct the spectra (1 (or anything else)) or read the existing fit parameters (2) :

ch = 1

# Do you want to see the plots  (Y/N) ?:
pl = 'Y'

(x, y) = read_raw_spectra.read_spectra(folder,  filename)

sss = numpy.array([x, y])
sorted = sss[:, sss[1].argsort()]
x = sorted[0, :]
y = sorted[1, :]


x_max = round(max(x))
y_max = round(max(y))

if ch == 2:
    #fit=loadtxt('water_540ev_1_fit.txt')
    fit_load = numpy.load(fitfile)
    fit = fit_load['fit']
    x_m = fit_load['x_m']
    y_cor = fit_load['y_cor']

else:
    slice_num = 12
    prec_sl = 1
    [fit, x_m, x_md, y_cor] =find_curvature.fc(x, y, prec_sl, slice_num);
    fitname=filename.replace(".txt", "-slice_"+str(slice_num)+"-prec_"+str(prec_sl)+"-fit")
    #savetxt(fitname, fit,,fmt='%1.8f')
    numpy.savez(fitname, fit=fit, x_m=x_m, y_cor=y_cor)

# Correct the x position of the points
x_new=zeros(len(x))
for i in range(len(x)):
    x_new[i]=x[i]-(fit[1]*y[i]+fit[0]*y[i]**2)
    
x_new_max=max(x_new)

# integrate spectra in y direction:
# prec = split pixel in #parts
prec = 1
Int_correct=zeros(round(x_new_max*prec)+1)

for i in range(len(x)):
    ind=int(round(x_new[i]*prec));
    Int_correct[ind]= Int_correct[ind]+1;
       
chan = arange(0, x_new_max, 1/prec)

filename_save = filename.replace(".txt","-cor-slice_"+str(slice_num)+"-prec_"+str(prec)+".txt")


savetxt(filename_save,list(zip(chan, Int_correct)),fmt='%1.4g')

if pl == 'Y':
    plt.figure('curvature fit')
    xedge=numpy.arange(0, x_max+1)
    yedge=numpy.arange(0, y_max+1)
    pl_xy = histogram2d( y, x,  [yedge,  xedge])
    #plt.scatter(x, y, s=1)
    plt.imshow(pl_xy[0], cmap='terrain', origin='lower')
    plt.scatter(x_m, y_cor,  c='r')
    #plt.scatter(x_md, y_cor,  c='m')
    yf=arange(0,y_max)
    xf=fit[2]+fit[1]*yf+fit[0]*yf**2
    plt.plot(xf, yf, c='r')
        
    plt.figure('corrected 2D image')
    xnedge=numpy.arange(0, x_new_max+1)
    pl_xny = histogram2d(y, x_new, [yedge,  xnedge])
    plt.imshow(pl_xny[0], cmap='terrain',  origin='lower')
    #plt.scatter(x_new, y, s=1)
            
    plt.figure('corrected 1D spectra')
    plt.plot(numpy.linspace(0, x_new_max, len(Int_correct)), Int_correct)
    plt.show()