# read_raw_spectra.py
def read_spectra(folder='/opt/data/work/python', filename='cab_3985_1.txt'):
    import os
    os.chdir(folder)
    x=[]
    y=[]
    file = open(filename,'r')
    
    data = file.read()
    
    data = data.split('time)\n')
    
    data[1] = data[1].replace(",",".");
    data[1] = data[1].split('\n');
    
    for line in data[1]:
        if line.strip():
            line = line.split('\t')
            num = [float(g) for g in [line[0], line[1]]];
            x.append(num[0]);
            y.append(num[1])
    
    # print(x, y)
    #scatter(x, y, s=1)
    #show()
    file.close()
    return(x, y)
    
