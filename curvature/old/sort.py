
import read_raw_spectra

(x, y) = read_raw_spectra.read_spectra(folder = '/opt/data/work/python',  filename = 'cab_3985_s.txt')

# sort by y
temp = list(zip(y, x))
temp.sort()
(y,  x) = zip(*temp)

print(x,  y)
