from pylab import *

import numpy as np
from scipy.optimize import curve_fit

gaussian = lambda x: 3*exp(-(30-x)**2/20.)
data = gaussian(arange(100))


def gauss(x,  B,  mu,  sigma):
       return B*np.exp(-(x-mu)**2/(2*sigma**2))
       

p0 = [2.4,  33,  2.5]
popt, pcov = curve_fit(gauss, arange(100), data,  p0)
print(popt)
