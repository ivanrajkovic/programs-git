# plot raw spectra
from pylab import *

import read_raw_spectra

(x, y) = read_raw_spectra.read_spectra(folder = '/opt/data/work/python',  filename = 'cab_3985_s.txt')
#(x, y) = read_raw_spectra.read_spectra()
scatter(x, y, s=1)
show()
