# correct raw spectra
# from pylab import *
from pylab import *
import numpy
from scipy.optimize import curve_fit
import read_raw_spectra

filename = 'cab_3985_1.txt'
folder = '/opt/data/work/python'

(x, y) = read_raw_spectra.read_spectra(folder = folder,  filename = filename)

# image resolution:
x_max = round(max(x))
y_max = round(max(y))

# how many slices?
slice_num = 11

Int=zeros((slice_num, x_max))

y_step = y_max/slice_num
y_cor=numpy.linspace(y_step/2, y_max-y_step/2, num=11)


# how many +- y_rows to integrate?
#rows_int = 50
y_ind=[len(y)]*(slice_num+1)




# sort by y:
temp = list(zip(y, x))
temp.sort()

(y,  x) = zip(*temp)

# find slices borders:

for k in range(0, slice_num):
    y_ind[k] = next(i for i,v in enumerate(y) if v > y_step*k)

# integrate in y-direction in each slice:
for k in range(0, slice_num):
#    print(k, y_ind[k], y_ind[k+1]);
    for o in range(y_ind[k], y_ind[k+1]):
        Int[k][round(x[o])-1]=Int[k][round(x[o])-1]+1;

# find points to fit in some x range:
x_m=zeros(slice_num)
x_m_min=650
x_m_max=800

# just finding maximum, change to gaussian or similar!!!
#for k in range(0, slice_num):
#    x_m[k]=Int[k].argmax()

# fitting gaussian, use this or maximum, not both!! 
def gauss(x, A,  B,  mu,  sigma):
       return A+B*numpy.exp(-(x-mu)**2/(2*sigma**2))
   
for k in range(0, slice_num):
    p0 = [15,  20,  Int[k].argmax(),  5]
    popt, pcov = curve_fit(gauss, arange(x_m_min, x_m_max), Int[k][x_m_min:x_m_max], p0)
    x_m[k]=popt[2]

   
#figure(1)
#scatter(x, y, s=1)
#scatter(x_m, y_cor)


# fit curvature:
fit=numpy.polyfit(y_cor, x_m, 2)
x_fixed=sum(x_m)/slice_num
# fix curvature

x_new=zeros(len(x))
for i in range(len(x)):
   x_new[i]=x[i]-(fit[2]+fit[1]*y[i]+fit[0]*y[i]**2-x_fixed)
#
#figure(2)
#scatter(x_new, y, s=1)

x_new_max=max(x_new)

# integrate spectra in y direction:
# prec = split pixel in #parts
prec = 5
Int_correct=zeros(round(x_new_max*prec))

for i in range(len(x)):
    ind=int(round(x_new[i]*prec));
    Int_correct[ind-1]= Int_correct[ind-1]+1;
       
chan = arange(1/prec, x_new_max+1/prec, 1/prec)

filename = filename.replace(".txt","_cor_prec"+str(prec)+".txt")

savetxt(filename,list(zip(chan, Int_correct)),fmt="%10.5f")
#figure(3)
#plot(numpy.linspace(0, x_new_max, len(Int_correct_prec)), Int_correct_prec/max(Int_correct_prec))
#plot(numpy.linspace(0, x_new_max, len(Int_correct)), Int_correct/max(Int_correct))
#show()
