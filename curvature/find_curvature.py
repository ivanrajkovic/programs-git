# find the curvature:
def fc(x, y, prec_sl=1, slice_num=12):
    import numpy
    from scipy.optimize import curve_fit
    from pylab import zeros, arange
        
    # image resolution:
    x_max = round(max(x))
    y_max = round(max(y))
    
    
    Int=numpy.zeros((slice_num, round(max(x)*prec_sl)+1))
        
    y_step = y_max/slice_num
    y_cor=numpy.linspace(y_step/2, y_max-y_step/2, num=slice_num)
    
    
    # how many +- y_rows to integrate?
    #rows_int = 50
    y_ind=[len(y)]*(slice_num+1)

    
    # find slices borders:
    for k in range(0, slice_num):
        y_ind[k] = next(i for i,v in enumerate(y) if v > y_step*k)
    
    # integrate in y-direction in each slice:
    for k in range(0, slice_num):
    #    print(k, y_ind[k], y_ind[k+1]);
        for o in range(y_ind[k], y_ind[k+1]):
            Int[k][round(x[o]*prec_sl)]=Int[k][round(x[o])*prec_sl]+1;
    
    
    
    
    x_m_min=400
    x_m_max=900
    
    # find points to fit in some x range:
    x_m=zeros(slice_num)
    x_md=zeros(slice_num)
    # just finding maximum, change to gaussian or similar!!!
    #for k in range(0, slice_num):
    #    x_m[k]=Int[k].argmax()
    
    # fitting gaussian, use this or maximum, not both!! 
    
           
    def gauss(x, A,  B,  mu,  sigma):
           return A+B*numpy.exp(-(x-mu)**2/(2*sigma**2))
           

    
#     for k in range(0, slice_num):
#         p0 = [10,  100,  x_m_min+Int[k][x_m_min*prec_sl:x_m_max*prec_sl].argmax()/prec_sl,  10]
#         popt, pcov = curve_fit(gauss, arange(x_m_min, x_m_max,1/prec_sl), Int[k][x_m_min*prec_sl:x_m_max*prec_sl], p0)
#         x_m[k]=popt[2]
    
    
    #or double gauss:
    
    def dgauss(x, A,  B,  mu,  sigma, C,  nu,  delta):
          return A+B*numpy.exp(-(x-mu)**2/(2*sigma**2))+C*numpy.exp(-(x-nu)**2/(2*delta**2))

    for k in range(0, slice_num):
        p0 = [10,  300, x_m_min+Int[k,x_m_min:x_m_max].argmax() ,  10,  200,  -30+x_m_min+Int[k,x_m_min:x_m_max].argmax(), 5]
        #p0 = [10,  300, 580 ,  30,  200,  700, 30]
        popt, pcov = curve_fit(dgauss, arange(x_m_min, x_m_max), Int[k, x_m_min:x_m_max], p0, maxfev=100000)
        x_m[k]=max(popt[2],  popt[5])
        x_md[k]=min(popt[2],  popt[5])
#     
        
    # fit curvature:
    fit=numpy.polyfit(y_cor, x_m, 2)    
    fit2=numpy.polyfit(y_cor, x_md, 2)
    #fit = (fit + fit2)/2
    return([fit,x_m,x_md,y_cor])
