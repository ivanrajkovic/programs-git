import os
import sys
from pylab import *
import numpy
import matplotlib.pyplot as plt
import tkinter as tk
import tkinter.simpledialog as tksd
import tkinter.filedialog as tkfd
from scipy.stats.stats import pearsonr



root = tk.Tk()
root.withdraw()

folder_a, filename_a = os.path.split(tkfd.askopenfilename(title="Choose a first spectra:", filetypes=[('txt', '.txt'), ('all files', '.*')]))

os.chdir(folder_a)
ch_a, Int_a= numpy.loadtxt(filename_a, unpack=True)

# precision of spectra a, to be read from the filename
prec_a = int(round(1/(ch_a[1]-ch_a[0])))

folder_b, filename_b = os.path.split(tkfd.askopenfilename(title="Choose a file to overlap with the first one:", filetypes=[('txt', '.txt'), ('all files', '.*')]))

os.chdir(folder_b)
ch_b, Int_b = loadtxt(filename_b, unpack=True)

# precision of spectra b, to be read from the filename
prec_b = int(round(1/(ch_b[1]-ch_b[0])))

if prec_a != prec_b:
    print('Files don\'t have the same precision!!! This will not work (for now)!!!')
#     sys.exit()
    
#plt.plot(Int_a[:,0], Int_a[:,1])
#plt.plot(Int_b[:,0], Int_b[:,1])
#
#plt.show()

# X-values where to compare (channel numbers)
x_m_min = 500
x_m_max = 900


x_a_min = x_m_min*prec_a
x_a_max = x_m_max*prec_a
x_b_min = x_m_min*prec_b
x_b_max = x_m_max*prec_b

# range in which to search for best overlab, +-px_num channels
px_num = 30

cor=numpy.zeros(2*px_num*prec_a)

for px in range(-px_num*prec_b, px_num*prec_b):
    #use Pearson correlation:
    #cor[px+px_num*prec_a],  ttt = pearsonr(Int_a[x_a_min:x_a_max], Int_b[x_b_min-px:x_b_max-px])

    # use 1D RMS
    cor[px+px_num*prec_b] = -sqrt(mean((Int_a[x_a_min:x_a_max] - Int_b[x_b_min-px:x_b_max-px])**2))

px_move = argmax(cor)-px_num*prec_b

ch_b = ch_b+px_move/prec_b

filename_b_moved = filename_b.replace(".txt", "-moved_"+str(-px_move/prec_b)+"px.txt")

savetxt(filename_b_moved,list(zip(ch_b, Int_b)),fmt='%1.4g')


