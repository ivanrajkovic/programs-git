import os
from pylab import *
import numpy
# import locale
#from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import tkinter as tk
import tkinter.simpledialog as tksd
import tkinter.filedialog as tkfd
from scipy.optimize import curve_fit
from scipy.stats.stats import pearsonr

if os.name == 'nt':
    os.chdir('d:\\data\\azobenzene - 2012 jan\\python\\curvature')
else:
    os.chdir('/opt/data/work/python')

import read_raw_spectra
#import find_curvature

root = tk.Tk()
root.withdraw()
folder, filename = os.path.split(tkfd.askopenfilename(title="Choose a file to correct:", filetypes=[('txt', '.txt'), ('all files', '.*')]))

# filename = locale.format_string('%s', path[1])
# folder = locale.format_string('%s', path[0])

#filename = 'cab_3985_1.txt'
#folder = 'd:\\data\\BESSY_2012_jan\\12-01-24\\'


# Correct the spectra (1) or read the existing fit parameters (2):

ch = 1
#ch = tksd.askinteger("Find the curvature or use the existing?","1 - Calculate curvature. 2 - use existing fit", minvalue=1, maxvalue=2, initialvalue=1)


if ch == 1:
    curv_type = tksd.askinteger("Which method to use for finding the curvature:","1 - Pearson correlation. \n2 - Maximum of summed spectra. \n3 - 2D RMS difference. \n4 - 1D RMS difference. \n5 - Maximum of summed spectra squered. \n6 - Gaussian fit", minvalue=1, maxvalue=6, initialvalue=1)

# Do you want to see the plots (Y/N) ?:
pl = 'Y'
#pl = tksd.askstring("Plots", "Do you want to see the plots? (Y/N)", initialvalue='Y')


(x, y) = read_raw_spectra.read_spectra(folder,  filename)

# image resolution:
x_max = round(max(x))
y_max = round(max(y))



if ch == 1:
    # Calculate the curvature
        
    #slice_num = tksd.askinteger("Slices","How many slices (2-100)?",minvalue = 2, maxvalue = 100, initialvalue = 12)
    slice_num=10
    # integrate in y-direction in each slice, split each pixel in prec_sl parts:
    #prec_sl = tksd.askinteger("Pixel splitting","Split the pixel in how many (1-10)?",minvalue = 1, maxvalue = 10, initialvalue = 1)
    prec_sl = 1
    y_step = y_max/slice_num
    y_cor=numpy.linspace(y_step/2, y_max-y_step/2, num=slice_num)
    y_ind=[len(y)]*(slice_num+1)

    # sort by y:
    sss = numpy.array([x, y])
    sorted = sss[:, sss[1].argsort()]
    x = sorted[0, :]
    y = sorted[1, :]
    
    # find slices borders:
    for k in range(0, slice_num):
        y_ind[k] = next(i for i,v in enumerate(y) if v > y_step*k)
        
    
    # area of x where to find the fit
    x_m_min=500
    x_m_max=900
    x_m=zeros(slice_num)    
    
    for k in range(0, slice_num):
        #    print(k, y_ind[k], y_ind[k+1]);
            for o in range(y_ind[k], y_ind[k+1]):
                Int[k, round(prec_sl*x[o])]=Int[k, round(prec_sl*x[o])]+1;    
                
    # methods 1-5 use similar method with moving of the neighbouring slices
    if curv_type != 6:
        
        x_mo = argmax(Int[0])/prec_sl
        # find the best correlation:
        sl_px_mv = zeros(slice_num)
        px_num = 30
                
        for k in range(0, slice_num-1):
            sl_px_temp=zeros(2*px_num*prec_sl)
            
            # find a reference 2d histogram for a slice - histogram2d doesn't work, not enough counts per pixel !!!
                # these next 4 lines are needed just for 2D RMS
            if curv_type == 3:
                xedgef=numpy.arange(0, x_max+1, 1/prec_sl)
                yedgef=numpy.arange(0, y_max+1, 1/prec_sl)
                ref_hist, bbb,  bbbb = histogram2d(x[y_ind[k]:y_ind[k+1]], y[y_ind[k]:y_ind[k+1]], [xedgef,  yedgef])
                mov_hist, ccc,  cccc = histogram2d(x[y_ind[k+1]:y_ind[k+2]], y[y_ind[k+1]:y_ind[k+2]], [xedgef, yedgef])
                
            for px in range(-px_num*prec_sl, px_num*prec_sl):
                
                # use the pearson's correlation
                if curv_type == 1:
                    sl_px_temp[px+px_num*prec_sl],  ttt =pearsonr(Int[k, x_m_min*prec_sl:x_m_max*prec_sl], Int[k+1, x_m_min*prec_sl-px:x_m_max*prec_sl-px])
                
                # find when the maximum of the sum of two spectra is the highest
                elif curv_type == 2:
                    sl_px_temp[px+px_num*prec_sl] = max(Int[k,  x_m_min*prec_sl:x_m_max*prec_sl] + Int[k+1, x_m_min*prec_sl-px:x_m_max*prec_sl-px])
                
                # find the RMS difference between 2D slices:
                elif curv_type == 3:
                    sl_px_temp[px+px_num*prec_sl]=-sqrt(mean((ref_hist[x_m_min*prec_sl:x_m_max*prec_sl,round(prec_sl*y[y_ind[k]]):round(prec_sl*y[y_ind[k+1]])]-mov_hist[x_m_min*prec_sl-px:x_m_max*prec_sl-px, round(prec_sl*y[y_ind[k+1]]):2*round(prec_sl*y[y_ind[k+1]])-round(prec_sl*y[y_ind[k]])])**2))
                
                # find a RMS difference between 1d spectra:
                elif curv_type == 4:
                    sl_px_temp[px+px_num*prec_sl] = -sqrt(mean((Int[k,  x_m_min*prec_sl:x_m_max*prec_sl] - Int[k+1, x_m_min*prec_sl-px:x_m_max*prec_sl-px])**2))
                
                # find the maximum of the sum squared ('focus' the spectra)
                elif curv_type == 5:
                    sl_px_temp[px+px_num*prec_sl] = sum((Int[k,  x_m_min*prec_sl:x_m_max*prec_sl] + Int[k+1, x_m_min*prec_sl-px:x_m_max*prec_sl-px])**2)
                    
                       
                        
            sl_px_mv[k+1]=argmax(sl_px_temp)/prec_sl-px_num
            
            
        for k in range(0, slice_num):    
            
            x_m[k]=-sum(sl_px_mv[0:k+1])+x_mo
                
    
    # method 6 doesn't move slices, so it has to go separately:
    elif curv_type == 6:
        def gauss(x, A,  B,  mu,  sigma):
           return A+B*numpy.exp(-(x-mu)**2/(2*sigma**2))
        
        for k in range(0, slice_num):
            p0 = [10,  100,  x_m_min+Int[k][x_m_min*prec_sl:x_m_max*prec_sl].argmax()/prec_sl,  10]
            popt, pcov = curve_fit(gauss, arange(x_m_min, x_m_max,1/prec_sl), Int[k][x_m_min*prec_sl:x_m_max*prec_sl], p0)
            x_m[k]=popt[2]
        
    
    fit=numpy.polyfit(y_cor, x_m, 2)
    fitname=filename.replace(".txt", "-slice_"+str(slice_num)+"-prec_"+str(prec_sl)+"-fit")
    numpy.savez(folder+'\\'+fitname, fit=fit, x_m=x_m, y_cor=y_cor)




if ch == 2:
    #  load the existing fit
    #   fit=loadtxt('water_540ev_1_fit.txt')
    fitfile = tkfd.askopenfilename(title="Choose a saved fit:", filetypes=[('npz', '.npz'), ('all files', '.*')])
    fit_load = numpy.load(fitfile)
    fit = fit_load['fit']
    x_m = fit_load['x_m']
    y_cor = fit_load['y_cor']

        
        
#    Correct the x position of the points
x_new=zeros(len(x))
for i in range(len(x)):
    x_new[i]=x[i]-(fit[1]*y[i]+fit[0]*y[i]**2)
        
x_new_max=max(x_new)
    
#    integrate spectra in y direction:
#    prec = split pixel in #parts
prec = 1
Int_correct=zeros(round(x_new_max*prec)+1)
    
for i in range(len(x)):
    ind=int(round(x_new[i]*prec));
    Int_correct[ind]= Int_correct[ind]+1;
        
chan = arange(0, (round(x_new_max*prec)+1)/prec, 1/prec)

filename_save = filename.replace(".txt","-cor-slice_"+str(slice_num)+"-prec_"+str(prec)+".txt")
savetxt(filename_save,list(zip(chan, Int_correct)),fmt='%1.4f')

print(fit)
print(max(Int_correct))

if pl == 'Y':
    plt.figure('curvature fit')
    xedge=arange(0, x_max+1)
    yedge=arange(0, y_max+1)
    pl_xy = histogram2d( y, x,  [yedge,  xedge])
    #plt.scatter(x, y, s=1)
    plt.imshow(pl_xy[0], cmap='terrain', origin='lower')
    plt.scatter(x_m, y_cor,  c='r')
    #plt.scatter(x_md, y_cor,  c='m')
    yf=arange(0,y_max)
    xf=fit[2]+fit[1]*yf+fit[0]*yf**2
    plt.plot(xf, yf, c='r')
        
    plt.figure('corrected 2D image')
    xnedge=arange(0, x_new_max+1)
    pl_xny = histogram2d(y, x_new, [yedge,  xnedge])
    plt.imshow(pl_xny[0], cmap='terrain',  origin='lower')
    #plt.scatter(x_new, y, s=1)
            
    plt.figure('corrected 1D spectra')
    plt.plot(chan, Int_correct)
    plt.show()


print(max(Int_correct))
os.chdir('d:\\data\\python\\curvature')
