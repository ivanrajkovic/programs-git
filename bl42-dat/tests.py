#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division

import sys
import os
import glob
import re
import getpass
import signal as sg
import time
import fileinput
import shutil
import subprocess
import struct


#from PyQt5 import QtGui,QtWidgets
#from PyQt5.QtWidgets import QFileDialog, QGraphicsPixmapItem, QMainWindow, QApplication, QGraphicsScene
#from PyQt5.QtGui import QPixmap, QColor
#from PyQt5.QtCore import Qt, QRect, QRectF

try:
    from PyQt4.QtGui import QFileDialog, QGraphicsPixmapItem, QMainWindow, QApplication, QGraphicsScene, QPixmap, QColor, QStyleFactory, QCursor, QWidget, QMouseEvent, QGraphicsItem, QPen, QBrush,  QPainter, QPainterPath, QPolygonF, QMessageBox, QImage, QGraphicsView, QGridLayout, QTabWidget, QSizePolicy, QFrame, QVBoxLayout, QHBoxLayout, QPushButton, QFormLayout, QLabel, QSlider, QDoubleSpinBox, QSpacerItem, QLayout, QSpinBox, QStatusBar, QLineEdit, QCheckBox, QDialog, QPlainTextEdit, QScrollArea, QScrollBar # QIcon, QStyle
    from PyQt4.QtCore import QDir, Qt, QEvent, QPointF, QSize, QMetaObject, QRectF, QString, QRect#, QLineF
    qq = 4
except:
    from PyQt5.QtCore import QDir, Qt, QEvent, QPointF, QSize, QMetaObject, QRectF, QRect, QCoreApplication

    from PyQt5.QtWidgets import QFileDialog, QGraphicsPixmapItem, QMainWindow, QApplication, QGraphicsScene, QStyleFactory, QWidget, QGraphicsItem, QMessageBox, QGraphicsView, QGridLayout, QTabWidget, QSizePolicy, QFrame, QVBoxLayout, QHBoxLayout, QPushButton, QFormLayout, QLabel, QSlider, QDoubleSpinBox, QSpacerItem, QLayout, QSpinBox, QStatusBar, QLineEdit, QCheckBox, QDialog, QPlainTextEdit, QScrollArea, QScrollBar


    from PyQt5.QtGui import QPixmap, QColor, QCursor, QMouseEvent, QPen, QBrush, QPainter, QPainterPath, QPolygonF, QImage
    qq=5

if qq == 4:
    from qt_tests import Ui_MainWindow
else:
    from qt_tests5 import Ui_MainWindow
class MyWindow(QMainWindow):
    from ttt.tests_import import slider_changed,box_changed,btgr_clicked
    def __init__(self):
        super(MyWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # set theme: "Motif" "Windows" "CDE" "Plastique"  "Cleanlooks" "GTK+"
        QApplication.setStyle(QStyleFactory.create("Cleanlooks"))
        self.show()



        self.ui.slider.valueChanged.connect(self.slider_changed)
        self.ui.box.valueChanged.connect(self.box_changed)

        self.ui.btgr.buttonClicked.connect(self.btgr_clicked)

        self.ui.btgr.setId(self.ui.b1, 0)
        self.ui.btgr.setId(self.ui.b2, 5)


    #import tests_import

    # def slider_changed(self):
    #     self.ui.box.setValue(self.ui.slider.value())
    #
    # def box_changed(self):
    #     self.ui.slider.setValue(self.ui.box.value())
    #     self.ui.label.setText('{}'.format(self.ui.box.value()))
    #
    # def btgr_clicked(self):
    #     self.ui.label.setText('{}'.format(self.ui.btgr.checkedId()))




if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWindow()
    sys.exit(app.exec_())