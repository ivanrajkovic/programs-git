from matplotlib import pyplot as plt
from shapely.geometry import Point, Polygon, MultiPoint


from shapely.ops import cascaded_union, unary_union


poly = Polygon([(0, 0), (0.2, 2), (1, 1),(1.8, 2), (2, 0), (1, 0.8), (0, 0)])

x,y = poly.exterior.xy

fig = plt.figure(1, figsize=(5,5), dpi=90)
ax = fig.add_subplot(111)
ax.plot(x, y, color='#6699cc', alpha=0.7, linewidth=3, solid_capstyle='round', zorder=2)


fig.show()

ee = Point([.2,.2])

print(ee.within(poly))
print(poly.contains(ee))

ee.xy
list(ee.coords)

tt=MultiPoint([(0,0),(1,3),(3,1),(10,4),(1,10),(3,3),(2,6),(10,20)])


tp = ([0,0],[1,3],[3,1],[10,4],[1,10],[3,3],[2,6],[10,20])
xp = []
yp = []



tt.geoms[0].xy
len(tt)

for a in list(tt.geoms):
    print(a.xy)
    
# this doesn't work well, makes biggest polygon
yy = tt.convex_hull
x,y = yy.exterior.xy


# this is better
coords = [(p.x, p.y) for p in tt]
rr = Polygon(coords)

x2,y2  = rr.exterior.xy


fig = plt.figure(1, figsize=(5,5), dpi=90)

ax = fig.add_subplot(111)
ax.plot(x, y, color='#6699cc', alpha=0.7, linewidth=3, solid_capstyle='round', zorder=2)
ax.plot(x2, y2, color='#cc0000', alpha=0.7, linewidth=3, solid_capstyle='round', zorder=2)


fig.show()

# import mask image into polygon

import numpy
from skimage import measure

ww = numpy.zeros([50,50])

ww[10:20,15:25] = 1

contours = measure.find_contours(ww, 0.99999999, fully_connected="high")

simplified_contours = [measure.approximate_polygon(c, tolerance=0.1) for c in contours]

aa  = Polygon(simplified_contours[0])

plt.figure()
plt.imshow(ww)
for n, contour in enumerate(simplified_contours):
    plt.plot(contour[:, 1], contour[:, 0], linewidth=2)
for n, contour in enumerate(contours):
    plt.plot(contour[:, 1], contour[:, 0], linewidth=2)
plt.show()



#Using Polygons2 package, most promising

# conda install -c tofuproject polygon2

import Polygon
q = Polygon.Polygon(((0.0, 0.0), (10.0, 0.0), (10.0, 5.0), (0.0, 5.0)))
t = Polygon.Polygon(((1.0, 1.0), (3.0, 1.0), (2.0, 3.0)))
e = Polygon.Polygon()
z = Polygon.Polygon(((0,0),(100,0),(100,100),(0,100)))

a = q - t

a.simplify()

print(a.isInside(1.5,1.5))
print(a.isInside(1.5,1.5,1))
print(a.isInside(1.5,1.5,2))