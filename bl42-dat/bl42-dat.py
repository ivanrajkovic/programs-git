#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from __future__ import division

import sys
import os
import glob
import re
import getpass
import signal as sg
import time
import fileinput
import shutil
import subprocess
import struct
import fabio

from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4 import uic

from matplotlib.ticker import LinearLocator
from matplotlib.ticker import AutoLocator

from matplotlib import path
from PIL import Image
from PIL.ImageQt import ImageQt
import numpy
numpy.seterr(divide='ignore',invalid='ignore')
from scipy import signal
from scipy.misc import toimage
import heapq

from matplotlib import rcParams
#rcParams.update({'figure.autolayout': True})

# import external functions/classes
from bl42datmod.bl42main import *
from bl42datmod.mpl import *

# exit on CRTL-C (had to import signal as sg because of signal from scipy)
sg.signal(sg.SIGINT, sg.SIG_DFL)

#sys.path.append(os.path.split(os.path.abspath(__file__))[0])
#print(sys.path)

class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        # import external .ui file
        uic.loadUi(os.path.join(os.path.split(os.path.abspath(__file__))[0],'bl42datmod/bl42dat_gui.ui'),self)
        # set theme: "Motif" "Windows" "CDE" "Plastique"  "Cleanlooks" "GTK+"
        QApplication.setStyle(QStyleFactory.create("Cleanlooks"))
        self.show()
        
        # import connections file
        from bl42datmod.gui_connect import gui_connect
        gui_connect(self)
        
        # initialize default values
        from bl42datmod.bl42dat_init import bl42dat_init
        bl42dat_init(self)
        
    
    ## all tabs
    
    def onChange(self,i):
        ''' what to do when surrent tab is changed
        '''
        #print("Current Tab Index: {}".format(i) ) #changed!
        # uncheck buttons, delete any point data that is left, delete mask/zoom lines 
        # self.status_message()
        # change cursor style if needed
        if i == 2:
            self.update_im_cm_img()
        
    def eventFilter(self, source, event):
        ''' catch mouse clicks
        '''
        modifiers = QApplication.keyboardModifiers()
        # q-calib tab
        if self.tabWidget.currentIndex() == 0:
            if (event.type() == QEvent.MouseButtonPress and QMouseEvent.button(event) == 1):
                click = self.graphicsView.mapToScene(event.pos())
                if self.set_center_1.isChecked():
                    self.set_center_point(click.x(),click.y())
                    
                if self.set_angles_man.isChecked():
                    self.set_angles_point(click.x(),click.y())
                
                if self.zoom_select.isChecked():
                    self.zoom_into_select(click.x(),click.y())
                    
            if event.type() == QEvent.Wheel:
            #     
            # zoom only with crtl+wheel:
            #
                
                if modifiers == Qt.ControlModifier:
                    if event.delta() > 0 and self.zoom_spin.value()<100000:
                        zoomFactor = min(1.2,100000/(self.zoom_spin.value()))
                    elif event.delta() < 0 and self.zoom_spin.value()>1:
                        zoomFactor = max(1/1.2,1/(self.zoom_spin.value()))
                    else:
                        zoomFactor = 1
                    
                    if zoomFactor != 1:
                        #self.graphicsView.scale(zoomFactor, zoomFactor)
                        if int(self.zoom_spin.value()*zoomFactor) == int(self.zoom_spin.value()):
                                self.zoom_spin.setValue(self.zoom_spin.value()*zoomFactor+1)
                        else:
                            self.zoom_spin.setValue(self.zoom_spin.value()*zoomFactor)
                    
            
            if event.type() == QEvent.MouseMove:
                # tooltip about current pixel
                try:
                    click = self.graphicsView.mapToScene(event.pos())
                    if self.agbe_size_h > click.x() > 0 and self.agbe_size_v > click.y() > 0 :
                        self.graphicsView.setToolTip("{} x {} : {}".format(numpy.int(click.x()),self.agbe_size_v-numpy.int(click.y())-1,self.agbe_2d[numpy.int(click.y()),numpy.int(click.x())]))
                    else:
                        self.graphicsView.setToolTip('')
                except:
                    pass
                # draw rectangle for zooming-in:
                if event.buttons() == Qt.NoButton and self.zoom_select.isChecked() == True and self.zoom_select_points != []:
                    click = self.graphicsView.mapToScene(event.pos())
                    self.zoom_mouse_x = click.x()
                    self.zoom_mouse_y = click.y()
                    if self.zoom_pol != '':
                        self.scene.removeItem(self.zoom_pol)
                    self.zoom_pol = self.scene.addRect(self.zoom_select_points[0],self.zoom_select_points[1],self.zoom_mouse_x-self.zoom_select_points[0],self.zoom_mouse_y-self.zoom_select_points[1],pen=self.zoompen)
        
        # mask tab
            
        elif self.tabWidget.currentIndex() == 1:
            if (event.type() == QEvent.MouseButtonPress and QMouseEvent.button(event) == 1):
                click = self.graphicsView_mask.mapToScene(event.pos())
                if self.add_polygon.isChecked() or self.remove_polygon.isChecked():
                    #self.polygon_points.append([numpy.int(click.x()),numpy.int(click.y())])
                    self.polygon_points.append([click.x(),click.y()])
                    if len(self.polygon_points) > 1:
                        self.mask_scene.addLine(self.polygon_points[-1][0],self.polygon_points[-1][1],self.polygon_points[-2][0],self.polygon_points[-2][1],pen=self.maskpen)
                
                if self.add_pixels.isChecked():
                    self.masked_data[numpy.int(click.y()),numpy.int(click.x())] = 0
                    self.show_masked()
                   
                if self.remove_pixels.isChecked():
                    self.masked_data[numpy.int(click.y()),numpy.int(click.x())] = 1
                    self.show_masked()
                
                if self.mask_zoom_select.isChecked():
                    self.mask_zoom_into_select(click.x(),click.y())
         
            if event.type() == QEvent.MouseMove:
                if (event.buttons() == Qt.NoButton and self.im4mask != ''):
                    click = self.graphicsView_mask.mapToScene(event.pos())
                    self.mask_mouse_x = click.x()
                    self.mask_mouse_y = click.y()
                    if (self.im4mask_size_v > self.mask_mouse_y > 0  and self.im4mask_size_h >self.mask_mouse_x > 0) :
                        try:
                            self.graphicsView_mask.setToolTip("{} x {} : {}".format(numpy.int(self.mask_mouse_x),self.im4mask_size_v-numpy.int(self.mask_mouse_y)-1, self.im4mask_data[numpy.int(self.mask_mouse_y),numpy.int(self.mask_mouse_x)]))
                            #self.graphicsView_mask.setToolTip("{} x {}".format(numpy.int(click.x()),numpy.int(click.y())))
                        except:
                            pass
                    else:
                        self.graphicsView_mask.setToolTip('')
                    
                    if self.add_polygon.isChecked() or self.remove_polygon.isChecked():
                        if len(self.polygon_points) > 0:
                            if self.track_mouse_line != '':
                                    self.mask_scene.removeItem(self.track_mouse_line)
                            self.track_mouse_line = self.mask_scene.addLine(self.polygon_points[-1][0],self.polygon_points[-1][1],self.mask_mouse_x,self.mask_mouse_y,pen=self.maskpen)
            
            if event.type() == QEvent.Wheel:
            #     
            # zoom only with crtl+wheel:
            #
                
                if modifiers == Qt.ControlModifier:
                    if event.delta() > 0 and self.zoom_mask_spin.value()<100000:
                        zoomFactor = min(1.2,100000/(self.zoom_mask_spin.value()))
                    elif event.delta() < 0 and self.zoom_mask_spin.value()>1:
                        zoomFactor = max(1/1.2,1/(self.zoom_mask_spin.value()))
                    else:
                        zoomFactor = 1
                    
                    if zoomFactor != 1:
                        #self.graphicsView_mask.scale(zoomFactor, zoomFactor)
                        if int(self.zoom_mask_spin.value()*zoomFactor) == int(self.zoom_mask_spin.value()):
                            self.zoom_mask_spin.setValue(self.zoom_mask_spin.value()*zoomFactor+1)
                        else:
                            self.zoom_mask_spin.setValue(self.zoom_mask_spin.value()*zoomFactor)
                    
            # draw rectangle for zooming-in:
            if event.type() == QEvent.MouseMove:
                if event.buttons() == Qt.NoButton and self.mask_zoom_select.isChecked() == True and self.mask_zoom_select_points != []:
                    click = self.graphicsView_mask.mapToScene(event.pos())
                    self.zoom_mouse_x = click.x()
                    self.zoom_mouse_y = click.y()
                    if self.mask_zoom_pol != '':
                        self.mask_scene.removeItem(self.mask_zoom_pol)
                    self.mask_zoom_pol = self.mask_scene.addRect(self.mask_zoom_select_points[0],self.mask_zoom_select_points[1],self.zoom_mouse_x-self.mask_zoom_select_points[0],self.zoom_mouse_y-self.mask_zoom_select_points[1],pen=self.zoompen)
        
        
        #  img math tab
        elif self.tabWidget.currentIndex() == 2:
            if (event.type() == QEvent.MouseButtonPress and QMouseEvent.button(event) == 1):
                click = self.img_math_display.mapToScene(event.pos())
                if self.img_math_select_zoom.isChecked():
                    self.im_mt_zoom_into_select(click.x(),click.y())
                    
            if event.type() == QEvent.Wheel:
            #     
            # zoom only with crtl+wheel:
            #
                if modifiers == Qt.ControlModifier:
                    if event.delta() > 0 and self.math_zoom_spin.value()<100000:
                        zoomFactor = min(1.2,100000/(self.math_zoom_spin.value()))
                    elif event.delta() < 0 and self.math_zoom_spin.value()>1:
                        zoomFactor = max(1/1.2,1/(self.math_zoom_spin.value()))
                    else:
                        zoomFactor = 1
                    
                    if zoomFactor != 1:
                        if int(self.math_zoom_spin.value()*zoomFactor) == int(self.math_zoom_spin.value()):
                                self.math_zoom_spin.setValue(self.math_zoom_spin.value()*zoomFactor+1)
                        else:
                            self.math_zoom_spin.setValue(self.math_zoom_spin.value()*zoomFactor)
                    
            # tooltip
            if event.type() == QEvent.MouseMove:
                try:
                    click = self.img_math_display.mapToScene(event.pos())
                    if self.im_mt_show_type() == 0:
                        self.img_math_display.setToolTip("{} x {} : {}".format(numpy.int(click.x()),self.im_mt_size_view_v-numpy.int(click.y())-1,self.im_mt_data_avg[self.im_mt_ci()][numpy.int(click.y()),numpy.int(click.x())]))  
                    elif self.im_mt_show_type() == 1:
                        self.img_math_display.setToolTip("{} x {} : {}".format(numpy.int(click.x()),self.im_mt_size_view_v-numpy.int(click.y())-1,self.im_mt_data[self.im_mt_ci()][self.img_math_cur_pix.value()-1,numpy.int(click.y()),numpy.int(click.x())]))                     
                   
                    elif self.im_mt_show_type() == 2:
                        # for some reason ne need for -1 in y-coordinate ?
                        self.img_math_display.setToolTip("{} x {} :{}".format(numpy.int(numpy.mod(click.x(),self.im_mt_size_h[self.im_mt_ci()])),numpy.int(numpy.mod(self.im_mt_size_view_v-click.y(),self.im_mt_size_v[self.im_mt_ci()])),self.im_mt_data_mos[self.im_mt_ci()][numpy.int(click.y()),numpy.int(click.x())]))
                except:
                    pass
                # draw rectangle for zooming-in:
                if event.buttons() == Qt.NoButton and self.img_math_select_zoom.isChecked() == True and self.im_mt_zoom_select_points != []:
                    click = self.img_math_display.mapToScene(event.pos())
                    self.im_mt_zoom_mouse_x = click.x()
                    self.im_mt_zoom_mouse_y = click.y()
                    if self.im_mt_zoom_pol != '':
                        self.im_mt_scene.removeItem(self.im_mt_zoom_pol)
                    self.im_mt_zoom_pol = self.im_mt_scene.addRect(self.im_mt_zoom_select_points[0],self.im_mt_zoom_select_points[1],self.im_mt_zoom_mouse_x-self.im_mt_zoom_select_points[0],self.im_mt_zoom_mouse_y-self.im_mt_zoom_select_points[1],pen=self.zoompen)
            
            
            
            
                        
        # block ctrl+wheel from forwarding
        if event.type() == QEvent.Wheel and modifiers == Qt.ControlModifier:
            return True
        else:
            return QWidget.eventFilter(self, source, event)
     
    ## qcalib_tab
    
    def find_fol_agbe(self):
        ''' find data and analysis folders
        find agbe file in data folder
        if agbe no found, set 10x10 gray pixels as image
        '''
        c_user = getpass.getuser()
        if  os.path.isdir('../data/'):
            self.data_folder = os.path.normpath(os.path.abspath('../data/'))
            self.analysis_folder = os.path.normpath(os.path.abspath('.'))
        elif c_user == 'staff':
            #self.last_folder =  max(glob.glob('/mnt/home/staff/UserSetup/*/'), key=os.path.getmtime)
            self.last_folder =  max(glob.glob('/mnt/home/staff/UserSetup/2*/'))
            if os.path.isdir(os.path.join(self.last_folder,'data')):
                self.data_folder = os.path.join(self.last_folder,'data')
            else:
                self.data_folder = self.last_folder
        else:
            try:
                self.last_folder =  max(glob.glob(os.path.expanduser('~/*/')), key=os.path.getmtime)
                self.no_data = 1
                while self.no_data == 1:
                    if os.path.isdir(os.path.join(self.last_folder,'data')):
                            self.data_folder = os.path.realpath(os.path.join(self.last_folder,'data'))
                            self.no_data = 0
                    else:
                        try:
                            self.last_folder_tmp = max(glob.glob(os.path.join(self.last_folder,'*/')), key=os.path.getmtime)
                            self.last_folder = self.last_folder_tmp
                        except:
                            # no more subfolders
                            self.data_folder = os.path.realpath(self.last_folder)
                            self.no_data = 0
            except:
                pass
        
        if self.data_folder != '' and self.analysis_folder == '':
            if os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
            else:
                self.analysis_folder = self.data_folder
        
        self.agbe_list=[]
        rrr = re.compile('agbe.*0+1\.(?:tif|cbf)',flags=re.I)
        for self.file in os.listdir(self.data_folder):
            if rrr.search(self.file):
                self.agbe_list.append(os.path.join(self.data_folder,self.file))
        
        self.agbe_list_s = sorted(self.agbe_list,key=lambda f: os.stat(f).st_mtime,reverse=True)
        if len(self.agbe_list_s) > 0:
            self.agbe_path = self.agbe_list_s[0]
            self.agbe_file= os.path.split(self.agbe_path)[1]
        else:
            self.agbe_file = ''
            self.empty_image()
        
        self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        
        try:
            self.expname = str(os.path.split(self.analysis_folder)[1].split('_')[1:])
            if self_expname == []:
                self.expname = ''
        except:
            self.expname = ''
        
        if self.agbe_file != '':
            self.cx1 = ''
            self.cx2 = ''    
            self.angles_list = []
            self.angles_width = []
        
            self.read_agbe()
            self.show_agbe()
            self.update_footer()
            self.update_buttons()
        
    def f_open_agbe(self):
        ''' manualy set agbe file 
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        elif self.analysis_folder != '':
            if os.path.isdir(os.path.normpath(os.path.join(self.analysis_folder,'../data/'))):
                self.start_folder = os.path.normpath(os.path.join(self.analysis_folder,'../data/'))
            else:
                self.start_folder = self.analysis_folder
        else:
            self.start_folder = os.path.normpath(os.path.abspath('.'))
        
        self.agbe_path = str(QFileDialog.getOpenFileName(self,"Select agbe file", self.start_folder,".tif files (*.cbf *.tif *.tiff);;All Files (*)", options=options))
        self.data_folder,self.agbe_file = os.path.split(self.agbe_path)
        if self.analysis_folder == '':
            try:
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis'+self.data_folder.split('/')[-1].split('data')[1]))
                if not os.path.isdir(self.analysis_folder):
                    if os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                        self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
                    else:
                        self.analysis_folder = self.data_folder
            except:
                if os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                    self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
                else:
                    self.analysis_folder = self.data_folder
            
        self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        
        try:
            self.expname = str(os.path.split(self.analysis_folder)[1].split('_')[1:])
            if self_expname == []:
                self.expname = ''
        except:
            self.expname = ''
        
        

        self.cx1 = ''    
        self.cx2 = ''
        self.angles_list = []
        self.angles_width = []
        
        self.read_agbe()
        self.show_agbe()
        self.update_buttons()
        self.update_footer()
        self.q_reset_cm()
    
    def f_set_analysis(self):
        ''' manualy set analysis folder
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
            self.start_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        elif self.data_folder != '':
            self.start_folder = self.data_folder
        elif self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder = os.path.normpath(os.path.abspath('.'))
        self.analysis_folder = str(QFileDialog.getExistingDirectory(self,'Select analysis folder',self.start_folder,QFileDialog.ShowDirsOnly))
        self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        self.update_footer()

    def read_agbe(self):
        ''' open agbe file
        '''
        app.setOverrideCursor(QCursor(Qt.WaitCursor))

        if os.path.isfile(self.agbe_path):
            #self.agbe_2d_im = Image.open(self.agbe_path)#.convert('I')
            #self.agbe_2d = numpy.array(self.agbe_2d_im)
            
            self.agbe_2d = fabio.open(self.agbe_path).data.astype('int32')
            
            self.gray_data = self.agbe_2d.astype('int32')
            self.gray_data_log = numpy.log(numpy.clip(self.gray_data,0.01,None))
            #self.gray_data_orig = self.agbe_2d.astype('int32')
            self.agbe_size_v = int(self.agbe_2d.shape[0])
            self.agbe_size_h = int(self.agbe_2d.shape[1])
            
            #self.agbe_data_max = numpy.amax(self.gray_data)
            self.agbe_data_max = 1.3*heapq.nlargest(5,self.gray_data.flatten())[-1]
            #self.agbe_data_min = numpy.amin(self.gray_data)
            self.agbe_data_min = -10
            #self.agbe_data_log_max = numpy.amax(self.gray_data_log)
            self.agbe_data_log_max = 1+heapq.nlargest(5,self.gray_data_log.flatten())[-1]
            #self.agbe_data_log_min = numpy.amin(self.gray_data_log)
            self.agbe_data_log_min = -1
            
            if os.path.isfile(os.path.join(self.data_folder,self.agbe_file[:-3]+'prp')):
                with open(os.path.join(self.data_folder,self.agbe_file[:-3]+'prp')) as agbe_prp:
                    for line in agbe_prp.readlines():
                        if 'Beam energy' in line:
                            e_kev=float(line.split('=')[1].split(' ')[0])/1000
                        if 'Pipe length' in line:
                            det_dist=float(line.split("=")[1].split(' ')[0])+150
                        if 'Detector mode' in line:
                            try:
                                # if detector mode is a number ('0' or '1' in .prp) -> Rayonix
                                int(line.split('=')[1][0])
                                self.det='mx'
                                self.det_bin = int(6144/self.agbe_size_h)
                                px_size = 225000/6144*self.det_bin
                                self.px_size.setValue(px_size)
                            except:
                                # if detector mode is not a number ('Pilatus' in .prp) -> Pilatus
                                self.det='pil'
                                px_size = 172
                                self.px_size.setValue(px_size)
                        if 'I_2' in line:
                            self.i2 = float(line.split('=')[1].split(' ')[0])
                self.energy_value.setValue(e_kev)
                self.app_distance_value.setValue(det_dist)
                #self.app_first_ring()
            
            self.show_angles()
        app.restoreOverrideCursor()

    def app_first_ring(self):
        ''' calulate approximate raidus of the first agbe ring
            px_size is in meters
        '''
        if (self.app_distance_value.value() != 0 and self.energy_value.value() !=0 and self.px_size.value() != 0):
            
            agbe_ring_est = (self.app_distance_value.value()/1000)*numpy.tan(2*numpy.arcsin((12.3984/self.energy_value.value())*(2*numpy.pi/self.d_agbe)/(4*numpy.pi)))/(self.px_size.value()*1e-6)
            # if if ring position < 100px, use a different ring
            ring_no = int(numpy.ceil(100/agbe_ring_est))
            
            if self.det == 'mx':
                px_peak_search_rg = int(min(160/self.det_bin,agbe_ring_est/2))
            else:
                px_peak_search_rg = int(min(40,agbe_ring_est/2))
            
            
            self.radius1_px.setValue(agbe_ring_est)
            self.radius1_width.setValue(px_peak_search_rg)
            self.ring_num.setValue(ring_no)
        else:
            self.radius1_px.setValue(0)
        
    def f_find_center_1(self):
        ''' find center (rough) using autocorrelation
        '''
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        data_corr = signal.fftconvolve(self.agbe_2d,self.agbe_2d)
        cx_crmax, cy_crmax = numpy.unravel_index(numpy.argmax(data_corr), data_corr.shape)
        self.cx1 = (1.0*cy_crmax+1)/2
        self.cy1 = (1.0*cx_crmax+1)/2
        self.add_target()
        #center_pix = self.graphicsView.mapToScene(self.graphicsView.viewport().rect()).boundingRect().center()
        self.cx2 = ''
        self.show_angles()
        #self.graphicsView.centerOn(center_pix)
        self.update_footer()
        self.update_buttons()
        
        app.restoreOverrideCursor()
        
    def f_set_center_1(self):
        ''' set center rough (manual) button is clicked
        '''
        if self.set_angles_man.isChecked():
            self.set_angles_man.setChecked(False)
        elif self.zoom_select.isChecked():
            self.zoom_select.setChecked(False)
        elif self.set_center_1.isChecked():
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.graphicsView.setDragMode(QGraphicsView.NoDrag)
        self.status_message()
    
    def set_center_point(self,xx,yy):
        ''' get the center point of zoomed-in graph
        '''
        self.cx1 = round(xx,1)
        self.cy1 = round(yy,1)
        #self.add_target()
        #center_pix = self.graphicsView.mapToScene(self.graphicsView.viewport().rect()).boundingRect().center()
        self.cx2 = ''
        self.show_angles()
        #self.graphicsView.centerOn(center_pix)
        self.update_footer()
        self.update_buttons()
        
    def f_set_angles_auto(self):
        ''' set angles for rign search
        '''
        self.angles_list = 45 + numpy.arange(-180,180,90)
        self.angles_width = numpy.array([self.angles_val.value()]*4)
        self.show_angles()
        
    def f_set_angles_man(self):
        ''' select angles for ring search
        '''
        if self.set_center_1.isChecked():
            self.set_center_1.setChecked(False)
        elif self.zoom_select.isChecked():
            self.zoom_select.setChecked(False)
        elif self.set_angles_man.isChecked():
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.graphicsView.setDragMode(QGraphicsView.NoDrag)
        self.status_message()

    def set_angles_point(self,xx,yy):
        '''ads or removes an angle
        '''
        ang = numpy.degrees(numpy.arctan2(yy-self.cy1,xx-self.cx1))
        dd = []
        if len(self.angles_list)>0:
            for ind in range(len(self.angles_list)):
                if (numpy.abs(self.angles_list[ind] - ang)< ((self.angles_val.value()+self.angles_width[ind])*0.5) ):
                    dd.append(ind)
            
        if dd != []:
            self.angles_list = numpy.delete(self.angles_list,dd)
            self.angles_width = numpy.delete(self.angles_width,dd)
        else:
            self.angles_list = numpy.append(self.angles_list,ang)
            self.angles_width = numpy.append(self.angles_width,self.angles_val.value())
        
        self.show_angles()
    
    def refine_center(self):
        ''' perform a routine to refine center position
        '''
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        cen_fold = os.path.join(self.analysis_folder,'cen')
        integcenmpp = os.path.join(cen_fold,'integ_cen.mpp')
        ang_mask_path = os.path.join(cen_fold,'angular_mask.tif')
        agbe_datfile = os.path.join(cen_fold,self.agbe_file[:-3]+'dat')
        logfile = time.strftime("%Y%m%d")+'_cen.log'
        
        if not os.path.isdir(cen_fold):
            os.makedirs(cen_fold)
        
        # create a new integcenmpp file
        
        ff = open(integcenmpp,'w')
        ff.write('-l '+logfile+'\n')
        ff.write('-c {0:.3f} {1:.3f}\n'.format(self.cx1,self.agbe_size_v-self.cy1))
        if self.i2 > 10:
            ff.write('-i yes i2 1000\n')
        #ff.write('-r yes 1.5\n')
        ff.write('-z yes 3.5\n')
        if self.det == 'pil':
            ff.write('-o 0\n')
        ff.write('-m yes {}\n'.format(os.path.relpath(ang_mask_path,cen_fold)))
        ff.write('-f {}\n'.format(os.path.relpath(self.agbe_path,cen_fold)))
        ff.close()
        
        # How many times to perform refinement
        repeats = 2
        
        ref_cen = numpy.zeros([repeats+1,2])
        ref_cen[0,:] = [self.cx1,self.agbe_size_v-self.cy1]
        
        num_of_ang = self.angles_list.size
        
        agbe_ring_dir = numpy.zeros([repeats,num_of_ang])
        #os.chdir(self.analysis_folder)
        
        for rr in range(1,repeats+1):
            for i in range(num_of_ang):
                for line in fileinput.input(integcenmpp, inplace = 1):
                    line = re.sub('^-c \d+(\.\d+)? \d+(\.\d+)?','-c '+str(ref_cen[rr-1,0])+' '+str(ref_cen[rr-1,1]),line)
                    sys.stdout.write(line)
                make_ang_mask(self.agbe_size_h,self.agbe_size_v,ref_cen[rr-1,0],ref_cen[rr-1,1],self.angles_list[i],self.angles_width[i],ang_mask_path)
                pp = subprocess.Popen(['sastool',integcenmpp],cwd = cen_fold)
                pp.communicate()
                agbe_1d = numpy.genfromtxt(agbe_datfile)
                shutil.move(agbe_datfile,os.path.join(cen_fold,self.agbe_file[:-4]+'_r'+str(rr)+'_d'+str(i)+'.dat'))
                agbe_max_px = agbe_1d[int(self.ring_num.value()*self.radius1_px.value()-self.radius1_width.value()):int(self.ring_num.value()*self.radius1_px.value()+self.radius1_width.value()),1].argmax() + self.ring_num.value()*self.radius1_px.value()-self.radius1_width.value()
                coeff, var_matrix = fit1dgauss(agbe_1d,agbe_max_px,self.radius1_px.value()/2,self.det_bin)
                agbe_ring_dir[rr-1,i] = coeff[1]
                if rr == repeats:
                    agbe_1d[:,1] = numpy.maximum(agbe_1d[:,1],.1)
                    #plt.plot(agbe_1d[:,0],numpy.log(agbe_1d[:,1]))
            
            #x_del = 0
            #y_del = 0
            #for j in range(0,4):
            #    x_del = x_del+agbe_ring_dir[rr-1,j]*numpy.cos(numpy.radians(start_ang+90*j))/2
            #    y_del = y_del+agbe_ring_dir[rr-1,j]*numpy.sin(numpy.radians(start_ang+90*j))/2
            ring_points_x = numpy.zeros(num_of_ang)
            ring_points_y = numpy.zeros(num_of_ang)
            # set ring positions or circle fitting
            for j in range(num_of_ang):
                ring_points_x[j] = ref_cen[rr-1,0]+agbe_ring_dir[rr-1,j]*numpy.cos(numpy.radians(self.angles_list[j]))
                ring_points_y[j] = ref_cen[rr-1,1]-agbe_ring_dir[rr-1,j]*numpy.sin(numpy.radians(self.angles_list[j]))
            
            rx_m = numpy.mean(ring_points_x)
            ry_m = numpy.mean(ring_points_y)
            
            center_2, ier = leastsq(f_2,(rx_m,ry_m),args=(ring_points_x,ring_points_y))
            
            ref_cen[rr,0] = center_2[0]
            ref_cen[rr,1] = center_2[1]
            
        #shutil.move(integcenmpp,os.path.join(cen_fold,'integ_cen.mpp'))
        #shutil.move(ang_mask_path,os.path.join(cen_fold,os.path.split(ang_mask_path)[1]))
        #shutil.move(os.path.join(self.analysis_folder,logfile),os.path.join(cen_fold,logfile))
        #shutil.move(os.path.join(self.analysis_folder,self.agbe_file[:-3]+'log'),os.path.join(cen_fold,self.agbe_file[:-3]+'log'))
        #shutil.move(os.path.join(self.analysis_folder,self.agbe_file[:-3]+'tot'),os.path.join(cen_fold,self.agbe_file[:-3]+'tot'))
        self.cx2 = ref_cen[-1,0]
        self.cy2 = self.agbe_size_v-ref_cen[-1,1]
        
        self.show_angles()
        self.update_footer()
        self.update_buttons()
    
        app.restoreOverrideCursor()
        
    def f_q_calib(self):
        ''' q calibration
        '''
        self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        no_mask = os.path.join(self.analysis_folder,'no_mask.tif')
        agbe_datfile = os.path.join(self.analysis_folder,self.agbe_file[:-3]+'dat')
        # redefine self.ring_px, lcear the values if it was already calculated
        self.ring_px = numpy.array([0])
        
        # Check if the mask file exists
        mask_msk = ''
        mask_tif = ''
        try:
            mask_tif = max(glob.iglob(os.path.join(self.analysis_folder,'mask*.tif')), key=os.path.getctime)
        except:
            pass
        
        if mask_tif == '':
            try:
                mask_msk = max(glob.iglob(os.path.join(self.analysis_folder,'*msk')), key=os.path.getctime)
            except:
                pass
    
        if mask_msk != '':
            #os.chdir(self.analysis_folder)
            pp=subprocess.Popen(['msk2tif.py',mask_msk],cwd = self.analysis_folder)
            pp.communicate()
            mask_tif = mask_msk[:-3]+'tif'
        
        
        # Prepare integ.mpp
        if os.path.exists(self.integmpp):
            # change existing file
            for line in fileinput.input(self.integmpp, inplace = 1):
                line = re.sub('^-c \d+(\.\d+)? \d+(\.\d+)?','-c {0:.3f} {1:.3f}'.format(self.cx2,self.agbe_size_v-self.cy2),line)
                if mask_tif!='':
                    line = re.sub('^#*-m.*$','-m yes {}'.format(os.path.relpath(mask_tif,self.analysis_folder)),line)
                else:
                    line = re.sub('^#*-m.*$','-m yes no_mask.tif',line)
                if self.expname == '':
                    line = re.sub('^-l.*$','-l '+time.strftime("%Y%m%d")+'.log',line)
                else:
                    line = re.sub('^-l.*$','-l '+time.strftime("%Y%m%d")+'_'+self.expname+'.log',line)
                line = re.sub('^-q yes','#-q yes',line)
                line = re.sub('^-q wide','#-q wide',line)
                line = re.sub('^-s','#-s',line)
                line = re.sub('^-f','#-f',line)
                if 'i2' in line:
                    if self.i2 < 10:
                        line = re.sub('^-i','#-i',line)
                if (not line.startswith('-a') and not re.match(r'^\s*$', line)):        
                    sys.stdout.write(line)
        
            ff = open(self.integmpp,'a')
            ff.write('-f {}\n'.format(os.path.relpath(self.agbe_path,self.analysis_folder)))
            ff.close()
        

        else:
            # make a new file
            ff = open(self.integmpp,'w')
            if self.expname == '':
                ff.write('-l '+time.strftime("%Y%m%d")+'.log\n')
            else:
                ff.write('-l '+time.strftime("%Y%m%d")+'_'+self.expname+'.log\n')
            ff.write('-c {0:.3f} {1:.3f}\n'.format(self.cx2,self.agbe_size_v-self.cy2))
            ff.write('#-q yes 0 0\n')
            if self.i2 > 10:
                ff.write('-i yes i2 10000000\n')
            ff.write('-r yes 1.5\n')
            ff.write('#-s yes\n')
            if mask_tif != '':
                ff.write('-m yes {} \n'.format(os.path.relpath(mask_tif,self.analysis_folder)))
            else:
                ff.write('-m yes no_mask.tif\n')
            ff.write('-z yes 3.5\n')
            if self.det == 'pil':
                ff.write('-o 0\n')
            ff.write('-f {}\n'.format(os.path.relpath(self.agbe_path,self.analysis_folder)))
            ff.close()
        
        if mask_tif=='':
            make_ang_mask(self.agbe_size_h,self.agbe_size_v,1,1,-45,90,no_mask)

        
        pp = subprocess.Popen(['sastool',self.integmpp],cwd = self.analysis_folder)
        pp.communicate()
        
        agbe_1d = numpy.genfromtxt(agbe_datfile)
        

        
        # find the position of the first agbe ring
        rad_max = int(agbe_1d[int(self.radius1_px.value()-self.radius1_width.value()):int(self.radius1_px.value()+1.5*self.radius1_width.value()),1].argmax()+self.radius1_px.value()-self.radius1_width.value())
        
        coeff, var_matrix = fit1dgauss(agbe_1d,rad_max,self.radius1_px.value()/2,self.det_bin)
        self.ring_px = numpy.append(self.ring_px,coeff[1])
        
        
        for jj in range(2,6):
            if self.det == 'mx':
                px_peak_search_rg = int(min(160/self.det_bin,jj*self.radius1_px.value()/2))
            else:
                px_peak_search_rg = int(min(40,jj*self.radius1_px.value()/2))
            px_num_g = int(px_peak_search_rg/2)
            if agbe_1d.shape[0] > int(jj * self.ring_px[1] + px_num_g):
                try:                  
                    coeff, var_matrix = fit1dgauss(agbe_1d,jj*self.ring_px[jj-1]/(jj-1),px_num_g,self.det_bin)
                    self.ring_px = numpy.append(self.ring_px,coeff[1])
                except:
                    pass
        
        # calculate distance
        distance = []
        for yy in range(1,len(self.ring_px)):
            dd = self.ring_px[yy]*(self.px_size.value()*1e-6)/numpy.tan(2*yy*numpy.arcsin((12.3984/self.energy_value.value())/(2*self.d_agbe)))
            distance = numpy.append(distance,dd)
            print("ring {}: distance {} mm".format(yy,dd))
        if len(distance)>3:
            dist_start = 1
        else:
            dist_start = 0
        
        
        self.calc_dist = 1000*numpy.mean(distance[dist_start:])
            
        self.show_angles()
            
    def f_save_integ(self):
        ''' save integ.mpp
        '''
        no_mask = os.path.join(self.analysis_folder,'no_mask.tif')

        # Check if the mask file exists
        mask_msk = ''
        mask_tif = ''
        try:
            mask_tif = max(glob.iglob(os.path.join(self.analysis_folder,'mask*.tif')), key=os.path.getctime)
        except:
            pass
        
        if mask_tif == '':
            try:
                mask_msk = max(glob.iglob(os.path.join(self.analysis_folder,'*msk')), key=os.path.getctime)
            except:
                pass
    
        if mask_msk != '':
            #os.chdir(self.analysis_folder)
            pp=subprocess.Popen(['msk2tif.py',mask_msk],cwd = self.analysis_folder)
            pp.communicate()
            mask_tif = mask_msk[:-3]+'tif'
            
        #if mask_tif=='':
        #    make_ang_mask(self.agbe_size_h,self.agbe_size_v,1,1,-45,90,no_mask)
        
        if mask_tif !='':
            mask_tif = os.path.relpath(mask_tif,self.analysis_folder)
        
        # add q info into integmpp
        qinfo = ''
        for jj in range(len(self.ring_px)):
            qinfo = qinfo+' '+'{:.2f}'.format(self.ring_px[jj])+' '+str(jj*0.1076)
         
        if not os.path.exists(self.integmpp):
            ff = open(self.integmpp,'w')
            if self.expname == '':
                ff.write('-l '+time.strftime("%Y%m%d")+'.log\n')
            else:
                ff.write('-l '+time.strftime("%Y%m%d")+'_'+self.expname+'.log\n')
            ff.write('-c {0:.3f} {1:.3f}\n'.format(self.cx2,self.agbe_size_v-self.cy2))
            ff.write('-q yes{}\n'.format(qinfo))
            if self.i2 > 10:
                ff.write('-i yes i2 10000000\n')
            ff.write('-r yes 1.5\n')
            ff.write('#-s yes\n')
            if mask_tif != '':
                ff.write('-m yes {} \n'.format(mask_tif))
            else:
                ff.write('-m yes {0}\n'.format(no_mask))
            ff.write('-z yes 3.5\n')
            if self.det == 'pil':
                ff.write('-o 0\n')
            ff.write('-f {} \n'.format(self.agbe_path))
            ff.close() 
        else: 
            for line in fileinput.input(self.integmpp, inplace = 1):
                line = re.sub('.*-q .*','-q yes'+qinfo,line)
                sys.stdout.write(line)
    
        
        if self.calc_dist < 1000:
            for line in fileinput.input(self.integmpp, inplace = 1):
                if '-q' in line:
                    line = '-q wide {0:.0f} {1}\n'.format(self.energy_value.value()*1000,self.calc_dist)
                sys.stdout.write(line)
        
        pp = subprocess.Popen(['sastool',self.integmpp],cwd = self.analysis_folder)
        pp.communicate()
        
        # display message
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("integ.mpp saved")
        msg.setWindowTitle("All done")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    
    def show_agbe(self):
        ''' show agbe image
        '''
        if self.gray_data.any() != 0:
            self.update_scale()
            self.update_agbe()
            
            self.scene.setSceneRect(-30,-30,self.agbe_size_h+60,self.agbe_size_v+60)
            self.update_buttons()
            self.graphicsView.fitInView(self.show_image,Qt.KeepAspectRatio)
            # get the initial zoom after fitinView
            self.init_zoom = self.graphicsView.transform().m11()
            # update zoom slider value
            self.zoom_slider.setValue(int(100*self.init_zoom))
            
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            
    def update_scale(self):
        if self.q_log.isChecked():
            self.max_slider.setValue(round(10000*numpy.log(max(0.01,self.max_spin.value()))))
            self.min_slider.setValue(round(10000*numpy.log(max(0.01,self.min_spin.value()))))
            self.min_slider.setMinimum(-40000)
            #self.min_slider.setMaximum(int(100*(2+2.5*self.agbe_data_log_max)))
            self.min_slider.setMaximum(170000)
            self.max_slider.setMinimum(-40000)
            #self.max_slider.setMaximum(int(100*(2+2.5*self.agbe_data_log_max)))
            self.max_slider.setMaximum(170000)
            self.min_slider.setSingleStep(1000)
            self.max_slider.setSingleStep(1000)
            self.min_slider.setPageStep(10000)
            self.max_slider.setPageStep(10000)
            #self.min_spin.setSingleStep(0.1)
            self.max_spin.setMinimum(0.01)
            self.min_spin.setMinimum(0.01)
        else:
            self.min_slider.setMinimum(-10000)
            self.min_slider.setMaximum(round(100*2*self.agbe_data_max+50))
            self.max_slider.setMinimum(-10000)
            self.max_slider.setMaximum(round(100*2*self.agbe_data_max+50))
            self.min_slider.setSingleStep(5000)
            self.max_slider.setSingleStep(5000)
            self.min_slider.setPageStep(50000)
            self.max_slider.setPageStep(50000)
            #self.min_spin.setSingleStep(1)
            #self.max_spin.setSingleStep(1)
            self.max_spin.setMinimum(-100)
            self.min_spin.setMinimum(-100)
            self.max_slider.setValue(round(100*self.max_spin.value()))
            self.min_slider.setValue(round(100*self.min_spin.value()))
            
    
    def q_log_change(self):
       
        self.update_scale()
        
        self.update_agbe()
            
    def update_agbe(self):
        if self.q_log.isChecked():
            self.pix_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.gray_data_log,numpy.log(self.scale_min),numpy.log(self.scale_max)))))
        else:
            self.pix_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.gray_data,self.scale_min,self.scale_max))))
        
        self.show_image.setPixmap(self.pix_data)
    
           
    def show_angles(self):
        ''' show center/angles/rings when needed
        '''
        # remove old center/angles/ring if exist
        ooo = self.scene.items()
        for pp in ooo:
            if 'Pixmap' not in str(pp):
                self.scene.removeItem(pp)
        
        
        if self.cx2 != '':
            self.add_target2()
            
            if len(self.ring_px)>1:
                pen = QPen(QColor(100, 100, 100, 200))
                pen.setWidth(5)
                brush = QBrush(QColor(0,0,0,0))
                for rn in numpy.arange(1,len(self.ring_px)):
                    self.scene.addEllipse(numpy.double(self.cx2)-self.ring_px[rn], numpy.double(self.cy2)-self.ring_px[rn], 2*self.ring_px[rn], 2*self.ring_px[rn], pen, brush)
            
        else:
            # add center target
            if self.cx1 != '':
                self.add_target()
            
                # add angles
                if len(self.angles_list) >0:
                    angles_area = QPainterPath()
                    rmax = self.ring_num.value()*self.radius1_px.value()+self.radius1_width.value()+40
                    for aa,jj in zip(self.angles_list,self.angles_width):
                        angles_area.addPolygon(QPolygonF([QPointF(numpy.double(self.cx1),numpy.double(self.cy1)), QPointF(numpy.double(self.cx1+rmax*numpy.cos(numpy.deg2rad(aa+jj))),numpy.double(self.cy1+rmax*numpy.sin(numpy.deg2rad(aa+jj)))), QPointF(numpy.double(self.cx1+rmax*numpy.cos(numpy.deg2rad(aa-jj))),numpy.double(self.cy1+rmax*numpy.sin(numpy.deg2rad(aa-jj))))]))
                        
                        #angles_area.addPolygon(QPolygonF([QPointF(numpy.double(self.cx1),numpy.double(self.cy1)), QPointF(*numpy.double(find_edge(self.agbe_size_h,self.agbe_size_v,self.cx1,self.cy1,aa+jj))),QPointF(*numpy.double(find_edge(self.agbe_size_h,self.agbe_size_v,self.cx1,self.cy1,aa-jj)))]))
                    
                    
                    self.scene.addPath(angles_area, QPen(QColor(0,0,255,150)), QBrush(QColor(0,0,255, 150)))
                    
                    
                # add agbe ring search area
                if self.radius1_px.value() > 0 and self.cx1 != '':
                    ring_search = QPainterPath()
                    rmax = self.ring_num.value()*self.radius1_px.value()+self.radius1_width.value()
                    rmin = self.ring_num.value()*self.radius1_px.value()-self.radius1_width.value()
                    ring_search.addEllipse(numpy.double(self.cx1)-rmax, numpy.double(self.cy1)-rmax, 2*rmax, 2*rmax)
                    ring_search.addEllipse(numpy.double(self.cx1)-rmin, numpy.double(self.cy1)-rmin, 2*rmin, 2*rmin)
                    
                    self.scene.addPath(ring_search, QPen(QColor(255,0,0, 100)), QBrush(QColor(255,0,0, 100)))

        # update footer/buttons
        self.update_footer()
        self.update_buttons()
            
        
    def do_all(self):
        ''' find center automatically
        '''
        self.find_fol_agbe()
        if self.energy_value.value() != 0 and self.app_distance_value.value != 0:
            self.app_first_ring()
            self.f_find_center_1()
            self.f_set_angles_auto()
            self.refine_center()
            self.f_q_calib()
            self.f_save_integ()
            
            
        self.update_footer()
        self.update_buttons()
        
    def empty_image(self):
        ''' empty image to show if there is no agbe
        '''
        self.gray_data = numpy.array(0)
        self.empty = QPixmap(10,10)
        self.empty.fill(QColor(200,200,200))
        self.scene.clear()
        self.create_scene()
        self.show_image.setPixmap(self.empty)
        self.graphicsView.fitInView(self.show_image,Qt.KeepAspectRatio)
    
    def add_target(self):
        ''' add target at the rough center
        '''
        step = numpy.ceil(self.agbe_size_v/500)
        rrr = numpy.arange(step*6,0,-step)
        for ttt in numpy.arange(len(rrr)):
            cc = 255*((-1)**ttt + 1)/2
            pen = QPen(QColor(cc, cc, cc, 50))
            brush = QBrush(pen.color())
            self.scene.addEllipse(numpy.double(self.cx1)-rrr[ttt], numpy.double(self.cy1)-rrr[ttt], 2*rrr[ttt], 2*rrr[ttt], pen, brush)
            
    def add_target2(self):
        ''' add target at the refined center
        '''
        step = numpy.ceil(self.agbe_size_v/500)
        rrr = numpy.arange(step*6,0,-step)
        for ttt in numpy.arange(len(rrr)):
            cc = 255*((-1)**ttt + 1)/2
            pen = QPen(QColor(cc, cc, 255, 150))
            brush = QBrush(pen.color())
            self.scene.addEllipse(numpy.double(self.cx2)-rrr[ttt], numpy.double(self.cy2)-rrr[ttt], 2*rrr[ttt], 2*rrr[ttt], pen, brush)
        
        
    
    def setup_change(self):
        ''' what to do if setup parameters change
        '''
        if self.energy_value.value() != 0 and self.app_distance_value.value != 0:
            self.app_first_ring()
            
    def f_zoom_select(self):
        if self.set_center_1.isChecked():
            self.set_center_1.setChecked(False)
        elif self.set_angles_man.isChecked():
            self.set_angles_man.setChecked(False)
        elif self.zoom_select.isChecked():
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.graphicsView.setDragMode(QGraphicsView.NoDrag)
        
        if not self.zoom_select.isChecked():
            self.zoom_select_points = []
            if self.zoom_pol != '':
                self.scene.removeItem(self.zoom_pol)
                self.zoom_pol = ''
        
        
        self.status_message()
        

    def zoom_into_select(self,xx,yy):
        if self.zoom_select_points == []:
            self.zoom_select_points.append(xx)
            self.zoom_select_points.append(yy)
        else:
            self.graphicsView.fitInView(min(xx,self.zoom_select_points[0]),min(yy,self.zoom_select_points[1]),abs(xx-self.zoom_select_points[0]),abs(yy-self.zoom_select_points[1]),Qt.KeepAspectRatio)
            self.zoom_select_points = []
            self.zoom_select.setChecked(False)
            self.graphicsView.setDragMode(QGraphicsView.ScrollHandDrag)
            self.scene.removeItem(self.zoom_pol)
            self.zoom_pol = ''
            #zoom = int(100*self.graphicsView.transform().m11()/self.init_zoom)
            zoom = int(100*self.graphicsView.transform().m11())
            if zoom <= 10000:
                self.zoom_slider.setValue(zoom)
            else:
                self.zoom_slider.setValue(10000)
                #self.graphicsView.scale(10000/zoom,10000/zoom)
            self.status_message()
        
    
    def zoom_spin_changed(self,vvv):
        ''' zoom change
        '''
        # current zoom
        self.zoom = self.graphicsView.transform().m11()
        # how much to zoom in this step
        self.zoom_factor = vvv/(100*self.zoom)
        # make zoom
        self.graphicsView.scale(self.zoom_factor, self.zoom_factor)
        # update spinbox value
        if self.zoom_slider.value() != vvv:
            self.zoom_slider.setValue(vvv)
        
    def zoom_changed(self,vvv):
        if not ((vvv == self.zoom_slider.maximum() and self.zoom_spin.value() > vvv) or (vvv == self.zoom_slider.minimum() and self.zoom_spin.value() < vvv) ):
            self.zoom_spin.setValue(vvv)
    
    
    
    def max_spin_changed(self,vvv):
        ''' lut maximum change
        '''
        center_pix = self.graphicsView.mapToScene(self.graphicsView.viewport().rect()).boundingRect().center()
        self.scale_max = vvv
        self.update_agbe()
        self.graphicsView.centerOn(center_pix)
        if self.q_log.isChecked():
            self.max_slider.setValue(round(10000*numpy.log(vvv)))
        else:
            self.max_slider.setValue(100*round(vvv))
        
    
    def max_changed(self,vvv):
        if self.q_log.isChecked():
            bbb = numpy.log(self.max_spin.value())*10000
        else:
            bbb = self.max_spin.value()*100
        
        if not ((vvv == self.max_slider.maximum() and bbb > vvv) or (vvv == self.max_slider.minimum() and bbb < vvv) ):
            if self.q_log.isChecked():
                if perc_dif(vvv,bbb) > 1 or abs(vvv/100)<1:
                    self.max_spin.setValue(numpy.exp(vvv/10000))
            else:
                self.max_spin.setValue(vvv/100)
        
        
    def min_spin_changed(self,vvv):
        ''' lut minimum change
        '''
        center_pix = self.graphicsView.mapToScene(self.graphicsView.viewport().rect()).boundingRect().center()
        self.scale_min = vvv
        self.update_agbe()
        self.graphicsView.centerOn(center_pix)
        if self.q_log.isChecked():
            self.min_slider.setValue(round(10000*numpy.log(vvv)))
        else:
            self.min_slider.setValue(round(100*vvv))      
            
    def min_changed(self,vvv):
        if self.q_log.isChecked():
            bbb = numpy.log(self.min_spin.value())*10000
        else:
            bbb = self.min_spin.value()*100
            
        if not ((vvv == self.min_slider.maximum() and bbb > vvv) or (vvv == self.min_slider.minimum() and bbb < vvv) ):
            if self.q_log.isChecked():
                if perc_dif(vvv,bbb) > 1 or abs(vvv)<1:
                    self.min_spin.setValue(numpy.exp(vvv/10000))
            else:
                self.min_spin.setValue(vvv/100)
    
    
    def q_reset_cm(self):
        self.min_spin.setValue(max(0.01,self.agbe_data_min*0.9))
        self.max_spin.setValue(self.agbe_data_max*1.1)
    
    def create_scene(self):
        ''' create scene in qgraphicsview in q_calib tab
        '''
        self.scene = QGraphicsScene(self.graphicsView)
        self.graphicsView.setScene(self.scene)
        self.show_image = QGraphicsPixmapItem()
        self.scene.addItem(self.show_image)
                       
        
    def update_buttons(self):
        ''' enable/disable buttons in q_calib tab, as needed
        '''
        if self.agbe_file != '':
            self.find_center_1.setEnabled(True)
            self.set_center_1.setEnabled(True)
            self.energy_label.setEnabled(True)
            self.app_distance_label.setEnabled(True)
            self.radius1_px_label.setEnabled(True)
            self.radius1_width_label.setEnabled(True)
            self.energy_value.setEnabled(True)
            self.app_distance_value.setEnabled(True)
            self.radius1_px.setEnabled(True)
            self.radius1_width.setEnabled(True)
            self.ring_num_label.setEnabled(True)
            self.ring_num.setEnabled(True)
            self.px_size_label.setEnabled(True)
            self.px_size.setEnabled(True)
            self.zoom_spin.setEnabled(True)
            self.max_spin.setEnabled(True)
            self.min_spin.setEnabled(True)
            
        else:
            self.find_center_1.setEnabled(False)
            self.set_center_1.setEnabled(False)
            self.set_center_1.setChecked(False)
            self.energy_label.setEnabled(False)
            self.app_distance_label.setEnabled(False)
            self.radius1_px_label.setEnabled(False)
            self.radius1_width_label.setEnabled(False)
            self.energy_value.setEnabled(False)
            self.app_distance_value.setEnabled(False)
            self.radius1_px.setEnabled(False)
            self.radius1_width.setEnabled(False)
            self.ring_num_label.setEnabled(False)
            self.ring_num.setEnabled(False)
            self.px_size_label.setEnabled(False)
            self.px_size.setEnabled(False)
            self.zoom_spin.setEnabled(False)
            self.max_spin.setEnabled(False)
            self.min_spin.setEnabled(False)
            self.empty_image()
            self.zoom_slider.setProperty("value", 100)
            self.max_slider.setProperty("value", 255)
            self.min_slider.setProperty("value", -2)
            self.cx1 = ''
            self.cy1 = ''
        
        if self.gray_data.any() != 0:
            self.max_slider.setEnabled(True)
            self.min_slider.setEnabled(True)
            self.lut_max.setEnabled(True)
            self.lut_min.setEnabled(True)
            self.zoom_slider.setEnabled(True)
            self.zoom_label.setEnabled(True)
            self.zoom_select.setEnabled(True)
        
        else:  
            self.max_slider.setEnabled(False)
            self.min_slider.setEnabled(False)
            self.lut_max.setEnabled(False)
            self.lut_min.setEnabled(False)
            self.zoom_slider.setEnabled(False)
            self.zoom_label.setEnabled(False)
            self.zoom_select.setEnabled(False)
        
        if self.cx1 != '' and self.radius1_px.value() > 1:
            self.angles_val.setEnabled(True)
            self.angle_width_text.setEnabled(True)
            self.set_angles_auto.setEnabled(True)
            self.set_angles_man.setEnabled(True)
        else:
            self.angles_val.setEnabled(False)
            self.angle_width_text.setEnabled(False)
            self.set_angles_auto.setEnabled(False)
            self.set_angles_man.setEnabled(False)
            self.set_angles_man.setChecked(False)
            
        if self.cx1 != 0 and len(self.angles_list)>2 and self.radius1_px.value()>1:
            self.find_center_2.setEnabled(True)
        else:
            self.find_center_2.setEnabled(False)
         
        if self.cx2 != '':
            self.q_calib.setEnabled(True)
            self.save_integ.setEnabled(True)
        else:
            self.q_calib.setEnabled(False)
            self.save_integ.setEnabled(False)
        
    ## end of qcalib_tab
    
    ## mask_tab
    
    def f_open_im4mask(self):
        ''' open image(s) for mask drawing 
        '''
        options = QFileDialog.Options()
        # opening files works faster when using native dialog
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            #self.start_folder = os.path.expanduser('~')
        
        #self.im4mask_path = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".tif files (*.tif *.tiff);;All Files (*)", options=options))
        # open one or multiple images
        self.im4mask_list = QFileDialog.getOpenFileNames(self,"Select one or multiple files", self.start_folder,".cbf or .tif files (*.cbf *.tif *.tiff);;All Files (*)", options=options)
        
        # QFileDialog object method for selecting files
        #dlg = QFileDialog()
        #dlg.setFileMode(QFileDialog.ExistingFiles)
        #dlg.setFilter("tif files (*.tif)")
        #if dlg.exec_():
        #    self.im4mask_list = dlg.selectedFiles()
        
        
        
        #get info from the first image:
        self.im4mask_folder, self.im4mask = os.path.split(str(self.im4mask_list[0]))
        if self.data_folder == "":
            self.data_folder = self.im4mask_folder
        
        #self.im4mask_data = numpy.array(Image.open(str(self.im4mask_list[0])))#.convert('I')).astype('int32')
        #self.im4mask_data = fabio.open(str(self.im4mask_list[0])).data
        self.im4mask_data = 0
        # open image(s)
        #if len(self.im4mask_list)> 1:
        for ii in range(len(self.im4mask_list)):
        
            self.im4mask_data = self.im4mask_data + fabio.open(str(self.im4mask_list[ii])).data.astype('int32')
            #self.im4mask_data = self.im4mask_data + numpy.array(Image.open(str(self.im4mask_list[ii])))#.convert('I')).astype('int32')
            #self.im4mask_im = Image.open(ll).convert('I')
            #self.im4mask_data = self.im4mask_data + numpy.array(self.im4mask_im).astype('int32')
        
        if len(self.im4mask_list) > 1:
            self.im4mask_data = self.im4mask_data/len(self.im4mask_list)
        
        self.im4mask_size_v = int(self.im4mask_data.shape[0])
        self.im4mask_size_h = int(self.im4mask_data.shape[1])
        self.im4mask_data_log = numpy.log(numpy.clip(self.im4mask_data,0.01,None))
        #self.im4mask_data_max = numpy.amax(self.im4mask_data)
        self.im4mask_data_max = 1.3*heapq.nlargest(5,self.im4mask_data.flatten())[-1]
        #self.im4mask_data_min = numpy.amin(self.im4mask_data)
        self.im4mask_data_min = -10
        #self.im4mask_data_log_max = numpy.amax(self.im4mask_data_log)
        self.im4mask_data_log_max = 1+heapq.nlargest(5,self.im4mask_data_log.flatten())[-1]
        #self.im4mask_data_log_min = numpy.amin(self.im4mask_data_log)
        self.im4mask_data_log_min = -1
        
        
        # prepare mask and alpha channel(?) of the same size, if doesn't exist
        try:
            if self.im4mask_data.shape != self.masked_data.shape:
                self.masked_data = numpy.ones_like(self.im4mask_data).astype('uint16')

        except:
            self.masked_data = numpy.ones_like(self.im4mask_data).astype('uint16')
            
        #self.alpha = QPixmap(self.im4mask_size_h,self.im4mask_size_v)
        #self.alpha.fill(QColor(120,120,120))
        
        self.show_im4mask()
        self.show_masked()
        self.update_mask_buttons()
        
        
        
    def show_im4mask(self):
        if self.im4mask_data.any() != 0:
            self.update_mask_scale()
            if self.mask_log.isChecked():
                self.pix_mask_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im4mask_data_log,numpy.log(self.mask_scale_min),numpy.log(self.mask_scale_max)))))
            else:
                self.pix_mask_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im4mask_data,self.mask_scale_min,self.mask_scale_max))))
                
            # set transparency; deprecated method, but works
            self.mask_reset_cm()
            #aa = QPixmap(self.im4mask_size_h,self.im4mask_size_v)
            #aa.fill(QColor(120,120,120))
            #self.pix_mask_data.setAlphaChannel(aa)
            self.show_mask_image.setPixmap(self.pix_mask_data)
            self.mask_scene.setSceneRect(-100,-100,200+self.im4mask_size_h,200+self.im4mask_size_v)
            #self.update_buttons()
            self.graphicsView_mask.fitInView(self.show_mask_image,Qt.KeepAspectRatio)
            # get the initial zoom after fitinView, correct for the scroolbars (1.02031930334 or 1.02982731554 )
            #if self.zoom_mask.value()>100:
            #    self.init_mask_zoom = self.graphicsView_mask.transform().m11()*1.02031930334
                #self.init_mask_zoom = self.graphicsView_mask.transform().m11()*1.02982731554
            #else:
            self.init_mask_zoom = self.graphicsView_mask.transform().m11()
            
            # apply the current zoom from the slider value if different than 1
            #if self.zoom_mask.value()!=100:
                #self.graphicsView_mask.scale(self.zoom_mask.value()*1.02031930334/100,self.zoom_mask.value()*1.02031930334/100)
            #    self.graphicsView_mask.scale(self.zoom_mask.value()/100.0,self.zoom_mask.value()/100.0)
            
            self.zoom_mask.setValue(int(100*self.init_mask_zoom))
    
        app.setOverrideCursor(QCursor(Qt.ArrowCursor))
        
    def update_mask_scale(self):
            if self.mask_log.isChecked():
                self.max_mask.setValue(round(10000*numpy.log(max(0.01,self.max_mask_spin.value()))))
                self.min_mask.setValue(round(10000*numpy.log(max(0.01,self.min_mask_spin.value()))))
                self.min_mask.setMinimum(-40000)
                self.min_mask.setMaximum(170000)
                self.max_mask.setMinimum(-40000)
                self.max_mask.setMaximum(170000)
                self.min_mask.setSingleStep(1000)
                self.max_mask.setSingleStep(1000)
                self.min_mask.setPageStep(10000)
                self.max_mask.setPageStep(10000)
                #self.min_mask_spin.setSingleStep(0.1)
                #self.max_mask_spin.setSingleStep(0.1)
                self.max_mask_spin.setMinimum(0.01)
                self.min_mask_spin.setMinimum(0.01)
            else:
                self.min_mask.setMinimum(-10000)
                self.min_mask.setMaximum(round(100*2*self.im4mask_data_max+50))
                self.max_mask.setMinimum(-10000)
                self.max_mask.setMaximum(round(100*2*self.im4mask_data_max+50))
                self.min_mask.setSingleStep(5000)
                self.max_mask.setSingleStep(5000)
                self.min_mask.setPageStep(50000)
                self.max_mask.setPageStep(50000)
                #self.min_mask_spin.setSingleStep(1)
                #self.max_mask_spin.setSingleStep(1)
                self.max_mask_spin.setMinimum(-100)
                self.min_mask_spin.setMinimum(-100)
                self.max_mask.setValue(round(100*self.max_mask_spin.value()))
                self.min_mask.setValue(round(100*self.min_mask_spin.value()))     
    
    
    def update_mask(self):
        if self.mask_log.isChecked():
            self.pix_mask_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im4mask_data_log,numpy.log(self.mask_scale_min),numpy.log(self.mask_scale_max)))))
            #self.pix_mask_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im4mask_data_log,self.mask_scale_min*(2*self.im4mask_data_log_max-self.im4mask_data_log_min)/1000 + self.im4mask_data_log_min,self.mask_scale_max*(2*self.im4mask_data_log_max-self.im4mask_data_log_min)/1000 + self.im4mask_data_log_min))))
        else:
            self.pix_mask_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im4mask_data,self.mask_scale_min,self.mask_scale_max))))
            #self.pix_mask_data = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im4mask_data,self.mask_scale_min*(2*self.im4mask_data_max-self.im4mask_data_min)/1000 + self.im4mask_data_min,self.mask_scale_max*(2*self.im4mask_data_max-self.im4mask_data_min)/1000 + self.im4mask_data_min))))
        
        self.show_mask_image.setPixmap(self.pix_mask_data)
        
    def mask_log_change(self):
        
        
        self.update_mask_scale()
        
       
        self.update_mask()
    
    def import_msk_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            #self.start_folder = os.path.expanduser('~')
        
        self.msk_path = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,"mask files (*.msk *.tif *.tiff);;All Files (*)", options=options))
        
        
        
        if os.path.splitext(self.msk_path)[1] == '.msk':
            
            # # adapted from FabIO
            # 
            # f = open(self.msk_path, 'rb')
            # 
            # header = f.read(1024)
            # 
            # ttt = numpy.fromstring(header,numpy.uint32)
            # d1 = ttt[4]
            # d2 = ttt[5]
            # 
            # #d1 = header[16]*16**3+header[17]*16**2+header[18]*16+header[19]
            # #d2 = header[20]*16**3+header[21]*16**2+header[22]*16+header[23]
            # 
            # num_ints = (d1 + 31) // 32
            # 
            # total = d2 * num_ints * 4 
            # msk_data = f.read()
            # f.close()
            # 
            # 
            # 
            # msk_data = numpy.fromstring(msk_data, numpy.uint8)
            # msk_data = numpy.reshape(msk_data, (d2, num_ints * 4))
            # result = numpy.zeros((d2, num_ints * 4 * 8), numpy.uint8)
            # 
            # #bits = numpy.ones((1), numpy.uint8)
            # for i in range(8):
            #     #temp = numpy.bitwise_and(bits, data)
            #     temp = numpy.bitwise_and(1<<i, msk_data)
            #     result[:, i::8] = temp.astype(numpy.uint8)
            #     #bits = bits * 2
            # 
            # spares = num_ints * 4 * 8 - d1
            # if spares == 0:
            #     msk_pix = numpy.where(result == 0, 1, 0)
            # else:
            #     msk_pix = numpy.where(result[:, :-spares] == 0, 1, 0)
            #     
            # msk_pix = numpy.reshape(msk_pix.astype(numpy.uint16),(d2, d1))
            
            msk_pix = 1 - fabio.open(self.msk_path).data
            
            self.masked_data = numpy.flipud(msk_pix)
        
        elif os.path.splitext(self.msk_path)[1] in ('.tif','.tiff'):
            self.masked_data = numpy.array(Image.open(self.msk_path).convert('L')).clip(0,1)
        
        self.show_masked()
        
        
    # def import_tif_file(self):
    #     options = QFileDialog.Options()
    #     #options |= QFileDialog.DontUseNativeDialog
    #     if self.analysis_folder != '':
    #         self.start_folder = self.analysis_folder
    #     else:
    #         self.start_folder=os.path.normpath(os.path.abspath('.'))
    #         #self.start_folder = os.path.expanduser('~')
    #     
    #     tif_path = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".tif files (*.tif *.tiff);;All Files (*)", options=options))
    #     
    #     self.masked_data = numpy.array(Image.open(tif_path).convert('L')).clip(0,1)
    #     
    #     self.show_masked()
    
    def add_msk_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            #self.start_folder = os.path.expanduser('~')
        
        self.add_msk_path = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,"mask files (*.msk *.tif *.tiff);;All Files (*)", options=options))
        
        if os.path.splitext(self.add_msk_path)[1] == '.msk':
            add_msk_pix = numpy.flipud(1 - fabio.open(self.add_msk_path).data)
        
        elif os.path.splitext(self.add_msk_path)[1] in ('.tif','.tiff'):
            add_msk_pix = numpy.array(Image.open(self.add_msk_path).convert('L')).clip(0,1)
        
        self.masked_data = self.masked_data*add_msk_pix
        self.show_masked()
        
        
    
    
    def show_masked(self):
        ''' show mask as overlay
        '''
        # create inverse mask (1<->0) and zero-mask 
        inv_data = 1 - numpy.transpose(self.masked_data)
        zero_data = numpy.zeros_like(inv_data)
        # prepare data for rgba image; fully transparent where 0 in inverse mask, somewhat transparent red where 1
        rr = numpy.array([inv_data*255,zero_data,zero_data,inv_data*self.mask_opacity.value()])
        # reshape to correct shape 
        rr = rr.ravel(order='F').reshape(self.im4mask_size_v,self.im4mask_size_h,4).astype('uint8')
        # create QPixmap
        self.pix_masked = QPixmap.fromImage(ImageQt(Image.fromarray(rr)))
        
        self.show_mask_masked.setPixmap(self.pix_masked)
    
    def export_msk_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
        
        #savemsk_path = str(QFileDialog.getSaveFileName(self, 'Save .msk File',self.start_folder,".msk files (*.msk);;All Files (*)", options=options))
        #if not(savemsk_path.endswith('.msk')):
        #    savemsk_path += '.msk'
                
        dialog = QFileDialog()
        dialog.setFilter(dialog.filter() | QDir.Hidden)
        dialog.setDefaultSuffix('msk')
        dialog.setAcceptMode(QFileDialog.AcceptSave)
        dialog.setNameFilters(['.msk files (*.msk)','All files (*)'])
        dialog.setDirectory(self.start_folder)
        
        if dialog.exec_() == QDialog.Accepted:
            savemsk_path = str(dialog.selectedFiles()[0])
        
            if self.analysis_folder == '':
                self.analysis_folder = os.path.split(savemsk_path)[0]
            

            # adapted from FabIO
    
            #if sys.version < '3':
            #    bytes = str
            
            header = bytearray(b"\x00" * 1024)
            header[0] = 77  # M
            header[4] = 65  # A
            header[8] = 83  # S
            header[12] = 75  # K
            header[24] = 1  # 1
            header[16:20] = struct.pack("<I", self.im4mask_size_h)
            header[20:24] = struct.pack("<I", self.im4mask_size_v)
            
            data_to_write = numpy.zeros((self.im4mask_size_v, ((self.im4mask_size_h + 31) // 32) * 4), dtype=numpy.uint8)
            expanded_data = numpy.zeros((self.im4mask_size_v, ((self.im4mask_size_h + 31) // 32) * 32), dtype=numpy.uint8)
            expanded_data[:self.im4mask_size_v, :self.im4mask_size_h] = (numpy.flipud(self.masked_data) != 1)
            for i in range(8):
                order = (1 << i)
                data_to_write += expanded_data[:, i::8] * order
            with open(savemsk_path, mode="wb") as outfile:
                outfile.write(bytes(header))
                outfile.write(data_to_write.tostring())
        
        self.update_mask_buttons()
        
    def export_tif_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
        
       
        #self.savemask_tif_path = str(QFileDialog.getSaveFileName(self, 'Save .tif File', self.start_folder,".tif files (*.tif *.tiff);;All Files (*)", options=options))
        #if not(self.savemask_tif_path.endswith('.tif')) and not(self.savemask_tif_path.endswith('.tif')):
        #    self.savemask_tif_path += '.tif'
        
        dialog = QFileDialog()
        dialog.setFilter(dialog.filter() | QDir.Hidden)
        dialog.setDefaultSuffix('tif')
        dialog.setAcceptMode(QFileDialog.AcceptSave)
        dialog.setNameFilters(['.tif files (*.tif *tiff)','All files (*)'])
        dialog.setDirectory(self.start_folder)
        
        if dialog.exec_() == QDialog.Accepted:
            self.savemask_tif_path = str(dialog.selectedFiles()[0])
        
            if self.analysis_folder == '':
                self.analysis_folder = os.path.split(self.savemask_tif_path)[0]
            
            qq = self.masked_data.astype(numpy.uint16)
            mask_im = Image.frombytes('I;16',(self.im4mask_size_h,self.im4mask_size_v),qq.tobytes())
            mask_im.save(self.savemask_tif_path)
            
        self.update_mask_buttons()
        


    def f_add_polygon(self):
        if self.remove_polygon.isChecked():
            self.remove_polygon.setChecked(False)
        elif self.mask_zoom_select.isChecked():
            self.mask_zoom_select.setChecked(False)
        elif self.add_polygon.isChecked():
            #app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.graphicsView_mask.setDragMode(QGraphicsView.NoDrag)
        
        if self.add_polygon.isChecked():
            self.polygon_points = []
            QApplication.setOverrideCursor(Qt.CrossCursor)
        else:
            ooo = self.mask_scene.items()
            QApplication.restoreOverrideCursor()
            for pp in ooo:
                if 'Pixmap' not in str(pp):
                    self.mask_scene.removeItem(pp)
            self.track_mouse_line = ''
            if len(self.polygon_points)>2:
                x, y = numpy.meshgrid(numpy.arange(self.im4mask_size_h), numpy.arange(self.im4mask_size_v))
                x, y = x.flatten(), y.flatten()
                points = numpy.vstack((x,y)).T
                #oo = numpy.array([x,y]).ravel(order='F').reshape(self.im4mask_size_h*self.im4mask_size_v,2).astype('float')
                pp = path.Path(self.polygon_points)
                # set radius depending if the path is cw or ccw
                r=.3
                r = r*is_ccw(pp) - r*(1-is_ccw(pp))
                grid = pp.contains_points(points,radius=r)
                grid = grid.reshape((self.im4mask_size_v,self.im4mask_size_h)) * 1
                self.masked_data = numpy.clip(self.masked_data - grid,0,1)
                self.show_masked()
                    
        self.status_message()
        
    def f_remove_polygon(self):
        if self.add_polygon.isChecked():
            self.add_polygon.setChecked(False)
        elif self.mask_zoom_select.isChecked():
            self.mask_zoom_select.setChecked(False)
        elif self.remove_polygon.isChecked():
            #app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.graphicsView_mask.setDragMode(QGraphicsView.NoDrag)
        
        if self.remove_polygon.isChecked():
            self.polygon_points = []
            QApplication.setOverrideCursor(Qt.CrossCursor)
        else:
            ooo = self.mask_scene.items()
            QApplication.restoreOverrideCursor()
            for pp in ooo:
                if 'Pixmap' not in str(pp):
                    self.mask_scene.removeItem(pp)
            self.track_mouse_line = ''
            if len(self.polygon_points)>2:
                x, y = numpy.meshgrid(numpy.arange(self.im4mask_size_h), numpy.arange(self.im4mask_size_v))
                x, y = x.flatten(), y.flatten()
                points = numpy.vstack((x,y)).T
                #oo = numpy.array([x,y]).ravel(order='F').reshape(self.im4mask_size_h*self.im4mask_size_v,2).astype('float')
                pp = path.Path(self.polygon_points)
                # set radius depending if the path is cc or ccw
                r=.3
                r = r*is_ccw(pp) - r*(1-is_ccw(pp))
                grid = pp.contains_points(points,radius=r)
                grid = grid.reshape((self.im4mask_size_v,self.im4mask_size_h)) * 1
                self.masked_data = numpy.clip(self.masked_data + grid,0,1)
                self.show_masked()
        
        self.status_message()
        
    def f_add_pixels(self):
        if self.remove_pixels.isChecked():
            self.remove_pixels.setChecked(False)
        elif self.mask_zoom_select.isChecked():
            self.mask_zoom_select.setChecked(False)
        elif self.add_pixels.isChecked():
            app.setOverrideCursor(QCursor(Qt.CrossCursor))
            self.graphicsView_mask.setDragMode(QGraphicsView.NoDrag)
        
        if not self.add_pixels.isChecked():
            QApplication.restoreOverrideCursor()
        
        self.status_message()    
            
    def f_remove_pixels(self):
        if self.add_pixels.isChecked():
            self.add_pixels.setChecked(False)
        elif self.mask_zoom_select.isChecked():
            self.mask_zoom_select.setChecked(False)
        elif self.remove_pixels.isChecked():
            app.setOverrideCursor(QCursor(Qt.CrossCursor))
            self.graphicsView_mask.setDragMode(QGraphicsView.NoDrag)
        
        if not self.remove_pixels.isChecked():
            QApplication.restoreOverrideCursor()
        
        self.status_message()
    
            
    def f_clear_mask(self):
        self.masked_data = numpy.ones_like(self.masked_data)
        self.show_masked()
        
    def f_invert_mask(self):
        self.masked_data = 1 - self.masked_data
        self.show_masked()
    
    def f_mask_below(self):
        ''' mask pixels below given intensity
        '''
        self.masked_data[numpy.where(self.im4mask_data<self.mask_below_num.value())]=0
        self.show_masked()
        
    def f_mask_above(self):
        ''' mask pixels above given intensity
        '''
        self.masked_data[numpy.where(self.im4mask_data>self.mask_above_num.value())]=0
        self.show_masked()
        
        
    def f_add_mask_frame(self):
        self.masked_data[0:self.add_frame_px.value(),0:self.im4mask_size_h] = 0
        self.masked_data[self.im4mask_size_v-self.add_frame_px.value():,0:self.im4mask_size_h] = 0
        self.masked_data[0:self.im4mask_size_v,0:self.add_frame_px.value()] = 0
        self.masked_data[0:self.im4mask_size_v,self.im4mask_size_h-self.add_frame_px.value():] = 0
        
        self.show_masked()
        
    
    def grow_mask_px(self):
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        self.masked_data = grow_mask(self.masked_data)
        self.show_masked()
        app.restoreOverrideCursor()
    
    def f_mask_zoom_select(self):
        if self.add_polygon.isChecked():
            self.add_polygon.setChecked(False)
        elif self.remove_polygon.isChecked():
            self.remove_polygon.setChecked(False)
        elif self.mask_zoom_select.isChecked():
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.graphicsView_mask.setDragMode(QGraphicsView.NoDrag)
        
        if not self.mask_zoom_select.isChecked():
            self.mask_zoom_select_points = []
            if self.mask_zoom_pol != '':
                self.mask_scene.removeItem(self.mask_zoom_pol)
                self.mask_zoom_pol = ''
            
        self.status_message()
    
    def mask_zoom_into_select(self,xx,yy):
        if self.mask_zoom_select_points == []:
            self.mask_zoom_select_points.append(xx)
            self.mask_zoom_select_points.append(yy)
        else:
            self.graphicsView_mask.fitInView(min(xx,self.mask_zoom_select_points[0]),min(yy,self.mask_zoom_select_points[1]),abs(xx-self.mask_zoom_select_points[0]),abs(yy-self.mask_zoom_select_points[1]),Qt.KeepAspectRatio)
            self.mask_zoom_select_points = []
            self.mask_zoom_select.setChecked(False)
            self.graphicsView_mask.setDragMode(QGraphicsView.ScrollHandDrag)
            self.mask_scene.removeItem(self.mask_zoom_pol)
            self.mask_zoom_pol = ''
            zoom = int(100*self.graphicsView_mask.transform().m11())
            if zoom <= 5000:
                self.zoom_mask.setValue(zoom)
            else:
                self.zoom_mask.setValue(5000)
                self.graphicsView_mask.scale(5000/zoom,5000/zoom)
            self.status_message()
    
    def mask_zoom_changed(self,vvv):
        if not ((vvv == self.zoom_mask.maximum() and self.zoom_mask_spin.value() > vvv) or (vvv == self.zoom_mask.minimum() and self.zoom_mask_spin.value() < vvv) ):
            self.zoom_mask_spin.setValue(vvv)
    
    def zoom_mask_spin_changed(self,vvv):
        ''' zoom change
        '''
        # current zoom 
        self.mask_zoom = self.graphicsView_mask.transform().m11()
        # how much to zoom in this step
        self.mask_zoom_factor = (vvv/100)/self.mask_zoom
        # make zoom
        self.graphicsView_mask.scale(self.mask_zoom_factor, self.mask_zoom_factor)
        # update slider value
        if self.zoom_mask.value() != vvv:
            self.zoom_mask.setValue(vvv)
        
        
    def mask_max_spin_changed(self,vvv):
        ''' lut maximum change
        '''
        center_pix = self.graphicsView_mask.mapToScene(self.graphicsView_mask.viewport().rect()).boundingRect().center()
        self.mask_scale_max = vvv
        self.update_mask()
        self.graphicsView_mask.centerOn(center_pix)
        
        if self.mask_log.isChecked():
            self.max_mask.setValue(round(10000*numpy.log(vvv)))
        else:
            self.max_mask.setValue(100*round(vvv))
        
    
    def mask_min_spin_changed(self,vvv):
        ''' lut minimum change
        '''
        center_pix = self.graphicsView_mask.mapToScene(self.graphicsView_mask.viewport().rect()).boundingRect().center()
        self.mask_scale_min = vvv
        self.update_mask()
        self.graphicsView_mask.centerOn(center_pix)
        if self.mask_log.isChecked():
            self.min_mask.setValue(round(10000*numpy.log(vvv)))
        else:
            self.min_mask.setValue(100*round(vvv))
    
    def mask_max_changed(self,vvv):
        if self.mask_log.isChecked():
            bbb = numpy.log(self.max_mask_spin.value())*10000
        else:
            bbb = self.max_mask_spin.value()*100
        
        if not ((vvv == self.max_mask.maximum() and bbb > vvv) or (vvv == self.max_mask.minimum() and bbb < vvv) ):
            if self.mask_log.isChecked():
                if perc_dif(vvv,bbb) > 1 or abs(vvv)<1:
                    self.max_mask_spin.setValue(numpy.exp(vvv/10000))
            else:
                self.max_mask_spin.setValue(vvv/100)

    def mask_min_changed(self,vvv):
        if self.mask_log.isChecked():
            bbb = numpy.log(self.min_mask_spin.value())*10000
        else:
            bbb = self.min_mask_spin.value()*100
        
        if not ((vvv == self.min_mask.maximum() and bbb > vvv) or (vvv == self.min_mask.minimum() and bbb < vvv) ):    
            if self.mask_log.isChecked():
                if perc_dif(vvv,bbb) > 1 or abs(vvv)<1:
                    self.min_mask_spin.setValue(numpy.exp(vvv/10000))
            else:
                self.min_mask_spin.setValue(vvv/100)
    
    def mask_reset_cm(self):
        #self.mask_scale_min = 50
        #self.mask_scale_max = 950
        #self.min_mask.setValue(50)
        #self.max_mask.setValue(950)
        #if self.mask_log.isChecked():
        #    self.min_mask_spin.setValue(self.im4mask_data_log_min*0.9)
        #    self.max_mask_spin.setValue(self.im4mask_data_log_max*1.1)
        #else:
        self.min_mask_spin.setValue(max(0.01,self.im4mask_data_min*0.9))
        self.max_mask_spin.setValue(self.im4mask_data_max*1.1)
    
    def create_mask_scene(self):
        ''' create scene in qgraphicsview in masktab
        '''
        self.mask_scene = QGraphicsScene(self.graphicsView_mask)
        self.graphicsView_mask.setScene(self.mask_scene)
        self.show_mask_image = QGraphicsPixmapItem()
        self.mask_scene.addItem(self.show_mask_image)
        self.show_mask_masked = QGraphicsPixmapItem()
        self.mask_scene.addItem(self.show_mask_masked)
        
    
    def update_integ(self):
        if self.savemask_tif_path != '':
            options = QFileDialog.Options()
            #options |= QFileDialog.DontUseNativeDialog
        
            integmpp_path = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".mpp files (*.mpp);;All Files (*)", options=options))
        
            for line in fileinput.input(integmpp_path, inplace = 1):
                line = re.sub('^#*-m.*$','-m yes {}'.format(os.path.relpath(self.savemask_tif_path,self.analysis_folder)),line)
                sys.stdout.write(line)
            
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("{} updated".format(os.path.split(integmpp_path)[1]))
            msg.setWindowTitle("mpp update")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
        
        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("Please export mask as .tif first!")
            msg.setWindowTitle("mpp update")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
    
    def update_mask_buttons(self):
        if self.im4mask_data.any() != 0:
            self.import_msk.setEnabled(True)
            self.add_msk.setEnabled(True)
            self.export_msk.setEnabled(True)
            self.export_tif.setEnabled(True)
            self.zoom_mask.setEnabled(True)
            self.zoom_mask_label.setEnabled(True)
            self.max_mask.setEnabled(True)
            self.max_mask_label.setEnabled(True)
            self.min_mask.setEnabled(True)
            self.min_mask_label.setEnabled(True)
            self.mask_opacity.setEnabled(True)
            self.mask_opacity_label.setEnabled(True)
            self.add_polygon.setEnabled(True)
            self.add_pixels.setEnabled(True)
            self.remove_polygon.setEnabled(True)
            self.remove_pixels.setEnabled(True)
            self.clear_mask.setEnabled(True)
            self.invert_mask.setEnabled(True)
            self.add_mask_frame.setEnabled(True)
            self.add_frame_px.setEnabled(True)
            self.mask_zoom_select.setEnabled(True)
            self.mask_below.setEnabled(True)
            self.mask_below_num.setEnabled(True)
            self.mask_above.setEnabled(True)
            self.mask_above_num.setEnabled(True)
            self.grow_mask.setEnabled(True)
            self.zoom_mask_spin.setEnabled(True)
            self.max_mask_spin.setEnabled(True)
            self.min_mask_spin.setEnabled(True)
            
        if self.savemask_tif_path != '':
            self.update_integmpp.setEnabled(True)
            
    ## end of mask_tab
        
    ## rad_integ_tab
    
    def f_radint_set_data(self):
        ''' manualy set data folder
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder = os.path.realpath(os.path.normpath('.'))
        self.data_folder = str(QFileDialog.getExistingDirectory(self,'Select analysis folder',self.start_folder,QFileDialog.ShowDirsOnly))
        if self.analysis_folder == '' and os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        
        self.update_footer()
        self.update_radint_buttons()
        
    def f_radint_set_analysis(self):
        ''' manualy set analysis folder
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
            self.start_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        elif self.data_folder != '':
            self.start_folder = self.data_folder
        elif self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder = os.path.realpath(os.path.normpath('.'))
        self.analysis_folder = str(QFileDialog.getExistingDirectory(self,'Select analysis folder',self.start_folder,QFileDialog.ShowDirsOnly))
        self.update_footer()
        self.update_radint_buttons()
        
    def f_select_buffer_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
        
        self.buffer_path = str(QFileDialog.getOpenFileName(self,"Select a buffer file", self.start_folder,"01.tif files (*01.tif *01.tiff);;.tif files (*.tif *.tiff);;All Files (*)", options=options))
        
        self.data_folder = os.path.dirname(self.buffer_path)
        if self.analysis_folder == '' and os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        self.update_footer()
        self.buffer_text.setText('{}'.format(self.buffer_path))
        
    def select_sample_first_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
    
        self.sample_first_path = str(QFileDialog.getOpenFileName(self,"Select a sample file", self.start_folder,"01.tif files (*01.tif *01.tiff);;.tif files (*.tif *.tiff);;All Files (*)", options=options))
    
        self.data_folder = os.path.dirname(self.sample_first_path)
        if self.analysis_folder == '' and os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        self.update_footer()
        self.sample_first_text.setText('{}'.format(self.sample_first_path))
        
    def select_sample_last_file(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
    
        self.sample_last_path = str(QFileDialog.getOpenFileName(self,"Select a sample file", self.start_folder,"01.tif files (*01.tif *01.tiff);;.tif files (*.tif *.tiff);;All Files (*)", options=options))
    
        self.data_folder = os.path.dirname(self.sample_last_path)
        if self.analysis_folder == '' and os.path.isdir(os.path.normpath(os.path.join(self.data_folder,'../analysis/'))):
                self.analysis_folder = os.path.normpath(os.path.join(self.data_folder,'../analysis/'))
        self.update_footer()
        self.sample_last_text.setText('{}'.format(self.sample_last_path))
        
    def f_do_2dto1d(self):
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        fs_text = self.sample_first_text.text()
        try:
            fs_num = int(fs_text)
            fs_path = ''
        except:
            fs_num = int(fs_text.split('_')[-3][1:])
            fs_path = str(fs_text)
        
        ls_path = ''
        if self.multiple_samples.isChecked():
            ls_text = self.sample_last_text.text()
            try:
                ls_num = int(ls_text)
            except:
                ls_num = int(ls_text.split('_')[-3][1:])
                ls_path = str(ls_text)
        
        else:
            ls_num = fs_num
        
        # for multiple samples make sure that they are in the same folder
        if self.multiple_samples.isChecked() and fs_path != '' and ls_path != '' and os.path.dirname(fs_path) != os.path.dirname(ls_path):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("samples have to be in the same folder!")
            msg.setWindowTitle("Samples path problem")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
       
        else:
            bf_path = ''
            if self.subtract_buffer.isChecked():
                bf_text = str(self.buffer_text.text())
                try:
                    bf_num = int(bf_text)
                    # for saxspipe auto sample finding
                    if self.use_saxspipe.isChecked() and bf_num == -1:
                        bf_path = -1
                    else:
                        bf_path = find_image(self.data_folder,bf_num)
                except:
                    #bf_num = int(bf_text.split('_')[-3][1:])
                    bf_path = bf_text
            
            if self.subtract_buffer.isChecked() and (bf_path == 1 or bf_path == 0):
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Information)
                msg.setText("found {} buffer files".format(['0','multiple'][bf_path]))
                msg.setWindowTitle("Buffer file problem")
                msg.setStandardButtons(QMessageBox.Ok)
                msg.exec_()
            else:
                
                # if no saxspipe, check if the integ.mpp is in the correct place and comment out -f lines
                
                if not self.use_saxspipe.isChecked():
                    if not os.path.isfile(os.path.join(self.analysis_folder,'integ.mpp')):
                        msg = QMessageBox()
                        msg.setIcon(QMessageBox.Information)
                        msg.setText('no integ.mpp file in {}'.format(self.analysis_folder))
                        msg.setWindowTitle("integ.mpp file problem")
                        msg.setStandardButtons(QMessageBox.Ok)
                        msg.exec_()
                    else:
                        integmpp = os.path.join(self.analysis_folder,'integ.mpp')
                        for line in fileinput.input(integmpp, inplace = 1):
                            line = re.sub('^-f','#-f',line)
                            sys.stdout.write(line)
                
                # loop through all samples
                for sn in numpy.arange(fs_num,ls_num+1):
                    if sn == fs_num and fs_path != '':
                        # use filename if given as first sample
                        sample_path = fs_path
                    elif self.multiple_samples.isChecked() and sn == ls_num and ls_path != '':
                        # use filename if given as last sample
                        sample_path = ls_path
                    else:
                        sample_path = find_image(self.data_folder,sn)
                    
                    if sample_path == 1 or sample_path == 0:
                        # if there are 0 or multiple files found continue to the next sample, write message on the console
                        print('series {} has {} sample files found, skipping'.format(sn,['0','multiple'][sample_path]))
                    else:
                        # use saxspipe
                        if self.use_saxspipe.isChecked():
                            # check for the integ.mpp in data/../analysis folder, where saxspipe expects it
                            if not os.path.isfile(os.path.join(os.path.normpath(os.path.join(self.data_folder,'../analysis/')),'integ.mpp')):
                                msg = QMessageBox()
                                msg.setIcon(QMessageBox.Information)
                                msg.setText('no integ.mpp file in {}'.format(self.analysis_folder))
                                msg.setWindowTitle("integ.mpp file problem")
                                msg.setStandardButtons(QMessageBox.Ok)
                                msg.exec_()
                            else:
                                if self.subtract_buffer.isChecked():
                                    # subtract buffer
                                    if bf_path == -1:
                                        # find buffer automatically
                                        pp = subprocess.Popen(['saxspipe2.py',sample_path])
                                        pp.communicate()
                                    else:
                                        # use buffer file from the input field
                                        pp = subprocess.Popen(['saxspipe2.py',sample_path,bf_path])
                                        pp.communicate()
                                else:
                                    # no buffer subtraction
                                    pp = subprocess.Popen(['saxspipe2.py',sample_path,'0'])
                                    pp.communicate()
                        else:
                            # use sastool
                            # prepare integ.mpp
                            for line in fileinput.input(integmpp, inplace = 1): 
                                line = re.sub('-s no','-s yes',line)
                                if not self.subtract_buffer.isChecked():
                                    line = re.sub('^-s','#-s',line)
                                else:
                                    line = re.sub('^#-s','-s',line)	
                                sys.stdout.write(line)
                            
                            # write new sample line in integ.mpp
                            new_line = '-f '+sample_path                          
                            if self.subtract_buffer.isChecked():
                                new_line = new_line+" "+bf_path
                                
                            with open(integmpp,'a') as ff:
                                ff.write(new_line+"\n")
                                
                                
            
                # run sastool once all the -f lines are in integ.mpp
                if not self.use_saxspipe.isChecked():
                    pp = subprocess.Popen(['sastool',integmpp],cwd=self.analysis_folder)
                    pp.communicate()
                            
        app.restoreOverrideCursor()
    
    def update_radint_buttons(self):
        if self.subtract_buffer.isChecked():
            self.buffer_label.setEnabled(True)
            self.select_buffer.setEnabled(True)
            self.buffer_text.setEnabled(True)
        else:
            self.buffer_label.setEnabled(False)
            self.select_buffer.setEnabled(False)
            self.buffer_text.setEnabled(False)
            
        if self.multiple_samples.isChecked():
            self.sample_last_label.setEnabled(True)
            self.select_sample_last.setEnabled(True)
            self.sample_last_text.setEnabled(True)
        else:
            self.sample_last_label.setEnabled(False)
            self.select_sample_last.setEnabled(False)
            self.sample_last_text.setEnabled(False)
        
        if self.use_saxspipe.isChecked():
            self.radint_set_analysis.setEnabled(False)
            self.radint_analysis_text.setText(os.path.normpath(os.path.join(self.data_folder,'../analysis/')))
            #self.radint_analysis_text.setEnabled(False)
        else:
            self.radint_set_analysis.setEnabled(True)
            self.radint_analysis_text.setText(self.analysis_folder)
            #self.radint_analysis_text.setEnabled(True)
        
        if self.subtract_buffer.isChecked() and self.use_saxspipe.isChecked():
            self.saxspipe_buffer_label.setText('buffer \'-1\' for saxspipe to automatically find autosampler buffer')
            self.saxspipe_buffer_label.setStyleSheet('color: red')
        else:
            self.saxspipe_buffer_label.setText('')
        
            
        if self.data_folder == '' or self.analysis_folder == '' or (self.subtract_buffer.isChecked() and str(self.buffer_text.text()) == '') or (self.multiple_samples.isChecked() and str(self.sample_last_text.text()) == '') or str(self.sample_first_text.text()) == '':
            self.do_2dto1d.setEnabled(False)
        else:
            self.do_2dto1d.setEnabled(True)
        
            
    
    ## end of rad_integ_tab
      
      
    ## hplc_tab 
      
    def hplc_select_data_file(self):
        ''' select first data file of hplc series
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.data_folder != '':
            self.start_folder = self.data_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            
        self.hplc_data = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,"01.tif files (*01.tif *01.tiff);;.tif files (*.tif *.tiff);;All Files (*)", options=options))
        
        # set analysis folder and integ.mpp, only if they are not previously set and they exist 
        if self.analysis_folder == '':
            if os.path.exists(os.path.normpath(os.path.join(os.path.split(self.hplc_data)[0],'../analysis'))):
                self.analysis_folder = os.path.normpath(os.path.join(os.path.split(self.hplc_data)[0],'../analysis'))
            if os.path.isfile(os.path.join(self.analysis_folder,'integ.mpp')):
                self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        self.hplc_info_label.setText('{}'.format(self.hplc_data))
        self.hplc_title= '_'.join(os.path.split(self.hplc_data)[1].split('_')[:-3])+' ' + os.path.split(self.hplc_data)[1].split('_')[-3]
        try:
            uv_bip = '_'.join(self.hplc_data.split('_')[:-2])+'uv.bip'
            uv_data = numpy.genfromtxt(uv_bip,skip_header=27)
            self.uv_num = uv_data[:,0]
            self.uv1 = uv_data[:,1]
            self.uv2 = uv_data[:,2]
            self.hplc_uv_info_label.setText('{}'.format(uv_bip))
            self.plot_hplc()
        except:
            pass
        self.update_footer()
    
    
    def hplc_set_analysis(self):
        ''' manualy set analysis folder
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        elif os.path.split(self.hplc_data)[0] != '':
            self.start_folder = os.path.join(os.path.split(self.hplc_data)[0],'..')
        else:
            self.start_folder = os.path.abspath('.')
        self.analysis_folder = str(QFileDialog.getExistingDirectory(self,'Select analysis folder',self.start_folder,QFileDialog.ShowDirsOnly))
        self.integmpp = os.path.join(self.analysis_folder,'integ.mpp')
        
    
    def hplc_sastool(self):
        ''' run sastool on hplc data
        '''
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        # create folders
        
        # get file name and series number
        hplc_name = os.path.split(self.hplc_data)[1]
        self.hplc_analysis_folder = os.path.join(self.analysis_folder,'_'.join(hplc_name.split('_')[:-2]))
        if not os.path.isdir(self.hplc_analysis_folder):
            os.makedirs(self.hplc_analysis_folder)
        
        self.hplc_ave_folder = os.path.join(self.hplc_analysis_folder,'average')
        self.hplc_plots_folder = os.path.join(self.hplc_analysis_folder,'plots')
        self.hplc_sastool_folder = os.path.join(self.hplc_analysis_folder,'sastool')
        
        for nn in [self.hplc_ave_folder,self.hplc_plots_folder,self.hplc_sastool_folder]:
            if not os.path.isdir(nn):
                os.makedirs(nn)
        
        # create integ.mpp in sastool folder and run sastool
        
        hplc_integmpp = os.path.join(self.hplc_sastool_folder,'integ.mpp')
        
        # get the data from the original integ.mpp
        newmpp=[]
        
        with open(self.integmpp, "r") as impp:
            for line in impp:
                if not line.startswith('#') and not line.startswith('-f') and not re.match(r'^\s*$', line):
                    if line.startswith('-m'):
                        if not os.path.isabs(line.split()[-1]):
                            line='-m yes {}'.format(os.path.normpath(os.path.expanduser(os.path.join(self.analysis_folder,line.split()[-1]))))
                        else:
                            line='-m yes {}'.format(os.path.abspath(line.split()[-1]))
                    if not line.endswith('\n'):
                        line=line+'\n'
                    newmpp.append(line)
        
        # create a new integ.mpp in the sample sastool folder
        with open(hplc_integmpp, 'w') as mpp_file:
            for item in newmpp:
                mpp_file.write("{}".format(item))
            
            num_length = len(hplc_name.split('_')[-1].split('.')[0])
            first_sample = '_'.join(self.hplc_data.split('_')[:-1]+[str(int(hplc_name.split('_')[-1].split('.')[0])+self.hplc_buf_num.value()).zfill(num_length)+'.tif'])
            
            mpp_file.write('-f {} {} {}'.format(first_sample, self.hplc_data, self.hplc_buf_num.value()))
        
        # run sastool
        pp = subprocess.Popen(['sastool','integ.mpp'],cwd = self.hplc_sastool_folder)
        pp.communicate()
        
        # read .dat files. calculate rg and i0, get iqmin
        
        self.open_sub_data = os.path.join(self.hplc_sastool_folder,os.path.splitext(os.path.split(first_sample)[1])[0]+'.dat')
        
        self.hplc_analysis_f(self.open_sub_data)
        
        self.plot_hplc()
        
        app.restoreOverrideCursor()
        
    
    def open_hplc_data(self):
        ''' select already integrated data
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            
        self.open_sub_data = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".dat files (*.dat);;All Files (*)", options=options))
        
        # set folders, create if not exist
        self.hplc_analysis_folder = os.path.normpath(os.path.join(os.path.split(self.open_sub_data)[0],'..'))
        self.hplc_sastool_folder = os.path.split(self.open_sub_data)[0]
        self.hplc_plots_folder = os.path.join(self.hplc_analysis_folder,'plots')
        self.hplc_ave_folder = os.path.join(self.hplc_analysis_folder,'average')
        
        for nn in [self.hplc_ave_folder,self.hplc_plots_folder]:
            if not os.path.isdir(nn):
                os.makedirs(nn)
        
        
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        # read .sub files. calculate rg and i0, get iqmin
        self.hplc_info_label.setText('{}'.format(self.open_sub_data))
        self.hplc_analysis_f(self.open_sub_data)
        self.hplc_title= '_'.join(os.path.split(self.open_sub_data)[1].split('_')[:-3])+' ' + os.path.split(self.open_sub_data)[1].split('_')[-3]
        
        try:
            uv_bip = os.path.join(self.hplc_plots_folder,'_'.join(os.path.split(self.open_sub_data)[1].split('_')[:-3])+'_UV_data.txt')
            uv_data = numpy.genfromtxt(uv_bip,skip_header=1)
            self.uv_num = uv_data[:,0]
            self.uv1 = uv_data[:,1]
            self.uv2 = uv_data[:,2]
            self.hplc_uv_info_label.setText('{}'.format(self.hplc_plots_folder,'_'.join(os.path.split(self.open_sub_data)[1].split('_')[:-3])+'_UV_data.txt'))
        except:
            pass
        
        self.plot_hplc()
        
        
        app.restoreOverrideCursor()
        
        
        
    def load_saxs_bip(self):
        ''' load pre-analysed saxs data, saved in a .bip file
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            
        saxs_bip = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".bip *.txt files (*.bip *.txt);;All Files (*)", options=options))
        
        if os.path.splitext(saxs_bip)[1] == '.bip':
            saxs_data = numpy.genfromtxt(saxs_bip,skip_header=30)
            self.im_num_ar = saxs_data[:,0]
            self.rg_ar = saxs_data[:,1]
            self.i0_ar = saxs_data[:,2]
            self.iqmin_ar = saxs_data[:,3]
            self.hplc_title= '_'.join(os.path.split(saxs_bip)[1].split('_')[:-1])+' S'+saxs_bip.split('_')[-1].split('.')[0]
        elif os.path.splitext(saxs_bip)[1] == '.txt':
            saxs_data = numpy.genfromtxt(saxs_bip,skip_header=1)
            
            self.im_num_ar = saxs_data[:,0]
            self.rg_ar = saxs_data[:,1]
            self.i0_ar = saxs_data[:,2]
            self.iqmin_ar = saxs_data[:,3]
            self.hplc_title= '_'.join(os.path.split(saxs_bip)[1].split('_')[:-2])
        
        self.hplc_info_label.setText('{}'.format(saxs_bip))   
        self.plot_hplc()
        
        
    def load_uv_bip(self):
        ''' load uv data saved in a .bip file
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            
        uv_bip = str(QFileDialog.getOpenFileName(self,"Select a file", self.start_folder,".bip *.txt files (*.bip *.txt);;All Files (*)", options=options))
        
        if os.path.splitext(uv_bip)[1] == '.bip':
            uv_data = numpy.genfromtxt(uv_bip,skip_header=27)
            self.uv_num = uv_data[:,0]
            self.uv1 = uv_data[:,1]
            self.uv2 = uv_data[:,2]
            self.hplc_title= '_'.join(os.path.split(uv_bip)[1].split('_')[:-1])+' S'+uv_bip.split('_')[-1].split('u')[0]
        elif os.path.splitext(uv_bip)[1] == '.txt':
            uv_data = numpy.genfromtxt(uv_bip,skip_header=1)
            self.uv_num = uv_data[:,0]
            self.uv1 = uv_data[:,1]
            self.uv2 = uv_data[:,2]
            self.hplc_title= '_'.join(os.path.split(uv_bip)[1].split('_')[:-2])
         
        self.hplc_uv_info_label.setText('{}'.format(uv_bip))
        self.plot_hplc()
    
    
    def save_saxs_txt(self):
        if len(self.im_num_ar) > 0:
            options = QFileDialog.Options()
            #options |= QFileDialog.DontUseNativeDialog
            
            if self.hplc_plots_folder != '':
                self.start_folder = self.hplc_plots_folder
            else:
                self.start_folder=os.path.normpath(os.path.abspath('.'))
            
            # try to set filename : os.path.join(plots_sfol,'{}_plot_data.txt'.format(sample_name)
            
            save_saxs_file = str(QFileDialog.getSaveFileName(self, 'Save .txt File', self.start_folder,".txt files (*.txt);;All Files (*)", options=options))
            
            numpy.savetxt(save_saxs_file,numpy.transpose([self.im_num_ar,self.rg_ar,self.i0_ar,self.iqmin_ar]),fmt='%.8G',delimiter='\t',header='im.num. \tRg \tI0 \tIqL')
        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("No data to save!")
            msg.setWindowTitle("Data missing")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()

            
            
    def save_uv_txt(self):
        if len(self.uv_num) > 0:
            options = QFileDialog.Options()
            #options |= QFileDialog.DontUseNativeDialog
            
            if self.hplc_plots_folder != '':
                self.start_folder = self.hplc_plots_folder
            else:
                self.start_folder=os.path.normpath(os.path.abspath('.'))
            
            # try to set filename : os.path.join(plots_sfol,'{}_plot_data.txt'.format(sample_name)
            
            save_uv_file = str(QFileDialog.getSaveFileName(self, 'Save .txt File', self.start_folder,".txt files (*.txt);;All Files (*)", options=options))
            
            numpy.savetxt(save_uv_file,numpy.transpose([self.uv_num+self.hplc_uv_xoffset.value(),self.uv1+self.hplc_uv1_yoffset.value(),self.uv2+self.hplc_uv2_yoffset.value()]),fmt='%.8G',delimiter='\t',header='im.num. \tUV1 \tUV2')
        
        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("No data to save!")
            msg.setWindowTitle("Data missing")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
        
    
    def f_hplc_average(self):
        
        if self.open_sub_data != '' and self.hplc_sastool_folder != '' and self.hplc_ave_folder != '':
            ave_data = os.path.split(self.open_sub_data)[1]
            sample_name_ser = '_'.join(ave_data.split('_')[:-2])
            num_length = len(ave_data.split('_')[-1].split('.')[0])
            
            for aa in numpy.arange(self.hplc_avg_from.value(),self.hplc_avg_to.value(), self.hplc_avg_step.value()):
                try:
                    sub_file = os.path.join(self.hplc_sastool_folder,sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+".dat")
                    avg_data = numpy.genfromtxt(sub_file)
                    # use variance instead of stdev
                    avg_data[:,2] = avg_data[:,2]**2
                    for zz in numpy.arange(1,self.hplc_avg_step.value()):
                        sub_file = os.path.join(self.hplc_sastool_folder,sample_name_ser+"_0_"+str(int(aa+zz)).zfill(num_length)+".dat")
                        sub_data = numpy.genfromtxt(sub_file)
                        avg_data[:,1]=avg_data[:,1]+sub_data[:,1]
                        avg_data[:,2]=avg_data[:,2]+sub_data[:,2]**2
                    
                    avg_data[:,1] = avg_data[:,1]/self.hplc_avg_step.value()
                    # back to stdev form variance
                    avg_data[:,2] = numpy.sqrt(avg_data[:,2])/self.hplc_avg_step.value()
                        
                    numpy.savetxt(os.path.join(self.hplc_ave_folder,"Ave_"+sample_name_ser+"_0_"+str(int(aa)).zfill(num_length)+"-"+str(int(aa+self.hplc_avg_step.value())).zfill(num_length)+'.dat'),avg_data,delimiter='\t',fmt='%.6e')
                except:
                    print("Problem averaging data!")
        else:
            print('problem reading data/folder')
            
            
        
    
    def hplc_reset_plot(self):
        self.hplc_left_min.setValue(0)
        self.hplc_left_max.setValue(0)
        
        self.hplc_right_min.setValue(0)
        self.hplc_right_max.setValue(0)
        
        self.hplc_x_min.setValue(0)
        self.hplc_x_max.setValue(0)
        
        self.hplc_yticks_num.setValue(5)
        
        self.plot_hplc()
        

    
    def replot_hplc(self):
        if self.hplc_left_min.value() !=self.hplc_left_max.value():
            self.mpl_plot.canvas.ax1.set_ylim(self.hplc_left_min.value(),self.hplc_left_max.value())
        if self.hplc_right_min.value() !=self.hplc_right_max.value():
            self.mpl_plot.canvas.ax2.set_ylim(self.hplc_right_min.value(),self.hplc_right_max.value())
        if self.hplc_x_min.value() != self.hplc_x_max.value():
            self.mpl_plot.canvas.ax1.set_xlim(self.hplc_x_min.value(),self.hplc_x_max.value())
        
       
        # align y-axes
        #self.mpl_plot.canvas.ax1.yaxis.set_major_locator(AutoLocator())
        self.mpl_plot.canvas.ax1.yaxis.set_major_locator(LinearLocator(1+self.hplc_yticks_num.value()))
        #self.mpl_plot.canvas.ax2.yaxis.set_major_locator(AutoLocator())
        self.mpl_plot.canvas.ax2.yaxis.set_major_locator(LinearLocator(1+self.hplc_yticks_num.value()))
        
        self.mpl_plot.canvas.draw()
        
    def hplc_rg_l_click(self):
        if self.hplc_rg_l.isChecked():
            self.hplc_rg_r.setChecked(False)
        if len(self.im_num_ar)>0 and not self.hplc_rg_r.isChecked():
            self.hplc_show_line(0)
        
    def hplc_rg_r_click(self):
        if self.hplc_rg_r.isChecked():
            self.hplc_rg_l.setChecked(False)
        if len(self.im_num_ar)>0 and not self.hplc_rg_l.isChecked():
            self.hplc_show_line(0)
     
    def hplc_rg_norm_click(self):
        if len(self.im_num_ar)>0:
            self.hplc_show_line(0)
        
    def hplc_i0_l_click(self):
        if self.hplc_i0_l.isChecked():
            self.hplc_i0_r.setChecked(False)
        if len(self.im_num_ar)>0 and not self.hplc_i0_r.isChecked():
            self.hplc_show_line(1)
        
    def hplc_i0_r_click(self):
        if self.hplc_i0_r.isChecked():
            self.hplc_i0_l.setChecked(False)
        if len(self.im_num_ar)>0 and not self.hplc_i0_l.isChecked():
            self.hplc_show_line(1)
     
    def hplc_i0_norm_click(self):
        if len(self.im_num_ar)>0:
            self.hplc_show_line(1)
        
    def hplc_iqmin_l_click(self):
        if self.hplc_iqmin_l.isChecked():
            self.hplc_iqmin_r.setChecked(False)
        if len(self.im_num_ar)>0 and not self.hplc_iqmin_r.isChecked():
            self.hplc_show_line(2)
        
    def hplc_iqmin_r_click(self):
        if self.hplc_iqmin_r.isChecked():
            self.hplc_iqmin_l.setChecked(False)
        if len(self.im_num_ar)>0 and not self.hplc_iqmin_l.isChecked():
            self.hplc_show_line(2)
    
    def hplc_iqmin_norm_click(self):
        if len(self.im_num_ar)>0:
            self.hplc_show_line(2)
    
    def hplc_uv1_l_click(self):
        if self.hplc_uv1_l.isChecked():
            self.hplc_uv1_r.setChecked(False)
        if len(self.uv_num)>0 and not self.hplc_uv1_r.isChecked():
            self.hplc_show_line(3)
        
    def hplc_uv1_r_click(self):
        if self.hplc_uv1_r.isChecked():
            self.hplc_uv1_l.setChecked(False)
        if len(self.uv_num)>0 and not self.hplc_uv1_l.isChecked():
            self.hplc_show_line(3)
    
    def hplc_uv1_norm_click(self):
        if len(self.uv_num)>0:
            self.hplc_show_line(3)
    
    def hplc_uv2_l_click(self):
        if self.hplc_uv2_l.isChecked():
            self.hplc_uv2_r.setChecked(False)
        if len(self.uv_num)>0 and not self.hplc_uv2_r.isChecked():
            self.hplc_show_line(4)
        
    def hplc_uv2_r_click(self):
        if self.hplc_uv2_r.isChecked():
            self.hplc_uv2_l.setChecked(False)
        if len(self.uv_num)>0 and not self.hplc_uv2_l.isChecked():
            self.hplc_show_line(4)
    
    def hplc_uv2_norm_click(self):
        if len(self.uv_num)>0:
            self.hplc_show_line(4)
    
    def hplc_uv12_l_click(self):
        if self.hplc_uv12_l.isChecked():
            self.hplc_uv12_r.setChecked(False)
        if len(self.uv_num)>0 and not self.hplc_uv12_r.isChecked():
            self.hplc_show_line(5)
        
    def hplc_uv12_r_click(self):
        if self.hplc_uv12_r.isChecked():
            self.hplc_uv12_l.setChecked(False)
        if len(self.uv_num)>0 and not self.hplc_uv12_l.isChecked():
            self.hplc_show_line(5)
    
    def hplc_uv12_norm_click(self):
        if len(self.uv_num)>0:
            self.hplc_show_line(5)
        
    def hplc_i0uv_l_click(self):
        if self.hplc_i0uv_l.isChecked():
            self.hplc_i0uv_r.setChecked(False)
        if len(self.uv_num)>0 and len(self.im_num_ar)>0 and not self.hplc_i0uv_r.isChecked():
            self.hplc_show_line(6)
        
    def hplc_i0uv_r_click(self):
        if self.hplc_i0uv_r.isChecked():
            self.hplc_i0uv_l.setChecked(False)
        if len(self.uv_num)>0 and len(self.im_num_ar)>0 and not self.hplc_i0uv_l.isChecked():
            self.hplc_show_line(6)
    
    def hplc_i0uv_norm_click(self):
        if len(self.uv_num)>0 and len(self.im_num_ar)>0:
            self.hplc_show_line(6)
    
    def hplc_uv_change(self):
        if len(self.uv1) > 0:
            plotuv1 = self.uv1+self.hplc_uv1_yoffset.value()
            plotuv2 = self.uv2+self.hplc_uv2_yoffset.value()
            uv12 = numpy.where(plotuv2==0,0,plotuv1/plotuv2)
            self.x_to_plot[1] = self.uv_num + self.hplc_uv_xoffset.value()
        else:
            plotuv1 = ''
            plotuv2 = ''
            uv12 = ''
        
        # calculate i0/uv1
        if len(self.i0_ar)> 0 and len(plotuv1) > 0:
            dd = len(plotuv1)-len(self.i0_ar)
            i0uv = numpy.where(plotuv1==0,0,numpy.pad(self.i0_ar,(dd-int(self.hplc_uv_xoffset.value()),int(self.hplc_uv_xoffset.value())),'constant')/plotuv1)
        else:
            i0uv = ''
        
        # organize data to plot
        self.data_to_plot[3:] = [plotuv1,plotuv2,uv12,i0uv]
        
        
        
        for kk in numpy.arange(3,7):
            self.hplc_show_line(kk)

    
    def hplc_analysis_f(self,sample_name):
        ''' read dat files and calculate rg and i0
        '''
        
        # sample name + series number
        hplc_name = os.path.split(sample_name)[1]
        hplc_name_ser = hplc_name.split('_')[:-2]
        # get first and last .sub file
        files_list_all = sorted(glob.glob(os.path.join(self.hplc_sastool_folder,'{}*.dat'.format(hplc_name_ser))))
        files_list = files_list_all[int(self.hplc_buf_num.value()):]
        first_num = int(files_list[0].split('_')[-1].split('.')[0])
        
        num_of_sam = len(files_list)
        
        # empty arrays, used for plotting later
        self.im_num_ar = numpy.zeros(num_of_sam)
        self.rg_ar = numpy.zeros(num_of_sam)
        self.i0_ar = numpy.zeros(num_of_sam)
        self.iqmin_ar = numpy.zeros(num_of_sam)
        
        for sub_file in files_list:
            # image number
            ii = int(sub_file.split('_')[-1].split('.')[0])
            
            #delete zeros from sub file:
            for line in fileinput.input(sub_file, inplace = 1):
                if (not float(line.split()[1]) == float(line.split()[2]) == 0 or fileinput.filelineno() > 30):
                    sys.stdout.write(line)
            
            # run autorg
            try:
                #rg_out = subprocess.check_output([autorg,'--sminrg=1','--smaxrg=1.3',sub_file])
                rg_out = subprocess.check_output(['autorg',sub_file])
                rg=float(rg_out.split()[2])
                
                #rg_stdev=rg_out.split()[4])
                i0=float(rg_out.split()[8])
                #i0_stdev=rg_out.split()[10]
            except:
                rg = 0
                i0 = 0
            
            if rg<0:
                rg = 0
            if i0<0:
                i0=0
            
            # read i_qmin value
            dat_d = numpy.genfromtxt(sub_file)
            iqmin = max(dat_d[self.hplc_iqmin.value()-1,1],0)
            # put the values in arrayes for printing later
            self.im_num_ar[ii-first_num] = ii
            self.rg_ar[ii-first_num] = rg
            self.i0_ar[ii-first_num] = i0
            self.iqmin_ar[ii-first_num] = iqmin
    
    
    def hplc_checkboxes(self):
        ''' get status of checkboxes
        '''
        self.left_axes = numpy.array([self.hplc_rg_l.isChecked(),self.hplc_i0_l.isChecked(),self.hplc_iqmin_l.isChecked(),self.hplc_uv1_l.isChecked(),self.hplc_uv2_l.isChecked(),self.hplc_uv12_l.isChecked(),self.hplc_i0uv_l.isChecked()])
        self.right_axes = numpy.array([self.hplc_rg_r.isChecked(),self.hplc_i0_r.isChecked(),self.hplc_iqmin_r.isChecked(),self.hplc_uv1_r.isChecked(),self.hplc_uv2_r.isChecked(),self.hplc_uv12_r.isChecked(),self.hplc_i0uv_r.isChecked()])
        self.norm = numpy.array([self.hplc_rg_norm.isChecked(),self.hplc_i0_norm.isChecked(),self.hplc_iqmin_norm.isChecked(),self.hplc_uv1_norm.isChecked(),self.hplc_uv2_norm.isChecked(),self.hplc_uv12_norm.isChecked(),self.hplc_i0uv_norm.isChecked()])
    
    def plot_hplc(self):
        # try to set folder for saving plots
        if os.path.exists(self.hplc_plots_folder):
            rcParams["savefig.directory"] = self.hplc_plots_folder
        else:
            rcParams["savefig.directory"] = os.path.abspath('.')
        
        # clear the graph
        self.mpl_plot.canvas.ax1.cla()
        self.mpl_plot.canvas.ax2.cla()
        
        self.hplc_checkboxes()
        
     
        # calculate uv1/uv2
        if len(self.uv1) > 0:
            plotuv1 = self.uv1+self.hplc_uv1_yoffset.value()
            plotuv2 = self.uv2+self.hplc_uv2_yoffset.value()
            uv12 = numpy.where(plotuv2==0,0,plotuv1/plotuv2)
        else:
            plotuv1 = ''
            plotuv2 = ''
            uv12 = ''
        
        # calculate i0/uv1
        if len(self.i0_ar)> 0 and len(plotuv1) > 0:
            dd = len(plotuv1)-len(self.i0_ar)
            i0uv = numpy.where(plotuv1==0,0,numpy.pad(self.i0_ar,(dd-int(self.hplc_uv_xoffset.value()),int(self.hplc_uv_xoffset.value())),'constant')/plotuv1)
        else:
            i0uv = ''
        
        # organize data to plot
        self.data_to_plot = [self.rg_ar,self.i0_ar,self.iqmin_ar,plotuv1,plotuv2,uv12,i0uv]
        self.x_to_plot = [self.im_num_ar,self.uv_num]
        if len(self.uv_num) > 0 :
                self.x_to_plot[1] = self.uv_num + self.hplc_uv_xoffset.value()
        
        #right_yax = ''
        #left_yax = ''
        #sc = []
        # first plot non-normalized data
        for ii in numpy.arange(len(self.left_axes)):
            if len(self.data_to_plot[ii]) > 0:
                if (self.left_axes[ii] or self.right_axes[ii]) and not self.norm[ii]:
                    axpos = self.right_axes[ii]*1 # 0 for left axis, 1 for right
                    self.hplc_plot_lines[ii] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[ii]],self.data_to_plot[ii],self.cc[ii],label = self.data_label[ii])
                    
                  
        self.hplc_norm = [self.mpl_plot.canvas.ax1.get_ylim()[1]*0.95,self.mpl_plot.canvas.ax2.get_ylim()[1]*0.95]

        
       
       # plot normalized data
        for ii in numpy.arange(len(self.left_axes)):
            if len(self.data_to_plot[ii]) > 0:
                if (self.left_axes[ii] or self.right_axes[ii]) and self.norm[ii]:
                    axpos = self.right_axes[ii]*1 # 0 for left axis, 1 for right
                    if ii<5:
                        self.hplc_plot_lines[ii] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[ii]],self.hplc_norm[axpos]*self.data_to_plot[ii]/max(self.data_to_plot[ii][numpy.where(numpy.isfinite(self.data_to_plot[ii]))]),self.cc[ii],label = self.data_label[ii]+'$_{[norm]}$')
                    # special normalization for uv1/uv2 and I0/uv1
                    else:
                        self.hplc_plot_lines[ii] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[ii]],self.hplc_norm[axpos]*(self.data_to_plot[ii]-min(self.data_to_plot[ii]))/max(self.data_to_plot[ii]-min(self.data_to_plot[ii])),self.cc[ii],label = self.data_label[ii]+'$_{[norm]}$')
           
                    
                    
              
        # et y-axes names and legend
        self.hplc_legend()
        
        # set x-axis name and plot title
        
        self.mpl_plot.canvas.ax1.set_xlabel('image number',fontsize=18)
        self.mpl_plot.canvas.ax2.set_title(self.hplc_title,fontsize=22)
        
        
        # read plot limits and set into spinboxes
        self.hplc_left_min.setValue(self.mpl_plot.canvas.ax1.get_ylim()[0])
        self.hplc_left_max.setValue(self.mpl_plot.canvas.ax1.get_ylim()[1])
        self.hplc_right_min.setValue(self.mpl_plot.canvas.ax2.get_ylim()[0])
        self.hplc_right_max.setValue(self.mpl_plot.canvas.ax2.get_ylim()[1])
        self.hplc_x_min.setValue(self.mpl_plot.canvas.ax1.get_xlim()[0])
        self.hplc_x_max.setValue(self.mpl_plot.canvas.ax1.get_xlim()[1])
        
        
        # draw grid and align both y-axes
        self.mpl_plot.canvas.ax1.grid()
        self.mpl_plot.canvas.ax1.yaxis.set_major_locator(LinearLocator(1+self.hplc_yticks_num.value()))
        #self.mpl_plot.canvas.ax1.yaxis.set_major_locator(AutoLocator())
        self.mpl_plot.canvas.ax2.yaxis.set_major_locator(LinearLocator(1+self.hplc_yticks_num.value()))
        #self.mpl_plot.canvas.ax2.yaxis.set_major_locator(AutoLocator())
        
        
        self.mpl_plot.canvas.draw_idle()
        

    def hplc_show_line(self,jj):
        ''' add/remove lines to the existing plot
        '''
        if len(self.hplc_plot_lines[jj]) > 0:
            self.hplc_plot_lines[jj].pop().remove()
        
        self.hplc_checkboxes()
        
        if self.left_axes[jj] == True or self.right_axes[jj] == True:
            axpos = self.right_axes[jj]*1 # 0 for left axis, 1 for right
            if not self.norm[jj]:
                self.hplc_plot_lines[jj] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[jj]],self.data_to_plot[jj],self.cc[jj],label = self.data_label[jj])
            elif jj < 5:
                self.hplc_plot_lines[jj] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[jj]],self.hplc_norm[axpos]*self.data_to_plot[jj]/max(self.data_to_plot[jj][numpy.where(numpy.isfinite(self.data_to_plot[jj]))]),self.cc[jj],label = self.data_label[jj]+'$_{[norm]}$')
            else:
                self.hplc_plot_lines[jj] = self.hplc_yaxes[axpos].plot(self.x_to_plot[self.hplc_xaxis[jj]],self.hplc_norm[axpos]*(self.data_to_plot[jj]-min(self.data_to_plot[jj]))/max(self.data_to_plot[jj]-min(self.data_to_plot[jj])),self.cc[jj],label = self.data_label[jj]+'$_{[norm]}$')
        
            
        #self.mpl_plot.canvas.draw_idle()
        self.hplc_legend()
        self.replot_hplc()
        
        
    def hplc_legend(self):
        ''' show legend and y-axes titles
        '''
        # add legend
        lines1, labels1 = self.mpl_plot.canvas.ax1.get_legend_handles_labels()
        lines2, labels2 = self.mpl_plot.canvas.ax2.get_legend_handles_labels()
        
        lines = lines1+lines2
        labels = labels1+labels2
        
        
        # sort the labels in the same order as the data select buttons
        rr= numpy.zeros(len(lines))
        
        for ii in range(len(lines)):
            rr[ii] = self.data_label.index(labels[ii].split('$_{[norm]}$')[0])
        
        rr_s, lines_s, labels_s = zip(*sorted(zip(rr,lines,labels)))
        
        
        try:
            rr1 = numpy.zeros(len(lines1))
            for ii in range(len(lines1)):
                rr1[ii] = self.data_label.index(labels1[ii].split('$_{[norm]}$')[0])
        
            rr1_s, lines1_s, labels1_s = zip(*sorted(zip(rr1,lines1,labels1)))
        
        except:
            labels1_s = labels1
        try:
            rr2 = numpy.zeros(len(lines2))
            for ii in range(len(lines2)):
                rr2[ii] = self.data_label.index(labels2[ii].split('$_{[norm]}$')[0])
            
            rr2_s, lines2_s, labels2_s = zip(*sorted(zip(rr2,lines2,labels2)))
        except:
            labels2_s = labels2
        
        # set lagend
        if self.ll != '':
            #vv = self.ll.get_bbox_to_anchor().inverse_transformed(self.mpl_plot.canvas.ax2.transAxes)
            vp = self.ll._loc_real
            self.ll.remove()
            # reposition the legend to the old location
            self.ll = self.mpl_plot.canvas.ax2.legend(lines_s, labels_s, numpoints=1, loc = vp , fontsize=14, frameon=False, framealpha=0.5, labelspacing = 0.1)
        else:
            self.ll = self.mpl_plot.canvas.ax2.legend(lines_s, labels_s, numpoints=1,loc=0, fontsize=14, frameon=False, framealpha=0.5,labelspacing = 0.1)
        
        self.ll.draggable(True)
        # add axes names
        self.mpl_plot.canvas.ax1.set_ylabel(','.join(labels1_s),fontsize=18)
        self.mpl_plot.canvas.ax2.set_ylabel(','.join(labels2_s),fontsize=18)
        
            
    
    def hplc_resize(self,event):
        if self.ll != '':
            if (self.left_axes[:3].any() and len(self.im_num_ar) >0) or (self.left_axes[3:].any() and len(self.uv_num) >0):
                self.hplc_left_min.setValue(self.mpl_plot.canvas.ax1.get_ylim()[0])
                self.hplc_left_max.setValue(self.mpl_plot.canvas.ax1.get_ylim()[1])
            
            if (self.right_axes[:3].any() and len(self.im_num_ar) >0) or (self.right_axes[3:].any() and len(self.uv_num) >0):
                self.hplc_right_min.setValue(self.mpl_plot.canvas.ax2.get_ylim()[0])
                self.hplc_right_max.setValue(self.mpl_plot.canvas.ax2.get_ylim()[1])
            
            self.hplc_x_min.setValue(self.mpl_plot.canvas.ax1.get_xlim()[0])
            self.hplc_x_max.setValue(self.mpl_plot.canvas.ax1.get_xlim()[1])
        
        # try: 
        #     self.leg_pos = self.ll.get_window_extent()
        # except:
        #     self.leg_pos = ''
        
    
    ## end hplc tab
    
    ## start integmpp tab
    
    def integmpp_open_file(self):
        ''' select integ.mpp top open
        '''
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.analysis_folder != '':
            self.start_folder = self.analysis_folder
        else:
            self.start_folder=os.path.normpath(os.path.abspath('.'))
            
        self.integmpp_file = str(QFileDialog.getOpenFileName(self,"Select integ.mpp file", self.start_folder,".mpp files (*.mpp);;All Files (*)", options=options))
        
        self.integmpp_toedit()
    
    
    def integmpp_toedit(self):
        with open(self.integmpp_file,'r') as f_in:
            integdata = f_in.read()
    
        self.integmpp_edit_txt.setPlainText(integdata)
        self.integmpp_label.setText(self.integmpp_file)
    
    def integmpp_save_file(self):
        ''' save integ.mpp and remove all new lines
        '''
        with open(self.integmpp_file,'w') as f_out:
            # remove new line at the end of the file
            #f_out.write(str(self.integmpp_edit.toPlainText()).rstrip())
            f_out.write(str(self.integmpp_edit_txt.toPlainText()))
        
        for line in fileinput.input(self.integmpp_file, inplace = 1):
            #if not re.match(r'^\s*$', line):
            if line.rstrip():
                sys.stdout.write(line)
        
    def integmpp_show_help(self):
        self.inhelp = QDialog(self)
        self.inhelp.resize(600,600)
        self.inhelp.setStyleSheet("QDialog{background:#f9f9f4;color:black;}")
        self.inhelp.setWindowTitle('integ.mpp help')
        self.inhelp_label = QLabel(self.inhelp)
        self.inhelp_label.setText('\n'
        #'switch: \t explanation:\n\n'
        '-l \t log-file name \n'
        '\t example: -l logfile.log \n\n'
        '-c \t beam center position in pixels\n'
        '\t example: -c 400.34 500.45 \n\n'
        '-q \t conersion form pixels to q (not required) \n'
        '\t example: -q yes 0 0.0 102 0.1076 \n\n'
        '-i \t use i1 or i2 for data normalization (not required) \n'
        '\t example: -i2 yes 1000 \n\n'
        '-r \t variance factor for rejection of individual files from \n\t the summation of the whole series (not required) \n'  
        '\t example: -r yes 3.5\n\n'
        '-s \t subtract buffer intensity from individual sample frames (not required) \n'
        '\t example: -s yes \n\n'
        '-t \t re-use existing buffer .tot file (not required) \n'
        '\t example: -t yes\n\n'
        '-m \t .tif mask file \n'
        '\t example: -m yes /path/to/mask.tif \n\n'
        '-z \t use dezinger (not required) \n'
        '\t example: -z yes 3.5 \n\n'
        '-o \t detector offset (not required, default 10)\n'
        '\t example: -o 0 \n\n'
        '-f \t file(s) to integrate \n'
        '\t example: -f ./path/sample.tif\n'
        '\t example: -f ./path/sample.tif ./path/buffer.tif \n')
        self.inhelp.show()
    
    
    ## end integmpp tab
    
    ## image math tab
    
    #
    #
    # all_images = {'a': numpy.array([]),'b': numpy.array([]),'c': numpy.array([])}
    # avg_images = ...
    # log_all_images = ...
    # log_avg_images = ...
    # current = 'a' (or 'b' or 'c')
    # all_images[current] -> images to show/work with
    #
    # swap variables a<=>b
    # tt['a'], tt['b'] = tt['b'], tt['a']
    
    def im_mt_ci(self):
        ci = ['a','b','c'][int(-2-self.img_math_select_group.checkedId())]
        return(ci)
    
    
    def im_mt_select(self):
        # remove mosaic polygon ix exists
        self.im_mt_del_mos_pol()
        #print(-1-self.img_math_select_group.checkedId())
        if self.im_mt_data[self.im_mt_ci()].any() != 0:
            if not self.img_math_lock_scale.isChecked():
                self.im_mt_reset_cm()
                
            self.update_im_mt_img()
        else:
            # draw empty screen
                self.im_mt_pix = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(numpy.zeros([3,3]),0,1))))
                self.im_mt_show_image.setPixmap(self.im_mt_pix)
                self.im_mt_title()
        
    def im_mt_show_type(self):
        rrr = int(-2-self.img_math_show_group.checkedId())
        return(rrr)
    
    
    def im_mt_title(self):
        if self.im_mt_text[self.im_mt_ci()] == "":
            self.im_mt_text[self.im_mt_ci()] = 'No Image'
        elif self.im_mt_show_type() == 1 or (self.im_mt_show_type() == 2 and self.img_math_cur_pix.value() != 0):
            self.img_math_im_name.setText('{} [{} total]'.format(self.im_mt_names[self.im_mt_ci()][self.img_math_cur_pix.value()-1],self.im_mt_img_num[self.im_mt_ci()]))
        else:
            self.img_math_im_name.setText('{} [{} total]'.format(self.im_mt_text[self.im_mt_ci()],self.im_mt_img_num[self.im_mt_ci()]))
    
    
    
    def im_mt_show_select(self):
        
        if self.im_mt_show_type() == 2:
            self.img_math_cur_pix.setMinimum(0)
        else:
            self.img_math_cur_pix.setMinimum(1)
        
        self.update_im_mt_img()
        
        
        
    def im_mt_open(self):
        
        options = QFileDialog.Options()
        # opening files works faster when using native dialog
        #options |= QFileDialog.DontUseNativeDialog
        if self.img_math_folder == '':
            if self.data_folder != '':
                self.img_math_folder = self.data_folder
            else:
                self.img_math_folder=os.path.normpath(os.path.abspath('.'))
            
        # open one or multiple images
        self.img_math_list = QFileDialog.getOpenFileNames(self,"Select one or multiple files", self.img_math_folder,".cbf or .tif files (*.cbf *.tif *.tiff);;All Files (*)", options=options)
        
        self.img_math_folder, self.img_mask_1st = os.path.split(str(self.img_math_list[0]))
        
        #if self.data_folder == "":
        #    self.data_folder = self.img_math_folder
        
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        # Current active image (GUI IDs A:-2 B:-3 C:-4  -> A:0 B:1 C:1)
        #im_id = numpy.abs(self.img_math_group.checkedId())-2
        
        self.im_mt_img_num[self.im_mt_ci()]  = len(self.img_math_list)
        
        self.im_mt_mos_factor_h[self.im_mt_ci()] = numpy.ceil(numpy.sqrt(self.im_mt_img_num[self.im_mt_ci()]))
        self.im_mt_mos_factor_v[self.im_mt_ci()] = numpy.round(numpy.sqrt(self.im_mt_img_num[self.im_mt_ci()]))
        
        # open image(s)
        im_buffer = fabio.open(str(self.img_math_list[0])).data.astype('int32')
        
        self.im_mt_names[self.im_mt_ci()]= [os.path.basename(str(self.img_math_list[0]))]
        
        if self.im_mt_img_num[self.im_mt_ci()]>1:
            for ii in self.img_math_list[1:]:
            
                im_buffer = numpy.dstack((im_buffer,fabio.open(str(ii)).data.astype('int32')))
                self.im_mt_names[self.im_mt_ci()].append(os.path.basename(str(ii)))
                
        else:
            im_buffer = numpy.expand_dims(im_buffer, axis=-1)
        
        
        # reshape - move image index to the front
        im_buffer= numpy.moveaxis(im_buffer,-1,0)
        
        self.im_mt_data[self.im_mt_ci()] = numpy.copy(im_buffer)
        self.im_mt_log_data[self.im_mt_ci()] = numpy.log(numpy.clip(im_buffer,0.01,None))
        self.im_mt_data_avg[self.im_mt_ci()] = numpy.average(im_buffer,axis=0)
        self.im_mt_log_data_avg[self.im_mt_ci()] = numpy.log(numpy.clip(self.im_mt_data_avg[self.im_mt_ci()],0.01,None))
        
        # saving image size for each a, b, c images
        self.im_mt_size_v[self.im_mt_ci()] = int(self.im_mt_data_avg[self.im_mt_ci()].shape[0])
        self.im_mt_size_h[self.im_mt_ci()] = int(self.im_mt_data_avg[self.im_mt_ci()].shape[1])
        
        # image size of currently shown image
        self.im_mt_size_view_v = self.im_mt_size_v[self.im_mt_ci()]
        self.im_mt_size_view_h = self.im_mt_size_h[self.im_mt_ci()]
        
        
        # making mosaic images
        
        # number of images to pad
        pad_im = int(self.im_mt_mos_factor_h[self.im_mt_ci()]*self.im_mt_mos_factor_v[self.im_mt_ci()] - self.im_mt_img_num[self.im_mt_ci()])
        
        self.im_mt_data_mos[self.im_mt_ci()] = numpy.pad(self.im_mt_data[self.im_mt_ci()],[(0,pad_im),(0,0),(0,0)],mode='constant',constant_values=0).reshape(int(self.im_mt_mos_factor_v[self.im_mt_ci()]), int(self.im_mt_mos_factor_h[self.im_mt_ci()]), int(self.im_mt_size_v[self.im_mt_ci()]), int(self.im_mt_size_h[self.im_mt_ci()])).swapaxes(1, 2).reshape(int(self.im_mt_mos_factor_v[self.im_mt_ci()]*self.im_mt_size_v[self.im_mt_ci()]), int(self.im_mt_mos_factor_h[self.im_mt_ci()]*self.im_mt_size_h[self.im_mt_ci()]))
        
        self.im_mt_log_data_mos[self.im_mt_ci()] = numpy.log(numpy.clip(self.im_mt_data_mos[self.im_mt_ci()],0.01,None))
        
        self.im_mt_data_range[self.im_mt_ci()]=numpy.array([numpy.min(self.im_mt_data[self.im_mt_ci()]),1.3*heapq.nlargest(5,self.im_mt_data[self.im_mt_ci()].flatten())[-1]])
        self.im_mt_data_avg_range[self.im_mt_ci()] = numpy.array([numpy.min(self.im_mt_data[self.im_mt_ci()]),1.3*heapq.nlargest(5,self.im_mt_data_avg[self.im_mt_ci()].flatten())[-1]])
        
        self.im_mt_text[self.im_mt_ci()] = '{}'.format(', '.join(self.im_mt_names[self.im_mt_ci()][:]))
        if len(self.im_mt_text[self.im_mt_ci()])> 55:
            self.im_mt_text[self.im_mt_ci()] = self.im_mt_text[self.im_mt_ci()][:55]+'....'
        
        if not self.img_math_lock_scale.isChecked():
            self.im_mt_reset_cm()
        self.im_mt_show_img()
        
        app.setOverrideCursor(QCursor(Qt.ArrowCursor))
        
    def im_mt_show_img(self):
        
        self.update_im_mt_scale()
        self.update_im_mt_img()
        
           
        self.img_math_display.fitInView(self.im_mt_show_image,Qt.KeepAspectRatio)
        
        
        self.im_mt_init_zoom = self.img_math_display.transform().m11()
        
        
        self.math_zoom_spin.setValue(round(100*self.im_mt_init_zoom))
        
        app.setOverrideCursor(QCursor(Qt.ArrowCursor))    
    
    
    def update_im_mt_img(self):
        # remove mosaic polygon ix exists
        self.im_mt_del_mos_pol()
        if (self.im_mt_data[self.im_mt_ci()] !=0).any() :
            self.img_math_cur_pix.setMaximum(self.im_mt_img_num[self.im_mt_ci()])
            # log images
            if self.img_math_im_log.isChecked():
                # average selected
                if self.im_mt_show_type() == 0:
                    self.im_mt_pix = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im_mt_log_data_avg[self.im_mt_ci()],numpy.log(self.math_min_spin.value()),numpy.log(self.math_max_spin.value()),str(self.img_math_cm_choice.currentText())))))
                    self.im_mt_refresh_screen()
                
                # series selected
                elif self.im_mt_show_type() == 1:
                    self.im_mt_pix = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im_mt_log_data[self.im_mt_ci()][self.img_math_cur_pix.value()-1],numpy.log(self.math_min_spin.value()),numpy.log(self.math_max_spin.value()),str(self.img_math_cm_choice.currentText())))))
                    self.im_mt_refresh_screen()
                
                # mosaic is selected
                elif self.im_mt_show_type() == 2:
                    #draw new polygon 
                    self.im_mt_draw_mos_pol()
                    self.im_mt_pix = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im_mt_log_data_mos[self.im_mt_ci()],numpy.log(self.math_min_spin.value()),numpy.log(self.math_max_spin.value()),str(self.img_math_cm_choice.currentText())))))
                    self.im_mt_refresh_screen()
                        
                
                        
            # linear images
            else:
                # average selected
                if self.im_mt_show_type() == 0:
                    self.im_mt_pix = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im_mt_data_avg[self.im_mt_ci()],self.math_min_spin.value(),self.math_max_spin.value(),str(self.img_math_cm_choice.currentText())))))
                    self.im_mt_refresh_screen()
                
                # series selected
                elif self.im_mt_show_type() == 1:
                    self.im_mt_pix = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im_mt_data[self.im_mt_ci()][self.img_math_cur_pix.value()-1],self.math_min_spin.value(),self.math_max_spin.value(),str(self.img_math_cm_choice.currentText())))))
                    self.im_mt_refresh_screen()
                    
                # mosaic is selected
                elif self.im_mt_show_type() == 2:
                    #draw new polygon 
                    self.im_mt_draw_mos_pol()
                    self.im_mt_pix = QPixmap.fromImage(ImageQt(Image.fromarray(gray_to_color(self.im_mt_data_mos[self.im_mt_ci()],self.math_min_spin.value(),self.math_max_spin.value(),str(self.img_math_cm_choice.currentText())))))
                    self.im_mt_refresh_screen()
                        
                else:
                    print('this should not happened, no view type selected!')
        
            self.im_mt_title()
    
    
    def im_mt_refresh_screen(self):
        self.im_mt_show_image.setPixmap(self.im_mt_pix)
        sx = self.im_mt_size_h[self.im_mt_ci()]
        sy = self.im_mt_size_v[self.im_mt_ci()]
        if self.im_mt_show_type() == 2:
            sx = sx*self.im_mt_mos_factor_h[self.im_mt_ci()]
            sy = sy*self.im_mt_mos_factor_v[self.im_mt_ci()]
        
        if self.im_mt_size_view_h != sx or self.im_mt_size_view_v != sy:
            self.im_mt_size_view_h = sx
            self.im_mt_size_view_v = sy
            self.im_mt_scene.setSceneRect(-50,-50,sx+100,sy+100)


        
    def im_mt_mos_recalc(self,mmm):
        
        pad_im = int(self.im_mt_mos_factor_h[mmm]*self.im_mt_mos_factor_v[mmm] - self.im_mt_img_num[mmm])
        
        self.im_mt_data_mos[mmm] = numpy.pad(self.im_mt_data[mmm],[(0,pad_im),(0,0),(0,0)],mode='constant',constant_values=0).reshape(int(self.im_mt_mos_factor_v[mmm]), int(self.im_mt_mos_factor_h[mmm]), int(self.im_mt_size_v[mmm]), int(self.im_mt_size_h[mmm])).swapaxes(1, 2).reshape(int(self.im_mt_mos_factor_v[mmm]*self.im_mt_size_v[mmm]), int(self.im_mt_mos_factor_h[mmm]*self.im_mt_size_h[mmm]))
        
        self.im_mt_log_data_mos[mmm] = numpy.log(numpy.clip(self.im_mt_data_mos[mmm],0.01,None))
    
    def im_mt_del_mos_pol(self):
            if self.im_mt_cur_pix_pol != '':
                self.im_mt_scene.removeItem(self.im_mt_cur_pix_pol)
                self.im_mt_cur_pix_pol = ''
    
    def im_mt_draw_mos_pol(self):
        if self.img_math_cur_pix.value() != 0:
            x1 = numpy.mod(self.img_math_cur_pix.value()-1,self.im_mt_mos_factor_h[self.im_mt_ci()])*self.im_mt_size_h[self.im_mt_ci()]
            y1 = (numpy.int((self.img_math_cur_pix.value()-1)/self.im_mt_mos_factor_h[self.im_mt_ci()]))*self.im_mt_size_v[self.im_mt_ci()]
            self.im_mt_cur_pix_pol = self.im_mt_scene.addRect(x1,y1,self.im_mt_size_h[self.im_mt_ci()],self.im_mt_size_v[self.im_mt_ci()],pen=self.mosaicpen)
    
    
    def update_cur_pix_nr(self):
        
        if self.im_mt_show_type() == 0:
            pass
        elif self.im_mt_show_type() == 1:
            self.update_im_mt_img()
        elif self.im_mt_show_type() == 2:
            self.im_mt_del_mos_pol()
            self.im_mt_draw_mos_pol()
            self.im_mt_title()
                
            
    
            
    def update_im_mt_scale(self):
            if self.img_math_im_log.isChecked():
                self.math_max_slider.setValue(round(10000*numpy.log(max(0.01,self.math_max_spin.value()))))
                self.math_min_slider.setValue(round(10000*numpy.log(max(0.01,self.math_min_spin.value()))))
                self.math_min_slider.setMinimum(-40000)
                self.math_min_slider.setMaximum(170000)
                self.math_max_slider.setMinimum(-40000)
                self.math_max_slider.setMaximum(170000)
                self.math_min_slider.setSingleStep(1000)
                self.math_max_slider.setSingleStep(1000)
                self.math_min_slider.setPageStep(10000)
                self.math_max_slider.setPageStep(10000)
                #self.min_mask_spin.setSingleStep(0.1)
                #self.max_mask_spin.setSingleStep(0.1)
                self.math_max_spin.setMinimum(0.01)
                self.math_min_spin.setMinimum(0.01)
            else:
                self.math_min_slider.setMinimum(-10000)
                self.math_min_slider.setMaximum(round(100*2*self.im_mt_data_range[self.im_mt_ci()][1]+50))
                self.math_max_slider.setMinimum(-10000)
                self.math_max_slider.setMaximum(round(100*2*self.im_mt_data_range[self.im_mt_ci()][1]+50))
                self.math_min_slider.setSingleStep(5000)
                self.math_max_slider.setSingleStep(5000)
                self.math_min_slider.setPageStep(50000)
                self.math_max_slider.setPageStep(50000)
                #self.min_mask_spin.setSingleStep(1)
                #self.max_mask_spin.setSingleStep(1)
                self.math_max_spin.setMinimum(-1000000)
                self.math_min_spin.setMinimum(-1000000)
                self.math_max_slider.setValue(round(100*self.math_max_spin.value()))
                self.math_min_slider.setValue(round(100*self.math_min_spin.value()))       
        
    def im_mt_log_change(self):
        self.update_im_mt_scale()
        self.update_im_mt_img()
        
        
    def im_mt_add_ab(self):
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        self.im_mt_data['c'] = numpy.clip(numpy.nan_to_num(self.im_mt_data['a'] + self.im_mt_data_avg['b']),-1e8,1e8)
        self.im_mt_data_avg['c'] = numpy.average(self.im_mt_data['c'],axis=0)
        self.im_mt_log_data['c']=numpy.log(numpy.clip(self.im_mt_data['c'],0.01,None))
        self.im_mt_log_data_avg['c']=numpy.log(numpy.clip(self.im_mt_data_avg['c'],0.01,None))
        self.im_mt_set_limits('c')
        self.im_mt_size_v['c']=self.im_mt_size_v['a']
        self.im_mt_size_h['c']=self.im_mt_size_h['a']
        self.im_mt_mos_factor_h['c']=self.im_mt_mos_factor_h['a']
        self.im_mt_mos_factor_v['c']=self.im_mt_mos_factor_v['a']
        self.im_mt_img_num['c'] = self.im_mt_img_num['a']
        self.im_mt_names['c'] = self.im_mt_img_num['a'] * ['a+b_avg']
        self.im_mt_text['c'] = 'a+b_avg'
        self.im_mt_mos_recalc('c')
        if self.im_mt_ci() == 'c':
            self.update_im_mt_img()
            if not self.img_math_lock_scale.isChecked():
                self.im_mt_reset_cm()
        app.setOverrideCursor(QCursor(Qt.ArrowCursor))
    def im_mt_sub_ab(self):
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        self.im_mt_data['c'] = numpy.clip(numpy.nan_to_num(self.im_mt_data['a'] - self.im_mt_data_avg['b']),-1e8,1e8)
        self.im_mt_data_avg['c'] = numpy.average(self.im_mt_data['c'],axis=0)
        self.im_mt_log_data['c']=numpy.log(numpy.clip(self.im_mt_data['c'],0.01,None))
        self.im_mt_log_data_avg['c']=numpy.log(numpy.clip(self.im_mt_data_avg['c'],0.01,None))
        self.im_mt_set_limits('c')
        self.im_mt_size_v['c']=self.im_mt_size_v['a']
        self.im_mt_size_h['c']=self.im_mt_size_h['a']
        self.im_mt_mos_factor_h['c']=self.im_mt_mos_factor_h['a']
        self.im_mt_mos_factor_v['c']=self.im_mt_mos_factor_v['a']
        self.im_mt_img_num['c'] = self.im_mt_img_num['a']
        self.im_mt_names['c'] = self.im_mt_img_num['a'] * ['a-b_avg']
        self.im_mt_text['c'] = 'a-b_avg'
        self.im_mt_mos_recalc('c')
        if self.im_mt_ci() == 'c':
            self.update_im_mt_img()
            if not self.img_math_lock_scale.isChecked():
                self.im_mt_reset_cm()
        app.setOverrideCursor(QCursor(Qt.ArrowCursor))
    def im_mt_div_ab(self):
        app.setOverrideCursor(QCursor(Qt.WaitCursor))
        self.im_mt_data['c'] = numpy.clip(numpy.nan_to_num(self.im_mt_data['a'] / self.im_mt_data_avg['b']),-1e8,1e8)
        self.im_mt_data_avg['c'] = numpy.average(self.im_mt_data['c'],axis=0)
        self.im_mt_log_data['c']=numpy.log(numpy.clip(self.im_mt_data['c'],0.01,None))
        self.im_mt_log_data_avg['c']=numpy.log(numpy.clip(self.im_mt_data_avg['c'],0.01,None))
        self.im_mt_set_limits('c')
        self.im_mt_size_v['c']=self.im_mt_size_v['a']
        self.im_mt_size_h['c']=self.im_mt_size_h['a']
        self.im_mt_mos_factor_h['c']=self.im_mt_mos_factor_h['a']
        self.im_mt_mos_factor_v['c']=self.im_mt_mos_factor_v['a']
        self.im_mt_img_num['c'] = self.im_mt_img_num['a']
        self.im_mt_names['c'] = self.im_mt_img_num['a'] * ['a/b_avg']
        self.im_mt_text['c'] = 'a/b_avg'
        self.im_mt_mos_recalc('c')
        if self.im_mt_ci() == 'c':
            self.update_im_mt_img()
            if not self.img_math_lock_scale.isChecked():
                self.im_mt_reset_cm()
        app.setOverrideCursor(QCursor(Qt.ArrowCursor))
    
    def im_mt_add_ab_each(self):
        if self.im_mt_img_num['a'] == self.im_mt_img_num['b'] and self.im_mt_img_num['a']>0:
            app.setOverrideCursor(QCursor(Qt.WaitCursor))
            self.im_mt_data['c'] = numpy.clip(numpy.nan_to_num(self.im_mt_data['a'] + self.im_mt_data['b']),-1e8,1e8)
            self.im_mt_data_avg['c'] = numpy.average(self.im_mt_data['c'],axis=0)
            self.im_mt_log_data['c']=numpy.log(numpy.clip(self.im_mt_data['c'],0.01,None))
            self.im_mt_log_data_avg['c']=numpy.log(numpy.clip(self.im_mt_data_avg['c'],0.01,None))
            self.im_mt_set_limits('c')
            self.im_mt_size_v['c']=self.im_mt_size_v['a']
            self.im_mt_size_h['c']=self.im_mt_size_h['a']
            self.im_mt_mos_factor_h['c']=self.im_mt_mos_factor_h['a']
            self.im_mt_mos_factor_v['c']=self.im_mt_mos_factor_v['a']
            self.im_mt_img_num['c'] = self.im_mt_img_num['a']
            self.im_mt_names['c'] = self.im_mt_img_num['a'] * ['a+b']
            self.im_mt_text['c'] = 'a+b'
            self.im_mt_mos_recalc('c')
            if self.im_mt_ci() == 'c':
                self.update_im_mt_img()
                if not self.img_math_lock_scale.isChecked():
                    self.im_mt_reset_cm()
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
        else:
            msg = QMessageBox(self)
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error")
            msg.setInformativeText('Number of A and B images must be the same and larger than 0!')
            msg.setWindowTitle("Error")
            msg.show()
            
    def im_mt_sub_ab_each(self):
        if self.im_mt_img_num['a'] == self.im_mt_img_num['b'] and self.im_mt_img_num['a']>0:
            app.setOverrideCursor(QCursor(Qt.WaitCursor))
            self.im_mt_data['c'] = numpy.clip(numpy.nan_to_num(self.im_mt_data['a'] - self.im_mt_data['b']),-1e8,1e8)
            self.im_mt_data_avg['c'] = numpy.average(self.im_mt_data['c'],axis=0)
            self.im_mt_log_data['c']=numpy.log(numpy.clip(self.im_mt_data['c'],0.01,None))
            self.im_mt_log_data_avg['c']=numpy.log(numpy.clip(self.im_mt_data_avg['c'],0.01,None))
            self.im_mt_set_limits('c')
            self.im_mt_size_v['c']=self.im_mt_size_v['a']
            self.im_mt_size_h['c']=self.im_mt_size_h['a']
            self.im_mt_mos_factor_h['c']=self.im_mt_mos_factor_h['a']
            self.im_mt_mos_factor_v['c']=self.im_mt_mos_factor_v['a']
            self.im_mt_img_num['c'] = self.im_mt_img_num['a']
            self.im_mt_names['c'] = self.im_mt_img_num['a'] * ['a-b']
            self.im_mt_text['c'] = 'a-b'
            self.im_mt_mos_recalc('c')
            if self.im_mt_ci() == 'c':
                self.update_im_mt_img()
                if not self.img_math_lock_scale.isChecked():
                    self.im_mt_reset_cm()
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
        else:
            msg = QMessageBox(self)
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error")
            msg.setInformativeText('Number of A and B images must be the same and larger than 0!')
            msg.setWindowTitle("Error")
            msg.show()
    
        
    def im_mt_div_ab_each(self):
        if self.im_mt_img_num['a'] == self.im_mt_img_num['b'] and self.im_mt_img_num['a']>0:
            app.setOverrideCursor(QCursor(Qt.WaitCursor))
            self.im_mt_data['c'] = numpy.clip(numpy.nan_to_num(self.im_mt_data['a'] / self.im_mt_data['b']),-1e8,1e8)
            self.im_mt_data_avg['c'] = numpy.average(self.im_mt_data['c'],axis=0)
            self.im_mt_log_data['c']=numpy.log(numpy.clip(self.im_mt_data['c'],0.01,None))
            self.im_mt_log_data_avg['c']=numpy.log(numpy.clip(self.im_mt_data_avg['c'],0.01,None))
            self.im_mt_set_limits('c')
            self.im_mt_size_v['c']=self.im_mt_size_v['a']
            self.im_mt_size_h['c']=self.im_mt_size_h['a']
            self.im_mt_mos_factor_h['c']=self.im_mt_mos_factor_h['a']
            self.im_mt_mos_factor_v['c']=self.im_mt_mos_factor_v['a']
            self.im_mt_img_num['c'] = self.im_mt_img_num['a']
            self.im_mt_names['c'] = self.im_mt_img_num['a'] * ['a/b']
            self.im_mt_text['c'] = 'a/b'
            self.im_mt_mos_recalc('c')
            if self.im_mt_ci() == 'c':
                self.update_im_mt_img()
                if not self.img_math_lock_scale.isChecked():
                    self.im_mt_reset_cm()
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
        else:
            msg = QMessageBox(self)
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error")
            msg.setInformativeText('Number of A and B images must be the same and larger than 0!')
            msg.setWindowTitle("Error")
            msg.show()
            
    
    def im_mt_add_const(self):
        # if series, change only current image
        if self.im_mt_show_type() == 1:
            self.im_mt_data[self.im_mt_ci()][self.img_math_cur_pix.value()-1] = self.im_mt_data[self.im_mt_ci()][self.img_math_cur_pix.value()-1] + self.img_math_const_add_box.value()
        else:
            self.im_mt_data[self.im_mt_ci()] = self.im_mt_data[self.im_mt_ci()] + self.img_math_const_add_box.value()
        self.im_mt_data_avg[self.im_mt_ci()] = numpy.average(self.im_mt_data[self.im_mt_ci()],axis=0)
        self.im_mt_log_data[self.im_mt_ci()]=numpy.log(numpy.clip(self.im_mt_data[self.im_mt_ci()],0.01,None))
        self.im_mt_log_data_avg[self.im_mt_ci()]=numpy.log(numpy.clip(self.im_mt_data_avg[self.im_mt_ci()],0.01,None))
        self.im_mt_mos_recalc(self.im_mt_ci())
        self.update_im_mt_img()
        
    def im_mt_mult_const(self):
        if self.im_mt_show_type() == 1:
            self.im_mt_data[self.im_mt_ci()][self.img_math_cur_pix.value()-1] = self.im_mt_data[self.im_mt_ci()][self.img_math_cur_pix.value()-1] * self.img_math_const_mult_box.value()
        else:
            self.im_mt_data[self.im_mt_ci()] = self.im_mt_data[self.im_mt_ci()] * self.img_math_const_mult_box.value()
        self.im_mt_data_avg[self.im_mt_ci()] = numpy.average(self.im_mt_data[self.im_mt_ci()],axis=0)
        self.im_mt_log_data[self.im_mt_ci()]=numpy.log(numpy.clip(self.im_mt_data[self.im_mt_ci()],0.01,None))
        self.im_mt_log_data_avg[self.im_mt_ci()]=numpy.log(numpy.clip(self.im_mt_data_avg[self.im_mt_ci()],0.01,None))
        self.im_mt_mos_recalc(self.im_mt_ci())
        self.update_im_mt_img()
        
    def im_mt_div_const(self):
        if self.im_mt_show_type() == 1:
                self.im_mt_data[self.im_mt_ci()][self.img_math_cur_pix.value()-1] = self.im_mt_data[self.im_mt_ci()][self.img_math_cur_pix.value()-1] / self.img_math_const_div_box.value()
        else:
            self.im_mt_data[self.im_mt_ci()] = self.im_mt_data[self.im_mt_ci()] / self.img_math_const_div_box.value()
        self.im_mt_data_avg[self.im_mt_ci()] = numpy.average(self.im_mt_data[self.im_mt_ci()],axis=0)
        self.im_mt_log_data[self.im_mt_ci()]=numpy.log(numpy.clip(self.im_mt_data[self.im_mt_ci()],0.01,None))
        self.im_mt_log_data_avg[self.im_mt_ci()]=numpy.log(numpy.clip(self.im_mt_data_avg[self.im_mt_ci()],0.01,None))
        self.im_mt_mos_recalc(self.im_mt_ci())
        self.update_im_mt_img()
    
    def im_mt_div_switch_ab(self):
        
        for ggg in [self.im_mt_data, self.im_mt_data_avg, self.im_mt_log_data, self.im_mt_log_data_avg, self.im_mt_data_mos, self.im_mt_log_data_mos, self.im_mt_names, self.im_mt_img_num, self.im_mt_data_range, self.im_mt_data_avg_range, self.im_mt_text, self.im_mt_size_v, self.im_mt_size_h, self.im_mt_mos_factor_v,self.im_mt_mos_factor_h]:
            ggg['a'], ggg['b'] = ggg['b'],ggg['a']
        
        if self.im_mt_ci() != 'c':
            self.im_mt_show_img()
        
    def im_mt_div_move_ca(self):
        
        for ggg in [self.im_mt_data, self.im_mt_data_avg, self.im_mt_log_data, self.im_mt_log_data_avg, self.im_mt_data_mos, self.im_mt_log_data_mos, self.im_mt_names, self.im_mt_img_num, self.im_mt_data_range, self.im_mt_data_avg_range, self.im_mt_text, self.im_mt_size_v, self.im_mt_size_h, self.im_mt_mos_factor_v,self.im_mt_mos_factor_h]:
            ggg['a'] = ggg['c']
    
        self.im_mt_show_img()

        
    def im_mt_set_limits(self,uu):
        self.im_mt_data_range[uu]=numpy.array([-10,1.3*heapq.nlargest(5,self.im_mt_data[uu].flatten())[-1]])
        self.im_mt_data_avg_range[uu] = numpy.array([-10,1.3*heapq.nlargest(5,self.im_mt_data_avg[uu].flatten())[-1]])
        #self.update_im_mt_img()
        
    def im_mt_create_scene(self):
        ''' create scene in image math tab
        '''
        self.im_mt_scene = QGraphicsScene(self.img_math_display)
        self.img_math_display.setScene(self.im_mt_scene)
        self.im_mt_show_image = QGraphicsPixmapItem()
        self.im_mt_scene.addItem(self.im_mt_show_image)
        
    def im_mt_create_cm(self):
        ''' create scene in image math tab
        '''
        self.im_mt_cm_scene = QGraphicsScene(self.img_math_cm_disp)
        self.img_math_cm_disp.setScene(self.im_mt_cm_scene)
        self.im_mt_show_cm = QGraphicsPixmapItem()
        self.im_mt_cm_scene.addItem(self.im_mt_show_cm)
        
    
    def update_im_cm_img(self):
        
        self.im_mt_cm_img = QPixmap.fromImage(ImageQt(Image.fromarray(cm_colors(str(self.img_math_cm_choice.currentText())))))
        self.im_mt_show_cm.setPixmap(self.im_mt_cm_img)
        self.img_math_cm_disp.fitInView(self.im_mt_show_cm,Qt.IgnoreAspectRatio)
        self.update_im_mt_img()
        
        
    def math_zoom_spin_changed(self,vvv):
        ''' zoom change
        '''
        # current zoom
        self.im_mt_zoom = self.img_math_display.transform().m11()
        # how much to zoom in this step
        self.im_mt_zoom_factor = vvv/(100*self.im_mt_zoom)
        # make zoom
        self.img_math_display.scale(self.im_mt_zoom_factor, self.im_mt_zoom_factor)
        # update spinbox value
        if self.math_zoom_slider.value() != vvv:
            self.math_zoom_slider.setValue(vvv)
        
    def math_zoom_slider_changed(self,vvv):
        if not ((vvv == self.math_zoom_slider.maximum() and self.math_zoom_spin.value() > vvv) or (vvv == self.math_zoom_slider.minimum() and self.math_zoom_spin.value() < vvv) ):
            self.math_zoom_spin.setValue(vvv)
    
    def im_mt_zoom_select(self):
        if self.img_math_select_zoom.isChecked():
            app.setOverrideCursor(QCursor(Qt.ArrowCursor))
            self.img_math_display.setDragMode(QGraphicsView.NoDrag)
        
        if not self.img_math_select_zoom.isChecked():
            self.im_mt_zoom_select_points = []
            if self.im_mt_zoom_pol != '':
                self.im_mt_scene.removeItem(self.im_mt_zoom_pol)
                self.im_mt_zoom_pol = ''
        
    def im_mt_zoom_into_select(self,xx,yy):
        if self.im_mt_zoom_select_points == []:
            self.im_mt_zoom_select_points.append(xx)
            self.im_mt_zoom_select_points.append(yy)
        else:
            self.img_math_display.fitInView(min(xx,self.im_mt_zoom_select_points[0]),min(yy,self.im_mt_zoom_select_points[1]),abs(xx-self.im_mt_zoom_select_points[0]),abs(yy-self.im_mt_zoom_select_points[1]),Qt.KeepAspectRatio)
            self.im_mt_zoom_select_points = []
            self.img_math_select_zoom.setChecked(False)
            app.restoreOverrideCursor()
            self.img_math_display.setDragMode(QGraphicsView.ScrollHandDrag)
            self.im_mt_scene.removeItem(self.im_mt_zoom_pol)
            self.im_mt_zoom_pol = ''
            #zoom = int(100*self.graphicsView.transform().m11()/self.init_zoom)
            zoom = int(100*self.img_math_display.transform().m11())
            if zoom <= 100000:
                self.math_zoom_spin.setValue(zoom)
            else:
                self.math_zoom_spin.setValue(100000)
                #self.img_math_display.scale(10000/zoom,10000/zoom)
            
    
    def math_max_spin_changed(self,vvv):
        ''' lut maximum change
        '''
        center_pix = self.img_math_display.mapToScene(self.img_math_display.viewport().rect()).boundingRect().center()
        self.update_im_mt_img()
        self.img_math_display.centerOn(center_pix)
        if self.img_math_im_log.isChecked():
            self.math_max_slider.setValue(round(10000*numpy.log(vvv)))
        else:
            self.math_max_slider.setValue(100*round(vvv))
        
    
    def math_max_slider_changed(self,vvv):
        if self.img_math_im_log.isChecked():
            bbb = numpy.log(self.math_max_spin.value())*10000
        else:
            bbb = self.math_max_spin.value()*100
        
        if not ((vvv == self.math_max_slider.maximum() and bbb > vvv) or (vvv == self.math_max_slider.minimum() and bbb < vvv) ):
            if self.img_math_im_log.isChecked():
                if perc_dif(vvv,bbb) > 1 or abs(vvv/100)<1:
                    self.math_max_spin.setValue(numpy.exp(vvv/10000))
            else:
                self.math_max_spin.setValue(vvv/100)
        
        
    def math_min_spin_changed(self,vvv):
        ''' lut minimum change
        '''
        center_pix = self.img_math_display.mapToScene(self.img_math_display.viewport().rect()).boundingRect().center()
        self.update_im_mt_img()
        self.img_math_display.centerOn(center_pix)
        if self.img_math_im_log.isChecked():
            self.math_min_slider.setValue(round(10000*numpy.log(vvv)))
        else:
            self.math_min_slider.setValue(round(100*vvv))      
            
    def math_min_slider_changed(self,vvv):
        if self.img_math_im_log.isChecked():
            bbb = numpy.log(self.math_min_spin.value())*10000
        else:
            bbb = self.math_min_spin.value()*100
            
        if not ((vvv == self.math_min_slider.maximum() and bbb > vvv) or (vvv == self.math_min_slider.minimum() and bbb < vvv) ):
            if self.img_math_im_log.isChecked():
                if perc_dif(vvv,bbb) > 1 or abs(vvv)<1:
                    self.math_min_spin.setValue(numpy.exp(vvv/10000))
            else:
                self.math_min_spin.setValue(vvv/100)
    
    
    
    def im_mt_reset_cm(self):
        if self.img_math_im_log.isChecked():
            self.math_min_spin.setValue(max(0.01,self.im_mt_data_range[self.im_mt_ci()][0]))
        else:
            self.math_min_spin.setValue(self.im_mt_data_range[self.im_mt_ci()][0])
        self.math_max_spin.setValue(1.1*self.im_mt_data_range[self.im_mt_ci()][1])
        
     
    def im_mt_save_imgs(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        if self.img_math_folder == '':
            self.img_math_folder=os.path.normpath(os.path.abspath('.'))
            
        self.im_save_base = str(QFileDialog.getSaveFileName(self,"Select basename for image file(s) (base_N.tif)", self.img_math_folder,"All Files (*)", options=options))
        
        for ooo in range(self.im_mt_img_num[self.im_mt_ci()]):
            temp_name = '{}_{}.tif'.format(self.im_save_base,str(ooo+1).zfill(len(str(self.im_mt_img_num[self.im_mt_ci()]))))
            qq = self.im_mt_data[self.im_mt_ci()][ooo].astype(numpy.uint16)
            qq_im = Image.frombytes('I;16',(self.im_mt_size_h[self.im_mt_ci()],self.im_mt_size_v[self.im_mt_ci()]),qq.tobytes())
            qq_im.save(temp_name)
    
    
    ## end of image math tab
    
    
   ####
    ## main window
   ####
    
    def status_message(self):
        ''' change status message
        '''
        if self.set_angles_man.isChecked() or self.set_center_1.isChecked() or self.add_polygon.isChecked() or self.remove_polygon.isChecked() or self.add_pixels.isChecked() or self.remove_pixels.isChecked() or self.zoom_select.isChecked() or self.mask_zoom_select.isChecked():
            self.statusBar.setStyleSheet("QStatusBar{background:yellow;color:black;font-weight:bold;}")
            self.statusBar.showMessage("           Select point(s) with mouse left-click")
        else:
            self.statusBar.setStyleSheet("QStatusBar{background:None}")
            self.statusBar.clearMessage()
            app.restoreOverrideCursor()
            #self.graphicsView_mask.setDragMode(QGraphicsView.ScrollHandDrag)
            #self.graphicsView.setDragMode(QGraphicsView.ScrollHandDrag)
            
    
    def update_footer(self):
        ''' update footer info
        '''
        self.data_folder_txt.setText("data folder: {}".format(self.data_folder))
        self.analysis_folder_txt.setText("analysis folder: {}".format(self.analysis_folder))
        self.radint_data_text.setText('{}'.format(self.data_folder))
        if self.use_saxspipe.isChecked():
            self.radint_analysis_text.setText(os.path.normpath(os.path.join(self.data_folder,'../analysis/')))
        else:
            self.radint_analysis_text.setText('{}'.format(self.analysis_folder))
        
        
        if self.agbe_file != '':
            self.agbe_file_txt.setText("agbe file: {}  {}x{}".format(self.agbe_file, self.agbe_size_h, self.agbe_size_v))
        else:
            self.agbe_file_txt.setText("agbe file: {}".format(self.agbe_file))
        if self.calc_dist == 0:
            self.calc_dist_str = ''
        else:
            self.calc_dist_str = '{:.1f}'.format(self.calc_dist) + ' mm'
        self.calculated_distance.setText("calculated distance: {}".format(self.calc_dist_str))
        
        if self.cx1 != '':
            if self.cx2 != '':
                self.center_info.setText('center: {}, {} (rough);\t{:.2f}, {:.2f} (refined)'.format(self.cx1,self.agbe_size_v-self.cy1,self.cx2,self.agbe_size_v-self.cy2))
            else:
                self.center_info.setText('center: {}, {} (rough)'.format(self.cx1,self.agbe_size_v-self.cy1))
        else:
            self.center_info.setText('center:')
    
       
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWindow()
    sys.exit(app.exec_())
    
    
