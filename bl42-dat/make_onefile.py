#!/usr/bin/env python
import os

with open('bl42-dat_last1file.py','r') as f1:
    data_1 = f1.read()
    
data_1t, data_1b = data_1.split('#insert_gui_code_here')

# remove import from des_tabs.py into bl42-dat.py
data_1t = '\n'.join(data_1t.split('\n')[0:-2])

#data_1t = '#!/mnt/home/staff/SW/python/bin/python\n'+data_1t


with open('des_tabs4.py','r') as f2:
    data_2 = f2.read()

data_2 = '\n'.join(data_2.split('\n')[9:])
data_2 = data_2.replace('QtGui.','')
data_2 = data_2.replace('QtCore.','')

# remove mpl_widget import from des_tabs.py
data_2 = data_2.replace('from mpl_widget import mpl_widget','')

# with open('mpl_widget.py','r') as f3:
#     data_3 = f3.read()
# 
# 
# data_3 = data_3.replace('from PyQt4.QtGui import QWidget, QVBoxLayout','')

if os.path.isfile('bl42-dat'):
    os.remove('bl42-dat')

with open('bl42-dat','w') as out_file:
    out_file.write(data_1t)
    #out_file.write(data_3)
    out_file.write(data_2)
    out_file.write(data_1b)
    
os.chmod('bl42-dat', 0o0755)
