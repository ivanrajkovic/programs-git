import numpy
from PIL import Image
import math
import matplotlib.pyplot as plt

img = Image.open('/mnt/home/rajkovic/bitbucket/programs-git/bl42-dat/tif_nonint/agbe_nonint.tif')


img2d = numpy.array(img)

img2d=img2d.clip(0,500)

(ww, hh) = img2d.shape

(xc, yc) = (205,418)

polar_im = numpy.zeros(img2d.shape)

#maxr = sqrt(width**2 + height**2)/2
maxr = 1100
rscale = 1.0*ww / maxr
tscale = 1.0 * hh / (2*math.pi)
for y in range(0, hh):
    dy = y - yc
    for x in range(0, ww):
        dx = x - xc
        t =  (math.atan2(dy,dx)+math.pi)%(2*math.pi)
        r = math.sqrt(dx**2+dy**2)
        #color = getColor(getPixel(pic, x, y))
        #setColor( getPixel(radial,int(r*rscale),int(t*tscale)), color)
        try:
            polar_im[int(r*rscale),int(t*tscale)] = img2d[x,y]
        except:
            print(x,y,r*rscale,t*tscale)

plt.imshow(polar_im)
plt.show()

