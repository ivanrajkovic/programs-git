import numpy

from PyQt4.QtGui import QPen, QColor
def bl42dat_init(self):
    ## qcalib_tab init

    # set image viewer
    self.create_scene()

    # set some variables
    self.data_folder = ''
    self.analysis_folder = ''
    self.agbe_file = ''
    self.det = ''
    self.calc_dist = 0
    self.pix_data = ''
    self.scale_min = 50
    self.scale_max = 950
    self.gray_data = numpy.array(0)
    self.gray_data_log = numpy.array(0)
    self.agbe_data_min = numpy.array(0)
    self.agbe_data_max = numpy.array(0)
    self.agbe_data_log_min = numpy.array(0)
    self.agbe_data_log_max = numpy.array(0)
    self.zoom_factor = 1
    self.angles_list = []
    self.angles_width = []
    self.integmpp = ''
    self.d_agbe = 58.38
    self.zoom_select_points = []
    self.zoom_pol = ''

    self.cx1 = ''
    self.cy1 = ''
    self.cx2 = ''
    self.cy2 = ''
    self.i2 = 1
    self.det_bin=1
    #q=0 is always at 0 px
    self.ring_px = numpy.array([0])


    self.graphicsView.setMouseTracking(True)
    self.graphicsView.viewport().installEventFilter(self)

    ## mask_tab init
    # set image viewer
    self.create_mask_scene()

    self.im4mask_folder = ''
    self.im4mask = ''
    self.im4mask_path = ''
    self.mask_scale_min = 50
    self.mask_scale_max = 950
    self.track_mouse_line = ''
    self.maskpen = QPen(QColor(255,0,0))
    #self.maskpen.setWidthF(.5)
    self.zoompen = QPen(QColor(255,0,0))
    #self.zoompen.setWidth(2)
    self.savemask_tif_path = ''
    self.mask_zoom_select_points = []
    self.mask_zoom_pol = ''

    self.graphicsView_mask.setMouseTracking(True)
    self.graphicsView_mask.viewport().installEventFilter(self)

    ## rad_integ_tab init
    self.buffer_path = ''
    self.sample_first_path = ''
    self.sample_last_path = ''

    ## hplc tab
    self.hplc_data = ''
    self.hplc_analysis_folder = ''
    self.hplc_ave_folder = ''
    self.hplc_plots_folder = ''
    self.hplc_sastool_folder = ''
    self.im_num_ar = ''
    self.rg_ar = ''
    self.i0_ar = ''
    self.iqmin_ar = ''
    self.uv_num = ''
    self.uv1 = ''
    self.uv2 = ''
    self.leg_pos = ''
    self.ll = ''
    self.hplc_title = ''
    self.hplc_norm = [1,1]
    self.data_to_plot = list('' for i in range(7))
    self.x_to_plot = list('' for i in range(7))
    self.data_label = ['R$_g$ [$\AA$]','I$_0$','I$_{q_{L}}$','UV$_1$','UV$_2$','UV$_1$/UV$_2$','I$_0$/UV$_1$']
    self.cc = ['bo','go','ro','co','mo','ko','g-']
    self.hplc_plot_lines = list([] for i in range(7))
    self.hplc_xaxis = [0,0,0,1,1,1,1]
    self.hplc_yaxes=[self.mpl_plot.canvas.ax1,self.mpl_plot.canvas.ax2]

    ## integmpp tab
    self.integmpp_edit_file = ''

    ## img math tab
    self.im_mt_create_scene()
    self.im_mt_create_cm()
    self.im_mt_last_update = 0
    self.img_math_folder = ''

    self.mosaicpen = QPen(QColor(255,0,0))
    self.mosaicpen.setWidth(10)

    self.im_mt_current = 0

    self.im_mt_names = {'a': [],'b': [],'c': []}
    self.im_mt_img_num = {'a': 0,'b': 0,'c': 0}

    self.im_mt_data = {'a': numpy.array([]),'b': numpy.array([]),'c': numpy.array([])}
    self.im_mt_data_avg = {'a': numpy.array([]),'b': numpy.array([]),'c': numpy.array([])}
    self.im_mt_log_data = {'a': numpy.array([]),'b': numpy.array([]),'c': numpy.array([])}
    self.im_mt_log_data_avg = {'a': numpy.array([]),'b': numpy.array([]),'c': numpy.array([])}

    self.im_mt_data_mos = {'a': numpy.array([]),'b': numpy.array([]),'c': numpy.array([])}
    self.im_mt_log_data_mos = {'a': numpy.array([]),'b': numpy.array([]),'c': numpy.array([])}



    self.im_mt_data_range = {'a': numpy.array([0,0]),'b': numpy.array([0,0]),'c': numpy.array([0,0])}
    self.im_mt_data_avg_range = {'a': numpy.array([0,0]),'b': numpy.array([0,0]),'c': numpy.array([0,0])}
    #self.im_mt_log_data_range = {'a': numpy.array([1,1]),'b': numpy.array([1,1]),'c': numpy.array([1,1])}
    #self.im_mt_log_data_avg_range = {'a': numpy.array([1,1]),'b': numpy.array([1,1]),'c': numpy.array([1,1])}

    self.im_mt_text = {'a': '','b': '','c': ''}

    self.im_mt_size_v = {'a': 0,'b': 0,'c': 0}
    self.im_mt_size_h = {'a': 0,'b': 0,'c': 0}

    self.im_mt_mos_factor_v = {'a': 0,'b': 0,'c': 0}
    self.im_mt_mos_factor_h = {'a': 0,'b': 0,'c': 0}


    self.im_mt_zoom_select_points = []
    self.im_mt_zoom_pol = ''

    self.img_math_display.setMouseTracking(True)
    self.img_math_display.viewport().installEventFilter(self)

    self.im_mt_cur_pix_pol = ''
