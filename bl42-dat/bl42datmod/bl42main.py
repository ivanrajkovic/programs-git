# functions used in bl42-dat - general part
from __future__ import division

import numpy
import re
import os

from matplotlib import cm
from matplotlib import colors
from scipy.optimize import curve_fit, leastsq

from PIL import Image

def circ(sx,sy,rx,ry,rr,inth,intl):
    ''' create matrix filled with intl excpet for a cirle filled with inth
    '''
    ccxx, ccyy = numpy.mgrid[:sx, :sy]
    ccircle = (ccxx - rx) ** 2 + (ccyy - ry) ** 2
    cc=numpy.where(ccircle<rr**2,inth,intl)
    return cc

def gray_to_color(im2d_data,scale_min,scale_max,cm_data='nipy_spectral'):
    ''' create color image from grayscale
    '''
    cmap = cm.get_cmap(cm_data)
    norm = colors.Normalize(min(scale_min, scale_max),max(scale_min, scale_max),clip=True)
    #if scale_min > scale_max:
    #    cmap = cmap.reversed()

    mm = cm.ScalarMappable(norm=norm,cmap=cmap)
    mapped_img = (mm.to_rgba(im2d_data,bytes=True))
    return(mapped_img)


def cm_colors(cm_data='nipy_spectral'):
    #rr = numpy.arange(256)
    rr = numpy.tile(numpy.arange(256),(2,1))
    cmap = cm.get_cmap(cm_data)
    norm = colors.Normalize(0, 255)
    mm = cm.ScalarMappable(norm=norm,cmap=cmap)
    mapped_cm = (mm.to_rgba(rr,bytes=True))
    return(mapped_cm)



def is_ccw(p):
    ''' check if the pth is counter-clockwise
    '''
    v = p.vertices-p.vertices[0,:]
    a = numpy.arctan2(v[1:,1],v[1:,0])
    return (a[1:] >= a[:-1]).astype(int).mean() >= 0.5

def gauss(x, *p):
    ''' gauss function
    '''
    A, mu, sigma = p
    return A*numpy.exp(-(x-mu)**2/(2.*sigma**2))

def fit1dgauss(data,cen,dx,det_bin):
    ''' fit 1d gauss
    '''
    cen = int(cen)
    dx = int(dx)
    pp = [data[(cen-dx):(cen+dx),1].max()-data[(cen-dx):(cen+dx),1].min(),cen,40/det_bin]
    coeff, var_matrix = curve_fit(gauss,data[(cen-dx):(cen+dx),0],data[(cen-dx):(cen+dx),1],p0=pp)
    return coeff, var_matrix

def calc_R(x,y, xc, yc):
    """ calculate the distance of each 2D points from the center (xc, yc) """
    return numpy.sqrt((x-xc)**2 + (y-yc)**2)

def f_2(c, x, y):
    """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
    Ri = calc_R(x, y, *c)
    return Ri - Ri.mean()

def make_ang_mask(sh,sv,cx,cy,start_ang,ang_range,name='angular_mask.tif'):
    ''' make a mask for integration over
    angular wedge start_ang +- ang_range/2
    '''
    xm, ym = numpy.meshgrid(numpy.arange(sh),numpy.arange(sv))
    zm = numpy.degrees(numpy.arctan2(-ym+cy,xm-cx))
    zm = numpy.where(numpy.abs(zm-start_ang)<(0.5*ang_range),1,0)
    zm = numpy.flipud(zm.astype('uint16'))
    im = Image.frombytes('I;16',(sh,sv),zm.tobytes())
    im.save(name)

def find_image(folder,num):
    ''' find image file from 'num' series in 'folder' folder
    '''
    im_list=[]
    # find the image filename for the given series number
    rrr = re.compile('.*[SBT]'+str(num).zfill(3)+'_0_0+1\.tif')
    for file in os.listdir(folder):
        if rrr.match(file):
            im_list.append(file)
    if len(im_list) == 0:
        result = 0
    elif len(im_list) > 1:
        print("\n\033[0;37;41m\033[1m\033[4m Two or more files found! \033[0;0m")
        result = 1
    else:
        result = os.path.abspath(os.path.join(folder,im_list[0]))

    return result

def grow_mask(mm):
    cc = numpy.ones_like(mm)
    mx,my = mm.shape
    ii = numpy.where(mm==0)
    for xi,yi in zip(*ii):
        cc[max(0,xi-1):min(xi+2,mx),max(0,yi-1):min(yi+2,my)] = 0
    return(cc)

def perc_dif(a,b):
    if b!=0:
        pd = abs(100*(a-b)/b)
    else:
        pd = abs(a*100)
    return(pd)
