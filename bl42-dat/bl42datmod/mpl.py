# make matplotlib graph in PyQt4

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as Canvas
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

from PyQt4.QtGui import QWidget, QVBoxLayout

class MplCanvas(Canvas):
    def __init__(self):
        self.fig = Figure()
        self.ax1 = self.fig.add_subplot(111)
        self.ax1.grid()
        self.ax2 = self.ax1.twinx()
        Canvas.__init__(self, self.fig)
        Canvas.updateGeometry(self)

class mpl_widget(QWidget):
    def __init__(self, parent = None):
        QWidget.__init__(self, parent)
        self.canvas = MplCanvas()
        self.mpl_toolbar = NavigationToolbar(self.canvas, self)
        self.vbl = QVBoxLayout()
        self.vbl.addWidget(self.mpl_toolbar)
        self.vbl.addWidget(self.canvas)
        self.setLayout(self.vbl)
