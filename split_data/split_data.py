#!/usr/bin/env python

'''
@author: rajkovic

'''

import sys
import os
import shutil
import glob
import re
from distutils.dir_util import mkpath


if __name__ == "__main__":

    if (len(sys.argv) != 4):
        print('\nHow-to:\n')
        print('{} <folder> <start series> <end series>\n'.format(os.path.basename(sys.argv[0])))
        print('The program should be run inside the folder containing \'data\' and \'analysis\' folders.')
        print('The program will copy all the data and analysis files from <start series> to <end series> (both included) into the <folder> (created if not existing).\n')
        sys.exit('\n')
            
    
    copy_folder = sys.argv[1]
    start_ser = int(sys.argv[2])
    end_ser = int(sys.argv[3])
    
    n_analysis = os.path.join(copy_folder,'analysis')
    n_data = os.path.join(copy_folder,'data')
    

    
    mkpath(n_analysis)
    mkpath(n_data)
    
    # copy integ.mpp and mask files from analysis

    for ff in glob.glob('analysis/*mpp'):
        shutil.copy2(ff,n_analysis)
        
    for ff in glob.glob('analysis/*tif'):
        shutil.copy2(ff,n_analysis)
    
    for ff in glob.glob('analysis/*msk'):
        shutil.copy2(ff,n_analysis)
        
    # check if automatic_analysis exists
    if os.path.exists('./analysis/automatic_analysis'):
        aut_an = 1
        n_autan = os.path.join(n_analysis,'automatic_analysis')
        n_autan_sas = os.path.join(n_autan,'sastool')
        n_autan_sub = os.path.join(n_autan,'sub_files')
        n_autan_oth = os.path.join(n_autan,'other_files')
        mkpath(n_autan_sas)
        mkpath(n_autan_sub)
        mkpath(n_autan_oth)
        
        # copy bokeh and jquesry files, if there
        if os.path.exists('analysis/automatic_analysis/other_files/bokeh.min.css'):
            shutil.copy2('analysis/automatic_analysis/other_files/bokeh.min.css',n_autan_oth)
        if os.path.exists('analysis/automatic_analysis/other_files/bokeh.min.js'):
            shutil.copy2('analysis/automatic_analysis/other_files/bokeh.min.js',n_autan_oth)
        if os.path.exists('analysis/automatic_analysis/other_files/jquery.min.js'):
            shutil.copy2('analysis/automatic_analysis/other_files/jquery.min.js',n_autan_oth)
        
    else:
        aut_an = 0
    
    # copy files for each series
    for nn in range(start_ser,end_ser+1):
        print('copying series {}'.format(str(nn).zfill(3)))
        # start with data folder
        re_2d = re.compile('.*[SBT]'+str(nn).zfill(3)+'(_0_|_)\d+\.(tif|prp)$')
        re_int = re.compile('.*[SBT]'+str(nn).zfill(3)+'.*s1.int$')
        
        for ff in os.listdir('./data/'):
            if re_2d.match(ff) or re_int.match(ff):
                shutil.copy2(os.path.join('./data',ff),n_data)
                
        # copy analysis folder
        re_sas = re.compile('.*[SBT]'+str(nn).zfill(3)+'(_0_|_)\d+\.(dat|sub|tot|log)$')
        for ff in  os.listdir('./analysis/'):
            if re_sas.match(ff):
                shutil.copy2(os.path.join('./analysis',ff),n_analysis)
        
        # go through automatic_analysis, if exsits
        if aut_an == 1:
            # copy sastool files
            re_out = re.compile('.*[SBT]'+str(nn).zfill(3)+'_.*_.*\.out$')
            for ff in os.listdir('analysis/automatic_analysis/sastool'):
                if re_out.match(ff) or re_sas.match(ff):
                    shutil.copy2(os.path.join('analysis/automatic_analysis/sastool',ff),n_autan_sas)
            
            # copy sub files
            re_sub = re.compile('.*[SBT]'+str(nn).zfill(3)+'_[SBT]\d+_sub.dat$')
            for ff in os.listdir('analysis/automatic_analysis/sub_files'):
                if re_sub.match(ff):
                    shutil.copy2(os.path.join('analysis/automatic_analysis/sub_files',ff),n_autan_sub)
            
            # copy other files
            re_oth = re.compile('.*[SBT]'+str(nn).zfill(3)+'.*(png|txt|html|log)$')
            for ff in os.listdir('analysis/automatic_analysis/other_files'):
                if re_oth.match(ff):
                    shutil.copy2(os.path.join('analysis/automatic_analysis/other_files',ff),n_autan_oth)
                    
                    
    
    # copy the html results file
    res_html = 'analysis/automatic_analysis/results.html'
    if os.path.isfile(res_html):
        html_base = file(res_html).read()
        
        html_head, html_body = html_base.split('</thead>')
        n_html = html_head+'</thead>\n'
        
        html_r = html_body.split('</tr>')
        
        for hh in html_r[:-1]:
            filename = hh.split('<td>')[3].split('href=\"')[1].split('\"')[0].split('/')[1]
            f_nn = int(filename.split('_')[-3][1:])
            if start_ser <= f_nn <= end_ser:
                n_html = n_html + hh + '</tr>'
            
        n_html = n_html+ html_r[-1]
        
        with open(os.path.join(n_autan,'results.html'),'w') as n_results:
            n_results.write(n_html)
       
       

            
        
    
        
        
    
    
        
    
    