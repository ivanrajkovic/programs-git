import os
import numpy
import matplotlib.pyplot as plt

os.chdir("/mnt/home/rajkovic/bitbucket/programs-git/saxspipe2/uv_test/3")

uvb = 'l1b_02A_B048_0_00.uv'
uvs = 'l1b_02E_S052_0_00.uv'

uvs = 'uv_test_scan.uv'

s_uv = numpy.loadtxt(uvs)
b_uv = numpy.loadtxt(uvb)

uv_wl = s_uv[:,0]

s_uv_n = s_uv[:,1]
b_uv_n = b_uv[:,1]


uv_abs1 = 1-(s_uv_n/b_uv_n)
uv_abs2 = numpy.log10(b_uv_n/s_uv_n)

uv_ind_260 = numpy.absolute(uv_wl-260).argmin()
uv_ind_280 = numpy.absolute(uv_wl-280).argmin()
uv_ind_320 = numpy.absolute(uv_wl-320).argmin()


print(uv_abs1[uv_ind_260],uv_abs1[uv_ind_280],uv_abs1[uv_ind_320])
print(uv_abs2[uv_ind_260],uv_abs2[uv_ind_280],uv_abs2[uv_ind_320])

pl = 2.5
uv_dc=1
ext_coef = 1
line_comment = '#'
with open(uvs) as uu:
    while line_comment == '#':
        ll = uu.next().strip()
        line_comment=ll[0]
        print(ll)
        if ll.startswith('# absorption'):
            try:
                ext_coef = float(ll.split()[-1])
            except:
                ext_coef = 1
            print(ext_coef)
        elif ll.startswith('# wavelength'):
            if ll.split()[-1].startswith('corrected'):
                uv_dc = 1
            print(uv_dc)
        elif ll.startswith('# path'):
            try:
                pl = float(ll.split()[-1][:-2])
            except:
                pl = 1
            print(pl)



uv_px_avg = 2

uv_abs_320_avg = numpy.average(uv_abs[uv_ind_320-uv_px_avg:uv_ind_320+uv_px_avg+1])

uv_abs_260_320 = numpy.average(uv_abs[uv_ind_260-uv_px_avg:uv_ind_260+uv_px_avg+1]) - uv_abs_320_avg
uv_abs_280_320 = numpy.average(uv_abs[uv_ind_280-uv_px_avg:uv_ind_280+uv_px_avg+1]) - uv_abs_320_avg

uv_280_260 = uv_abs_280_320/uv_abs_260_320


# calculate protein concentration (needs a coefficient from BluIce, path length = 2.5mm)
s_uv_conc = uv_abs_280_320/(ext_coef*.25)

print(s_uv_conc)

plt.figure()
plt.plot(uv_wl,uv_abs1)
plt.plot(uv_wl,uv_abs2)
plt.axvline(x = 260, color = 'b', linestyle = 'dashed')
plt.axvline(x = 280, color = 'r', linestyle = 'dashed')
plt.axvline(x = 320, color = 'g', linestyle = 'dashed')
plt.show()