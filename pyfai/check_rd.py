#!/usr/bin/env python

# check 2d and integrated images for radiation damage

import sys
import os
import numpy
import argparse
import glob
import pyFAI
from PIL import Image
import time
import matplotlib.pyplot as plt

import statistics

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
    Check data for radiation damage.
    """)
    parser.add_argument('filename', help='first image file')
    parser.add_argument('-n','--num_im', type=int, default=1, help='number of images, default is 1')
    parser.add_argument('-p','--poni_file', default='', help='poni file to use, default is ../analysis/integ.poni.')
    parser.add_argument('-m','--mask_file', default='', help='mask file to use, default is imported from ../analysis/integ.mpp.')

    arg_in = parser.parse_args()

    im_file = arg_in.filename
    num_im = arg_in.num_im
    poni_file = arg_in.poni_file
    mask_file = arg_in.mask_file

    mpp_file = os.path.realpath(os.path.join(os.path.split(im_file)[0],'..','analysis','integ.mpp'))
    i_mpp = 1

    if poni_file == '':
        poni_file = os.path.realpath(os.path.join(os.path.split(im_file)[0],'..','analysis','integ.poni'))
        if not os.path.isfile(poni_file):
            sys.exit()('no poni')
            # create poni file


    with open(mpp_file) as mm:
        for ll in mm:
            if (mask_file == '') and ll.startswith('-m'):
                mask_file = os.path.join(os.path.split(mpp_file)[0],ll.split()[-1])
            if ll.startswith('-i'):
                i_mpp = float(ll.split()[-1])


    mask_data = numpy.asarray(Image.open(mask_file))
    inv_mask_data = 1-mask_data


    ai = pyFAI.load(poni_file)

    #im_shape = ai.get_shape()
    im_shape = mask_data.shape

    #num_pix = sum(mask_data)

    data_2d = numpy.zeros([num_im,im_shape[0],im_shape[1]])


    # get number of bins
    # read sastool .dat, find number of bins
    num_bins=1000

    data_1d = numpy.zeros([num_im,num_bins])

    num_dig = len(os.path.splitext(im_file)[0].split('_')[-1])

    vr_1d = []
    vr_2d = []
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    line1, = ax.plot(1, 0,'x')
    plt.xlim([0.5,num_im+.5])
    plt.ylim([-50,50])
    fig.canvas.flush_events()

    i0_norm = 1
    i_norm = 1

    for nn in range(0,num_im):
        im_current = '_'.join(os.path.splitext(im_file)[0].split('_')[:-1])+'_'+str(nn+1).zfill(num_dig)+'.tif'
        prp_current = os.path.splitext(im_current)[0]+'.prp'

        while not os.path.isfile(im_current):
            print('waiting for tif')
            print(im_current)
            time.sleep(.5)

        data_2d[nn,:,:] = numpy.asarray(Image.open(im_current))


        if nn == 0:
            # add hot pixels, do only once witht he first image
            #print(numpy.sum(mask_data))
            mask_data = mask_data - (data_2d[nn]>8e5)
            inv_mask_data = 1-mask_data
            #print(numpy.sum(mask_data))

        while not os.path.isfile(prp_current):
            print('waiting for prp')
            time.sleep(.5)



        with open(prp_current) as pp:
            for ll in pp:
                if ll.startswith('I_2'):
                    if nn == 0:
                        i0_norm = float(ll.split('=')[1].split()[0])
                    i_norm = float(ll.split('=')[1].split()[0])

        res=ai.integrate1d_ng(data_2d[nn,:,:],num_bins,mask=inv_mask_data, normalization_factor=i_norm/i_mpp)  # error_model='poisson'|'azimuthal'
        data_1d[nn,:] = res[1]
        if nn > 0:
            #svv1 = statistics.pvariance(data_1d[nn]-data_1d[0])
            #svv2 = statistics.pvariance(data_2d[nn].flatten() - data_2d[0].flatten())
            vv1 = numpy.sqrt(numpy.sum((data_1d[nn]-data_1d[0])**2))
            vv2 = numpy.sqrt(numpy.sum((mask_data*(data_2d[nn]*(i0_norm/i_norm) - data_2d[00]))**2))   # data_2d[nn]*(i0_norm/i_norm)
            # print(numpy.mean(mask_data*(data_2d[nn] - data_2d[00])))
            # print(numpy.mean(mask_data*(data_2d[nn]*i0_norm/i_norm - data_2d[00])))
            #print(numpy.max(mask_data*(data_2d[nn])))
            # print(numpy.max(numpy.abs(mask_data*(data_2d[nn] - data_2d[00]))))
            # print(numpy.max(numpy.abs(mask_data*(data_2d[nn]*i0_norm/i_norm - data_2d[00]))))
            # print('')
            vr_1d.append(vv1)
            vr_2d.append(vv2)

            # add new pont to the plot
            line1.set_xdata(numpy.append(line1.get_xdata(), nn+1))
            line1.set_ydata(numpy.append(line1.get_ydata(), vv2))

            # change ylim, redraw plot
            #plt.gca().set_ylim()
            plt.ylim(top=numpy.max(line1.get_ydata())*1.1)
            fig.canvas.draw()
            fig.canvas.flush_events()
    print(vr_1d,'\n',vr_2d)
    # don't close the graph automatically
    plt.ioff()
    plt.show()
