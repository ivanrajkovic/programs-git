#!/usr/bin/env python2

# convert sastool integ.mpp file to pyfai .poni file

import sys
import os
import numpy
import argparse
import glob


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="""
    Convert sastool integ.mpp to pyFDAI poni file.
    """)
    parser.add_argument('filename', help='sastool .mpp file')
    parser.add_argument('-o','--out_file', default='', help='output .poni file, default is the same basename as .mpp')
    parser.add_argument('-e','--energy', default=0, help='Beam energy. If not specified it will be read from .prp file')

    arg_in = parser.parse_args()

    mpp = arg_in.filename
    ener = arg_in.energy


    if arg_in.out_file != '':
        poni = arg_in.out_file
    else:
        poni = os.path.splitext(mpp)[0]+'.poni'


    if not os.path.isfile(mpp):
        sys.exit('nope2')

    c_x = ''
    c_y = ''
    q_cal = ''
    mask_f = ''
    im_file = ''
    prp_file = ''

    # rot angles are 0
    rot1 = 0
    rot2 = 0
    rot3 = 0

    with open(mpp) as m:
        for ll in m:
            if ll.startswith('-c'):
                c_x, c_y = ll.split()[1:3]
            if ll.startswith('-q'):
                q_cal = ll[3:]
            if ll.startswith('-m'):
                mask_f = ll.split()[-1]
            if ll.startswith('-f'):
                im_file = ll.split()[-1]


    p1 = (1043-float(c_y)-.5)*.000172
    p2 = (float(c_x)+.5)*.000172

    # get energy
    if ener == 0:
        if os.path.isfile(os.path.splitext(im_file)[0]+'.prp'):
            prp_file = os.path.splitext(im_file)[0]+'.prp'
        else:
            prp_files = glob.glob(os.path.join('..','analysis','*.prp'))
            prp_files.sort(key=os.path.getmtime)
            if len(prp_files) > 0:
                prp_file=prp_files[-1]

        if prp_file == '':
            sys.exit('Cannot determine bema energy!')

        with open(prp_file) as pp:
            for ll in pp:
                if 'Beam energy' in ll:
                    ener = float(ll.split()[-2].split('=')[-1])/1000

    #calculate distance

    if q_cal.split()[0] == 'wide':
        # in case we are in WAXS setup
        dist = float(q_cal.split()[1])
    else:
        dist_all = []
        q_all = q_cal.split()[3:]
        for ii in range(int(len(q_all)/2)):
            # ring number
            rn_nb = round((float(q_all[2*ii+1])/.1076))
            dd = float(q_all[2*ii])*(172e-6)/numpy.tan(2*rn_nb*numpy.arcsin((12.39842/ener)/(2*58.38)))
            dist_all.append(dd)
        dist = numpy.average(dist_all)

    with open(poni,'w') as pf:
        pf.write('# imported from {} file\n'.format(mpp))
        pf.write('poni_version: 2\n')
        pf.write('Detector: Pilatus1M\n')
        pf.write('Detector_config: {}\n')
        pf.write('Distance: {}\n'.format(dist))
        pf.write('Poni1: {}\n'.format(p1))
        pf.write('Poni2: {}\n'.format(p2))
        pf.write('Rot1: {}\n'.format(rot1))
        pf.write('Rot2: {}\n'.format(rot2))
        pf.write('Rot3: {}\n'.format(rot3))
        pf.write('Wavelength: {}\n'.format((1e-10)*12.39842/ener))










