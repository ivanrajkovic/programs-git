import numpy as np
import shutil
import os

import fileinput
import re

from bokeh.layouts import gridplot, row
from bokeh.plotting import figure, output_file, save # , show
from bokeh.sampledata.stocks import AAPL, GOOG, IBM, MSFT
from bokeh.resources import INLINE
from bokeh.models import HoverTool
import bokeh.settings


def datetime(x):
    return np.array(x, dtype=np.datetime64)

os.chdir('/fs/home/rajkovic/bitbucket/programs/bokeh_test')



for aa in bokeh.settings.settings.js_files():
    if os.path.basename(aa) == 'bokeh.min.js':
        b_js = aa

for aa in bokeh.settings.settings.css_files():
    if os.path.basename(aa) == 'bokeh.min.css':
        b_css = aa

if not (os.path.isfile('bokeh.min.js') and os.path.isfile('bokeh.min.css')):
    shutil.copy(b_js,'./')
    shutil.copy(b_css,'./')
    print('files copied')


hover = HoverTool(tooltips=[("(x,y)", "($x, $y)")])



p1 = figure(x_axis_type="datetime", plot_width=400,plot_height=400, title="Stock Closing Prices",toolbar_location="above",toolbar_sticky=False,tools="pan,box_zoom,zoom_in,zoom_out,wheel_zoom,undo,reset")
p1.add_tools(hover)
p1.grid.grid_line_alpha=0.3
p1.xaxis.axis_label = 'Date'
p1.yaxis.axis_label = 'Price'

p1.line(datetime(AAPL['date']), AAPL['adj_close'], color='#A6CEE3', legend='AAPL')
p1.line(datetime(GOOG['date']), GOOG['adj_close'], color='#B2DF8A', legend='GOOG')
p1.line(datetime(IBM['date']), IBM['adj_close'], color='#33A02C', legend='IBM')
p1.line(datetime(MSFT['date']), MSFT['adj_close'], color='#FB9A99', legend='MSFT')
p1.legend.location = "top_left"

aapl = np.array(AAPL['adj_close'])
aapl_dates = np.array(AAPL['date'], dtype=np.datetime64)

window_size = 30
window = np.ones(window_size)/float(window_size)
aapl_avg = np.convolve(aapl, window, 'same')

p2 = figure(x_axis_type="datetime", plot_width=400,plot_height=400,title="AAPL One-Month Average",toolbar_location="above",toolbar_sticky=False,tools="pan,box_zoom,zoom_in,zoom_out,wheel_zoom,undo,reset")
p2.add_tools(hover)
p2.grid.grid_line_alpha = 0
p2.xaxis.axis_label = 'Date'
p2.yaxis.axis_label = 'Price'
p2.ygrid.band_fill_color = "olive"
p2.ygrid.band_fill_alpha = 0.1

p2.circle(aapl_dates, aapl, size=4, legend='close',
          color='darkgrey', alpha=0.2)

p2.line(aapl_dates, aapl_avg, legend='avg', color='navy')
p2.legend.location = "top_left"

output_file("bokeh_test.html", title="stocks.py example", mode='absolute')
#output_file("bokeh_test_inline.html", title="stocks.py example", mode='inline')
#output_file("bokeh_test.html", title="stocks.py example")
#show(gridplot([[p1,p2]], plot_width=400, plot_height=400))
#save(gridplot([[p1,p2]], plot_width=400, plot_height=400))
save(row(p1,p2))

for line in fileinput.input('bokeh_test.html', inplace=1):
    line = re.sub(b_js,os.path.join('.',os.path.basename(b_js)), line.rstrip())
    line = re.sub(b_css,os.path.join('.',os.path.basename(b_css)), line.rstrip())
    print(line)
